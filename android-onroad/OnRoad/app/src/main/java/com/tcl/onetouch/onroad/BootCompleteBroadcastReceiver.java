package com.tcl.onetouch.onroad;

import com.tcl.onetouch.base.ActivityDetectionService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootCompleteBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            ActivityDetectionService.registerActivityListener(context);
        }
    }
}
