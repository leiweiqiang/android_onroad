package com.tcl.onetouch.onroad;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tcl.onetouch.model.AlbumInfo;
import com.tcl.onetouch.model.ArtistInfo;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.PlaylistInfo;
import com.tcl.onetouch.model.TrackInfo;
import com.tcl.onetouch.service.MusicService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.StringUtil;
import com.tcl.onetouch.util.SystemUtil;
import com.tcl.onetouch.view.AsyncImageView;
import com.tcl.onetouch.view.GestureDetectorLayout;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MusicFragment extends ContentFragment
    implements View.OnClickListener, Animation.AnimationListener, MusicService.OnStateChangeListener {
    public static final String TAG = MusicFragment.class.getName();

    private View playBar;
    private View playPanel;
    private View playlistPanel;
    private TextView trackPositionText;
    private TextView trackDurationText;
    private ProgressBar trackProgressBar;

    private Animation playPanelOutAnim;
    private Animation playPanelInAnim;

    private TrackInfo currentTrack;
    private List<PlaylistInfo> playlists;
    private boolean isPlaying;

    private Timer trackStateRefreshTimer;

    private Animation playBarOutAnim;
    private Animation playBarInAnim;

    private AudioManager audioManager;
    private boolean isDuckingPaused;
    private boolean isUserPaused;
    private AudioManager.OnAudioFocusChangeListener afChangeListener;

    private BaseAdapter playlistsAdpater = new BaseAdapter() {
        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public int getCount() {
            return playlists == null ? 0 : playlists.size();
        }

        @Override
        public Object getItem(int position) {
            return playlists == null ? null : playlists.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.element_music_list_item, parent, false);
            }
            ((AsyncImageView) convertView.findViewById(R.id.img)).setImageUri(playlists.get(position).getImageUrl());
            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return 1;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }
    };

    private Runnable hidePlayPanelRunnable = new Runnable() {
        @Override
        public void run() {
            if (playPanel.getVisibility() == View.VISIBLE) {
                playPanel.startAnimation(playPanelInAnim);
            }
        }
    };

    private Runnable hidePlayBarRunnable = new Runnable() {
        @Override
        public void run() {
            if (playBar.getVisibility() == View.VISIBLE) {
                playBar.startAnimation(playBarInAnim);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        isDuckingPaused = false;
        isUserPaused = false;
        afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            public void onAudioFocusChange(int focusChange) {
                switch (focusChange) {
                    case AudioManager.AUDIOFOCUS_GAIN:
                        LogUtil.d("MusicFragment audiofocus change: AUDIOFOCUS_GAIN");
                        MusicService.getDefaultService().setDuck(false);
                        if (isDuckingPaused) {
                            MusicService.getDefaultService().resume();
                            isDuckingPaused = false;
                        }
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                        LogUtil.d("MusicFragment audiofocus change: AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                        MusicService.getDefaultService().setDuck(true);
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    case AudioManager.AUDIOFOCUS_LOSS:
                        LogUtil.d("MusicFragment audiofocus change: AUDIOFOCUS_LOSS_TRANSIENT or AUDIOFOCUS_LOSS");
                        if (!isUserPaused) {
                            MusicService.getDefaultService().pause();
                            isDuckingPaused = true;
                        }
                        break;
                    default:
                        LogUtil.d("MusicFragment audiofocus change: unknown " + focusChange);
                        break;
                }
            }
        };
        audioManager.requestAudioFocus(afChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_music, container, false);

        playBar = view.findViewById(R.id.play_bar);
        GestureDetectorLayout trackInfoLayout = (GestureDetectorLayout) playBar.findViewById(R.id.track_info_layout);
        trackInfoLayout.setOnSwipeListener(new GestureDetectorLayout.OnSwipeListener() {
            @Override
            public void onSwipeUp() {
                if (currentTrack != null) {
                    SystemUtil.cancelMainThreadAction(hidePlayBarRunnable);
                    playBar.startAnimation(playBarInAnim);
                    playPanel.startAnimation(playPanelOutAnim);
                }
            }
        });
        trackInfoLayout.setOnTapListener(new GestureDetectorLayout.OnTapListener() {
            @Override
            public void onTap(float x, float y) {
                if (currentTrack != null) {
                    SystemUtil.cancelMainThreadAction(hidePlayBarRunnable);
                    playBar.startAnimation(playBarInAnim);
                    playPanel.startAnimation(playPanelOutAnim);
                }
            }
        });
        playBar.findViewById(R.id.play_button).setOnClickListener(this);
        playBar.findViewById(R.id.cancel_button).setOnClickListener(this);
        playBar.findViewById(R.id.track_name_text).setSelected(true);

        playPanel = view.findViewById(R.id.play_panel);
        ((GestureDetectorLayout) playPanel).setOnSwipeListener(new GestureDetectorLayout.OnSwipeListener() {
            @Override
            public void onSwipeDown() {
                SystemUtil.cancelMainThreadAction(hidePlayPanelRunnable);
                playPanel.startAnimation(playPanelInAnim);
            }

            @Override
            public void onSwipeLeft() {
                View nextButton = playPanel.findViewById(R.id.next_button);
                if (nextButton.getVisibility() == View.VISIBLE) {
                    nextButton.performClick();
                }
            }

            @Override
            public void onSwipeRight() {
                View prevButton = playPanel.findViewById(R.id.prev_button);
                if (prevButton.getVisibility() == View.VISIBLE) {
                    prevButton.performClick();
                }
            }
        });
        playPanel.findViewById(R.id.play_button).setOnClickListener(this);
        playPanel.findViewById(R.id.cancel_button).setOnClickListener(this);
        playPanel.findViewById(R.id.prev_button).setOnClickListener(this);
        playPanel.findViewById(R.id.next_button).setOnClickListener(this);
        playPanel.findViewById(R.id.track_name_text).setSelected(true);
        playPanel.findViewById(R.id.album_name_text).setSelected(true);
        trackPositionText = (TextView) playPanel.findViewById(R.id.track_position_text);
        trackDurationText = (TextView) playPanel.findViewById(R.id.track_duration_text);
        trackProgressBar = (ProgressBar) playPanel.findViewById(R.id.track_progress_bar);

        playlistPanel = view.findViewById(R.id.playlist_panel);
        playlistPanel.findViewById(R.id.cancel_playlist_button).setOnClickListener(this);
        TwoWayView listView = (TwoWayView) view.findViewById(R.id.list_view);
        listView.setItemMargin(0);
        listView.setSelector(R.drawable.selector_list_view);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PlaylistInfo playlistInfo = playlists.get(position);
                MusicService.getDefaultService().playPlayList(playlistInfo.getUri());
                if (isDuckingPaused) {
                    MusicService.getDefaultService().pause();
                }
                playlistPanel.setVisibility(View.INVISIBLE);
            }
        });

        listView.setAdapter(playlistsAdpater);

        playPanelOutAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.window_slide_out_from_bottom);
        playPanelOutAnim.setAnimationListener(this);
        playPanelInAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.window_slide_in_to_bottom);
        playPanelInAnim.setAnimationListener(this);

        playBar.setVisibility(View.INVISIBLE);
        playPanel.setVisibility(View.INVISIBLE);
        playlistPanel.setVisibility(View.INVISIBLE);

        trackStateRefreshTimer = null;

        playBarOutAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.window_slide_out_from_bottom);
        playBarOutAnim.setAnimationListener(this);
        playBarInAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.window_slide_in_to_bottom);
        playBarInAnim.setAnimationListener(this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        MusicService.getDefaultService().start();
        refreshTrackInfo();
        refreshTrackState(MusicService.getDefaultService().getState());
        isPlaying = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        MusicService.getDefaultService().stop();
        currentTrack = null;
        playlists = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        playBar = null;
        playPanel = null;
        playlistPanel = null;
        trackPositionText = null;
        trackDurationText = null;
        trackProgressBar = null;

        playPanelOutAnim = null;
        playPanelInAnim = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        audioManager.abandonAudioFocus(afChangeListener);
        audioManager = null;
        afChangeListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_button:
                if (MusicService.getDefaultService().getState().isPlaying) {
                    pauseMusic();
                } else {
                    resumeMusic();
                }
                SystemUtil.cancelMainThreadAction(hidePlayBarRunnable);
                SystemUtil.postToMainThread(hidePlayBarRunnable, 5000);
                break;
            case R.id.cancel_button:
                SystemUtil.cancelMainThreadAction(hidePlayBarRunnable);
                MainActivity.getInstance().hideMusic();
                break;
            case R.id.cancel_playlist_button:
                if (currentTrack != null) {
                    playlistPanel.setVisibility(View.INVISIBLE);
                } else {
                    SystemUtil.cancelMainThreadAction(hidePlayPanelRunnable);
                    MainActivity.getInstance().hideMusic();
                }
                break;
            case R.id.prev_button:
                MusicService.getDefaultService().skipToPrevious();
                SystemUtil.cancelMainThreadAction(hidePlayPanelRunnable);
                SystemUtil.postToMainThread(hidePlayPanelRunnable, 5000);
                break;
            case R.id.next_button:
                MusicService.getDefaultService().skipToNext();
                SystemUtil.cancelMainThreadAction(hidePlayPanelRunnable);
                SystemUtil.postToMainThread(hidePlayPanelRunnable, 5000);
                break;
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == playPanelOutAnim) {
            playPanel.setVisibility(View.VISIBLE);
            playPanel.requestLayout();
            if (trackStateRefreshTimer != null) {
                trackStateRefreshTimer.cancel();
            }
            trackStateRefreshTimer = new Timer();
            trackStateRefreshTimer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    MusicService.getDefaultService().refreshState();
                }
            }, 0, 1000);
        } else if (animation == playBarOutAnim) {
            playBar.setVisibility(View.VISIBLE);
            playBar.requestLayout();
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == playPanelOutAnim) {
            SystemUtil.postToMainThread(hidePlayPanelRunnable, 5000);
        } else if (animation == playPanelInAnim) {
            if (trackStateRefreshTimer != null) {
                trackStateRefreshTimer.cancel();
                trackStateRefreshTimer = null;
            }
            playPanel.setVisibility(View.INVISIBLE);
        } else if (animation == playBarOutAnim) {
            SystemUtil.postToMainThread(hidePlayBarRunnable, 5000);
        } else if (animation == playBarInAnim) {
            playBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onStateChanged(MusicService.State state) {
        if (state.currentTrackUri == null || state.currentTrackUri.isEmpty()) {
            currentTrack = null;
        } else if (currentTrack == null || !currentTrack.getUri().equals(state.currentTrackUri)) {
            if (currentTrack == null && playBar != null) {
                playBar.startAnimation(playBarOutAnim);
            }
            currentTrack = new TrackInfo(state.currentTrackUri);
            MusicService.getDefaultService().getTrack(state.currentTrackUri, new MusicService.OnObjectLoadedListener() {
                @Override
                public void onLoaded(Object result) {
                    currentTrack = (TrackInfo) result;
                    refreshTrackInfo();
                    refreshTrackState(MusicService.getDefaultService().getState());
                }

                @Override
                public void onError(ErrorCode error) {
                    OnRoadApplication.getInstance().handleError(error);
                }
            });
        } else {
            refreshTrackState(state);
        }
    }

    @Override
    public void onError(ErrorCode error) {
        OnRoadApplication.getInstance().handleError(error);
    }

    public void pauseMusic() {
        isUserPaused = true;
        isDuckingPaused = false;
        MusicService.getDefaultService().pause();
    }

    public void resumeMusic() {
        isUserPaused = false;
        isDuckingPaused = false;
        MusicService.getDefaultService().resume();
    }

    public void togglePlayBar() {
        if (playBar == null) {
            return;
        }
        if (playBar.getVisibility() != View.VISIBLE) {
            playBar.startAnimation(playBarOutAnim);
        } else {
            SystemUtil.cancelMainThreadAction(hidePlayBarRunnable);
            playBar.startAnimation(playBarInAnim);
        }
    }

    protected void refreshTrackInfo() {
        if (getView() == null) {
            return;
        }
        if (currentTrack == null) {
            ((ImageView) playPanel.findViewById(R.id.track_img)).setImageDrawable(null);
            ((TextView) playBar.findViewById(R.id.track_name_text)).setText("");
            ((TextView) playBar.findViewById(R.id.artist_name_text)).setText("");
        } else {
            ((TextView) playBar.findViewById(R.id.track_name_text)).setText(currentTrack.getName());
            ((TextView) playPanel.findViewById(R.id.track_name_text)).setText(currentTrack.getName());
            String duration = StringUtil.formatTrackTime(currentTrack.getDuration() / 1000);
            trackDurationText.setText(duration);
            Rect bounds = new Rect();
            Paint textPaint = trackPositionText.getPaint();
            textPaint.getTextBounds(duration, 0, duration.length(), bounds);
            LogUtil.d("duration width: " + bounds.width());
            ViewGroup.LayoutParams lp = trackPositionText.getLayoutParams();
            // need to set width first to prevent re-layout when updating state
            lp.width = bounds.width() + (int) (6 * SystemUtil.getDisplayMetrics().density);
            trackPositionText.setLayoutParams(lp);
            trackProgressBar.setMax(currentTrack.getDuration());
            MusicService.getDefaultService().getAlbum(currentTrack.getAlbumUri(), new MusicService.OnObjectLoadedListener() {
                @Override
                public void onLoaded(Object result) {
                    AlbumInfo albumInfo = (AlbumInfo) result;
                    ((TextView) playPanel.findViewById(R.id.album_name_text)).setText(albumInfo.getName());
                    ((TextView) playPanel.findViewById(R.id.release_date_text)).setText(albumInfo.getReleaseDate());
                    String imgUrl = albumInfo.getImageUrl();
                    if (imgUrl != null && !imgUrl.isEmpty()) {
                        ((AsyncImageView) playPanel.findViewById(R.id.track_img)).setImageUri(imgUrl);
                    }
                }

                @Override
                public void onError(ErrorCode error) {
                    OnRoadApplication.getInstance().handleError(error);
                }
            });
            MusicService.getDefaultService().getArtist(currentTrack.getArtistUri(), new MusicService.OnObjectLoadedListener() {
                @Override
                public void onLoaded(Object result) {
                    ArtistInfo artistInfo = (ArtistInfo) result;
                    ((TextView) playBar.findViewById(R.id.artist_name_text)).setText(artistInfo.getName());
                    ((TextView) playPanel.findViewById(R.id.artist_name_text)).setText(artistInfo.getName());
                }

                @Override
                public void onError(ErrorCode error) {
                    OnRoadApplication.getInstance().handleError(error);
                }
            });
        }
    }

    protected void refreshTrackState(MusicService.State state) {
        if (getView() == null) {
            return;
        }
        trackPositionText.setText(StringUtil.formatTrackTime(state.position / 1000));
        trackProgressBar.setProgress(state.position);
        if (state.isPlaying) {
            if (!isPlaying) {
                ((ImageView) playBar.findViewById(R.id.play_button)).setImageResource(R.drawable.icon_pause);
                ((ImageView) playPanel.findViewById(R.id.play_button)).setImageResource(R.drawable.icon_pause);
            }
            isPlaying = true;
        } else {
            if (isPlaying) {
                ((ImageView) playBar.findViewById(R.id.play_button)).setImageResource(R.drawable.icon_play);
                ((ImageView) playPanel.findViewById(R.id.play_button)).setImageResource(R.drawable.icon_play);
            }
            isPlaying = false;
        }
    }

    public void searchAndPlay(String name) {
        MusicService.getDefaultService().searchTracks(name, new MusicService.OnObjectLoadedListener() {
            @Override
            public void onLoaded(Object result) {
                if (result == null) {
                    OnRoadApplication.getInstance().tts("sorry track not found", TAG);
                    return;
                }
                List<TrackInfo> tracks = (List<TrackInfo>) result;
                List<String> trackUris = new ArrayList<String>();
                Iterator<TrackInfo> tit = tracks.iterator();
                while (tit.hasNext()) {
                    trackUris.add(tit.next().getUri());
                }
                MusicService.getDefaultService().playTracks(trackUris);
                if (isDuckingPaused) {
                    MusicService.getDefaultService().pause();
                }
//                final TrackInfo trackInfo = (TrackInfo) result;
//                OnRoadApplication.getInstance().tts("now playing " + trackInfo.getName(), TAG, TtsService.SPEECH_PRIORITY_NORMAL, new TtsService.OnResultListener() {
//                    @Override
//                    public void onFinished() {
//                        MusicService.getDefaultService().playTrack(trackInfo.getUri());
//                    }
//
//                    @Override
//                    public void onStopped() {
//                        MusicService.getDefaultService().playTrack(trackInfo.getUri());
//                    }
//
//                    @Override
//                    public void onError() {
//
//                    }
//                });
            }

            @Override
            public void onError(ErrorCode error) {
                OnRoadApplication.getInstance().handleError(error);
            }
        });
    }

    public void showPlaylist() {
        if (playlists == null) {
            MusicService.getDefaultService().getFeaturedPlayLists(new MusicService.OnObjectLoadedListener() {
                @Override
                public void onLoaded(Object result) {
                    playlists = (List<PlaylistInfo>) result;
                    playlistsAdpater.notifyDataSetChanged();
                    if (playlistPanel != null) {
                        playlistPanel.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onError(ErrorCode error) {
                    OnRoadApplication.getInstance().handleError(error);
                }
            });
        } else if (playlistPanel != null) {
            playlistPanel.setVisibility(View.VISIBLE);
        }
    }
}
