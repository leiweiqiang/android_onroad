package com.tcl.onetouch.service;

import android.content.Context;
import android.location.Location;
import android.view.View;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.MapCoordinate;
import com.tcl.onetouch.service.look4.Look4MapService;

import java.util.List;

public abstract class MapService extends OnRoadService {
    public static final int ANNOTATION_PIN = 1;
    public static final int ANNOTATION_START_POINT = 2;
    public static final int ANNOTATION_DESTINATION_POINT = 3;
    public static final int ANNOTATION_DESTINATION_FLAG = 4;
    public static final int ANNOTATION_MY_CAR = 5;

    public static final int ANNOTATION_ID_DESTINATION = 10;
    public static final int ANNOTATION_ID_DESTINATION_ALT_1 = 11;
    public static final int ANNOTATION_ID_DESTINATION_ALT_2 = 12;

    public static final int FOLLOWING_MODE_NONE = 0;
    public static final int FOLLOWING_MODE_POSITION = 1;
    public static final int FOLLOWING_MODE_POSITION_AND_HEADING = 2;
    public static final int FOLLOWING_MODE_POSITION_AND_HEADING_NORTH = 3;

    public static interface Listener {
        public void onServiceReady();

        public void onMapReady();

        public void onError(ErrorCode error);
    }

    public abstract void init(Context context, MapService.Listener listener);

    public abstract void pause();

    public abstract void resume();

    public abstract void destroy();

    public void centerMe() {
        centerMe(FOLLOWING_MODE_POSITION_AND_HEADING_NORTH, 0, 0, 0);
    }

    public abstract void centerMe(int followingMode, float zoomLevel, int offsetX, int offsetY);

    public abstract void centerLocation(Location location);

    public abstract void setBearing(float bearing, int duration);

    public abstract float getBearing();

    public abstract void updateCurrentLocation(Location location);

    public abstract void fitRegion(double topLat, double leftLng, double bottomLat, double rightLng);

    public abstract View getMapView();

    public abstract View createMapView(Context context);

    public abstract void releaseMapView();

    public abstract void addAnnotation(int id, MapCoordinate position, View view, float offsetX, float offsetY);

    public abstract void deleteAnnotation(int id);

    private static MapService sDefaultService;

    public static MapService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new Look4MapService();
        }
        return sDefaultService;
    }
}
