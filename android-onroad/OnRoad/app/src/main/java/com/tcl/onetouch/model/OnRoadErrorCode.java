package com.tcl.onetouch.model;

import com.tcl.onetouch.onroad.R;
import com.tcl.onetouch.service.BaseService;

public class OnRoadErrorCode extends ErrorCode {

    // Map service errors: 10000 - 10999
    public static final int MAP_ERROR = 9000;
    public static final int MAP_ERROR_STORAGE = 9001;
    public static final int MAP_ERROR_INITIALIZATION = 9002;

    // Navigation errors: 11000 - 11999
    public static final int NAVIGATION_ERROR = 10000;
    public static final int NAVIGATION_ERROR_NO_RESULT = 10001;
    public static final int NAVIGATION_ERROR_SAME_START_AND_DESTINATION = 10002;
    public static final int NAVIGATION_ERROR_INVALID_START = 10003;
    public static final int NAVIGATION_ERROR_INVALID_DESTINATION = 10004;
    public static final int NAVIGATION_ERROR_NO_ROUTE = 10005;

    // Parking errors: 12000 - 12999
    public static final int PARKING_ERROR = 12000;

    // Music service errors: 13000 - 13999
    public static final int MUSIC_ERROR = 13000;
    public static final int MUSIC_ERROR_PLAYER_INITIALIZATION = 13001;
    public static final int MUSIC_ERROR_LOGIN_FAILED = 13002;
    public static final int MUSIC_ERROR_CONNECTION = 13003;
    public static final int MUSIC_ERROR_PLAYBACK = 13100;
    public static final int MUSIC_ERROR_PLAYBACK_NO_TRACK = 13101;

    // Car service errors: 14000 - 14999
    public static final int CAR_ERROR = 14000;
    public static final int CAR_ERROR_NO_CAR = 14001;
    public static final int CAR_ERROR_NO_LOCATION = 14002;

    // DB error: 50000 - 50999
    public static final int DATABASE_ERROR = 50000;

    public OnRoadErrorCode(int error) {
        this(error, null, null);
    }

    public OnRoadErrorCode(int error, String message) {
        this(error, message, null);
    }

    public OnRoadErrorCode(int error, String message, BaseService service) {
        super(error, message, service);
        switch (error) {
            case CAR_ERROR:
                logString = "CAR_ERROR";
                displayResId = R.string.CAR_ERROR_DISPLAY;
                ttsResId = R.string.CAR_ERROR_TTS;
                break;
            case CAR_ERROR_NO_CAR:
                logString = "CAR_ERROR_NO_CAR";
                displayResId = R.string.CAR_ERROR_NO_CAR_DISPLAY;
                ttsResId = R.string.CAR_ERROR_NO_CAR_TTS;
                break;
            case CAR_ERROR_NO_LOCATION:
                logString = "CAR_ERROR_NO_LOCATION";
                displayResId = R.string.CAR_ERROR_NO_LOCATION_DISPLAY;
                ttsResId = R.string.CAR_ERROR_NO_LOCATION_TTS;
                break;
            case MUSIC_ERROR:
                logString = "MUSIC_ERROR";
                displayResId = R.string.MUSIC_ERROR_DISPLAY;
                ttsResId = R.string.MUSIC_ERROR_TTS;
                break;
            case MUSIC_ERROR_PLAYER_INITIALIZATION:
                logString = "MUSIC_ERROR_PLAYER_INITIALIZATION";
                displayResId = R.string.MUSIC_ERROR_PLAYER_INITIALIZATION_DISPLAY;
                ttsResId = R.string.MUSIC_ERROR_PLAYER_INITIALIZATION_TTS;
                break;
            case MUSIC_ERROR_LOGIN_FAILED:
                logString = "MUSIC_ERROR_LOGIN_FAILED";
                displayResId = R.string.MUSIC_ERROR_LOGIN_FAILED_DISPLAY;
                ttsResId = R.string.MUSIC_ERROR_LOGIN_FAILED_TTS;
                break;
            case MUSIC_ERROR_CONNECTION:
                logString = "MUSIC_ERROR_CONNECTION";
                displayResId = R.string.MUSIC_ERROR_CONNECTION_DISPLAY;
                ttsResId = R.string.MUSIC_ERROR_CONNECTION_TTS;
                break;
            case MUSIC_ERROR_PLAYBACK:
                logString = "MUSIC_ERROR_PLAYBACK";
                displayResId = R.string.MUSIC_ERROR_PLAYBACK_DISPLAY;
                ttsResId = R.string.MUSIC_ERROR_PLAYBACK_TTS;
                break;
            case MUSIC_ERROR_PLAYBACK_NO_TRACK:
                logString = "MUSIC_ERROR_PLAYBACK_NO_TRACK";
                displayResId = R.string.MUSIC_ERROR_PLAYBACK_NO_TRACK_DISPLAY;
                ttsResId = R.string.MUSIC_ERROR_PLAYBACK_NO_TRACK_TTS;
                break;
            case MAP_ERROR:
                logString = "MAP_ERROR";
                displayResId = R.string.MAP_ERROR_DISPLAY;
                ttsResId = R.string.MAP_ERROR_TTS;
                break;
            case MAP_ERROR_STORAGE:
                logString = "MAP_ERROR_STORAGE";
                displayResId = R.string.MAP_ERROR_STORAGE_DISPLAY;
                ttsResId = R.string.MAP_ERROR_STORAGE_TTS;
                break;
            case MAP_ERROR_INITIALIZATION:
                logString = "MAP_ERROR_INITIALIZATION";
                displayResId = R.string.MAP_ERROR_INITIALIZATION_DISPLAY;
                ttsResId = R.string.MAP_ERROR_INITIALIZATION_TTS;
                break;
            case NAVIGATION_ERROR:
                logString = "NAVIGATION_ERROR";
                displayResId = R.string.NAVIGATION_ERROR_DISPLAY;
                ttsResId = R.string.NAVIGATION_ERROR_TTS;
                break;
            case NAVIGATION_ERROR_NO_RESULT:
                logString = "NAVIGATION_ERROR_NO_RESULT";
                displayResId = R.string.NAVIGATION_ERROR_NO_RESULT_DISPLAY;
                ttsResId = R.string.NAVIGATION_ERROR_NO_RESULT_TTS;
                break;
            case NAVIGATION_ERROR_SAME_START_AND_DESTINATION:
                logString = "NAVIGATION_ERROR_SAME_START_AND_DESTINATION";
                displayResId = R.string.NAVIGATION_ERROR_SAME_START_AND_DESTINATION_DISPLAY;
                ttsResId = R.string.NAVIGATION_ERROR_SAME_START_AND_DESTINATION_TTS;
                break;
            case NAVIGATION_ERROR_INVALID_START:
                logString = "NAVIGATION_ERROR_INVALID_START";
                displayResId = R.string.NAVIGATION_ERROR_INVALID_START_DISPLAY;
                ttsResId = R.string.NAVIGATION_ERROR_INVALID_START_TTS;
                break;
            case NAVIGATION_ERROR_INVALID_DESTINATION:
                logString = "NAVIGATION_ERROR_INVALID_DESTINATION";
                displayResId = R.string.NAVIGATION_ERROR_INVALID_DESTINATION_DISPLAY;
                ttsResId = R.string.NAVIGATION_ERROR_INVALID_DESTINATION_TTS;
                break;
            case NAVIGATION_ERROR_NO_ROUTE:
                logString = "NAVIGATION_ERROR_NO_ROUTE";
                displayResId = R.string.NAVIGATION_ERROR_NO_ROUTE_DISPLAY;
                ttsResId = R.string.NAVIGATION_ERROR_NO_ROUTE_TTS;
                break;
            case PARKING_ERROR:
                logString = "PARKING_ERROR";
                displayResId = R.string.PARKING_ERROR_DISPLAY;
                ttsResId = R.string.PARKING_ERROR_TTS;
                break;
            case DATABASE_ERROR:
                logString = "DATABASE_ERROR";
                displayResId = R.string.DATABASE_ERROR_DISPLAY;
                ttsResId = R.string.DATABASE_ERROR_TTS;
                break;
        }
    }
}
