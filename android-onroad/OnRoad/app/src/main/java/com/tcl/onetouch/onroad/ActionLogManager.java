package com.tcl.onetouch.onroad;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.tcl.onetouch.model.ActionLog;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.OnRoadErrorCode;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ActionLogManager {

    public interface QueryResultListener {
        public void onResult(List<ActionLog> results);

        public void onError(ErrorCode errorCode);
    }

    public interface UpdateResultListener {
        public void onResult(int result);

        public void onError(ErrorCode errorCode);
    }

    protected static ActionLogManager instance;

    protected ActionLogManager() {
    }

    public static ActionLogManager getInstance() {
        if (instance == null) {
            instance = new ActionLogManager();
        }
        return instance;
    }

    private Uri contentUri = Uri.parse("content://" + OnRoadContentProvider.AUTHORITY + "/" +
        OnRoadContentProvider.ActionLogTable.TABLE_NAME);

    public void getLog(int[] types, QueryResultListener listener) {
        getLog(types, 0, 0, Integer.MAX_VALUE, listener);
    }

    public void getLog(int[] types, long startTime, QueryResultListener listener) {
        getLog(types, startTime, 0, Integer.MAX_VALUE, listener);
    }

    public void getLog(long startTime, long endTime, QueryResultListener listener) {
        getLog(null, startTime, endTime, Integer.MAX_VALUE, listener);
    }

    public void getLog(int[] types, long startTime, long endTime, QueryResultListener listener) {
        getLog(types, startTime, endTime, Integer.MAX_VALUE, listener);
    }

    public void getLog(final int[] types, final long startTime, final long endTime, final int limit,
                       final QueryResultListener listener) {
        if (listener == null || limit <= 0) {
            return;
        }
        SystemUtil.postToBgThread(new Runnable() {
            @Override
            public void run() {
                String[] projection = new String[]{
                    OnRoadContentProvider.ActionLogTable._ID,
                    OnRoadContentProvider.ActionLogTable.TYPE,
                    OnRoadContentProvider.ActionLogTable.KEY,
                    OnRoadContentProvider.ActionLogTable.LOG,
                    OnRoadContentProvider.ActionLogTable.LAST_MOD};
                StringBuilder selection = new StringBuilder();
                if (types != null && types.length > 0) {
                    selection.append(OnRoadContentProvider.ActionLogTable.TYPE).append(" IN (");
                    for (int i = 0; i < types.length; ++i) {
                        if (i > 0) {
                            selection.append(",");
                        }
                        selection.append(types[i]);
                    }
                    selection.append(")");
                }
                if (startTime > 0) {
                    if (selection.length() > 0) {
                        selection.append(" AND ");
                    }
                    selection.append(OnRoadContentProvider.ActionLogTable.LAST_MOD).
                        append(">=").append(startTime / 1000);
                }
                if (endTime > 0) {
                    if (selection.length() > 0) {
                        selection.append(" AND ");
                    }
                    selection.append(OnRoadContentProvider.ActionLogTable.LAST_MOD).
                        append("<").append(endTime / 1000);
                }
                try {
                    Cursor cursor = OnRoadApplication.getInstance().getContentResolver().query(
                        contentUri, projection, selection.toString(), null,
                        OnRoadContentProvider.ActionLogTable.LAST_MOD + " DESC");
                    final ArrayList<ActionLog> results = new ArrayList<ActionLog>();
                    if (cursor != null) {
                        results.ensureCapacity(Math.min(cursor.getCount(), limit));
                        if (cursor.moveToFirst()) {
                            int i = 0;
                            do {
                                results.add(new ActionLog(
                                    cursor.getInt(0), cursor.getInt(1),
                                    cursor.getString(2), DataUtil.parseJson(cursor.getString(3)),
                                    cursor.getInt(4)));
                            } while (++i < limit && cursor.moveToNext());
                        }
                    }
                    cursor.close();
                    SystemUtil.postToMainThread(new Runnable() {
                        @Override
                        public void run() {
                            listener.onResult(results);
                        }
                    });
                } catch (final Exception e) {
                    SystemUtil.postToMainThread(new Runnable() {
                        @Override
                        public void run() {
                            listener.onError(new ErrorCode(OnRoadErrorCode.DATABASE_ERROR, e.getMessage()));
                        }
                    });
                }
            }
        });
    }

    public void getLog(final int type, final String key, final QueryResultListener listener) {
        if (listener == null) {
            return;
        }
        SystemUtil.postToBgThread(new Runnable() {
            @Override
            public void run() {
                String[] projection = new String[]{
                    OnRoadContentProvider.ActionLogTable._ID,
                    OnRoadContentProvider.ActionLogTable.LOG,
                    OnRoadContentProvider.ActionLogTable.LAST_MOD};
                String selection = OnRoadContentProvider.ActionLogTable.TYPE + " = ? AND " +
                    OnRoadContentProvider.ActionLogTable.KEY + " = ?";
                String[] selectionArgs = new String[] {String.valueOf(type), key};
                try {
                    Cursor cursor = OnRoadApplication.getInstance().getContentResolver().query(
                        contentUri, projection, selection, selectionArgs,
                        OnRoadContentProvider.ActionLogTable.LAST_MOD + " DESC");
                    final ArrayList<ActionLog> results = new ArrayList<ActionLog>();
                    if (cursor != null) {
                        if (cursor.moveToFirst()) {
                            do {
                                results.add(new ActionLog(
                                    cursor.getInt(0), type, key,
                                    DataUtil.parseJson(cursor.getString(1)), cursor.getInt(2)));
                            } while (cursor.moveToNext());
                        }
                    }
                    cursor.close();
                    SystemUtil.postToMainThread(new Runnable() {
                        @Override
                        public void run() {
                            listener.onResult(results);
                        }
                    });
                } catch (final Exception e) {
                    SystemUtil.postToMainThread(new Runnable() {
                        @Override
                        public void run() {
                            listener.onError(new ErrorCode(OnRoadErrorCode.DATABASE_ERROR, e.getMessage()));
                        }
                    });
                }
            }
        });
    }

    public void insertLog(final int type, final String key, final Map<String, Object> log, final UpdateResultListener listener) {
        SystemUtil.postToBgThread(new Runnable() {
            @Override
            public void run() {
                ContentValues contentValues = new ContentValues();
                contentValues.put(OnRoadContentProvider.ActionLogTable.TYPE, type);
                contentValues.put(OnRoadContentProvider.ActionLogTable.KEY, key);
                contentValues.put(OnRoadContentProvider.ActionLogTable.LOG, DataUtil.toJson(log));
                contentValues.put(OnRoadContentProvider.ActionLogTable.LAST_MOD, (System.currentTimeMillis() / 1000));
                try {
                    Uri result = OnRoadApplication.getInstance().getContentResolver().insert(contentUri, contentValues);
                    if (listener != null) {
                        final int updateId = Integer.parseInt(result.getPathSegments().get(1));
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onResult(updateId);
                            }
                        });
                    }
                } catch (final Exception e) {
                    if (listener != null) {
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(new ErrorCode(OnRoadErrorCode.DATABASE_ERROR, e.getMessage()));
                            }
                        });
                    }
                }
            }
        });
    }

    public void deleteLog(final int type, final long startTime, final long endTime, final UpdateResultListener listener) {
        SystemUtil.postToBgThread(new Runnable() {
            @Override
            public void run() {
                StringBuilder selection = new StringBuilder();
                if (type != ActionLog.ACTION_TYPE_UNKNOWN) {
                    selection.append(OnRoadContentProvider.ActionLogTable.TYPE).
                        append("=").append(type).append(" AND ");
                }
                if (startTime > 0) {
                    if (selection.length() > 0) {
                        selection.append(" AND ");
                    }
                    selection.append(OnRoadContentProvider.ActionLogTable.LAST_MOD).
                        append(">=").append(startTime / 1000);
                }
                if (endTime > 0) {
                    if (selection.length() > 0) {
                        selection.append(" AND ");
                    }
                    selection.append(OnRoadContentProvider.ActionLogTable.LAST_MOD).
                        append("<").append(endTime / 1000);
                }
                try {
                    final int count = OnRoadApplication.getInstance().getContentResolver().delete(
                        contentUri, selection.toString(), null);
                    if (listener != null) {
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onResult(count);
                            }
                        });
                    }
                } catch (final Exception e) {
                    if (listener != null) {
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(new ErrorCode(OnRoadErrorCode.DATABASE_ERROR, e.getMessage()));
                            }
                        });
                    }
                }
            }
        });
    }

    public void deleteLog(ActionLog actionLog, UpdateResultListener listener) {
        deleteLog(actionLog.getId(), listener);
    }

    public void deleteLog(final int logId, final UpdateResultListener listener) {
        SystemUtil.postToBgThread(new Runnable() {
            @Override
            public void run() {
                try {
                    final int count = OnRoadApplication.getInstance().getContentResolver().delete(
                        Uri.parse(contentUri + "/" + logId), null, null);
                    if (listener != null) {
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onResult(count > 0 ? logId : 0);
                            }
                        });
                    }
                } catch (final Exception e) {
                    if (listener != null) {
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(new ErrorCode(OnRoadErrorCode.DATABASE_ERROR, e.getMessage()));
                            }
                        });
                    }
                }
            }
        });
    }

    public void updateLog(final int id, final Map<String, Object> log, final UpdateResultListener listener) {
        SystemUtil.postToBgThread(new Runnable() {
            @Override
            public void run() {
                ContentValues contentValues = new ContentValues();
                if (log != null) {
                    contentValues.put(OnRoadContentProvider.ActionLogTable.LOG, DataUtil.toJson(log));
                }
                contentValues.put(OnRoadContentProvider.ActionLogTable.LAST_MOD, (System.currentTimeMillis() / 1000));
                try {
                    final int count = OnRoadApplication.getInstance().getContentResolver().update(
                        Uri.parse(contentUri + "/" + id), contentValues, null, null);
                    if (listener != null) {
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onResult(count);
                            }
                        });
                    }
                } catch (final Exception e) {
                    if (listener != null) {
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(new ErrorCode(OnRoadErrorCode.DATABASE_ERROR, e.getMessage()));
                            }
                        });
                    }
                }
            }
        });
    }
}
