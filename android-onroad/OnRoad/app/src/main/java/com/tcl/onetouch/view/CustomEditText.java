package com.tcl.onetouch.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class CustomEditText extends EditText {
    public interface KeyboardListener {
        public void onKeyboardShown();
        public void onKeyboardHidden();
    }

    protected KeyboardListener keyboardListener;
    protected boolean isKeyboardShown = false;

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            return hideKeyboard();
        }
        return super.onKeyPreIme(keyCode, event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            showKeyboard();
        }
        return super.onTouchEvent(event);
    }

    public KeyboardListener getKeyboardListener() {
        return keyboardListener;
    }

    public void setKeyboardListener(KeyboardListener keyboardListener) {
        this.keyboardListener = keyboardListener;
    }

    public boolean isKeyboardShown() {
        return isKeyboardShown;
    }

    public boolean showKeyboard() {
        if (!isKeyboardShown) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(this, 0);
            isKeyboardShown = true;
            if (keyboardListener != null) {
                keyboardListener.onKeyboardShown();
            }
            return true;
        }
        return false;
    }

    public boolean hideKeyboard() {
        if (isKeyboardShown) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindowToken(), 0);
            isKeyboardShown = false;
            if (keyboardListener != null) {
                keyboardListener.onKeyboardHidden();
            }
            return true;
        }
        return false;
    }
}
