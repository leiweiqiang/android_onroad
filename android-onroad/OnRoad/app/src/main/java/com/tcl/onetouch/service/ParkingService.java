package com.tcl.onetouch.service;

import android.content.Context;
import android.location.Location;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.ParkingInfo;
import com.tcl.onetouch.model.ParkingReservationInfo;
import com.tcl.onetouch.service.onetouch.OneTouchParkingService;

import java.util.Collection;
import java.util.Date;

public abstract class ParkingService extends BaseService {
    public static final int ERROR_NETWORK = 1;
    public static final int ERROR_NO_RESULT = 2;

    public static interface Listener {
        public void onResult(Object data);

        public void onError(ErrorCode error);
    }

    public abstract void init(Context context);

    public abstract void findOffStreetParking(Location location, Date arrivalTime, int durationInMins, Listener listener);

    public abstract void findOnStreetParking(Location location, Date arrivalTime, int durationInMins, Listener listener);

    public abstract Collection<ParkingReservationInfo> getReservations();

    public abstract void reserveParking(ParkingInfo parkingInfo, Date arrivalTime, int durationInMins, Listener listener);

    public abstract void checkParkingAvailability(Location location, Date arrivalTime, Listener listener);

    public abstract void findParking(Location location, Date arrivalTime, int durationInMins, Listener listener);

    private static ParkingService sDefaultService;

    public static ParkingService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new OneTouchParkingService();
        }
        return sDefaultService;
    }
}
