package com.tcl.onetouch.modules;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.tcl.onetouch.model.NavigationInfo;
import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.onroad.ContentFragment;
import com.tcl.onetouch.onroad.R;

import java.util.List;

/**
 * Created by leiweiqiang2 on 16/5/23.
 */
public class NaviFragment extends ContentFragment implements View.OnClickListener {
    public static final String TAG = NaviFragment.class.getName();

    private int routeType = NavigationInfo.ROUTE_TYPE_CAR_FASTEST;

    private NaviPoiInfoView navi_poi_info;
    private ListView navi_poi_list;
    private PoiInfosAdapter poiInfosAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navi, container, false);
        poiInfosAdapter = new PoiInfosAdapter();
        navi_poi_list = (ListView) view.findViewById(R.id.navi_poi_list);
        navi_poi_list.setAdapter(poiInfosAdapter);
        navi_poi_info = (NaviPoiInfoView)view.findViewById(R.id.navi_poi_info);
        return view;
    }

    @Override
    public void onClick(View v) {

    }

    public void setRouteType(int routeType) {
        this.routeType = routeType;
    }

    private void setPanelVisible(int id) {
        switch (id) {
            case R.id.navi_poi_list:
                navi_poi_list.setVisibility(View.VISIBLE);
                navi_poi_info.setVisibility(View.INVISIBLE);
                break;

            case R.id.navi_poi_info:
                navi_poi_list.setVisibility(View.INVISIBLE);
                navi_poi_info.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void setDestinations(String query, List<PlaceInfo> placeInfos) {

        if (placeInfos.size() == 1) {
            setDestination(placeInfos.get(0));
        } else {
            setPanelVisible(R.id.navi_poi_list);
            poiInfosAdapter.setPlaceInfos(placeInfos);
        }

    }

    public void setDestination(PlaceInfo destination) {
        setPanelVisible(R.id.navi_poi_info);
        navi_poi_info.setData(destination);
    }

    private class PoiInfosAdapter extends BaseAdapter {

        List<PlaceInfo> placeInfos;

        public void setPlaceInfos(List<PlaceInfo> placeInfos) {
            this.placeInfos = placeInfos;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return placeInfos == null ? 0 : placeInfos.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                ViewHolder viewHolder = new ViewHolder(NaviFragment.this.getActivity());
                convertView = viewHolder.getNaviPoiListItemView();
                convertView.setTag(viewHolder);
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.setData(placeInfos.get(position));
            return convertView;
        }
    }

    private static class ViewHolder {
        public NaviPoiListItemView naviPoiListItemView;

        public ViewHolder(Context context) {
            naviPoiListItemView = new NaviPoiListItemView(context);
        }

        public void setData(PlaceInfo placeInfo){
            naviPoiListItemView.setData(placeInfo);
        }

        public NaviPoiListItemView getNaviPoiListItemView() {
            return naviPoiListItemView;
        }
    }


}
