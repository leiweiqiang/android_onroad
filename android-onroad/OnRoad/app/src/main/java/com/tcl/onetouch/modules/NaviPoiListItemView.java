package com.tcl.onetouch.modules;

import android.content.Context;
import android.location.Location;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.onroad.R;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.util.StringUtil;

/**
 * Created by leiweiqiang2 on 16/5/23.
 */
public class NaviPoiListItemView extends LinearLayout {

    public NaviPoiListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.navi_poi_list_item_view, this);
    }

    public NaviPoiListItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.navi_poi_list_item_view, this);
    }

    public NaviPoiListItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.navi_poi_list_item_view, this);
    }

    public NaviPoiListItemView(Context context) {
        super(context);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.navi_poi_list_item_view, this);
    }

    public void setData(PlaceInfo placeInfo){
        ((TextView)findViewById(R.id.navi_poi_name)).setText(placeInfo.getName());
        ((TextView)findViewById(R.id.navi_poi_address)).setText(placeInfo.getAddressInfo().getShortAddress());
        ((TextView)findViewById(R.id.navi_poi_info_distance)).setText(getDistance(placeInfo));
    }

    public String getDistance(PlaceInfo placeInfo){
        Location currentLocation = LocationService.getDefaultService().getLastLocation();
        Location placeLocation = placeInfo.getLocation();
        float[] rs = new float[1];
        Location.distanceBetween(currentLocation.getLatitude(), currentLocation.getLongitude(),
                placeLocation.getLatitude(), placeLocation.getLongitude(), rs);
        return StringUtil.formatDistance((int) rs[0]);
    }
}
