package com.tcl.onetouch.modules;

import android.content.Context;
import android.location.Location;

import com.tcl.onetouch.model.ActionLog;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.NavigationInfo;
import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.onroad.ActionLogManager;
import com.tcl.onetouch.onroad.MainActivity;
import com.tcl.onetouch.onroad.OnRoadApplication;
import com.tcl.onetouch.onroad.R;
import com.tcl.onetouch.service.GeoService;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.service.NluService;
import com.tcl.onetouch.util.DataUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by leiweiqiang2 on 16/5/23.
 */

public class IntentHandler {

    private static final String TAG = IntentHandler.class.getName();
    private Context context;
    private MainActivity mainActivity;
    private NaviFragment naviFragment;


    public IntentHandler(Context context, MainActivity mainActivity, NaviFragment naviFragment) {
        this.context = context;
        this.mainActivity = mainActivity;
        this.naviFragment = naviFragment;
    }

    public boolean processIntent(int intent, Map<String, Object> data) {
        switch (intent){
            case NluService.GEO_ROUTE_INTENT:
            case NluService.GEO_SEARCH_INTENT:
                on_geo_route_intent(data);
                return true;

            default:
                return false;
        }
    }

    private void on_geo_route_intent(Map<String, Object> data){
        String address = "";
        String poi = "";
        if (DataUtil.getBoolean(data, "is_poi")) {
            poi = DataUtil.getString(data, "destination_address");
        } else {
            poi = DataUtil.getString(data, "poi_name");
            address = DataUtil.getString(data, "destination_address");
            if (address.isEmpty()) {
                address = DataUtil.getString(data, "location");
            }
            if (address.isEmpty()) {
                address = DataUtil.getString(data, "poi_address");
            }
        }
        final Location currentLocation = LocationService.getDefaultService().getLastLocation();
        if (!address.isEmpty()) {
            if (address.equalsIgnoreCase("your home")) {
                ActionLogManager.getInstance().getLog(
                        ActionLog.ACTION_TYPE_BOOKMARK,
                        context.getResources().getString(R.string.home).toLowerCase(),
                        new ActionLogManager.QueryResultListener() {
                            @Override
                            public void onResult(List<ActionLog> results) {
                                if (results.size() > 0) {
                                    ActionLog actionLog = results.get(0);
                                    PlaceInfo destination = new PlaceInfo();
                                    destination.setBookmark(DataUtil.getString(actionLog.getLog(), "bookmark"));
                                    destination.setId(DataUtil.getString(actionLog.getLog(), "id"));
                                    mainActivity.showNavigation();
                                    naviFragment.setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                                    naviFragment.setDestination(destination);
                                } else {
                                    // notify home is not set
                                }
                            }

                            @Override
                            public void onError(ErrorCode errorCode) {
                            }
                        });
            } else if (address.equalsIgnoreCase("your work")) {
                ActionLogManager.getInstance().getLog(
                        ActionLog.ACTION_TYPE_BOOKMARK,
                        context.getResources().getString(R.string.work).toLowerCase(),
                        new ActionLogManager.QueryResultListener() {
                            @Override
                            public void onResult(List<ActionLog> results) {
                                if (results.size() > 0) {
                                    ActionLog actionLog = results.get(0);
                                    PlaceInfo destination = new PlaceInfo();
                                    destination.setBookmark(DataUtil.getString(actionLog.getLog(), "bookmark"));
                                    destination.setId(DataUtil.getString(actionLog.getLog(), "id"));
                                    mainActivity.showNavigation();
                                    naviFragment.setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                                    naviFragment.setDestination(destination);
                                } else {
                                    // notify home is not set
                                }
                            }

                            @Override
                            public void onError(ErrorCode errorCode) {
                            }
                        });
            } else {
                GeoService.getDefaultService().geocodeQuery(address, currentLocation, new GeoService.GeocodeResultListener() {
                    @Override
                    public void onResult(PlaceInfo place) {
                        mainActivity.showNavigation();
                        naviFragment.setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                        naviFragment.setDestination(place);
                    }

                    @Override
                    public void onError(ErrorCode error) {
                        OnRoadApplication.getInstance().handleError(error);
                    }
                });
            }
        } else if (!poi.isEmpty()) {
            final String query = poi;
            GeoService.getDefaultService().poiQuery(poi, currentLocation, new GeoService.PoiResultListener() {
                @Override
                public void onResult(List<PlaceInfo> places) {
                    if (places.size() > 0) {
                        mainActivity.showNavigation();
                        naviFragment.setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                        naviFragment.setDestinations(query, places.subList(0, Math.min(3, places.size())));
                    }
                }

                @Override
                public void onError(ErrorCode error) {
                    OnRoadApplication.getInstance().handleError(error);
                }
            });
        }
    }
}
