package com.tcl.onetouch.model;

import java.util.Map;

public class ActionLog {
    public static final int ACTION_TYPE_UNKNOWN = 0;
    public static final int ACTION_TYPE_GEO_QUERY = 1;
    public static final int ACTION_TYPE_DESTINATION = 2;
    public static final int ACTION_TYPE_NAVIGATE = 3;
    public static final int ACTION_TYPE_BOOKMARK = 4;

    private int id;
    private int type;
    private String key;
    private Map<String, Object> log;
    private int time;

    public ActionLog(int type, String key, Map<String, Object> log) {
        this(0, type, key, log, (int) (System.currentTimeMillis() / 1000));
    }

    public ActionLog(int type, String key, Map<String, Object> log, int time) {
        this(0, type, key, log, time);
    }

    public ActionLog(int id, int type, String key, Map<String, Object> log, int time) {
        this.id = id;
        this.type = type;
        this.key = key;
        this.log = log;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, Object> getLog() {
        return log;
    }

    public void setLog(Map<String, Object> log) {
        this.log = log;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
