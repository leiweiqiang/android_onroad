package com.tcl.onetouch.onroad;

import android.app.Fragment;
import android.view.ViewTreeObserver;

public class ContentFragment extends Fragment implements ViewTreeObserver.OnGlobalLayoutListener {
    @Override
    public void onStart() {
        super.onStart();
        getView().getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        MainActivity.getInstance().onContentFragmentRemoved(this);
    }

    @Override
    public void onGlobalLayout() {
        MainActivity.getInstance().onContentFragmentAdded(ContentFragment.this);
        getView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    public boolean goBack() {
        return false;
    }
}
