package com.tcl.onetouch.onroad;

import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.base.QnATask;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.NavigationInfo;
import com.tcl.onetouch.model.ParkingInfo;
import com.tcl.onetouch.model.ParkingReservationInfo;
import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.service.GeoService;
import com.tcl.onetouch.service.MapService;
import com.tcl.onetouch.service.NavigationService;
import com.tcl.onetouch.service.NluService;
import com.tcl.onetouch.service.ParkingService;
import com.tcl.onetouch.service.SpeechRecognizerService;
import com.tcl.onetouch.service.TtsService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.StringUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class ParkingFragment extends ContentFragment implements View.OnClickListener {
    public static final String TAG = ParkingFragment.class.getName();
    private static final int MAX_OPTION_COUNT = 6;

    private View parkingHeader;
    private View selectHoursPanel;
    private ListView parkingOptionsListView;
    private View reserveParkingPanel;
    private View reservationConfirmedPanel;

    private int parkingLengthInMins;
    private List<ParkingInfo> parkingOptions;

    private ParkingInfo selectedParkingInfo;
    private ParkingInfo.ReservableProduct selectedReservableProduct;
    private ParkingReservationInfo parkingReservationInfo;

    private ParkingInfo reservedParkingInfo;
    private ParkingReservationInfo reservedParkingReservationInfo;

    private QnATask qnaTask;

    private Comparator<ParkingInfo> parkingInfoComparator = new Comparator<ParkingInfo>() {
        @Override
        public int compare(ParkingInfo lhs, ParkingInfo rhs) {
            if (lhs.isReservable() && !rhs.isReservable()) {
                return -1;
            } else if (!lhs.isReservable() && rhs.isReservable()) {
                return 1;
            } else {
                double costDiff = lhs.getTotalCost() - rhs.getTotalCost();
                if (Math.abs(costDiff) > 0.5) {
                    return (int) (costDiff * 10);
                }
                return (int) (lhs.getDistance() - rhs.getDistance());
            }
        }
    };

    private View.OnClickListener hourOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            parkingLengthInMins = Integer.parseInt(v.getTag().toString()) * 60;
            findParking();
        }
    };

    private View.OnClickListener parkingOptionOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Object tag = v.getTag();
            if (tag instanceof ParkingInfo) {
                selectedParkingInfo = (ParkingInfo) tag;
                selectedReservableProduct = null;
                routeToParking();
            } else {
                Object[] objs = (Object[]) tag;
                selectedParkingInfo = (ParkingInfo) objs[0];
                selectedReservableProduct = (ParkingInfo.ReservableProduct) objs[1];
                if (selectedReservableProduct == null) {
                    routeToParking();
                } else {
                    ((TextView) reserveParkingPanel.findViewById(R.id.amount_text)).setText(
                        StringUtil.formatCost(selectedReservableProduct.getTotalAmount()));
                    int hours = parkingLengthInMins / 60;
                    if (hours > 1) {
                        ((TextView) reserveParkingPanel.findViewById(R.id.duration_text)).setText(hours + " hours");
                    } else {
                        ((TextView) reserveParkingPanel.findViewById(R.id.duration_text)).setText("1 hours");
                    }
                    ((TextView) reserveParkingPanel.findViewById(R.id.name_text)).setText(selectedParkingInfo.getName());
                    ((TextView) reserveParkingPanel.findViewById(R.id.distance_text)).setText(
                        StringUtil.formatDistance((int) selectedParkingInfo.getDistance()));
                    showPage(reserveParkingPanel);
                }
            }
        }
    };

    private class ParkingOptionsAdapter extends BaseAdapter {
        LayoutInflater inflater;

        ParkingOptionsAdapter(LayoutInflater inflater) {
            this.inflater = inflater;
        }

        @Override
        public int getCount() {
            return parkingOptions == null ? 0 : parkingOptions.size();
        }

        @Override
        public Object getItem(int position) {
            return parkingOptions == null ? null : parkingOptions.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private void setupReserveOption(View view, ParkingInfo parkingInfo, ParkingInfo.ReservableProduct reservableProduct) {
            TextView nameText = (TextView) view.findViewById(R.id.name_text);
            if (!reservableProduct.getName().isEmpty()) {
                nameText.setVisibility(View.VISIBLE);
                nameText.setText(reservableProduct.getName());
            } else {
                nameText.setVisibility(View.GONE);
            }
            ((TextView) view.findViewById(R.id.amount_text)).setText("$" + reservableProduct.getTotalAmount());
            ((TextView) view.findViewById(R.id.action_text)).setText("Reserve");
            view.setTag(new Object[]{parkingInfo, reservableProduct});
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.element_parking_list_item, parent, false);
            }
            ParkingInfo parkingInfo = parkingOptions.get(position);
            ((TextView) convertView.findViewById(R.id.item_text)).setText("" + (position + 1));
            if (parkingInfo.getOccupancyPct() > 0) {
                ((TextView) convertView.findViewById(R.id.occupancy_text)).setText(
                    parkingInfo.getOccupancyPct() + "%\nFULL");
            } else {
                ((TextView) convertView.findViewById(R.id.occupancy_text)).setText("");
            }
            ((TextView) convertView.findViewById(R.id.name_text)).setText(parkingInfo.getName());
            ((TextView) convertView.findViewById(R.id.distance_text)).setText(
                StringUtil.formatDistance((int) parkingInfo.getDistance()));
            View defaultOption = convertView.findViewById(R.id.drive_option);
            ((TextView) defaultOption.findViewById(R.id.amount_text)).setText(StringUtil.formatCost(parkingInfo.getTotalCost()));
            defaultOption.setTag(new Object[]{parkingInfo, null});
            defaultOption.setOnClickListener(parkingOptionOnClickListener);
            if (parkingInfo.getReservableProducts() == null || parkingInfo.getReservableProducts().size() == 0) {
                convertView.findViewById(R.id.reserve_option).setVisibility(View.GONE);
            } else {
                View optionView = convertView.findViewById(R.id.reserve_option);
                ParkingInfo.ReservableProduct reservableProduct = parkingInfo.getReservableProducts().get(0);
                optionView.setVisibility(View.VISIBLE);
                ((TextView) optionView.findViewById(R.id.amount_text)).setText(StringUtil.formatCost(reservableProduct.getTotalAmount()));
                ((TextView) optionView.findViewById(R.id.action_text)).setText("Reserve");
                optionView.setTag(new Object[]{parkingInfo, reservableProduct});
                optionView.setOnClickListener(parkingOptionOnClickListener);
            }
            return convertView;
        }
    }

    private ParkingOptionsAdapter parkingOptionsAdapter;

    private int firstItem;
    private int lastItem;
    private Runnable addParkingAnnotationsTask = new Runnable() {
        @Override
        public void run() {
            if (parkingOptions == null || parkingOptions.isEmpty()) {
                deleteParkingAnnotations();
                return;
            }
            int newFirstItem = parkingOptionsListView.getFirstVisiblePosition();
            int newLastItem = parkingOptionsListView.getLastVisiblePosition();
            int[] listLoc = new int[2];
            parkingOptionsListView.getLocationInWindow(listLoc);
            View view = parkingOptionsListView.getChildAt(0).findViewById(R.id.item_text);
            int[] loc = new int[2];
            view.getLocationInWindow(loc);
            if (loc[1] + view.getHeight() - listLoc[1] < SystemUtil.getDisplayMetrics().density * 22) {
                ++newFirstItem;
            }
            view = parkingOptionsListView.getChildAt(parkingOptionsListView.getChildCount() - 1).findViewById(R.id.item_text);
            view.getLocationInWindow(loc);
            if (listLoc[1] + parkingOptionsListView.getHeight() - loc[1] < SystemUtil.getDisplayMetrics().density * 22) {
                --newLastItem;
            }
            if (newFirstItem == firstItem && newLastItem == lastItem) {
                return;
            }
            deleteParkingAnnotations();
            firstItem = newFirstItem;
            lastItem = newLastItem;
            NavigationInfo navigationInfo = NavigationService.getDefaultService().getCurrentNavigationInfo();
            Location location = navigationInfo.getDestination().getLocation();
            double southLat = location.getLatitude(),
                northLat = location.getLatitude(),
                westLng = location.getLongitude(),
                eastLng = location.getLongitude();
            for (int i = firstItem; i <= lastItem; ++i) {
                ParkingInfo parkingInfo = parkingOptions.get(i);
                MainActivity.getInstance().getMapFragment().addDestinationOptionAnnotation(
                    parkingInfo, parkingInfo.getFacilityId().hashCode(), i,
                    R.drawable.icon_destination_parking, parkingOptionOnClickListener);
                double placeLat = parkingInfo.getLocation().getLatitude(),
                    placeLng = parkingInfo.getLocation().getLongitude();
                if (placeLat < southLat) {
                    southLat = placeLat;
                } else if (placeLat > northLat) {
                    northLat = placeLat;
                }
                if (placeLng < westLng) {
                    westLng = placeLng;
                } else if (placeLng > eastLng) {
                    eastLng = placeLng;
                }
            }
            if (southLat == northLat && westLng == eastLng) {
                return;
            }
            Resources rs = getActivity().getResources();
            int margin = rs.getDimensionPixelSize(R.dimen.map_fit_padding);
            int destinationAnnotationHeight = rs.getDimensionPixelSize(R.dimen.map_annotation_destination_height),
                startAnnotationWidth = rs.getDimensionPixelSize(R.dimen.map_annotation_start_width),
                startAnnotationHeight = rs.getDimensionPixelSize(R.dimen.map_annotation_start_height);
            int marginTop = margin + destinationAnnotationHeight,
                marginBottom = margin + startAnnotationHeight / 2,
                marginLeft = margin + startAnnotationWidth / 2,
                marginRight = margin + startAnnotationWidth / 2;
            int x = parkingOptionsListView.getRight(), y = parkingHeader.getBottom(),
                viewWidth = getView().getWidth() - x - marginLeft - marginRight,
                viewHeight = getView().getHeight() - y - marginTop - marginBottom;
            int[] viewLoc = new int[2];
            getView().getLocationInWindow(viewLoc);
            x += viewLoc[0];
            y += viewLoc[1];
            double[] scales = new double[2];
            MainActivity.getInstance().getMapFragment().calculateScale(
                southLat, westLng, northLat, eastLng, viewWidth, viewHeight, scales);
            double centerLat = (southLat + northLat) / 2.0d, centerLng = (westLng + eastLng) / 2.0d;
            double leftLng = centerLng - scales[1] * (viewWidth / 2.0d + marginLeft + x),
                rightLng = centerLng + scales[1] * (viewWidth / 2.0d + marginRight),
                bottomLat = centerLat - scales[0] * (viewHeight / 2.0d + marginBottom),
                topLat = centerLat + scales[0] * (viewHeight / 2.0d + marginTop + y);
            MapService.getDefaultService().setBearing(0f, 0);
            MapService.getDefaultService().fitRegion(topLat, leftLng, bottomLat, rightLng);
        }
    };


    public ParkingFragment() {
        super();
        parkingLengthInMins = 60;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parking, container, false);

        view.findViewById(R.id.cancel_parking_button).setOnClickListener(this);

        parkingHeader = view.findViewById(R.id.parking_header);

        selectHoursPanel = view.findViewById(R.id.select_hours_panel);
        ViewGroup hourOptionsPanel = (ViewGroup) selectHoursPanel.findViewById(R.id.hour_options_panel);
        for (int i = 0; i < hourOptionsPanel.getChildCount(); ++i) {
            hourOptionsPanel.getChildAt(i).setOnClickListener(hourOnClickListener);
        }

        parkingOptionsListView = (ListView) view.findViewById(R.id.parking_list);
        parkingOptionsAdapter = new ParkingOptionsAdapter(inflater);
        parkingOptionsListView.setAdapter(parkingOptionsAdapter);
        parkingOptionsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState != SCROLL_STATE_IDLE) {
                    return;
                }
                SystemUtil.postToMainThread(addParkingAnnotationsTask);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        reserveParkingPanel = view.findViewById(R.id.reserve_panel);
        reserveParkingPanel.findViewById(R.id.reserve_button).setOnClickListener(this);

        reservationConfirmedPanel = view.findViewById(R.id.reservation_confirmed_panel);
        reservationConfirmedPanel.findViewById(R.id.start_navigation_button).setOnClickListener(this);

        WebView webView = (WebView) reservationConfirmedPanel.findViewById(R.id.confirmation_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        selectedParkingInfo = null;
        parkingReservationInfo = null;
        reservedParkingInfo = null;
        reservedParkingReservationInfo = null;
        qnaTask = new QnATask().setSource(TAG);
        OneTouchApplication.getInstance().disableWakeupService(TAG, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        OnRoadApplication.getInstance().requestTtsAudioFocus(TAG);
    }

    @Override
    public void onPause() {
        super.onPause();
        OnRoadApplication.getInstance().releaseTtsAudioFocus(TAG);
    }

    @Override
    public void onStop() {
        super.onStop();
        parkingOptions = null;
        selectedParkingInfo = null;
        parkingReservationInfo = null;
        reservedParkingInfo = null;
        reservedParkingReservationInfo = null;
        qnaTask = null;
        OneTouchApplication.getInstance().enableWakeupService(TAG);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        selectHoursPanel = null;
        parkingOptionsListView = null;
        parkingOptionsAdapter = null;
        reserveParkingPanel = null;
        reservationConfirmedPanel = null;
    }

    @Override
    public void onClick(View v) {
        SpeechRecognizerService.getDefaultService().cancelRecognize();
        switch (v.getId()) {
            case R.id.cancel_parking_button:
                qnaTask.end();
                MainActivity.getInstance().cancelParking();
                break;
            case R.id.reserve_button:
                reserveParking();
                break;
            case R.id.start_navigation_button:
                routeToParking();
                break;
        }
    }

    private void resetHeader() {
        parkingHeader.findViewById(R.id.loading_icon).setVisibility(View.GONE);
        ((TextView) parkingHeader.findViewById(R.id.parking_title_text)).setText("Parking");
    }

    private void showPage(View page) {
        selectHoursPanel.setVisibility(View.GONE);
        parkingOptionsListView.setVisibility(View.GONE);
        reserveParkingPanel.setVisibility(View.GONE);
        reservationConfirmedPanel.setVisibility(View.GONE);
        page.setVisibility(View.VISIBLE);
    }

    public void findParking() {
        final NavigationInfo navigationInfo = NavigationService.getDefaultService().getCurrentNavigationInfo();
        if (navigationInfo == null) {
            return;
        }
        parkingHeader.findViewById(R.id.loading_icon).setVisibility(View.VISIBLE);
        ((TextView) parkingHeader.findViewById(R.id.parking_title_text)).setText("Searching parking");
        ParkingService.getDefaultService().findParking(
            navigationInfo.getDestination().getLocation(),
            navigationInfo.getArrivalTime(),
            parkingLengthInMins,
            new ParkingService.Listener() {
                @Override
                public void onResult(Object data) {
                    Map<String, Object> results = (Map<String, Object>) data;
                    parkingOptions = new ArrayList<ParkingInfo>();
                    firstItem = -1;
                    lastItem = -1;
                    parkingOptionsAdapter.notifyDataSetChanged();
                    for (Object parkingInfo : DataUtil.getCollection(results, "OffStreet")) {
                        parkingOptions.add((ParkingInfo) parkingInfo);
                    }
                    for (Object parkingInfo : DataUtil.getCollection(results, "OnStreet")) {
                        parkingOptions.add((ParkingInfo) parkingInfo);
                    }
                    Collections.sort(parkingOptions, parkingInfoComparator);
                    parkingOptions = parkingOptions.subList(0, Math.min(MAX_OPTION_COUNT, parkingOptions.size()));
                    parkingOptionsAdapter.notifyDataSetChanged();
                    SystemUtil.postToMainThread(addParkingAnnotationsTask);
                    resetHeader();
                    showPage(parkingOptionsListView);
                }

                @Override
                public void onError(ErrorCode error) {
                    resetHeader();
                    LogUtil.d("find parking error: " + error);
                    OnRoadApplication.getInstance().handleError(error);
                }
            }
        );
    }

    protected void promptParkingHours() {
        qnaTask.
            setQuestion(R.string.tts_parking_select_hours).
            setTimeResultHandler(new QnATask.ResultHandler() {
                @Override
                public void onResult(Map<String, Object> result) {
                    int time = DataUtil.getInt(result, NluService.RESULT_KEY_RESULT);
                    if (time <= 0) {
                        time = 60;
                    }
//                    if (time % 60 == 0) {
//                        int hours = time / 60;
//                        if (hours <= hoursRadioGroup.getChildCount()) {
//                            hoursRadioGroup.getChildAt(hours - 1).performClick();
//                        } else if (hours == 24) {
//                            fullDayText.performClick();
//                        } else {
//                            if (time != parkingLengthInMins) {
//                                parkingLengthInMins = time;
//                                refreshParkingInfo();
//                            }
//                            goNextPage();
//                        }
//                    } else {
//                        if (time != parkingLengthInMins) {
//                            parkingLengthInMins = time;
//                            refreshParkingInfo();
//                        }
//                        goNextPage();
//                    }
                }
            }).
            start();
    }

    protected void promptReservedParking() {
        qnaTask.
            setQuestion(R.string.tts_parking_select_reserved_parking).
            setYesNoResultHandler(NluService.YES_NO_MODE_PARKING, new QnATask.ResultHandler() {
                @Override
                public void onResult(Map<String, Object> result) {
                    if (DataUtil.getBoolean(result, NluService.RESULT_KEY_RESULT)) {
//                        final View yesButton = reservedParkingPanel.findViewById(R.id.reserved_parking_yes_button);
//                        yesButton.setPressed(true);
//                        yesButton.invalidate();
//                        yesButton.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                yesButton.setPressed(false);
//                                yesButton.invalidate();
//                                yesButton.performClick();
//                            }
//                        });
                    } else {
//                        OnRoadApplication.getInstance().tts(DataUtil.getString(result, NluService.RESULT_KEY_INTERPRETATION), TAG);
//                        final View noButton = reservedParkingPanel.findViewById(R.id.reserved_parking_no_button);
//                        noButton.setPressed(true);
//                        noButton.invalidate();
//                        noButton.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                noButton.setPressed(false);
//                                noButton.invalidate();
//                                noButton.performClick();
//                            }
//                        });
                    }
                }
            }).
            start();
    }

    protected void promptParkingOptions(final List<String> items) {
        qnaTask.
            setQuestion(R.string.tts_parking_select_option).
            setSelectionItems(items).
            setSelectionResultHandler(new QnATask.ResultHandler() {
                @Override
                public void onResult(Map<String, Object> result) {
                    int index = DataUtil.getInt(result, NluService.RESULT_KEY_RESULT);
                }
            }).
            start();
    }

    protected void promptReservation() {
        qnaTask.
            setQuestion(R.string.tts_parking_reservation_question).
            setYesNoResultHandler(NluService.YES_NO_MODE_PARKING, new QnATask.ResultHandler() {
                @Override
                public void onResult(Map<String, Object> result) {
                    if (DataUtil.getBoolean(result, NluService.RESULT_KEY_RESULT)) {
//                        final View yesButton = reservationPanel.findViewById(R.id.reservation_yes_button);
//                        yesButton.setPressed(true);
//                        yesButton.invalidate();
//                        yesButton.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                yesButton.setPressed(false);
//                                yesButton.invalidate();
//                                yesButton.performClick();
//                            }
//                        });
                    } else {
//                        OnRoadApplication.getInstance().tts(DataUtil.getString(result, NluService.RESULT_KEY_INTERPRETATION), TAG);
//                        final View noButton = reservationPanel.findViewById(R.id.reservation_no_button);
//                        noButton.setPressed(true);
//                        noButton.invalidate();
//                        noButton.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                noButton.setPressed(false);
//                                noButton.invalidate();
//                                noButton.performClick();
//                            }
//                        });
                    }
                }
            }).
            start();
    }

    protected void promptReservationConfirmation() {
        OnRoadApplication.getInstance().tts(R.string.tts_parking_reservation_confirmation, TAG, TtsService.SPEECH_PRIORITY_HIGH, new TtsService.OnSpeechListener() {
            @Override
            public void onFinished() {
                routeToParking();
            }

            @Override
            public void onStopped() {
                routeToParking();
            }

            @Override
            public void onError(ErrorCode error) {
                routeToParking();
            }
        });
    }

    protected void reserveParking() {
        deleteParkingAnnotations();
        final NavigationInfo navigationInfo = NavigationService.getDefaultService().getCurrentNavigationInfo();
        if (navigationInfo == null) {
            return;
        }
        parkingHeader.findViewById(R.id.loading_icon).setVisibility(View.VISIBLE);
        ((TextView) parkingHeader.findViewById(R.id.parking_title_text)).setText("Reserving parking");
        ParkingService.getDefaultService().reserveParking(
            selectedParkingInfo,
            navigationInfo.getArrivalTime(),
            parkingLengthInMins,
            new ParkingService.Listener() {
                @Override
                public void onResult(Object data) {
                    resetHeader();
                    parkingReservationInfo = (ParkingReservationInfo) data;
                    LogUtil.d("received reservation: " + parkingReservationInfo.getUrl());
                    ((WebView) reservationConfirmedPanel.findViewById(R.id.confirmation_webview)).
                        loadUrl(parkingReservationInfo.getUrl());
                    ((TextView) reservationConfirmedPanel.findViewById(R.id.amount_text)).setText(
                        StringUtil.formatCost(selectedReservableProduct.getTotalAmount()));
                    int hours = parkingLengthInMins / 60;
                    if (hours > 1) {
                        ((TextView) reservationConfirmedPanel.findViewById(R.id.duration_text)).setText(hours + " hours");
                    } else {
                        ((TextView) reservationConfirmedPanel.findViewById(R.id.duration_text)).setText("1 hours");
                    }
                    ((TextView) reservationConfirmedPanel.findViewById(R.id.name_text)).setText(selectedParkingInfo.getName());
                    ((TextView) reservationConfirmedPanel.findViewById(R.id.arrival_time_text)).setText(
                        StringUtil.formatArrivalTime(navigationInfo.getArrivalTime()));
                    showPage(reservationConfirmedPanel);
                }

                @Override
                public void onError(ErrorCode error) {
                    resetHeader();
                    LogUtil.d("reservation error: " + error);
                    OnRoadApplication.getInstance().handleError(error);
                }
            });
        Location location = navigationInfo.getDestination().getLocation();
        double southLat = location.getLatitude(),
            northLat = location.getLatitude(),
            westLng = location.getLongitude(),
            eastLng = location.getLongitude();
        MainActivity.getInstance().getMapFragment().addDestinationOptionAnnotation(
            selectedParkingInfo, selectedParkingInfo.getFacilityId().hashCode(), -1,
            R.drawable.icon_destination_parking, parkingOptionOnClickListener);
        double placeLat = selectedParkingInfo.getLocation().getLatitude(),
            placeLng = selectedParkingInfo.getLocation().getLongitude();
        if (placeLat < southLat) {
            southLat = placeLat;
        } else if (placeLat > northLat) {
            northLat = placeLat;
        }
        if (placeLng < westLng) {
            westLng = placeLng;
        } else if (placeLng > eastLng) {
            eastLng = placeLng;
        }
        if (southLat == northLat && westLng == eastLng) {
            return;
        }
        Resources rs = getActivity().getResources();
        int margin = rs.getDimensionPixelSize(R.dimen.map_fit_padding);
        int destinationAnnotationHeight = rs.getDimensionPixelSize(R.dimen.map_annotation_destination_height),
            startAnnotationWidth = rs.getDimensionPixelSize(R.dimen.map_annotation_start_width),
            startAnnotationHeight = rs.getDimensionPixelSize(R.dimen.map_annotation_start_height);
        int marginTop = margin + destinationAnnotationHeight,
            marginBottom = margin + startAnnotationHeight / 2,
            marginLeft = margin + startAnnotationWidth / 2,
            marginRight = margin + startAnnotationWidth / 2;
        int x = parkingOptionsListView.getRight(), y = parkingHeader.getBottom(),
            viewWidth = getView().getWidth() - x - marginLeft - marginRight,
            viewHeight = getView().getHeight() - y - marginTop - marginBottom;
        int[] viewLoc = new int[2];
        getView().getLocationInWindow(viewLoc);
        x += viewLoc[0];
        y += viewLoc[1];
        double[] scales = new double[2];
        MainActivity.getInstance().getMapFragment().calculateScale(
            southLat, westLng, northLat, eastLng, viewWidth, viewHeight, scales);
        double centerLat = (southLat + northLat) / 2.0d, centerLng = (westLng + eastLng) / 2.0d;
        double leftLng = centerLng - scales[1] * (viewWidth / 2.0d + marginLeft + x),
            rightLng = centerLng + scales[1] * (viewWidth / 2.0d + marginRight),
            bottomLat = centerLat - scales[0] * (viewHeight / 2.0d + marginBottom),
            topLat = centerLat + scales[0] * (viewHeight / 2.0d + marginTop + y);
        MapService.getDefaultService().setBearing(0f, 0);
        MapService.getDefaultService().fitRegion(topLat, leftLng, bottomLat, rightLng);

    }

    protected void routeToParking() {
        final NavigationInfo navigationInfo = NavigationService.getDefaultService().getCurrentNavigationInfo();
        if (selectedParkingInfo != null && navigationInfo != null) {
            final PlaceInfo parkingInfo = selectedParkingInfo;
            GeoService.getDefaultService().updatePlace(
                parkingInfo,
                new GeoService.PlaceResultListener() {
                    @Override
                    public void onResult(PlaceInfo place) {
                        MainActivity.getInstance().hideParking();
                        MainActivity.getInstance().navigateTo(parkingInfo);
                    }

                    @Override
                    public void onError(ErrorCode error) {
                        if (error.getCode() != ErrorCode.GEO_ERROR_NO_RESULT) {
                            OnRoadApplication.getInstance().handleError(error);
                        }
                        MainActivity.getInstance().hideParking();
                        MainActivity.getInstance().navigateTo(parkingInfo);
                    }
                });
        }
    }

    @Override
    public boolean goBack() {
        if (selectHoursPanel.getVisibility() == View.VISIBLE) {
            MainActivity.getInstance().hideParking();
            return true;
        }
        if (parkingOptionsListView.getVisibility() == View.VISIBLE) {
            showPage(selectHoursPanel);
            return true;
        }
        if (reserveParkingPanel.getVisibility() == View.VISIBLE ||
            reservationConfirmedPanel.getVisibility() == View.VISIBLE) {
            showPage(parkingOptionsListView);
            return true;
        }
        return false;
    }

    protected void deleteParkingAnnotations() {
        if (parkingOptions != null) {
            for (ParkingInfo parkingInfo : parkingOptions) {
                MapService.getDefaultService().deleteAnnotation(parkingInfo.getFacilityId().hashCode());
            }
        }
    }
}
