package com.tcl.onetouch.onroad;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class OnRoadContentProvider extends ContentProvider {
    public static final String AUTHORITY = "com.tcl.onetouch.onroad";

    private static final String DATABASE_NAME;
    private static final int DATABASE_VERSION = 1;

    static {
        if (BuildConfig.DEBUG) {
            DATABASE_NAME = Environment.getExternalStorageDirectory() + "/OnRoad.db";
        } else {
            DATABASE_NAME = "OnRoad.db";
        }
    }

    public static abstract class ActionLogTable implements BaseColumns {
        public static final String TABLE_NAME = "actionlog";
        public static final String TYPE = "type";
        public static final String KEY = "key";
        public static final String LOG = "log";
        public static final String LAST_MOD = "lastmod";

        public static final String CREATE_QUERY =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY, " +
                TYPE + " INTEGER, " +
                KEY + " TEXT, " +
                LOG + " TEXT, " +
                LAST_MOD + " INTEGER " + ")";
        public static final String CREATE_TYPE_KEY_INDEX_QUERY =
            "CREATE UNIQUE INDEX IF NOT EXISTS type_key_index ON " + TABLE_NAME + " (" + TYPE + "," + KEY + ")";
        public static final String CREATE_LAST_MODE_INDEX_QUERY =
            "CREATE INDEX IF NOT EXISTS lastmod_index ON " + TABLE_NAME + " (" + LAST_MOD + ")";
    }

    private static final int URI_ACTION_LOG = 1;
    private static final int URI_ACTION_LOG_ID = 2;

    private DatabaseHelper dbHelper;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, ActionLogTable.TABLE_NAME, URI_ACTION_LOG);
        uriMatcher.addURI(AUTHORITY, ActionLogTable.TABLE_NAME + "/#", URI_ACTION_LOG_ID);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case URI_ACTION_LOG:
                return "vnd.android.cursor.dir/actionlog";
            case URI_ACTION_LOG_ID:
                return "vnd.android.cursor.item/actionlog";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        int match = uriMatcher.match(uri);
        switch (match) {
            case URI_ACTION_LOG:
                queryBuilder.setTables(ActionLogTable.TABLE_NAME);
                break;
            case URI_ACTION_LOG_ID:
                queryBuilder.setTables(ActionLogTable.TABLE_NAME);
                queryBuilder.appendWhere(ActionLogTable._ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        return queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case URI_ACTION_LOG: {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                long id = db.insertWithOnConflict(ActionLogTable.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(uri + "/" + id);
            }
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case URI_ACTION_LOG:
                break;
            case URI_ACTION_LOG_ID:
                selection = ActionLogTable._ID + "=" + uri.getPathSegments().get(1) +
                    (!TextUtils.isEmpty(selection) ? " AND (" + selection + ")" : "");
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int count = db.delete(ActionLogTable.TABLE_NAME, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case URI_ACTION_LOG:
                break;
            case URI_ACTION_LOG_ID:
                selection = ActionLogTable._ID + "=" + uri.getPathSegments().get(1) +
                    (!TextUtils.isEmpty(selection) ? " AND (" + selection + ")" : "");
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int count = db.update(ActionLogTable.TABLE_NAME, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    private class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(ActionLogTable.CREATE_QUERY);
            db.execSQL(ActionLogTable.CREATE_TYPE_KEY_INDEX_QUERY);
            db.execSQL(ActionLogTable.CREATE_LAST_MODE_INDEX_QUERY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
