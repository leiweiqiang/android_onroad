package com.tcl.onetouch.onroad;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

public class SetBookmarkDialogFragment extends DialogFragment {

    public interface OnBookmarkSelectedListener {
        public void onBookmarkSelected(SetBookmarkDialogFragment dialog, CharSequence name);
    }

    private OnBookmarkSelectedListener onBookmarkSelectedListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View contentView = inflater.inflate(R.layout.dialog_set_bookmark, null);
        contentView.findViewById(R.id.home_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onBookmarkSelectedListener != null) {
                    onBookmarkSelectedListener.onBookmarkSelected(SetBookmarkDialogFragment.this,
                        getResources().getString(R.string.home));
                }
                dismiss();
            }
        });
        contentView.findViewById(R.id.work_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onBookmarkSelectedListener != null) {
                    onBookmarkSelectedListener.onBookmarkSelected(SetBookmarkDialogFragment.this,
                        getResources().getString(R.string.work));
                }
                dismiss();
            }
        });
        contentView.findViewById(R.id.favorite_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onBookmarkSelectedListener != null) {
                    onBookmarkSelectedListener.onBookmarkSelected(SetBookmarkDialogFragment.this, "");
                }
                dismiss();
            }
        });

        builder.setTitle("Bookmark").
            setView(contentView).
            setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dismiss();
                }
            });
        return builder.create();
    }

    public void setOnBookmarkSelectedListener(OnBookmarkSelectedListener listener) {
        onBookmarkSelectedListener = listener;
    }
}
