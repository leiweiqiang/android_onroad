package com.tcl.onetouch.service.look4;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Location;
import android.view.View;
import android.view.ViewGroup;

import com.locationtoolkit.common.data.Coordinates;
import com.locationtoolkit.common.data.Point;
import com.locationtoolkit.map3d.MapController;
import com.locationtoolkit.map3d.MapDecoration;
import com.locationtoolkit.map3d.MapOptions;
import com.locationtoolkit.map3d.MapStatusListener;
import com.locationtoolkit.map3d.MapView;
import com.locationtoolkit.map3d.model.AnimationParameters;
import com.locationtoolkit.map3d.model.Avatar;
import com.locationtoolkit.map3d.model.CameraFactory;
import com.locationtoolkit.map3d.model.CameraParameters;
import com.locationtoolkit.map3d.model.HBAOParameters;
import com.locationtoolkit.map3d.model.LatLngBound;
import com.locationtoolkit.map3d.model.OptionalLayer;
import com.locationtoolkit.map3d.model.Pin;
import com.locationtoolkit.map3d.model.PinParameters;
import com.locationtoolkit.map3d.model.Polyline;
import com.locationtoolkit.map3d.model.Projection;
import com.tcl.onetouch.model.MapCoordinate;
import com.tcl.onetouch.onroad.BuildConfig;
import com.tcl.onetouch.service.MapService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Look4MapService extends MapService
    implements MapStatusListener, MapController.OnCameraUpdateListener,
    MapController.OnMapLongClickListener, MapController.OnOptionalLayersUpdatedListener,
    MapController.OnPinClickListener, MapController.OnPolylineClickListener,
    MapController.OnUnlockedListener {

    public static final double EARTH_MEAN_RADIUS_METERS = 6371009.0;

    private static final float DEFAULT_TILT_ANGLE = 90.0f;
    private static final float DEFAULT_ZOOM_LEVEL = 18.0f;
    private static final float HEADING_ANGLE_NORTH = 0.0f;
    private static final int DEFAULT_ANIMATION_DURATION = 200;
    private static final int FOLLOW_ANIMATION_DURATION_LIMIT = 3000;
    private static final int ANIMATION_INTERVAL = 17; // 60 fps
    private static final float ANIMATION_MIN_DISTANCE = 1.0f; // in meters

    private static final String LAYER_BUILDING_FOOTPRINTS = "Building Footprints";
    private static final String LAYER_WEATHER = "Weather";
    private static final String LAYER_3D_LANDMARKS_BUILDINGS = "3D Landmarks & Buildings";
    private static final String LAYER_POINTS_OF_INTEREST = "Points of Interest";
    private static final String LAYER_SATELLITE = "Satellite";

    private MapOptions mapViewConfig;
    private MapView mapView;
    private AnimationParameters centerAnimation;
    private CameraFactory cameraFactory;
    private boolean initialized;
    private MapService.Listener listener;
    private com.locationtoolkit.common.data.Location currentLocation;
    private int followingMode;
    private int offsetX;
    private int offsetY;

    private Map<Integer, Pin> addedPins;
    private Map<Pin, View> addedViews;

    @Override
    public void init(Context context, Listener listener) {
        this.listener = listener;
        mapViewConfig = new MapOptions();
        mapViewConfig.cacheFolder = context.getFilesDir().getAbsolutePath() + "/nbserviceresource";
        mapViewConfig.assetsFolder = "nbgmresource";
        mapViewConfig.zorderLevel = 0;
        mapViewConfig.enableFullScreenAntiAliasing = true;
        centerAnimation = new AnimationParameters(AnimationParameters.ACCELERATION_DECELERATION, DEFAULT_ANIMATION_DURATION);
        mapView = null;
        initialized = false;
        currentLocation = null;
        followingMode = FOLLOWING_MODE_POSITION_AND_HEADING_NORTH;
        addedPins = new HashMap<>();
        addedViews = new HashMap<>();
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                @Override
                public void run() {
                    Look4MapService.this.listener.onServiceReady();
                }
            });
        }
    }

    public static Coordinates move(Coordinates start, double bearing, double distance) {
        double bR = Math.toRadians(bearing);
        double lat1R = Math.toRadians(start.getLatitude());
        double lon1R = Math.toRadians(start.getLongitude());
        double dR = distance / EARTH_MEAN_RADIUS_METERS;

        double a = Math.sin(dR) * Math.cos(lat1R);
        double lat2 = Math.asin(Math.sin(lat1R) * Math.cos(dR) + a * Math.cos(bR));
        double lon2 = lon1R + Math.atan2(Math.sin(bR) * a, Math.cos(dR) - Math.sin(lat1R) * Math.sin(lat2));
        return new Coordinates(Math.toDegrees(lat2), Math.toDegrees(lon2));
    }

    @Override
    public void onMapCreated(MapController controller) {
        initialized = true;

        mapView.getController().addCameraUpdateListener(this);
        mapView.getController().setOnMapLongClickListener(this);
        mapView.getController().setOnOptionalLayersUpdatedListener(this);
        mapView.getController().setOnPinClickListener(this);
        mapView.getController().setOnUnlockedListener(this);
        mapView.getController().setNightMode(MapController.NightMode.NIGHTMODE_DAY);
//        mapView.getController().showTraffic(true);
        if (BuildConfig.DEBUG) {
//            mapView.getController().enableDebugView(true);
        }

        MapDecoration mapDecoration = new MapDecoration();
        mapDecoration.setCompassEnabled(false);
        mapDecoration.setLocateMeButtonEnabled(false);
        mapDecoration.setZoomButtonsEnabled(false);
        mapView.getController().setMapDecoration(mapDecoration);
        mapView.getController().setGpsMode(MapController.GPSMode.GPS_MODE_STAND_BY);

        cameraFactory = mapView.getController().createCameraFactory();
    }

    @Override
    public void onMapReady() {
        if (listener != null) {
            listener.onMapReady();
        }
    }

    @Override
    public void onMapLongClick(Coordinates coordinate) {
    }

    @Override
    public void onCameraUpdate(CameraParameters camera) {
    }

    @Override
    public void onOptionalLayersUpdated() {
        List<OptionalLayer> layers = mapView.getController().getOptionalLayers();
        if (layers != null) {
            for (OptionalLayer layer : layers) {
                LogUtil.d("optional layers: " + layer.getName() + ", " + layer.isEnabled());
                if (layer.getName().startsWith(LAYER_POINTS_OF_INTEREST)) {
                    layer.setEnabled(false);
                }
            }
        }
    }

    @Override
    public void onPinClick(Pin pin) {
        View view = addedViews.get(pin);
        if (view != null) {
            view.performClick();
        }
    }

    @Override
    public void pause() {
        if (mapView != null) {
            mapView.onPause();
        }
    }

    @Override
    public void resume() {
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    public void destroy() {
        releaseMapView();
    }

    @Override
    public void centerMe(int followingMode, float zoomLevel, int offsetX, int offsetY) {
        this.followingMode = FOLLOWING_MODE_NONE;
        animator.stop();
        if (mapView != null && currentLocation != null) {
            if (zoomLevel <= 0) {
                zoomLevel = DEFAULT_ZOOM_LEVEL;
            }
            CameraParameters camera = mapView.getController().getCameraPosition();
            camera.setZoomLevel(zoomLevel);
            if (followingMode == FOLLOWING_MODE_POSITION_AND_HEADING) {
                camera.setHeadingAngle((float) currentLocation.getHeading());
            } else if (followingMode == FOLLOWING_MODE_POSITION_AND_HEADING_NORTH) {
                camera.setHeadingAngle(HEADING_ANGLE_NORTH);
            }
            mapView.getController().moveTo(camera);
            this.offsetX = offsetX;
            this.offsetY = offsetY;
            if (offsetX != 0 || offsetY != 0) {
                Projection projection = mapView.getController().getProjection();
                Point p = projection.toScreenLocation(new Coordinates(currentLocation.getLatitude(), currentLocation.getLongitude()));
                p.setX(p.getX() - offsetX);
                p.setY(p.getY() - offsetY);
                camera.setPosition(projection.fromScreenLocation(p));
            } else {
                camera.setPosition(new Coordinates(currentLocation.getLatitude(), currentLocation.getLongitude()));
            }
            mapView.getController().animateTo(camera, centerAnimation);
        }
        this.followingMode = followingMode;
    }

    @Override
    public void centerLocation(Location location) {

    }

    @Override
    public void setBearing(float bearing, int duration) {
        CameraParameters camera = mapView.getController().getCameraPosition();
        camera.setHeadingAngle(bearing);
        mapView.getController().moveTo(camera);
    }

    @Override
    public float getBearing() {
        CameraParameters camera = mapView.getController().getCameraPosition();
        return camera.getHeadingAngle();
    }

    @Override
    public void onPolylineClick(Polyline[] polylines) {

    }

    private class Animator implements Runnable {
        com.locationtoolkit.common.data.Location currentLocation;
        com.locationtoolkit.common.data.Location endLocation;
        long duration;
        long interval;
        int frames;
        double deltaLat;
        double deltaLng;
        double deltaHeading;

        void start() {
            if (currentLocation == null || endLocation == null) {
                return;
            }
            frames = (int) Math.round((double) duration / ANIMATION_INTERVAL);
            deltaLat = 0;
            deltaLng = 0;
            deltaHeading = 0;
            if (frames <= 1) {
                currentLocation = endLocation;
                frames = 1;
            } else {
                float[] rs = new float[1];
                Location.distanceBetween(currentLocation.getLatitude(), currentLocation.getLongitude(),
                    endLocation.getLatitude(), endLocation.getLongitude(), rs);
                if (rs[0] < ANIMATION_MIN_DISTANCE) {
                    currentLocation = endLocation;
                    frames = 1;
                } else {
                    deltaLat = (endLocation.getLatitude() - currentLocation.getLatitude()) / frames;
                    deltaLng = (endLocation.getLongitude() - currentLocation.getLongitude()) / frames;
                    double diffHeading = endLocation.getHeading() - currentLocation.getHeading();
                    if (diffHeading > 180) {
                        diffHeading = diffHeading - 360;
                    } else if (diffHeading < -180) {
                        diffHeading = 360 + diffHeading;
                    }
                    deltaHeading = diffHeading / frames;
                }
            }
            interval = Math.round((double) duration / frames);
            run();
        }

        void stop() {
            duration = 0;
            interval= 0;
            deltaLat = 0;
            deltaLng = 0;
            deltaHeading = 0;
            frames = 0;
            currentLocation = null;
            endLocation = null;
            SystemUtil.cancelMainThreadAction(this);
        }

        @Override
        public void run() {
            if (currentLocation == null || !initialized) {
                return;
            }
            currentLocation.setLatitude(currentLocation.getLatitude() + deltaLat);
            currentLocation.setLongitude(currentLocation.getLongitude() + deltaLng);
            double heading = currentLocation.getHeading() + deltaHeading;
            if (heading >= 360) {
                heading -= 360;
            } else if (heading < 0) {
                heading += 360;
            }
            currentLocation.setHeading(heading);
            Avatar avatar = mapView.getController().getAvatar();
            avatar.setLocation(currentLocation);
            avatar.setMode(Avatar.AvatarMode.AVATAR_MODE_ARROW);
            if (followingMode != FOLLOWING_MODE_NONE) {
                CameraParameters camera = mapView.getController().getCameraPosition();
                if (followingMode == FOLLOWING_MODE_POSITION_AND_HEADING) {
                    camera.setHeadingAngle((float) heading);
                } else if (followingMode == FOLLOWING_MODE_POSITION_AND_HEADING_NORTH) {
                    camera.setHeadingAngle(HEADING_ANGLE_NORTH);
                }
                if (offsetX == 0 && offsetY == 0) {
                    camera.setPosition(new Coordinates(currentLocation.getLatitude(), currentLocation.getLongitude()));
                    mapView.getController().moveTo(camera);
                } else {
                    mapView.getController().moveTo(camera);
                    Projection projection = mapView.getController().getProjection();
                    Point p = projection.toScreenLocation(new Coordinates(currentLocation.getLatitude(), currentLocation.getLongitude()));
                    p.setX(p.getX() - offsetX);
                    p.setY(p.getY() - offsetY);
                    camera.setPosition(projection.fromScreenLocation(p));
                    mapView.getController().moveTo(camera);
                }
                mapView.getController().lockCameraPosition(true);
            }
            if (--frames > 0) {
                SystemUtil.postToMainThread(this, interval);
            } else {
                stop();
            }
        }
    };
    private Animator animator = new Animator();

    @Override
    public void updateCurrentLocation(Location location) {
        com.locationtoolkit.common.data.Location lastLocation = currentLocation;
        currentLocation = Look4Common.convertLocation(location);
        animator.stop();
        if (mapView != null && currentLocation != null) {
            long elapse = 0;
            if (lastLocation != null) {
                elapse = currentLocation.getGpsTime() - lastLocation.getGpsTime();
            }
            elapse /= 1000000;
            if (elapse > FOLLOW_ANIMATION_DURATION_LIMIT) {
                elapse = DEFAULT_ANIMATION_DURATION;
            }
            animator.endLocation = currentLocation;
            if (lastLocation != null) {
                animator.currentLocation = lastLocation;
            } else {
                animator.currentLocation = currentLocation;
            }
            animator.duration = elapse;
            animator.start();
        }
    }

    @Override
    public void fitRegion(double topLat, double leftLng, double bottomLat, double rightLng) {
        if (mapView != null && cameraFactory != null) {
            LatLngBound bound = new LatLngBound();
            bound.topLeftLatitude = topLat;
            bound.topLeftLongitude = leftLng;
            bound.bottomRightLatitude = bottomLat;
            bound.bottomRightLongitude = rightLng;
            CameraParameters camera = cameraFactory.createCamera(
                SystemUtil.getDisplayMetrics().widthPixels, SystemUtil.getDisplayMetrics().heightPixels,
                90, bound);
            mapView.getController().moveTo(camera);
            mapView.getController().lockCameraPosition(false);
            followingMode = FOLLOWING_MODE_NONE;
        }
    }

    @Override
    public View getMapView() {
        return mapView;
    }

    @Override
    public View createMapView(Context context) {
        if (mapView == null) {
            mapView = new MapView(context);
            mapView.initialize(mapViewConfig, Look4Common.getLTKContext());
            mapView.setMapStatusListener(this);
        }
        return mapView;
    }

    @Override
    public void releaseMapView() {
        if (initialized && mapView != null) {
            mapView.getController().clear();
            mapView.getController().setGpsMode(MapController.GPSMode.GPS_MODE_STAND_BY);
            com.locationtoolkit.common.data.Location location = new com.locationtoolkit.common.data.Location();
            location.setLatitude(CameraParameters.INVALID_LATITUED);
            location.setLongitude(CameraParameters.INVALID_LONGTITUED);
            location.setHeading(CameraParameters.INVALID_HEADING_VALUE);
            location.setValid(com.locationtoolkit.common.data.Location.VALID_LATITUDE | com.locationtoolkit.common.data.Location.VALID_LONGITUDE | com.locationtoolkit.common.data.Location.VALID_HEADING);
            Avatar avatar = mapView.getController().getAvatar();
            avatar.setMode(Avatar.AvatarMode.AVATAR_MODE_NONE);
            avatar.setLocation(location);
            MapDecoration mapDecoration = new MapDecoration();
            mapDecoration.setCompassEnabled(false);
            mapDecoration.setLocateMeButtonEnabled(false);
            mapDecoration.setZoomButtonsEnabled(true);
            mapView.getController().setMapDecoration(mapDecoration);
            mapView.setControlsPadding(0, 0, 0, 0);
            mapView.getController().setHBAOParameters(new HBAOParameters());
            initialized = false;
        }
        mapView = null;
        cameraFactory = null;
    }

    @Override
    public void addAnnotation(int id, MapCoordinate position, View view, float offsetX, float offsetY) {
        if (mapView == null) {
            return;
        }
        if (view.getMeasuredWidth() == 0 || view.getMeasuredHeight() == 0) {
            view.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        }
        int width = view.getMeasuredWidth();
        int height = view.getMeasuredHeight();
        Bitmap img = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(img);
        view.layout(0, 0, width, height);
        view.draw(canvas);
        PinParameters.PinImageInfo pinImageInfo = new PinParameters.PinImageInfo();
        pinImageInfo.bitmap(img).pinAnchor(0.5f + offsetX / (float) width, 0.5f + offsetY / (float) height);
        PinParameters pinParameters = new PinParameters();
        pinParameters.pinId(id).position(new Coordinates(position.getLatitude(), position.getLongitude()));
        pinParameters.setSelectedImage(pinImageInfo);
        pinParameters.setUnselectedImage(pinImageInfo);
        Pin pin = mapView.getController().addPin(pinParameters);
        addedPins.put(id, pin);
        addedViews.put(pin, view);
    }

    @Override
    public void deleteAnnotation(int id) {
        Pin pin = addedPins.get(id);
        if (pin != null) {
            pin.remove();
            addedPins.remove(id);
            addedViews.remove(pin);
        }
    }

    @Override
    public void onUnlocked() {
        LogUtil.d("location unlocked: " + mapView.getController().getGpsMode());
        switch (mapView.getController().getGpsMode()) {
            case GPS_MODE_FOLLOW_ME:
                followingMode = FOLLOWING_MODE_POSITION_AND_HEADING;
                break;
            case GPS_MODE_FOLLOW_ME_ANY_HEADING:
                followingMode = FOLLOWING_MODE_POSITION;
                break;
            default:
                followingMode = FOLLOWING_MODE_NONE;
        }
    }
}
