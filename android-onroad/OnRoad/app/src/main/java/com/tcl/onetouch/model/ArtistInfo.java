package com.tcl.onetouch.model;

public class ArtistInfo {
    private String uri;
    private String name;
    private String imageUrl;

    public ArtistInfo(String uri) {
        this.uri = uri;
        name = "";
        imageUrl = null;
    }

    public String getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
