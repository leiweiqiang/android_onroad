package com.tcl.onetouch.model;

import java.util.ArrayList;
import java.util.List;

public class PlaylistInfo {
    private String uri;
    private String name;
    private String description;
    private String imageUrl;
    private List<String> tracks;

    public PlaylistInfo(String uri) {
        this.uri = uri;
        name = "";
        description = "";
        imageUrl = "";
        tracks = new ArrayList<>();
    }

    public String getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getTracks() {
        return tracks;
    }

    public void addTrack(String trackUri) {
        tracks.add(trackUri);
    }
}
