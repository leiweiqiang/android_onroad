package com.tcl.onetouch.service;

import android.content.Context;
import android.location.Location;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.automatic.AutomaticCarService;

public abstract class CarService extends OnRoadService {
    public static final int DRIVE_STATE_UNKNOWN = 0;
    public static final int DRIVE_STATE_DRIVING = 1;
    public static final int DRIVE_STATE_END_DRIVE = 2;
    public static final int DRIVE_STATE_IGNITION_WAIT = 3;
    public static final int DRIVE_STATE_DISCONNECTED = 4;

    public interface Listener {
        public void onDriveStateChanged(int state);
        public void onConnectionChanged(boolean isConnected);
        public void onIgnitionEvent(boolean status);
    }

    public interface LocationListener {
        public void onLocation(Location location);
        public void onError(ErrorCode error);
    }

    public abstract void init(Context context, CarService.Listener listener);
    public abstract void destroy();
    public abstract void findMyCar(CarService.LocationListener listener);
    public abstract int getCurrentDriveState();
    public abstract boolean isIgnitionOn();
    public abstract boolean isConnected();

    private static CarService sDefaultService;

    public static CarService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new AutomaticCarService();
        }
        return sDefaultService;
    }

}
