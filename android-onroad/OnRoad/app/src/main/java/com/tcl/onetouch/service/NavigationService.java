package com.tcl.onetouch.service;

import android.content.Context;
import android.location.Location;
import android.view.View;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.NavigationInfo;
import com.tcl.onetouch.service.look4.Look4NavigationService;

public abstract class NavigationService extends OnRoadService {
    public static final String PREF_KEY_NAVIGATION_TYPE = "navigation_type";

    public static interface Listener {
        public void onNavigationInitialized(NavigationInfo navigationInfo);
        public void onNavigationStarted(NavigationInfo navigationInfo);
        public void onNavigationUpdated(NavigationInfo navigationInfo);
        public void onNavigationInstruction(NavigationInfo navigationInfo);
        public void onNavigationEnded(NavigationInfo navigationInfo);
        public void onDestinationReached(NavigationInfo navigationInfo);
        public void onRouteCalculationStarted(NavigationInfo navigationInfo);
        public void onRouteCalculationCompleted(NavigationInfo navigationInfo);
        public void onRouteCalculationCanceled(NavigationInfo navigationInfo);
        public void onRouteCalculationFailed(NavigationInfo navigationInfo, ErrorCode error);
    }

    public abstract void init(Context context, NavigationService.Listener listener);
    public abstract void initNavigation(NavigationInfo navigationInfo, View mapView);
    public abstract void calculateRoute();
    public abstract void cancelRouteCalculation();
    public abstract int getNumberOfRoutes();
    public abstract int getSelectedRoute();
    public abstract void selectRoute(int index);
    public abstract double[][] getRouteBoundingBox();
    public abstract void setRoutesVisibility(boolean visibility);
    public abstract void startNavigation();
    public abstract void stopNavigation();
    public abstract void updateCurrentLocation(Location location);
    public abstract NavigationInfo getCurrentNavigationInfo();

    private static NavigationService sDefaultService;

    public static NavigationService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new Look4NavigationService();
        }
        return sDefaultService;
    }
}
