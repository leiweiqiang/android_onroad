package com.tcl.onetouch.view;

import android.content.Context;
import android.content.Intent;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.SignInButton;
import com.tcl.onetouch.onroad.MainActivity;
import com.tcl.onetouch.onroad.R;
import com.tcl.onetouch.service.UserService;

import java.util.Map;

public class GoogleLoginPreference extends Preference {

    private SignInButton signInButton;
    private Button signOutButton;

    public GoogleLoginPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public GoogleLoginPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GoogleLoginPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GoogleLoginPreference(Context context) {
        super(context);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        signInButton = (SignInButton) view.findViewById(R.id.google_sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserService.getInstance().googleSignIn(MainActivity.getInstance());
            }
        });

        signOutButton = (Button) view.findViewById(R.id.google_sign_out_button);
        signOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserService.getInstance().googleSignOut();
            }
        });

        updateUi(UserService.getInstance().getGoogleSignInResult() != null);
    }

    public void updateUi(boolean isSignedIn) {
        if (signInButton == null || signOutButton == null) {
            return;
        }
        if (isSignedIn) {
            signInButton.setVisibility(View.GONE);
            signOutButton.setVisibility(View.VISIBLE);
        } else {
            signInButton.setVisibility(View.VISIBLE);
            signOutButton.setVisibility(View.GONE);
        }
    }
}
