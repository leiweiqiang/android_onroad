package com.tcl.onetouch.service.look4;

import android.content.Context;
import android.location.Location;
import android.text.Html;
import android.view.View;

import com.locationtoolkit.common.LTKContext;
import com.locationtoolkit.common.LTKException;
import com.locationtoolkit.common.data.BoundingBox;
import com.locationtoolkit.common.data.ColorSegment;
import com.locationtoolkit.common.data.Coordinates;
import com.locationtoolkit.common.data.Maneuver;
import com.locationtoolkit.common.data.ManeuverList;
import com.locationtoolkit.common.data.Place;
import com.locationtoolkit.common.data.RouteOptions;
import com.locationtoolkit.common.data.RouteTrafficSegment;
import com.locationtoolkit.common.data.SegmentAttribute;
import com.locationtoolkit.common.data.TrafficEvent;
import com.locationtoolkit.common.route.RouteInformation;
import com.locationtoolkit.common.traffic.TrafficInformation;
import com.locationtoolkit.map3d.MapController;
import com.locationtoolkit.map3d.MapView;
import com.locationtoolkit.map3d.model.Polyline;
import com.locationtoolkit.map3d.model.PolylineParameters;
import com.locationtoolkit.navigation.Navigation;
import com.locationtoolkit.navigation.Preferences;
import com.locationtoolkit.navigation.data.AnnouncementInformation;
import com.locationtoolkit.navigation.data.LaneInformation;
import com.locationtoolkit.navigation.data.RoadSignInformation;
import com.locationtoolkit.navigation.data.SpecialRegionInformation;
import com.locationtoolkit.navigation.data.SpeedLimitInformation;
import com.locationtoolkit.navigation.event.listeners.AnnouncementListener;
import com.locationtoolkit.navigation.event.listeners.SessionListener;
import com.locationtoolkit.navigation.model.listeners.ManeuverUpdateListener;
import com.locationtoolkit.navigation.model.listeners.NavEventListener;
import com.locationtoolkit.navigation.model.listeners.PositionUpdateListener;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.MapCoordinate;
import com.tcl.onetouch.model.NavigationInfo;
import com.tcl.onetouch.model.OnRoadErrorCode;
import com.tcl.onetouch.service.NavigationService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Look4NavigationService extends NavigationService implements MapController.OnPolylineClickListener {

    private static final int ROUTE_COLOR = 0x7AFFFF;
    private static final int ALT_ROUTE_COLOR = 0x9A9A9AB4;
    private static final int NORMAL_CONGESTION_COLOR = 0xFFE600FF;
    private static final int MODERATE_CONGESTION_COLOR = 0xFF7900FF;
    private static final int SEVERE_CONGESTION_COLOR = 0xFF0000FF;

    private SessionListener sessionListener = new SessionListener() {

        @Override
        public void offroute() {
        }

        @Override
        public void onroute() {
        }

        @Override
        public void routeReceived(int reason, RouteInformation[] routes) {
            if (navigationInfo == null) {
                return;
            }
            if (routes == null) {
                currentRoutes = new RouteInformation[0];
            } else {
                currentRoutes = routes;
            }
            if (navigationInfo.isRouting()) {
                onCalculateRouteComplete();
            }
        }

        @Override
        public void routeProgress(int progress) {
            LogUtil.d("SessionListener.routeProgress: " + progress);
        }

        @Override
        public void routeError(LTKException exception) {
            LogUtil.d("SessionListener.routeError: " + exception.getErrorCode());
            navigationInfo.setIsRouting(false);
            if (listener != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    NavigationInfo info = navigationInfo;

                    @Override
                    public void run() {
                        ErrorCode error = createErrorCode(OnRoadErrorCode.NAVIGATION_ERROR);
                        listener.onRouteCalculationFailed(info, error);
                    }
                });
            }
            clearNavigation();
        }

        @Override
        public void routeFinish() {
            LogUtil.d("SessionListener.routeFinish");
            if (navigationInfo == null || !navigationInfo.isNavigating()) {
                return;
            }
            navigationInfo.setIsNavigating(false);
            navigationInfo.setIsDestinationReached(true);
            if (listener != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    NavigationInfo info = navigationInfo;

                    @Override
                    public void run() {
                        listener.onDestinationReached(info);
                    }
                });
            }
        }

        @Override
        public void routeRequested(int reason) {
            LogUtil.d("SessionListener.routeRequested: " + reason);
        }

        @Override
        public void routeUpdating() {
            LogUtil.d("SessionListener.routeUpdating");
        }
    };

    private ManeuverUpdateListener maneuverUpdateListener = new ManeuverUpdateListener() {
        @Override
        public void maneuverUpdateListener(String currentRoadPrimaryName,
                                           String currentRoadSecondaryName,
                                           String maneuverExitNumber,
                                           String maneuverImageId,
                                           Coordinates maneuverPoint,
                                           String maneuverType,
                                           String nextRoadPrimaryName,
                                           String nextRoadSecondaryName,
                                           String maneuverStackTurnImageTTF,
                                           ManeuverList maneuvers) {
            if (navigationInfo == null) {
                return;
            }
            navigationInfo.setCurrentStreetName(currentRoadPrimaryName);
            NavigationInfo.Advice advice = navigationInfo.getCurrentAdvice();
            NavigationInfo.Advice nextAdvice = navigationInfo.getNextAdvice();
            if (nextRoadPrimaryName == null) {
                nextRoadPrimaryName = "";
            }
            if (maneuverType == null) {
                maneuverType = "";
            }
            boolean newAdvice = false;
            if (!nextRoadPrimaryName.equalsIgnoreCase(advice.streetName)) {
                advice.advice = null;
                advice.instruction = null;
                newAdvice = true;
            }
            advice.streetName = nextRoadPrimaryName;
            advice.exitNumber = maneuverExitNumber;
            Maneuver.ManeuverCode code = Maneuver.maneuverMap.get(maneuverType);
            if (code != null) {
                advice.direction = Look4Common.convertDirection(code);
            } else {
                advice.direction = NavigationInfo.DIRECTION_INVALID;
            }
            Maneuver maneuver = null;
            Maneuver nextManeuver = null;
            if (maneuvers != null && maneuverPoint != null) {
                for (int i = 0; i < maneuvers.getNumberOfManeuvers(); ++i) {
                    Maneuver m = maneuvers.getManeuver(i);
                    if (maneuver != null) {
                        nextManeuver = m;
                        break;
                    }
                    Coordinates c = m.getPoint();
                    if (Double.doubleToLongBits(maneuverPoint.getLatitude()) == Double.doubleToLongBits(c.getLatitude()) &&
                        Double.doubleToLongBits(maneuverPoint.getLongitude()) == Double.doubleToLongBits(c.getLongitude())) {
                        maneuver = m;
                    }
                }
                if (maneuver != null) {
                    advice.instruction = advice.advice = maneuver.getDescription();
                    newAdvice = true;
                }
            }
            if (nextManeuver != null) {
                nextAdvice.streetName = nextManeuver.getPrimaryStreet();
                nextAdvice.exitNumber = nextManeuver.getExitNumber();
                code = Maneuver.maneuverMap.get(maneuverType);
                if (code != null) {
                    nextAdvice.direction = Look4Common.convertDirection(code);
                } else {
                    nextAdvice.direction = NavigationInfo.DIRECTION_INVALID;
                }
                nextAdvice.distance = nextManeuver.getDistance();
            } else if (maneuver != null) {
                nextAdvice.streetName = "";
                nextAdvice.exitNumber = "";
                nextAdvice.direction = NavigationInfo.DIRECTION_INVALID;
                nextAdvice.distance = 0;
            }
            if (newAdvice && listener != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    NavigationInfo info = navigationInfo;

                    @Override
                    public void run() {
                        listener.onNavigationUpdated(info);
                    }
                });
            }
        }
    };

    private PositionUpdateListener positionUpdateListener = new PositionUpdateListener() {
        @Override
        public void positionUpdateListener(int maneuverRemainingDelay,
                                           double maneuverRemainingDistance,
                                           int maneuverRemainingTime,
                                           Coordinates coordinates,
                                           int speed,
                                           int heading,
                                           int tripRemainingDelay,
                                           double tripRemainingDistance,
                                           int tripRemainingTime) {
            if (navigationInfo == null) {
                return;
            }
            navigationInfo.setTravelTimeInSec(tripRemainingTime);
            navigationInfo.setTravelDistanceInMeter((int) tripRemainingDistance);
            navigationInfo.setTravelDelayInSec(tripRemainingDelay);
            NavigationInfo.Advice advice = navigationInfo.getCurrentAdvice();
            advice.distance = maneuverRemainingDistance;
            advice.time = maneuverRemainingTime;
            advice.delay = maneuverRemainingDelay;
            if (listener != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    NavigationInfo info = navigationInfo;

                    @Override
                    public void run() {
                        listener.onNavigationUpdated(info);
                    }
                });
            }
        }
    };

    private NavEventListener navEventListener = new NavEventListener() {
        @Override
        public void navEventListener(int navEventTypeMask,
                                     LaneInformation laneInfo,
                                     RoadSignInformation signInfo,
                                     SpeedLimitInformation speedLimitInfo,
                                     SpecialRegionInformation specialRegionInfo,
                                     TrafficEvent trafficEvent,
                                     TrafficInformation trafficInfo) {
        }
    };

    private AnnouncementListener announcementListener = new AnnouncementListener() {
        @Override
        public void announce(AnnouncementInformation announcement) {
            if (navigationInfo == null) {
                return;
            }
            String instruction = announcement.getText();
            if (instruction == null) {
                return;
            }
            instruction = Html.fromHtml(announcement.getText()).toString();
            navigationInfo.getCurrentAdvice().instruction = instruction;
            if (listener != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    NavigationInfo info = navigationInfo;

                    @Override
                    public void run() {
                        listener.onNavigationInstruction(info);
                    }
                });
            }
        }
    };

    private Listener listener;
    private Preferences preferences;
    private NavigationInfo navigationInfo;
    private MapView mapView;
    private Navigation navigation;

    private RouteInformation[] currentRoutes;
    private Set<Polyline>[] routePolylines;
    private int selectedRoute;
    private double[][] routeBoundingBox;

    @Override
    public void init(Context context, Listener listener) {
        this.listener = listener;
        preferences = new Preferences();
        preferences.setNaturalGuidanceEnabled(true);
        preferences.setLaneGuidanceEnabled(true);
        preferences.setOverheadSignEnabled(true);
        preferences.setTrafficAnnouncements(true);
        preferences.setNavTrafficFor(Preferences.NAV_TRAFFIC_FOR_NAVIGATION);
        preferences.setMeasurement(LTKContext.Measurement.NON_METRIC);
        preferences.setMultipleRoutes(true);

        navigationInfo = null;
        mapView = null;
        navigation = null;

        routePolylines = null;
        currentRoutes = null;
        selectedRoute = -1;
        routeBoundingBox = null;
    }

    @Override
    public void initNavigation(NavigationInfo info, View view) {
        clearNavigation();
        navigationInfo = info;
        navigationInfo.setIsRouting(false);
        navigationInfo.setIsRouteReady(false);
        navigationInfo.setIsNavigating(false);
        navigationInfo.setIsReRouting(false);
        navigationInfo.setIsDestinationReached(false);

        mapView = null;
        if (view instanceof MapView) {
            mapView = (MapView) view;
            mapView.getController().setOnPolylineClickListener(this);
        }

        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onNavigationInitialized(info);
                }
            });
        }

        RouteOptions routeOption = null;
        if (navigationInfo.getRouteType() == NavigationInfo.ROUTE_TYPE_CAR_FASTEST) {
            routeOption = new RouteOptions(RouteOptions.ROUTE_FASTEST, RouteOptions.TRANSPORTATION_MODE_CAR, 0);
        } else if (navigationInfo.getRouteType() == NavigationInfo.ROUTE_TYPE_CAR_SHORTEST) {
            routeOption = new RouteOptions(RouteOptions.ROUTE_SHORTEST, RouteOptions.TRANSPORTATION_MODE_CAR, 0);
        } else {
            routeOption = new RouteOptions(RouteOptions.ROUTE_SHORTEST, RouteOptions.TRANSPORTATION_MODE_PEDESTRIAN, 0);
        }

        navigation = Navigation.getNavigation(Look4Common.getLTKContext(),
            new Place(Look4Common.convertLocation(navigationInfo.getNavigationDestination().getLocation())),
            routeOption, preferences);
        navigation.updatePosition(Look4Common.convertLocation(navigationInfo.getStartCoordinate()));
        navigation.addSessionListener(sessionListener);
        navigation.addAnnouncementListener(announcementListener);
        navigation.addManeuverUpdateListener(maneuverUpdateListener);
        navigation.addNavEventListener(navEventListener);
        navigation.addPositionUpdateListener(positionUpdateListener);
    }

    protected void clearNavigation() {
        if (routePolylines != null) {
            for (Set<Polyline> polylines : routePolylines) {
                if (polylines == null) {
                    continue;
                }
                for (Polyline polyline : polylines) {
                    polyline.remove();
                }
            }
        }
        routePolylines = null;
        navigationInfo = null;
        if (navigation != null) {
            navigation.removeSessionListener(sessionListener);
            navigation.removeAnnouncementListener(announcementListener);
            navigation.removeManeuverUpdateListener(maneuverUpdateListener);
            navigation.removeNavEventListener(navEventListener);
            navigation.removePositionUpdateListener(positionUpdateListener);
            navigation.stopSession();
        }
        navigation = null;
        currentRoutes = null;
        selectedRoute = -1;
        routeBoundingBox = null;
        if (mapView != null) {
            mapView.getController().setOnPolylineClickListener(null);
        }
        mapView = null;
    }

    @Override
    public void calculateRoute() {
        if (navigationInfo == null) {
            return;
        }

        navigationInfo.setIsRouting(true);
        navigationInfo.setRoute(null);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onRouteCalculationStarted(info);
                }
            });
        }
        if (currentRoutes != null) {
            onCalculateRouteComplete();
        }
    }

    private void onCalculateRouteComplete() {
        if (currentRoutes == null) {
            return;
        }

        if (currentRoutes.length == 0) {
            navigationInfo.setIsRouting(false);
            if (listener != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    NavigationInfo info = navigationInfo;

                    @Override
                    public void run() {
                        listener.onRouteCalculationFailed(
                            info,
                            createErrorCode(OnRoadErrorCode.NAVIGATION_ERROR_NO_RESULT));
                    }
                });
            }
            clearNavigation();
            return;
        }

        routeBoundingBox = new double[][]{
            {Double.MAX_VALUE, Double.MAX_VALUE},
            {-Double.MAX_VALUE, -Double.MAX_VALUE}};
        for (int i = 0; i < currentRoutes.length; ++i) {
            BoundingBox bb = currentRoutes[i].getBoundingBox();
            if (bb.getPoint1().getLatitude() < routeBoundingBox[0][0]) {
                routeBoundingBox[0][0] = bb.getPoint1().getLatitude();
            }
            if (bb.getPoint1().getLatitude() > routeBoundingBox[1][0]) {
                routeBoundingBox[1][0] = bb.getPoint1().getLatitude();
            }
            if (bb.getPoint1().getLongitude() < routeBoundingBox[0][1]) {
                routeBoundingBox[0][1] = bb.getPoint1().getLongitude();
            }
            if (bb.getPoint1().getLongitude() > routeBoundingBox[1][1]) {
                routeBoundingBox[1][1] = bb.getPoint1().getLongitude();
            }
            if (bb.getPoint2().getLatitude() < routeBoundingBox[0][0]) {
                routeBoundingBox[0][0] = bb.getPoint2().getLatitude();
            }
            if (bb.getPoint2().getLatitude() > routeBoundingBox[1][0]) {
                routeBoundingBox[1][0] = bb.getPoint2().getLatitude();
            }
            if (bb.getPoint2().getLongitude() < routeBoundingBox[0][1]) {
                routeBoundingBox[0][1] = bb.getPoint2().getLongitude();
            }
            if (bb.getPoint2().getLongitude() > routeBoundingBox[1][1]) {
                routeBoundingBox[1][1] = bb.getPoint2().getLongitude();
            }
        }
        selectRouteInternal(0);
        navigationInfo.setIsRouting(false);
        navigationInfo.setIsRouteReady(true);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onRouteCalculationCompleted(info);
                }
            });
        }
    }

    private boolean selectRouteInternal(int routeIndex) {
        if (currentRoutes == null || routeIndex >= currentRoutes.length || selectedRoute == routeIndex) {
            return false;
        }
        selectedRoute = routeIndex;
        RouteInformation route = currentRoutes[routeIndex];
        navigation.setActiveRoute(route);
        navigationInfo.setRouteSummary(route.getRouteDescription());
        navigationInfo.setTravelDistanceInMeter((int) route.getDistance());
        navigationInfo.setTravelTimeInSec((int) route.getTime());
        navigationInfo.getCurrentAdvice().instruction = route.getInitialGuidanceText();
        Coordinates[] coordinates = route.getPolyline();
        if (coordinates != null) {
            List<MapCoordinate> points = new ArrayList<>(coordinates.length);
            for (Coordinates loc : coordinates) {
                points.add(new MapCoordinate(loc.getLatitude(), loc.getLongitude()));
            }
            navigationInfo.setRoute(points);
        }
        if (routePolylines != null) {
            for (Set<Polyline> polylines : routePolylines) {
                if (polylines == null) {
                    continue;
                }
                for (Polyline polyline : polylines) {
                    polyline.remove();
                }
            }
        }
        routePolylines = null;
        if (mapView != null) {
            int numRoutes = Math.min(currentRoutes.length, 3);
            routePolylines = new Set[numRoutes];
            for (int i = 0; i < numRoutes; ++i) {
                Coordinates[] routeCoordinates = currentRoutes[i].getPolyline();
                if (routeCoordinates == null || routeCoordinates.length == 0) {
                    continue;
                }
                Set<Polyline> polylines = new HashSet<Polyline>();
                int routeColor = ALT_ROUTE_COLOR;
                int routeZorder = (i + 1) * 10;
                int trafficZorder = routeZorder - 1;
                if (i == selectedRoute) {
                    routeColor = ROUTE_COLOR;
                    routeZorder += 100;
                    trafficZorder = routeZorder + 1;
                }
                PolylineParameters polyline = new PolylineParameters();
                for (Coordinates loc : routeCoordinates) {
                    polyline.addPoints(loc);
                }
                List<SegmentAttribute> segments = new ArrayList<>();
                segments.add(new ColorSegment(routeCoordinates.length - 1, 0));
                polyline.startCap(new PolylineParameters.RoundCapParameter(10)).
                    endCap(new PolylineParameters.RoundCapParameter(10)).
                    segmentAttributes(segments).
                    unhighlightColor(routeColor).
                    width(15).
                    zOrder(routeZorder).
                    visible(true);
                polylines.add(mapView.getController().addPolyline(polyline));
                List<RouteTrafficSegment> trafficSegments = route.getRouteTrafficSegment();
                if (trafficSegments != null && !trafficSegments.isEmpty()) {
                    for (RouteTrafficSegment trafficSegment : trafficSegments) {
                        Coordinates[] trafficCoordinates = trafficSegment.getPolyline();
                        if (trafficCoordinates == null || trafficCoordinates.length == 0) {
                            continue;
                        }
                        ColorSegment seg = trafficSegment.getSegColor();
                        int trafficColor;
                        if (seg.getColor() == ColorSegment.COLORS.YELLOW.ordinal()) {
                            trafficColor = MODERATE_CONGESTION_COLOR;
                        } else if (seg.getColor() == ColorSegment.COLORS.RED.ordinal()) {
                            trafficColor = SEVERE_CONGESTION_COLOR;
                        } else {
                            continue;
                        }
                        PolylineParameters trafficPolyline = new PolylineParameters();
                        for (Coordinates loc : trafficCoordinates) {
                            trafficPolyline.addPoints(loc);
                        }
                        List<SegmentAttribute> trafficColorSegments = new ArrayList<>();
                        trafficColorSegments.add(new ColorSegment(trafficCoordinates.length - 1, 0));
                        trafficPolyline.
                            segmentAttributes(trafficColorSegments).
                            unhighlightColor(trafficColor).
                            width(15).
                            zOrder(trafficZorder).
                            visible(true);
                        polylines.add(mapView.getController().addPolyline(trafficPolyline));
                    }
                }
                routePolylines[i] = polylines;
            }
        }
        return true;
    }

    @Override
    public void cancelRouteCalculation() {
        navigationInfo.setIsRouting(false);
        navigationInfo.setIsRouteReady(false);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onRouteCalculationCanceled(info);
                }
            });
        }
        clearNavigation();
    }

    @Override
    public void selectRoute(int routeIndex) {
        if (selectRouteInternal(routeIndex) && listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onNavigationUpdated(info);
                }
            });
        }
    }

    @Override
    public double[][] getRouteBoundingBox() {
        return routeBoundingBox;
    }

    @Override
    public void setRoutesVisibility(boolean visibility) {
        for (Set<Polyline> polylines : routePolylines) {
            if (polylines == null) {
                continue;
            }
            for (Polyline polyline : polylines) {
                polyline.setVisible(visibility);
            }
        }
    }

    @Override
    public int getNumberOfRoutes() {
        return currentRoutes == null ? 0 : currentRoutes.length;
    }

    @Override
    public int getSelectedRoute() {
        return selectedRoute;
    }

    @Override
    public void startNavigation() {
        if (navigationInfo == null || !navigationInfo.isRouteReady()) {
            return;
        }
        for (int i = 0; i < routePolylines.length; ++i) {
            Set<Polyline> polylines = routePolylines[i];
            if (i == selectedRoute) {
                continue;
            }
            for (Polyline polyline : polylines) {
                polyline.remove();
            }
            routePolylines[i] = null;
        }
        routeBoundingBox = new double[][]{
            {Double.MAX_VALUE, Double.MAX_VALUE},
            {Double.MIN_VALUE, Double.MIN_VALUE}};
        BoundingBox bb = currentRoutes[selectedRoute].getBoundingBox();
        if (bb.getPoint1().getLatitude() < routeBoundingBox[0][0]) {
            routeBoundingBox[0][0] = bb.getPoint1().getLatitude();
        } else if (bb.getPoint1().getLatitude() > routeBoundingBox[1][0]) {
            routeBoundingBox[1][0] = bb.getPoint1().getLatitude();
        }
        if (bb.getPoint1().getLongitude() < routeBoundingBox[0][1]) {
            routeBoundingBox[0][1] = bb.getPoint1().getLongitude();
        } else if (bb.getPoint1().getLongitude() > routeBoundingBox[1][1]) {
            routeBoundingBox[1][1] = bb.getPoint1().getLongitude();
        }
        if (bb.getPoint2().getLatitude() < routeBoundingBox[0][0]) {
            routeBoundingBox[0][0] = bb.getPoint1().getLatitude();
        } else if (bb.getPoint2().getLatitude() > routeBoundingBox[1][0]) {
            routeBoundingBox[1][0] = bb.getPoint1().getLatitude();
        }
        if (bb.getPoint2().getLongitude() < routeBoundingBox[0][1]) {
            routeBoundingBox[0][1] = bb.getPoint1().getLongitude();
        } else if (bb.getPoint2().getLongitude() > routeBoundingBox[1][1]) {
            routeBoundingBox[1][1] = bb.getPoint1().getLongitude();
        }

        navigationInfo.setIsNavigating(true);
        navigationInfo.setIsRouteReady(false);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onNavigationStarted(info);
                }
            });
        }
    }

    @Override
    public void stopNavigation() {
        if (navigationInfo == null || (!navigationInfo.isNavigating() && !navigationInfo.isDestinationReached())) {
            return;
        }

        if (navigation != null) {
            navigation.stopSession();
        }

        navigationInfo.setIsNavigating(false);
        navigationInfo.setIsDestinationReached(false);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onNavigationEnded(info);
                }
            });
        }
        clearNavigation();
    }

    @Override
    public void updateCurrentLocation(Location location) {
        if (navigation != null && navigationInfo != null && navigationInfo.isNavigating()) {
            navigation.updatePosition(Look4Common.convertLocation(location));
        }
    }

    @Override
    public NavigationInfo getCurrentNavigationInfo() {
        return navigationInfo;
    }

    @Override
    public void onPolylineClick(Polyline[] polylines) {
        LogUtil.d("onPolylineClick: " + polylines);
        if (polylines == null || routePolylines == null || routePolylines.length == 0) {
            return;
        }
        for (Polyline polyline : polylines) {
            for (int i = 0; i < routePolylines.length; ++i) {
                if (routePolylines[i].contains(polyline)) {
                    selectRoute(0);
                    if (listener != null) {
                        SystemUtil.postToMainThread(new Runnable() {
                            NavigationInfo info = navigationInfo;

                            @Override
                            public void run() {
                                listener.onNavigationUpdated(info);
                            }
                        });
                    }
                    return;
                }
            }
        }
    }
}
