package com.tcl.onetouch.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.tcl.onetouch.onroad.R;
import com.tcl.onetouch.util.SystemUtil;

public class PagedListView extends ListView {
    private static final int DEFAULT_PAGE_SIZE = 3;

    public static interface PageChangedListener {
        public void onPageChanged(PagedListView view, int startPos, int endPos);
    }

    private int pageSize;
    private boolean touchScrolled;
    private boolean flingDown;
    private boolean flingUp;
    private int firstVisibleItem;
    private int firstPageItem;
    private int lastPageItem;
    private PageChangedListener pageChangedListener;

    private DataSetObserver dataSetObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            touchScrolled = false;
            flingDown = false;
            flingUp = false;
            firstVisibleItem = getFirstVisiblePosition();
            updatePagedItems();
        }
    };

    public PagedListView(Context context) {
        super(context);
        init(context, null);
    }

    public PagedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PagedListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public PagedListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        boolean processed = super.onTouchEvent(ev);
        final int actionMasked = ev.getActionMasked();
        if (processed && actionMasked == MotionEvent.ACTION_MOVE) {
            touchScrolled = true;
        }
        flingDown = false;
        flingUp = false;
        return processed;
    }

    protected void init(Context context, AttributeSet attrs) {
        pageSize = DEFAULT_PAGE_SIZE;
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.PagedListView,
                0, 0);

            try {
                pageSize = a.getInteger(R.styleable.PagedListView_pageSize, DEFAULT_PAGE_SIZE);
            } finally {
                a.recycle();
            }
        }
        flingDown = false;
        flingUp = false;
        touchScrolled = false;
        firstPageItem = -1;
        lastPageItem = -1;
        firstVisibleItem = 0;
        setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState != SCROLL_STATE_IDLE) {
                    return;
                }
                int firstPos = getFirstVisiblePosition();
                if (touchScrolled) {
                    int scrollingDirection = firstPos - firstVisibleItem;
                    if (flingDown || scrollingDirection > 0) {
                        int scrollingTo = pageSize * (firstVisibleItem / pageSize) + pageSize;
                        if (scrollingTo >= getCount()) {
                            scrollingTo -= pageSize;
                        }
                        final int scrollToPos = scrollingTo;
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                smoothScrollToPositionFromTop(scrollToPos, 0, 200);
                            }
                        });
                    } else if (flingUp || scrollingDirection < 0) {
                        int scrollingTo = pageSize * (firstVisibleItem / pageSize) - pageSize;
                        if (scrollingTo < 0) {
                            scrollingTo = 0;
                        }
                        final int scrollToPos = scrollingTo;
                        SystemUtil.postToMainThread(new Runnable() {
                            @Override
                            public void run() {
                                smoothScrollToPositionFromTop(scrollToPos, 0, 200);
                            }
                        });
                    } else {
                        View itemView = view.getChildAt(0);
                        final int top = itemView.getTop();
                        if (top != 0) {
                            SystemUtil.postToMainThread(new Runnable() {
                                @Override
                                public void run() {
                                    smoothScrollBy(top, 200);
                                }
                            });
                        }
                    }
                } else {
                    if (updatePagedItems() && pageChangedListener != null) {
                        pageChangedListener.onPageChanged(PagedListView.this, firstPageItem, lastPageItem);
                    }
                }
                firstVisibleItem = firstPos;
                flingDown = false;
                flingUp = false;
                touchScrolled = false;
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
    }

    @Override
    public boolean dispatchNestedPreFling(float velocityX, float velocityY) {
        if (velocityY > 0) {
            flingDown = true;
        } else {
            flingUp = true;
        }
        return true;
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        ListAdapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterDataSetObserver(dataSetObserver);
        }
        super.setAdapter(adapter);
        adapter.registerDataSetObserver(dataSetObserver);
        firstPageItem = -1;
        lastPageItem = -1;
        firstVisibleItem = 0;
    }

    public void setPageChangedListener(PageChangedListener listener) {
        pageChangedListener = listener;
    }

    public int getFirstPageItem() {
        return firstPageItem;
    }

    public int getLastPageItem() {
        return lastPageItem;
    }

    protected boolean updatePagedItems() {
        boolean updated = false;
        int firstItem = getFirstVisiblePosition();
        if (firstItem != firstPageItem) {
            firstPageItem = firstItem;
            updated = true;
        }
        int lastItem = firstPageItem + pageSize - 1;
        int lastVisibleItem = getLastVisiblePosition();
        if (lastItem > lastVisibleItem) {
            lastItem = lastVisibleItem;
        }
        if (lastItem != lastPageItem) {
            lastPageItem = lastItem;
            updated = true;
        }
        return updated;
    }

    public void reset() {
        firstPageItem = 0;
        lastPageItem = firstPageItem + pageSize - 1;
        if (lastPageItem >= getCount()) {
            lastPageItem = getCount() - 1;
        }
        smoothScrollToPositionFromTop(0, 0, 0);
    }
}
