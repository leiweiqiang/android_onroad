package com.tcl.onetouch.model;

import java.util.ArrayList;
import java.util.List;

public class ParkingInfo extends PlaceInfo {
    public static final int PARKING_TYPE_ON_UNKNOWN = 0;
    public static final int PARKING_TYPE_ON_STREET = 1;
    public static final int PARKING_TYPE_OFF_STREET = 2;

    public static class ReservableProduct {
        private String productId;
        private String name;
        private String startTime;
        private String endTime;
        private int duration;
        private float totalAmount;
        private float serviceFee;
        private String displayText;

        public ReservableProduct(String productId) {
            this.productId = productId;
            name = "";
            startTime = "";
            endTime = "";
            duration = 0;
            totalAmount = 0;
            serviceFee = 0;
            displayText = "";
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }

        public float getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(float totalAmount) {
            this.totalAmount = totalAmount;
        }

        public float getServiceFee() {
            return serviceFee;
        }

        public void setServiceFee(float serviceFee) {
            this.serviceFee = serviceFee;
        }

        public String getDisplayText() {
            return displayText;
        }

        public void setDisplayText(String displayText) {
            this.displayText = displayText;
        }
    }

    private int parkingType;
    private String facilityId;
    private double totalCost;
    private double occupancyPct;
    private List<ReservableProduct> reservableProducts;
    private float distance;
    private String ratesInfo;
    private String operationHours;

    public ParkingInfo() {
        super();
        parkingType = PARKING_TYPE_ON_UNKNOWN;
        facilityId = "";
        totalCost = 0;
        occupancyPct = 0;
        distance = 0;
        ratesInfo = "";
        operationHours = "";
        reservableProducts = null;
    }

    public int getParkingType() {
        return parkingType;
    }

    public void setParkingType(int parkingType) {
        this.parkingType = parkingType;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public double getOccupancyPct() {
        return occupancyPct;
    }

    public void setOccupancyPct(double occupancyPct) {
        this.occupancyPct = occupancyPct;
    }

    public boolean isReservable() {
        return reservableProducts != null && !reservableProducts.isEmpty();
    }

    public List<ReservableProduct> getReservableProducts() {
        return reservableProducts;
    }

    public void addReservableProduct(ReservableProduct reservableProduct) {
        if (reservableProduct == null) {
            return;
        }
        if (reservableProducts == null) {
            reservableProducts = new ArrayList<>();
        }
        reservableProducts.add(reservableProduct);
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getRatesInfo() {
        return ratesInfo;
    }

    public void setRatesInfo(String ratesInfo) {
        this.ratesInfo = ratesInfo;
    }

    public String getOperationHours() {
        return operationHours;
    }

    public void setOperationHours(String operationHours) {
        this.operationHours = operationHours;
    }
}
