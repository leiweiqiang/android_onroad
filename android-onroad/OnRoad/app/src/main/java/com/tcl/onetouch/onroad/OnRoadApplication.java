package com.tcl.onetouch.onroad;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.tcl.onetouch.base.ActivityDetectionService;
import com.tcl.onetouch.base.OneTouchApplication;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OnRoadApplication extends OneTouchApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }
        ActivityDetectionService.registerActivityListener(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/GothamRounded-Medium.otf")
            .setFontAttrId(R.attr.fontPath)
            .build());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static OnRoadApplication getInstance() {
        return (OnRoadApplication) sInstance;
    }
}
