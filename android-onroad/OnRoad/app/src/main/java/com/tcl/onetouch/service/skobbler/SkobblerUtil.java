package com.tcl.onetouch.service.skobbler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.location.Location;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Surface;

import com.google.common.io.ByteStreams;
import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.SKDeveloperKeyException;
import com.skobbler.ngx.SKMaps;
import com.skobbler.ngx.SKMapsInitSettings;
import com.skobbler.ngx.map.SKMapSettings;
import com.skobbler.ngx.map.SKMapSurfaceView;
import com.skobbler.ngx.map.SKMapViewStyle;
import com.skobbler.ngx.navigation.SKAdvisorSettings;
import com.skobbler.ngx.util.SKGeoUtils;
import com.skobbler.ngx.util.SKLogging;
import com.skobbler.ngx.util.SKUtils;
import com.tcl.onetouch.onroad.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SkobblerUtil {

    /**
     * the number of km/h in 1 m/s
     */
    private static final double SPEED_IN_KILOMETRES = 3.6;

    /**
     * number of mi/h in 1 m/s
     */
    private static final double SPEED_IN_MILES = 2.2369;

    /**
     * the number of meters in a km
     */
    private static final int METERS_IN_KM = 1000;

    /**
     * the number of meters in a mile
     */
    private static final double METERS_IN_MILE = 1609.34;

    /**
     * converter from meters to feet
     */
    private static final double METERS_TO_FEET = 3.2808399;

    /**
     * converter from meters to yards
     */
    private static final double METERS_TO_YARDS = 1.0936133;

    /**
     * the number of yards in a mile
     */
    private static final int YARDS_IN_MILE = 1760;

    /**
     * the number of feet in a yard
     */
    private static final int FEET_IN_YARD = 3;

    /**
     * the number of feet in a mile
     */
    private static final int FEET_IN_MILE = 5280;

    /**
     * the limit of feet where the distance should be converted into miles
     */
    private static final int LIMIT_TO_MILES = 1500;

    /**
     * true if multiple map instances can be created
     */
    public static boolean isMultipleMapSupportEnabled;

    /**
     * Gets formatted time from a given number of seconds
     *
     * @param timeInSec
     * @return
     */
    public static String formatTime(int timeInSec) {
        StringBuilder builder = new StringBuilder();
        int hours = timeInSec / 3600;
        int minutes = (timeInSec - hours * 3600) / 60;
        int seconds = timeInSec - hours * 3600 - minutes * 60;
        builder.insert(0, seconds + "s");
        if (minutes > 0 || hours > 0) {
            builder.insert(0, minutes + "m ");
        }
        if (hours > 0) {
            builder.insert(0, hours + "h ");
        }
        return builder.toString();
    }

    /**
     * Formats a given distance value (given in meters)
     *
     * @param distInMeters
     * @return
     */
    public static String formatDistance(int distInMeters) {
        if (distInMeters < 1000) {
            return distInMeters + "m";
        } else {
            return ((float) distInMeters / 1000) + "km";
        }
    }

    /**
     * converts a distance given in meters to the according distance in yards
     *
     * @param distanceInMeters
     * @return
     */
    private static double distanceInYards(double distanceInMeters) {
        if (distanceInMeters != -1) {
            return distanceInMeters *= METERS_TO_YARDS;
        } else {
            return distanceInMeters;
        }
    }

    /**
     * converts a distance given in meters to the according distance in feet
     *
     * @param distanceInMeters
     * @return
     */
    private static double distanceInFeet(double distanceInMeters) {
        if (distanceInMeters != -1) {
            return distanceInMeters * METERS_TO_YARDS * FEET_IN_YARD;
        } else {
            return distanceInMeters;
        }
    }

    /**
     * Converts (to imperial units if necessary) and formats as string a
     * distance value given in meters.
     *
     * @param distanceValue distance value in meters
     * @param context       context object used to get the app preferences and the
     *                      distance unit labels
     * @return
     */
    public static String convertAndFormatDistance(double distanceValue, SKMaps.SKDistanceUnitType distanceUnitType,
                                                  Context context) {

        if (distanceUnitType != SKMaps.SKDistanceUnitType.DISTANCE_UNIT_KILOMETER_METERS) {
            // convert meters to feet or yards if needed
            if (distanceUnitType == SKMaps.SKDistanceUnitType.DISTANCE_UNIT_MILES_FEET) {
                distanceValue = (float) distanceInFeet(distanceValue);
            } else {
                distanceValue = (float) distanceInYards(distanceValue);
            }
        }

        String distanceValueText;

        if (distanceUnitType == SKMaps.SKDistanceUnitType.DISTANCE_UNIT_KILOMETER_METERS) {
            if (distanceValue >= METERS_IN_KM) {
                distanceValue /= METERS_IN_KM;
                if (distanceValue >= 10) {
                    // if distance is >= 10 km => display distance without any
                    // decimals
                    distanceValueText =
                        (Math.round(distanceValue) + " " + context.getResources().getString(R.string.km_label));
                } else {
                    // distance displayed in kilometers
                    distanceValueText =
                        (((float) Math.round(distanceValue * 10) / 10)) + " "
                            + context.getResources().getString(R.string.km_label);
                }
            } else {
                // distance displayed in meters
                distanceValueText =
                    ((int) distanceValue) + " " + context.getResources().getString(R.string.meters_label);
            }
        } else if (distanceUnitType == SKMaps.SKDistanceUnitType.DISTANCE_UNIT_MILES_FEET) {
            // if the distance in feet > 1500 => convert it in miles (FMA-2577)
            if (distanceValue >= LIMIT_TO_MILES) {
                distanceValue /= FEET_IN_MILE;
                if (distanceValue >= 10) {
                    // for routing if the distance is > 10 should be rounded to
                    // be an int; rounded distance displayed in miles
                    distanceValueText =
                        (Math.round(distanceValue) + " " + context.getResources().getString(R.string.mi_label));

                } else {
                    // distance displayed in miles
                    distanceValueText =
                        (((float) Math.round(distanceValue * 10) / 10)) + " "
                            + context.getResources().getString(R.string.mi_label);
                }
            } else {
                // distance displayed in feet
                distanceValueText =
                    ((int) distanceValue) + " " + context.getResources().getString(R.string.feet_label);
            }
        } else {
            if (distanceValue >= METERS_IN_KM) {
                distanceValue /= YARDS_IN_MILE;
                if (distanceValue >= 10) {
                    distanceValueText =
                        (Math.round(distanceValue) + " " + context.getResources().getString(R.string.mi_label));
                } else {
                    // distance displayed in miles
                    distanceValueText =
                        (((float) Math.round(distanceValue * 10) / 10)) + " "
                            + context.getResources().getString(R.string.mi_label);
                }
            } else {
                // distance displayed in yards
                distanceValueText =
                    ((int) distanceValue) + " " + context.getResources().getString(R.string.yards_label);
            }
        }

        if ((distanceValueText != null) && distanceValueText.startsWith("0 ")) {
            return "";
        }
        return distanceValueText;

    }

    /**
     * Copies files from assets to destination folder
     *
     * @param assetManager
     * @param sourceFolder
     * @throws IOException
     */
    public static void copyAssetsToFolder(AssetManager assetManager, String sourceFolder, String destinationFolder)
        throws IOException {
        final String[] assets = assetManager.list(sourceFolder);

        final File destFolderFile = new File(destinationFolder);
        if (!destFolderFile.exists()) {
            destFolderFile.mkdirs();
        }
        copyAsset(assetManager, sourceFolder, destinationFolder, assets);
    }

    /**
     * Copies files from assets to destination folder
     *
     * @param assetManager
     * @param sourceFolder
     * @param assetsNames
     * @throws IOException
     */
    public static void copyAsset(AssetManager assetManager, String sourceFolder, String destinationFolder,
                                 String... assetsNames) throws IOException {

        for (String assetName : assetsNames) {
            OutputStream destinationStream = new FileOutputStream(new File(destinationFolder + "/" + assetName));
            String[] files = assetManager.list(sourceFolder + "/" + assetName);
            if (files == null || files.length == 0) {

                InputStream asset = assetManager.open(sourceFolder + "/" + assetName);
                try {
                    ByteStreams.copy(asset, destinationStream);
                } finally {
                    asset.close();
                    destinationStream.close();
                }
            }
        }
    }

    /**
     * Initializes the SKMaps framework
     */
    public static boolean initializeLibrary(final Context context, final String mapResourcesPath) {
        SKLogging.enableLogs(true);

        // get object holding map initialization settings
        SKMapsInitSettings initMapSettings = new SKMapsInitSettings();
        // set path to map resources and initial map style
        initMapSettings.setMapResourcesPaths(mapResourcesPath,
            new SKMapViewStyle(mapResourcesPath + "daystyle/", "daystyle.json"));

        final SKAdvisorSettings advisorSettings = initMapSettings.getAdvisorSettings();
        advisorSettings.setAdvisorConfigPath(mapResourcesPath + "/Advisor");
        advisorSettings.setResourcePath(mapResourcesPath + "/Advisor/Languages");
        advisorSettings.setLanguage(SKAdvisorSettings.SKAdvisorLanguage.LANGUAGE_EN);
        advisorSettings.setAdvisorVoice("en");
        initMapSettings.setAdvisorSettings(advisorSettings);

        // EXAMPLE OF ADDING PREINSTALLED MAPS
//         initMapSettings.setPreinstalledMapsPath(((DemoApplication)context.getApplicationContext()).getMapResourcesDirPath()
//         + "/PreinstalledMaps");
        // initMapSettings.setConnectivityMode(SKMaps.CONNECTIVITY_MODE_OFFLINE);

        // Example of setting light maps
        // initMapSettings.setMapDetailLevel(SKMapsInitSettings.SK_MAP_DETAIL_LIGHT);
        // initialize map using the settings object

        try {
            SKMaps.getInstance().initializeSKMaps(context, initMapSettings);
            return true;
        } catch (SKDeveloperKeyException exception) {
            exception.printStackTrace();
            showApiKeyErrorDialog(context);
            return false;
        }
    }


    /**
     * Shows the api key not set dialog.
     */
    public static void showApiKeyErrorDialog(Context currentActivity) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
            currentActivity);

        alertDialog.setTitle("Error");
        alertDialog.setMessage("API_KEY not set");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(
            "ok",
            new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            });

        alertDialog.show();
    }

    public static int getExactScreenOrientation(Activity activity) {
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        int rotation = defaultDisplay.getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        defaultDisplay.getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int orientation;
        // if the device's natural orientation is portrait:
        if ((rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) && height > width || (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) &&
            width > height) {
            switch (rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_180:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_270:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                default:
                    // Logging.writeLog(TAG, "Unknown screen orientation. Defaulting to " + "portrait.", Logging.LOG_DEBUG);
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
            }
        }
        // if the device's natural orientation is landscape or if the device
        // is square:
        else {
            switch (rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case Surface.ROTATION_270:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                default:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
            }
        }

        return orientation;
    }

    /**
     * Deletes all files and directories from <>file</> except PreinstalledMaps
     *
     * @param file
     */
    public static void deleteFileOrDirectory(File file) {
        if (file.isDirectory()) {
            String[] children = file.list();
            for (int i = 0; i < children.length; i++) {
                if (new File(file, children[i]).isDirectory() && !children[i].equals("PreinstalledMaps") && !children[i].equals("Maps")) {
                    deleteFileOrDirectory(new File(file, children[i]));
                } else {
                    new File(file, children[i]).delete();
                }
            }
        } else if (file.exists()) {
            file.delete();
        }
    }

    public static void applySettingsOnMapView(SKMapSurfaceView mapView) {
        mapView.getMapSettings().setMapRotationEnabled(true);
        mapView.getMapSettings().setMapZoomingEnabled(true);
        mapView.getMapSettings().setMapPanningEnabled(true);
        mapView.getMapSettings().setZoomWithAnchorEnabled(true);
        mapView.getMapSettings().setInertiaRotatingEnabled(true);
        mapView.getMapSettings().setInertiaZoomingEnabled(true);
        mapView.getMapSettings().setInertiaPanningEnabled(true);
        mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.POSITION);
        mapView.getMapSettings().setTrafficMode(SKMapSettings.SKTrafficMode.FLOW_AND_INCIDENTS);
    }

    public static SKCoordinate convertFrom(Location pos) {
        return new SKCoordinate(pos.getLongitude(), pos.getLatitude());
    }

    /**
     * Get the display size in inches.
     *
     * @return the value in inches
     */
    public static double getDisplaySizeInches(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();

        double x = Math.pow((double) dm.widthPixels / (double) dm.densityDpi, 2);
        double y = Math.pow((double) dm.heightPixels / (double) dm.densityDpi, 2);

        return Math.sqrt(x + y);
    }
}