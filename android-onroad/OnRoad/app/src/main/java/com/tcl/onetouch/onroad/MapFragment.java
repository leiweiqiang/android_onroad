package com.tcl.onetouch.onroad;

import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.automatic.android.sdk.Automatic;
import com.automatic.android.sdk.AutomaticLoginCallbacks;
import com.automatic.android.sdk.LoginButton;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.MapCoordinate;
import com.tcl.onetouch.model.NavigationInfo;
import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.service.MapService;
import com.tcl.onetouch.service.MusicService;
import com.tcl.onetouch.service.NavigationService;
import com.tcl.onetouch.service.spotify.SpotifyMusicService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.view.CutoutView;
import com.tcl.onetouch.view.GestureDetectorLayout;

public class MapFragment extends Fragment implements Animation.AnimationListener, View.OnClickListener {
    private static final String LEFT_PADDING_KEY = "left_padding";

    private View mapView;
    private GestureDetectorLayout mapViewHolder;
    private CutoutView cutoutView;
    private View controlPanel;
    private View compassButton;
    private View centerMeButton;

    private int leftPadding = 0;
    private View hiddenPanel;
    private View settingsPanel;
    private Animation slideOutFromTopAnim;
    private Animation slideInToTopAnim;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        LogUtil.d("MapFragment onCreateView");
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        hiddenPanel = view.findViewById(R.id.hidden_top_header);
        ((GestureDetectorLayout) hiddenPanel).setOnSwipeListener(new GestureDetectorLayout.OnSwipeListener() {
            @Override
            public void onSwipeDown() {
                settingsPanel.startAnimation(slideOutFromTopAnim);
            }
        });

        mapViewHolder = (GestureDetectorLayout) view.findViewById(R.id.map_view_holder);
        mapView = MapService.getDefaultService().createMapView(getActivity());
        mapViewHolder.addView(mapView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mapViewHolder.setOnTapListener(new GestureDetectorLayout.OnTapListener() {
            @Override
            public void onTap(float x, float y) {
                MainActivity.getInstance().onMapViewTapped();
            }
        }, false);

        settingsPanel = view.findViewById(R.id.settings_panel);
        ((GestureDetectorLayout) settingsPanel).setOnSwipeListener(new GestureDetectorLayout.OnSwipeListener() {
            @Override
            public void onSwipeUp() {
                settingsPanel.startAnimation(slideInToTopAnim);
            }
        });
        LoginButton automaticLoginButton = (LoginButton) settingsPanel.findViewById(R.id.automatic_login_button);
        settingsPanel.findViewById(R.id.simulation_drive_check).setOnClickListener(this);
        automaticLoginButton.setOnLoginResultCallback(new AutomaticLoginCallbacks() {
            @Override
            public void onLoginSuccess() {
                Toast.makeText(getActivity(), "Automatic Login Successful", Toast.LENGTH_SHORT).show();
                settingsPanel.startAnimation(slideInToTopAnim);
            }

            @Override
            public void onLoginFailure(RuntimeException error) {
                Toast.makeText(getActivity(), "Automatic Login Failed", Toast.LENGTH_SHORT).show();
                settingsPanel.startAnimation(slideInToTopAnim);
            }
        });
        Automatic.get().addLoginButton(automaticLoginButton, getActivity());

        slideOutFromTopAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.window_slide_out_from_top);
        slideInToTopAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.window_slide_in_to_top);

        slideOutFromTopAnim.setAnimationListener(this);
        slideInToTopAnim.setAnimationListener(this);

        cutoutView = (CutoutView) view.findViewById(R.id.cutout_view);

        controlPanel = view.findViewById(R.id.control_panel);
        compassButton = view.findViewById(R.id.compass_button);
        compassButton.setOnClickListener(this);
        centerMeButton = view.findViewById(R.id.center_me_button);
        centerMeButton.setOnClickListener(this);

        if (savedInstanceState != null) {
            int padding = savedInstanceState.getInt(LEFT_PADDING_KEY, 0);
            if (padding != 0) {
                leftPadding = padding;
            }
        }

        controlPanel.setPadding(leftPadding, controlPanel.getPaddingTop(),
            controlPanel.getPaddingRight(), controlPanel.getPaddingBottom());

        settingsPanel.setVisibility(View.INVISIBLE);

        return view;
    }

    public View getMapView() {
        return mapView;
    }

    public GestureDetectorLayout getMapViewHolder() {
        return mapViewHolder;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.center_me_button:
                NavigationInfo navigationInfo = NavigationService.getDefaultService().getCurrentNavigationInfo();
                if (navigationInfo != null && navigationInfo.isNavigating()) {
                    MapService.getDefaultService().centerMe(
                        MapService.FOLLOWING_MODE_POSITION_AND_HEADING, 0,
                        leftPadding / 2,
                        getResources().getDimensionPixelSize(R.dimen.map_navigation_offset));
                } else {
                    MapService.getDefaultService().centerMe(
                        MapService.FOLLOWING_MODE_POSITION_AND_HEADING_NORTH, 0, leftPadding / 2, 0);
                }
                break;
            case R.id.automatic_login_button:
                settingsPanel.startAnimation(slideInToTopAnim);
                break;
            case R.id.simulation_drive_check:
                if (((CheckBox) v).isChecked()) {
                    OnRoadApplication.getInstance().savePreference(
                            NavigationService.PREF_KEY_NAVIGATION_TYPE, NavigationInfo.NAVIGATION_TYPE_SIMULATION);
                } else {
                    OnRoadApplication.getInstance().savePreference(
                            NavigationService.PREF_KEY_NAVIGATION_TYPE, NavigationInfo.NAVIGATION_TYPE_REAL);
                }
                settingsPanel.startAnimation(slideInToTopAnim);
                break;
        }
    }

    @Override
    public void onPause() {
        MapService.getDefaultService().pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        MapService.getDefaultService().resume();
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (leftPadding != 0) {
            outState.putInt(LEFT_PADDING_KEY, leftPadding);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapService.getDefaultService().releaseMapView();
        mapViewHolder = null;
        mapView = null;

        settingsPanel = null;
        slideOutFromTopAnim = null;
        slideInToTopAnim = null;
    }

    public void showCutout(int x, int y, int width, int height) {
        int[] loc = new int[2];
        cutoutView.getLocationInWindow(loc);
        cutoutView.setWindow(x - loc[0], y - loc[1], width, height);
        cutoutView.setVisibility(View.VISIBLE);
    }

    public void hideCutout() {
        cutoutView.setVisibility(View.INVISIBLE);
    }

    public void setLeftPadding(int padding) {
        leftPadding = padding;
        if (controlPanel == null) {
            return;
        }
        controlPanel.setPadding(leftPadding, controlPanel.getPaddingTop(),
            controlPanel.getPaddingRight(), controlPanel.getPaddingBottom());
    }

    public void addDestinationOptionAnnotation(PlaceInfo place, int id, int index, int bgResId, View.OnClickListener onClickListener) {
        MapService.getDefaultService().deleteAnnotation(id);
        View annotationView =
            ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.annotation_destination_opt, null, false);
        TextView titleText = (TextView) annotationView.findViewById(android.R.id.title);
        titleText.setBackgroundResource(bgResId);
        if (index >= 0) {
            titleText.setText(String.valueOf(index + 1));
        } else {
            titleText.setText("");
        }
        annotationView.setTag(place);
        annotationView.setOnClickListener(onClickListener);
        Resources rs = getActivity().getResources();
        float offsetY = rs.getDimension(R.dimen.map_annotation_destination_height) / 2.0f;
        MapService.getDefaultService().addAnnotation(
            id,
            new MapCoordinate(place.getLocation().getLatitude(), place.getLocation().getLongitude()),
            annotationView, 0, offsetY);
    }

    public void calculateScale(double southLat, double westLng, double northLat, double eastLng,
                               int viewWidth, int viewHeight, double[] scales) {
        double viewRatio = (double) viewWidth / (double) viewHeight;
        double centerLat = (southLat + northLat) / 2.0d, centerLng = (westLng + eastLng) / 2.0d;
        float[] rs = new float[1];
        Location.distanceBetween(centerLat, westLng, centerLat, eastLng, rs);
        float boundingWidth = rs[0];
        Location.distanceBetween(southLat, centerLng, northLat, centerLng, rs);
        float boundingHeight = rs[0];
        double boundingRatio = boundingWidth / boundingHeight;
        if (boundingRatio > viewRatio) {
            scales[1] = (eastLng - westLng) / viewWidth;
            scales[0] = (northLat - southLat) / (viewWidth / boundingRatio);
        } else {
            scales[1] = (eastLng - westLng) / (viewHeight * boundingRatio);
            scales[0] = (northLat - southLat) / viewHeight;
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == slideOutFromTopAnim) {
            Button spotifyLoginButton = (Button) settingsPanel.findViewById(R.id.spotify_login_button);
            if ( OneTouchApplication.getInstance().getBooleanPreference(SpotifyMusicService.KEY_SPOTIFY_LOGIN_STATUS, false)) {
                spotifyLoginButton.setText("Spotify Logout");
                spotifyLoginButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MusicService.getDefaultService().logout();
                    }
                });
            } else {
                spotifyLoginButton.setText("Spotify Login");
                spotifyLoginButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MusicService.getDefaultService().login(getActivity());
                    }
                });
            }
            ((CheckBox) settingsPanel.findViewById(R.id.simulation_drive_check)).setChecked(
                    OnRoadApplication.getInstance().getIntPreference(
                            NavigationService.PREF_KEY_NAVIGATION_TYPE, NavigationInfo.NAVIGATION_TYPE_SIMULATION) ==
                            NavigationInfo.NAVIGATION_TYPE_SIMULATION);
            settingsPanel.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == slideInToTopAnim) {
            settingsPanel.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public void setHiddenPanelEnabled(boolean enabled) {
        if (enabled) {
            hiddenPanel.setVisibility(View.VISIBLE);
        } else {
            hiddenPanel.setVisibility(View.GONE);
        }
    }
}
