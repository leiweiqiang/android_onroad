package com.tcl.onetouch.service.onetouch;

import android.content.Context;
import android.location.Location;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tcl.onetouch.BuildConfig;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.AddressInfo;
import com.tcl.onetouch.model.ParkingInfo;
import com.tcl.onetouch.model.ParkingReservationInfo;
import com.tcl.onetouch.service.ParkingService;
import com.tcl.onetouch.service.UserService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.HttpUtil;
import com.tcl.onetouch.util.LogUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class OneTouchParkingService extends ParkingService {
    private static final String OFF_STREET_URL = "http://" + BuildConfig.SERVICE_HOST + "/thridPartAPIServer/ParkMe/Lots?";
    private static final String ON_STREET_URL = "http://" + BuildConfig.SERVICE_HOST + "/thridPartAPIServer/ParkMe/Meters?";
    private static final String RESERVATION_URL = "http://" + BuildConfig.SERVICE_HOST + "/parkService/ParkMe/reservation?";
    private static final String TEST_URL = "http://" + BuildConfig.SERVICE_HOST + "/parkService/ParkMe/test?";
    private static final String QUERY_URL = "http://" + BuildConfig.SERVICE_HOST + "/parkService/ParkMe/unitedPark/search?";
    private static final int QUERY_RADIUS = 2500; //in meters
    private static final int MAX_RESULT = 20;

    private static final String TEST_RESERVATION_ID = "PM3C5C9A";
    private static final String RESERVATION_FILE_NAME = "parking_reservations";

    private static final String ERROR_CODE_KEY = "errorCode";
    private static final String ERROR_MESSAGE_KEY = "errorMessage";

    private SimpleDateFormat testDateFormat;
    private SimpleDateFormat queryDateFormat;
    private SimpleDateFormat reservationDateFormat;
    private Gson gson;
    private Collection<ParkingReservationInfo> reservations;

    @Override
    public void init(Context context) {
        testDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        queryDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm z");
        reservationDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        gson = new Gson();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.openFileInput(RESERVATION_FILE_NAME)));
            Type jsonType = new TypeToken<Collection<ParkingReservationInfo>>() {
            }.getType();
            reservations = gson.fromJson(reader, jsonType);
            reader.close();
        } catch (FileNotFoundException e) {
            LogUtil.d(RESERVATION_FILE_NAME + " not found");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reservations == null) {
                reservations = new ArrayList<>();
            }
        }
    }

    private Map<String, String> createQueryData(Location location, Date arrivalTime, int durationInMins) {
        Map<String, String> queryData = new HashMap<>();
        String geo = location.getLongitude() + "|" + location.getLatitude() + "|" + QUERY_RADIUS;
        queryData.put("pt", geo);
        queryData.put("det", "1");
        queryData.put("max", String.valueOf(MAX_RESULT));
        queryData.put("entry_time", queryDateFormat.format(arrivalTime));
        queryData.put("duration", "" + durationInMins);
        return queryData;
    }

    @Override
    public void findOffStreetParking(Location location, final Date arrivalTime, final int durationInMins, final Listener listener) {
        if (listener == null) {
            // TODO: throw error
            return;
        }
        HttpUtil.doGet(OFF_STREET_URL,
            createQueryData(location, arrivalTime, durationInMins),
            new HttpUtil.RequestListener() {
                @Override
                public void onResult(HttpUtil.HttpRequestResult result) {
                    if (result.error == null) {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        Collection<Object> results = DataUtil.getCollection(data, "Facilities");
                        Iterator<Object> it = results.iterator();
                        List<ParkingInfo> lotsData = new ArrayList(results.size());
                        while (it.hasNext()) {
                            Map<String, Object> lot = (Map<String, Object>) it.next();
                            ParkingInfo info = new ParkingInfo();
                            info.setName(DataUtil.getString(lot, "name"));
                            info.setParkingType(ParkingInfo.PARKING_TYPE_OFF_STREET);
                            info.setFacilityId(String.valueOf(DataUtil.getInt(lot, "f_id")));
                            AddressInfo addressInfo = new AddressInfo();
                            addressInfo.setFormattedAddress(DataUtil.getString(lot, "address"));
                            info.setAddressInfo(addressInfo);
                            Collection<Object> point = DataUtil.getCollection(lot, "point");
                            Iterator<Object> pit = point.iterator();
                            Location location = new Location("");
                            try {
                                location.setLongitude(pit.hasNext() ? Double.parseDouble(pit.next().toString()) : 0D);
                                location.setLatitude(pit.hasNext() ? Double.parseDouble(pit.next().toString()) : 0D);
                            } catch (NumberFormatException e) {
                            }
                            info.setLocation(location);
                            info.setTotalCost(DataUtil.getDouble(lot, "amount"));
                            info.setOccupancyPct(DataUtil.getInt(lot, "occupancy_pct"));
                            info.setDistance((float) DataUtil.getDouble(lot, "distance"));
                            Collection<Object> rates = DataUtil.getCollection(lot, "rates");
                            if (rates != null) {
                                StringBuilder sb = new StringBuilder();
                                for (Object obj : rates) {
                                    if (sb.length() > 0) {
                                        sb.append("\n");
                                    }
                                    sb.append(obj.toString());
                                }
                                info.setRatesInfo(sb.toString());
                            }
                            Collection<Object> hrs = DataUtil.getCollection(lot, "hrs");
                            if (hrs != null) {
                                StringBuilder sb = new StringBuilder();
                                for (Object obj : hrs) {
                                    if (sb.length() > 0) {
                                        sb.append("\n");
                                    }
                                    sb.append(obj.toString());
                                }
                                info.setOperationHours(sb.toString());
                            }
                            lotsData.add(info);
                        }
                        listener.onResult(lotsData);
                    } else {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        if (data != null) {
                            int errorCode = DataUtil.getInt(data, ERROR_CODE_KEY);
                            String errorMessage = DataUtil.getString(data, ERROR_MESSAGE_KEY);
                            if (errorCode != 0 && !errorMessage.isEmpty()) {
                                result.error.setMessage(errorCode + ": " + errorMessage);
                            }
                        }
                        listener.onError(amendErrorCode(result.error));
                    }
                }
            }
        );
    }

    @Override
    public void findOnStreetParking(final Location location, Date arrivalTime, int durationInMins, final Listener listener) {
        if (listener == null) {
            // TODO: throw error
            return;
        }
        HttpUtil.doGet(ON_STREET_URL,
            createQueryData(location, arrivalTime, durationInMins),
            new HttpUtil.RequestListener() {
                @Override
                public void onResult(HttpUtil.HttpRequestResult result) {
                    if (result.error == null) {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        Collection<Object> results = DataUtil.getCollection(data, "Meters");
                        Iterator<Object> it = results.iterator();
                        List<ParkingInfo> lotsData = new ArrayList(results.size());
                        while (it.hasNext()) {
                            Map<String, Object> lot = (Map<String, Object>) it.next();
                            ParkingInfo info = new ParkingInfo();
                            info.setParkingType(ParkingInfo.PARKING_TYPE_ON_STREET);
                            info.setFacilityId(DataUtil.getString(lot, "m_id"));
                            info.setName("On Street Parking");
                            AddressInfo addressInfo = new AddressInfo();
                            addressInfo.setFormattedAddress(DataUtil.getString(lot, "address"));
                            info.setAddressInfo(addressInfo);
                            Collection<Object> point = DataUtil.getCollection(lot, "point");
                            Iterator<Object> pit = point.iterator();
                            Location lotLocation = new Location("");
                            try {
                                lotLocation.setLongitude(pit.hasNext() ? Double.parseDouble(pit.next().toString()) : 0D);
                                lotLocation.setLatitude(pit.hasNext() ? Double.parseDouble(pit.next().toString()) : 0D);
                            } catch (NumberFormatException e) {
                            }
                            info.setLocation(lotLocation);
                            info.addType("parking");
                            float[] distance = new float[1];
                            Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                                lotLocation.getLatitude(), lotLocation.getLongitude(), distance);
                            info.setDistance(distance[0]);
                            info.setOccupancyPct(DataUtil.getInt(lot, "occupancy_pct"));
                            lotsData.add(info);
                        }
                        listener.onResult(lotsData);
                    } else {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        if (data != null) {
                            int errorCode = DataUtil.getInt(data, ERROR_CODE_KEY);
                            String errorMessage = DataUtil.getString(data, ERROR_MESSAGE_KEY);
                            if (errorCode != 0 && !errorMessage.isEmpty()) {
                                result.error.setMessage(errorCode + ": " + errorMessage);
                            }
                        }
                        listener.onError(amendErrorCode(result.error));
                    }
                }
            }
        );
    }

    @Override
    public Collection<ParkingReservationInfo> getReservations() {
        return reservations;
    }

    @Override
    public void reserveParking(final ParkingInfo parkingInfo, final Date arrivalTime, final int durationInMins, final Listener listener) {
        if (listener == null) {
            // TODO: throw error
            return;
        }
        Map<String, String> queryData = new HashMap<>();
        queryData.put("userId", UserService.getInstance().getUserId());
        queryData.put("f_id", parkingInfo.getFacilityId());
        queryData.put("entry_time", testDateFormat.format(arrivalTime));
        queryData.put("duration", "" + durationInMins);
        HttpUtil.doPost(RESERVATION_URL, null, queryData, new byte[0], new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                if (result.error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    ParkingReservationInfo reservationInfo = new ParkingReservationInfo();
                    reservationInfo.setFacilityId(parkingInfo.getFacilityId());
                    reservationInfo.setFacilityName(parkingInfo.getName());
                    reservationInfo.setFacilityAddress(parkingInfo.getAddressInfo().getFormattedAddress());
                    String reservationId = DataUtil.getString(data, "pk_reservation");
                    if (!TEST_RESERVATION_ID.equalsIgnoreCase(reservationId)) {
                        String startTime = DataUtil.getString(data, "dt_start_time");
                        try {
                            reservationInfo.setStartTime(reservationDateFormat.parse(startTime).getTime());
                        } catch (ParseException e) {
                            LogUtil.d("reserveParking invalid start time: " + startTime);
                        }
                        String endTime = DataUtil.getString(data, "dt_end_time");
                        try {
                            reservationInfo.setEndTime(reservationDateFormat.parse(endTime).getTime());
                        } catch (ParseException e) {
                            LogUtil.d("reserveParking invalid end time: " + endTime);
                        }
                    } else {
                        reservationInfo.setStartTime(arrivalTime.getTime());
                        reservationInfo.setEndTime(arrivalTime.getTime() + durationInMins * 60000);
                    }
                    reservationInfo.setUrl(DataUtil.getString(data, "str_reservation_url"));
                    reservationInfo.setReservationId(DataUtil.getString(data, "pk_reservation"));
                    reservationInfo.setCost(DataUtil.getFloat(data, "d_amount_paid"));
                    reservations.add(reservationInfo);
                    try {
                        BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(OneTouchApplication.getInstance().openFileOutput(RESERVATION_FILE_NAME, Context.MODE_PRIVATE)));
                        Type jsonType = new TypeToken<Collection<ParkingReservationInfo>>() {
                        }.getType();
                        gson.toJson(reservations, jsonType, writer);
                        writer.flush();
                        writer.close();
                    } catch (FileNotFoundException e) {
                        LogUtil.d("write to " + RESERVATION_FILE_NAME + " failed.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    listener.onResult(reservationInfo);
                } else {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    if (data != null) {
                        int errorCode = DataUtil.getInt(data, ERROR_CODE_KEY);
                        String errorMessage = DataUtil.getString(data, ERROR_MESSAGE_KEY);
                        if (errorCode != 0 && !errorMessage.isEmpty()) {
                            result.error.setMessage(errorCode + ": " + errorMessage);
                        }
                    }
                    listener.onError(amendErrorCode(result.error));
                }
            }
        });
    }

    @Override
    public void checkParkingAvailability(Location location, Date arrivalTime, final Listener listener) {
        Map<String, String> queryData = new HashMap<>();
        queryData.put("userId", UserService.getInstance().getUserId());
        String geo = location.getLongitude() + "|" + location.getLatitude() + "|" + QUERY_RADIUS;
        queryData.put("pt", geo);
        queryData.put("entry_time", testDateFormat.format(arrivalTime));
        queryData.put("duration", "60");
        HttpUtil.doGet(
            TEST_URL,
            queryData,
            new HttpUtil.RequestListener() {
                @Override
                public void onResult(HttpUtil.HttpRequestResult result) {
                    if (result.error == null) {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        listener.onResult(data);
                    } else {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        if (data != null) {
                            int errorCode = DataUtil.getInt(data, ERROR_CODE_KEY);
                            String errorMessage = DataUtil.getString(data, ERROR_MESSAGE_KEY);
                            result.error.setMessage(errorCode + ": " + errorMessage);
                        }
                        listener.onError(amendErrorCode(result.error));
                    }
                }
            }
        );
    }

    @Override
    public void findParking(final Location location, Date arrivalTime, int durationInMins, final Listener listener) {
        Map<String, String> queryData = new HashMap<>();
        queryData.put("userId", UserService.getInstance().getUserId());
        String geo = location.getLongitude() + "|" + location.getLatitude() + "|" + QUERY_RADIUS;
        queryData.put("pt", geo);
        queryData.put("max", String.valueOf(MAX_RESULT));
        queryData.put("entry_time", queryDateFormat.format(arrivalTime));
        queryData.put("duration", "" + durationInMins);
        HttpUtil.doGet(
            QUERY_URL,
            queryData,
            new HttpUtil.RequestListener() {
                @Override
                public void onResult(HttpUtil.HttpRequestResult result) {
                    if (result.error == null) {
                        Map<String, Object> results = new HashMap<String, Object>();
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        Collection<Object> offStreetResults = DataUtil.getCollection(data, "OffStreet");
                        List<ParkingInfo> offStreetData = new ArrayList(offStreetResults.size());
                        Map<String, ParkingInfo> offStreetParkings = new HashMap<String, ParkingInfo>();
                        if (offStreetResults != null && !offStreetResults.isEmpty()) {
                            Iterator<Object> it = offStreetResults.iterator();
                            while (it.hasNext()) {
                                Map<String, Object> lot = (Map<String, Object>) it.next();
                                ParkingInfo info = new ParkingInfo();
                                info.setName(DataUtil.getString(lot, "name"));
                                info.setParkingType(ParkingInfo.PARKING_TYPE_OFF_STREET);
                                info.setFacilityId(String.valueOf(DataUtil.getInt(lot, "f_id")));
                                AddressInfo addressInfo = new AddressInfo();
                                addressInfo.setFormattedAddress(DataUtil.getString(lot, "address"));
                                info.setAddressInfo(addressInfo);
                                Collection<Object> point = DataUtil.getCollection(lot, "point");
                                Iterator<Object> pit = point.iterator();
                                Location loc = new Location("");
                                try {
                                    loc.setLongitude(pit.hasNext() ? Double.parseDouble(pit.next().toString()) : 0D);
                                    loc.setLatitude(pit.hasNext() ? Double.parseDouble(pit.next().toString()) : 0D);
                                } catch (NumberFormatException e) {
                                }
                                info.setLocation(loc);
                                info.setTotalCost(DataUtil.getDouble(lot, "amount"));
                                info.setOccupancyPct(DataUtil.getInt(lot, "occupancy_pct"));
                                info.setDistance((float) DataUtil.getDouble(lot, "distance"));
                                Collection<Object> rates = DataUtil.getCollection(lot, "rates");
                                if (rates != null) {
                                    StringBuilder sb = new StringBuilder();
                                    for (Object obj : rates) {
                                        if (sb.length() > 0) {
                                            sb.append("\n");
                                        }
                                        sb.append(obj.toString());
                                    }
                                    info.setRatesInfo(sb.toString());
                                }
                                Collection<Object> hrs = DataUtil.getCollection(lot, "hrs");
                                if (hrs != null) {
                                    StringBuilder sb = new StringBuilder();
                                    for (Object obj : hrs) {
                                        if (sb.length() > 0) {
                                            sb.append("\n");
                                        }
                                        sb.append(obj.toString());
                                    }
                                    info.setOperationHours(sb.toString());
                                }
                                offStreetData.add(info);
                                offStreetParkings.put(info.getFacilityId(), info);
                            }
                        }
                        results.put("OffStreet", offStreetData);
                        Collection<Object> onStreetResults = DataUtil.getCollection(data, "OnStreet");
                        List<ParkingInfo> onStreetData = new ArrayList(results.size());
                        if (onStreetResults != null && !onStreetResults.isEmpty()) {
                            Iterator<Object> it = onStreetResults.iterator();
                            while (it.hasNext()) {
                                Map<String, Object> lot = (Map<String, Object>) it.next();
                                ParkingInfo info = new ParkingInfo();
                                info.setParkingType(ParkingInfo.PARKING_TYPE_ON_STREET);
                                info.setFacilityId(DataUtil.getString(lot, "m_id"));
                                info.setName("On Street Parking");
                                AddressInfo addressInfo = new AddressInfo();
                                addressInfo.setFormattedAddress(DataUtil.getString(lot, "address"));
                                info.setAddressInfo(addressInfo);
                                Collection<Object> point = DataUtil.getCollection(lot, "point");
                                Iterator<Object> pit = point.iterator();
                                Location loc = new Location("");
                                try {
                                    loc.setLongitude(pit.hasNext() ? Double.parseDouble(pit.next().toString()) : 0D);
                                    loc.setLatitude(pit.hasNext() ? Double.parseDouble(pit.next().toString()) : 0D);
                                } catch (NumberFormatException e) {
                                }
                                info.setLocation(loc);
                                info.addType("parking");
                                float[] distance = new float[1];
                                Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                                    loc.getLatitude(), loc.getLongitude(), distance);
                                info.setDistance(distance[0]);
                                info.setOccupancyPct(DataUtil.getInt(lot, "occupancy_pct"));
                                onStreetData.add(info);
                            }
                        }
                        results.put("OnStreet", onStreetData);
                        Collection<Object> reservableResults = DataUtil.getCollection(data, "Reservable");
                        if (reservableResults != null && !reservableResults.isEmpty()) {
                            Iterator<Object> it = reservableResults.iterator();
                            while (it.hasNext()) {
                                Map<String, Object> reservable = (Map<String, Object>) it.next();
                                ParkingInfo info = offStreetParkings.get(String.valueOf(DataUtil.getInt(reservable, "f_id")));
                                if (info == null) {
                                    continue;
                                }
                                ParkingInfo.ReservableProduct reservableProduct = new ParkingInfo.ReservableProduct(
                                    DataUtil.getString(reservable, "pk_reservation_product"));
                                reservableProduct.setName(DataUtil.getString(reservable, "name"));
                                reservableProduct.setStartTime(DataUtil.getString(reservable, "start_time"));
                                reservableProduct.setEndTime(DataUtil.getString(reservable, "end_time"));
                                reservableProduct.setDuration(DataUtil.getInt(reservable, "max_duration"));
                                reservableProduct.setTotalAmount(DataUtil.getFloat(reservable, "amount"));
                                reservableProduct.setServiceFee(DataUtil.getFloat(reservable, "service_fee"));
                                reservableProduct.setDisplayText(DataUtil.getString(reservable, "promo_text"));
                                info.addReservableProduct(reservableProduct);
                            }
                        }
                        listener.onResult(results);
                    } else {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        if (data != null) {
                            int errorCode = DataUtil.getInt(data, ERROR_CODE_KEY);
                            String errorMessage = DataUtil.getString(data, ERROR_MESSAGE_KEY);
                            if (errorCode != 0 && !errorMessage.isEmpty()) {
                                result.error.setMessage(errorCode + ": " + errorMessage);
                            }
                        }
                        listener.onError(amendErrorCode(result.error));
                    }
                }
            }
        );
    }
}
