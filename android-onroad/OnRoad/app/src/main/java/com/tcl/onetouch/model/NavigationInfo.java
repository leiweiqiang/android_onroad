package com.tcl.onetouch.model;

import android.location.Location;

import java.util.Date;
import java.util.List;

public class NavigationInfo {
    public static final int ROUTE_TYPE_CAR_SHORTEST = 1;
    public static final int ROUTE_TYPE_CAR_FASTEST = 2;
    public static final int ROUTE_TYPE_PEDESTRIAN = 3;

    public static final int NAVIGATION_TYPE_REAL = 1;
    public static final int NAVIGATION_TYPE_SIMULATION = 2;

    public static final int DIRECTION_INVALID = -1;
    public static final int DIRECTION_STRAIGHT_AHEAD = 0;
    public static final int DIRECTION_SLIGHT_RIGHT = 1;
    public static final int DIRECTION_SLIGHT_LEFT = 2;
    public static final int DIRECTION_LEFT = 3;
    public static final int DIRECTION_RIGHT = 4;
    public static final int DIRECTION_HARD_RIGHT = 5;
    public static final int DIRECTION_HARD_LEFT = 6;
    public static final int DIRECTION_U_TURN = 7;
    public static final int DIRECTION_T_STREET = 8;
    public static final int DIRECTION_BIFURCATION = 9;
    public static final int DIRECTION_IGNORE_ANGLE = 10;
    public static final int DIRECTION_ROUNDABOUT = 11;


    public static class Advice {
        public double distance;
        public double time;
        public int delay;
        public String exitNumber;
        public int direction;
        public String streetName;
        public int nextDirection;
        public String nextStreetName;
        public String advice;
        public String instruction;
    }

    /**
     * The start coordinate of the route
     */
    private Location startCoordinate;

    /**
     * The original destination
     */
    private PlaceInfo destination;
    /**
     * The destination of the route
     */
    private PlaceInfo navigationDestination;
    /**
     * The route mode. Default is Simulation.
     */
    private int routeType;

    /**
     * Indicates whether to avoid toll roads when calculating the route.
     */
    private boolean tollRoadsAvoided;

    /**
     * Indicates whether to avoid highways when calculating the route.
     */
    private boolean highWaysAvoided;

    /**
     * Indicates whether to avoid ferries when calculating the route
     */
    private boolean ferriesAvoided;

    private int travelTimeInSec;
    private int travelDistanceInMeter;
    private int travelDelayInSec;

    private int navigationType;

    private int distanceToDestinationInMeter;
    private int timeToDestinationInSec;
    private String routeSummary;

    private String currentStreetName;
    private Advice currentAdvice;
    private Advice nextAdvice;

    private boolean isRouting;
    private boolean isRouteReady;
    private boolean isReRouting;
    private boolean isNavigating;
    private boolean isDestinationReached;

    private List<MapCoordinate> route;

    private ParkingReservationInfo parkingReservationInfo;
    private boolean isParkingAvailable;

    public NavigationInfo() {
        routeType = ROUTE_TYPE_CAR_FASTEST;
        tollRoadsAvoided = false;
        highWaysAvoided = false;
        ferriesAvoided = false;

        travelTimeInSec = 0;
        travelDistanceInMeter = 0;
        travelDelayInSec = 0;
        navigationType = NAVIGATION_TYPE_REAL;

        distanceToDestinationInMeter = 0;
        timeToDestinationInSec = 0;
        routeSummary = "";

        currentStreetName = "";
        currentAdvice = new Advice();
        nextAdvice = new Advice();

        isRouting = false;
        isReRouting = false;
        isNavigating = false;

        route = null;

        parkingReservationInfo = null;
        isParkingAvailable = false;
    }

    /**
     * Sets the route mode used for route calculation.
     *
     * @param routeType
     */
    public void setRouteType(int routeType) {
        this.routeType = routeType;
    }

    /**
     * @return the route mode used for route calculation.
     */
    public int getRouteType() {
        return routeType;
    }

    /**
     * @return the start coordinate of the route.
     */
    public Location getStartCoordinate() {
        return startCoordinate;
    }

    /**
     * Sets the start coordinate of the route
     *
     * @param startCoordinate
     */
    public void setStartCoordinate(Location startCoordinate) {
        this.startCoordinate = startCoordinate;
    }

    public void setDestination(PlaceInfo address) {
        destination = address;
    }

    public PlaceInfo getDestination() {
        return destination;
    }

    /**
     * @return Returns a boolean that indicates whether to avoid ferries when
     * calculating the route.
     */
    public boolean isTollRoadsAvoided() {
        return tollRoadsAvoided;
    }

    /**
     * Sets a boolean that indicates whether to avoid ferries when calculating
     * the route.
     *
     * @param tollRoadsAvoided
     */
    public void setTollRoadsAvoided(boolean tollRoadsAvoided) {
        this.tollRoadsAvoided = tollRoadsAvoided;
    }

    /**
     * @return Returns a boolean that indicates whether to avoid highways roads
     * when calculating the route.
     */
    public boolean isHighWaysAvoided() {
        return highWaysAvoided;
    }

    /**
     * Sets a boolean that indicates whether to avoid highways roads when
     * calculating the route.
     *
     * @param highWaysAvoided
     */
    public void setHighWaysAvoided(boolean highWaysAvoided) {
        this.highWaysAvoided = highWaysAvoided;
    }

    /**
     * @return Returns a boolean that indicates whether to avoid ferries when
     * calculating the route.
     */
    public boolean isFerriesAvoided() {
        return ferriesAvoided;
    }

    /**
     * Sets a boolean that indicates whether to avoid ferries when calculating
     * the route.
     *
     * @param ferriesAvoided
     */
    public void setFerriesAvoided(boolean ferriesAvoided) {
        this.ferriesAvoided = ferriesAvoided;
    }

    public void setTravelTimeInSec(int timeInSec) {
        travelTimeInSec = timeInSec;
        timeToDestinationInSec = timeInSec;
    }

    public int getTravelTimeInSec() {
        return travelTimeInSec;
    }

    public int getTravelDelayInSec() {
        return travelDelayInSec;
    }

    public void setTravelDelayInSec(int travelDelayInSec) {
        this.travelDelayInSec = travelDelayInSec;
    }

    public void setTravelDistanceInMeter(int distanceInMeter) {
        travelDistanceInMeter = distanceInMeter;
        distanceToDestinationInMeter = distanceInMeter;
    }

    public int getTravelDistanceInMeter() {
        return travelDistanceInMeter;
    }

    public void setNavigationType(int type) {
        navigationType = type;
    }

    public int getNavigationType() {
        return navigationType;
    }

    public int getDistanceToDestinationInMeter() {
        return distanceToDestinationInMeter;
    }

    public void setDistanceToDestinationInMeter(int distanceToDestination) {
        distanceToDestinationInMeter = distanceToDestination;
    }

    public int getTimeToDestinationInSec() {
        return timeToDestinationInSec;
    }

    public void setTimeToDestinationInSec(int timeToDestination) {
        timeToDestinationInSec = timeToDestination;
    }

    public Date getArrivalTime() {
        Date arrivalTime = new Date();
        arrivalTime.setTime(System.currentTimeMillis() + timeToDestinationInSec * 1000);
        return arrivalTime;
    }

    public String getRouteSummary() {
        return routeSummary;
    }

    public void setRouteSummary(String routeSummary) {
        this.routeSummary = routeSummary;
    }

    public String getCurrentStreetName() {
        return currentStreetName;
    }

    public void setCurrentStreetName(String streetName) {
        if (streetName != null && !streetName.equalsIgnoreCase(currentStreetName)) {
            currentStreetName = streetName;
        }
    }

    public Advice getCurrentAdvice() {
        return currentAdvice;
    }

    public Advice getNextAdvice() {
        return nextAdvice;
    }
    public boolean isRouting() {
        return isRouting;
    }

    public void setIsRouting(boolean isRouting) {
        this.isRouting = isRouting;
    }

    public boolean isRouteReady() {
        return isRouteReady;
    }

    public void setIsRouteReady(boolean isRouteReady) {
        this.isRouteReady = isRouteReady;
    }

    public boolean isReRouting() {
        return isReRouting;
    }

    public void setIsReRouting(boolean isReRouting) {
        this.isReRouting = isReRouting;
    }

    public boolean isNavigating() {
        return isNavigating;
    }

    public void setIsNavigating(boolean isNavigating) {
        this.isNavigating = isNavigating;
    }

    public boolean isDestinationReached() {
        return isDestinationReached;
    }

    public void setIsDestinationReached(boolean isDestinationReached) {
        this.isDestinationReached = isDestinationReached;
    }

    public boolean isToParking() {
        return (navigationDestination instanceof ParkingInfo);
    }

    public ParkingReservationInfo getParkingReservationInfo() {
        return parkingReservationInfo;
    }

    public void setParkingReservationInfo(ParkingReservationInfo parkingReservationInfo) {
        this.parkingReservationInfo = parkingReservationInfo;
    }

    public void setIsParkingAvailable(boolean isParkingAvailable) {
        this.isParkingAvailable = isParkingAvailable;
    }

    public boolean isParkingAvailable() {
        return isParkingAvailable;
    }

    public void setNavigationDestination(PlaceInfo dest) {
        navigationDestination = dest;
    }

    public PlaceInfo getNavigationDestination() {
        return navigationDestination;
    }

    public List<MapCoordinate> getRoute() {
        return route;
    }

    public void setRoute(List<MapCoordinate> route) {
        this.route = route;
    }
}
