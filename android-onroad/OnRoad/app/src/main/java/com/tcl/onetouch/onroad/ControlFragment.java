package com.tcl.onetouch.onroad;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.base.QnATask;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.service.NluService;
import com.tcl.onetouch.service.SpeechRecognizerService;
import com.tcl.onetouch.service.TtsService;
import com.tcl.onetouch.service.WakeupService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.Map;

public class ControlFragment
        extends Fragment
        implements View.OnClickListener, Animation.AnimationListener,
        WakeupService.OnWakeupListener, SensorEventListener {
    public static final String TAG = ControlFragment.class.getName();

    private View menuPanel;

    private View menuButton;
    private View micView;
    private View micButton;
    private ProgressBar micProgressBar;
    private ObjectAnimator micProgressAnimator;

    private Animation slideOutFromTopAnim;
    private Animation slideInToTopAnim;

    private PowerManager.WakeLock wakeLock;

    private float lastProximitySensorValue;
    private long lastProximitySensorTimestamp;

    public ControlFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PowerManager pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_control, container, false);

        menuPanel = view.findViewById(R.id.menu_panel);
        menuPanel.findViewById(R.id.phone_button).setOnClickListener(this);
        menuPanel.findViewById(R.id.navigation_button).setOnClickListener(this);
        menuPanel.findViewById(R.id.music_button).setOnClickListener(this);
        menuPanel.findViewById(R.id.message_button).setOnClickListener(this);
        menuPanel.findViewById(R.id.settings_button).setOnClickListener(this);
        menuPanel.setVisibility(View.INVISIBLE);

//        LoginButton automaticLoginButton = (LoginButton) settingsPanel.findViewById(R.id.automatic_login_button);
//        automaticLoginButton.setOnLoginResultCallback(new AutomaticLoginCallbacks() {
//            @Override
//            public void onLoginSuccess() {
//                Toast.makeText(getActivity(), "Automatic Login Successful", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onLoginFailure(RuntimeException error) {
//                Toast.makeText(getActivity(), "Automatic Login Failed", Toast.LENGTH_SHORT).show();
//            }
//        });
//        Automatic.get().addLoginButton(automaticLoginButton, getActivity());

        menuButton = view.findViewById(R.id.menu_button);
        menuButton.setOnClickListener(this);
        micView = view.findViewById(R.id.mic_view);
        micButton = micView.findViewById(R.id.mic_button);
        micButton.setOnClickListener(this);
        micProgressBar = (ProgressBar) view.findViewById(R.id.mic_progress_bar);
//        micProgressBar.setVisibility(View.INVISIBLE);
        micProgressAnimator = ObjectAnimator.ofInt(micProgressBar, "progress", 0, 500);
        micProgressAnimator.setInterpolator(new LinearInterpolator());

        slideOutFromTopAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.window_slide_out_from_top);
        slideInToTopAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.window_slide_in_to_top);

        slideOutFromTopAnim.setAnimationListener(this);
        slideInToTopAnim.setAnimationListener(this);

        menuPanel.setVisibility(View.INVISIBLE);
        menuPanel.setClickable(true);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        wakeLock.acquire();
        WakeupService.getDefaultService().setOnWakeupListener(this);
        OneTouchApplication.getInstance().enableWakeupService(TAG);
        lastProximitySensorValue = -1;
        lastProximitySensorTimestamp = -1;
        SensorManager sm = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        sm.registerListener(this, sm.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        if (menuPanel.getVisibility() == View.VISIBLE) {
            slideOutFromTopAnim.cancel();
            slideInToTopAnim.cancel();
            menuPanel.setVisibility(View.INVISIBLE);
        }
        SpeechRecognizerService.getDefaultService().cancelRecognize();
        OneTouchApplication.getInstance().disableWakeupService(TAG, null);
        WakeupService.getDefaultService().setOnWakeupListener(null);
        wakeLock.release();
        SensorManager sm = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        sm.unregisterListener(this);
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        menuPanel = null;

        menuButton = null;
        micView = null;
        micButton = null;
        micProgressBar = null;
        micProgressAnimator = null;

        slideOutFromTopAnim = null;
        slideInToTopAnim = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_button:
                if (!hideMenu()) {
                    menuPanel.startAnimation(slideOutFromTopAnim);
                }
                break;
            case R.id.mic_button:
                if (micButton.isActivated()) {
                    cancelRecognize();
                } else {
                    startRecognize();
                }
                break;
//            case R.id.cancel_input_button:
//                cancelRecognize();
//                break;
//            case R.id.spotify_login_button:
//                if (MusicService.getDefaultService().isAuthenticated()) {
//                    MusicService.getDefaultService().logout();
//                } else {
//                    MusicService.getDefaultService().login(getActivity());
//                }
//                break;
//            case R.id.car_location_button:
//                Location location = LocationService.getDefaultService().getLastVehicleLocation();
//                if (location != null) {
//                    MapService.getDefaultService().addAnnotation(MapService.ANNOTATION_DESTINATION_FLAG, location);
//                }
//                boolean inVehicle = OnRoadApplication.getInstance().getBooleanPreference(ActivityDetectionService.IN_VEHICLE_KEY, false);
//                String text = "";
//                if (inVehicle) {
//                    text = "Currently in vehicle";
//                } else {
//                    long time = OnRoadApplication.getInstance().getLongPreference(ActivityDetectionService.LAST_IN_VEHICLE_TIME_KEY, 0);
//                    if (time > 0) {
//                        text = "Last in vehicle " + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date(time));
//                    }
//                }
//                if (!text.isEmpty()) {
//                    Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
//                }
//                settingsPanel.startAnimation(slideInToTopAnim);
//                break;
            case R.id.message_button:
            case R.id.phone_button:
                hideMenu();
                break;
            case R.id.music_button:
                hideMenu();
                ((MainActivity) getActivity()).showMusic();
                break;
            case R.id.navigation_button:
                hideMenu();
                ((MainActivity) getActivity()).showNavigation();
                break;
            case R.id.settings_button:
                hideMenu();
                ((MainActivity) getActivity()).showSettings();
                break;
        }
    }

    @Override
    public void onWakeup() {
        startRecognize();
    }

    @Override
    public void onError(ErrorCode error) {
        OnRoadApplication.getInstance().handleError(error);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float value = event.values[0];
        if (WakeupService.getDefaultService().isActive() &&
                lastProximitySensorValue >= 0 && lastProximitySensorTimestamp > 0 &&
                event.timestamp - lastProximitySensorTimestamp <= 1000000000 &&
                value > lastProximitySensorValue) {
//            startRecognize();
        }
//        LogUtil.d("proximity sensor: " + value + ", " + event.timestamp);
        lastProximitySensorValue = value;
        lastProximitySensorTimestamp = event.timestamp;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    protected void resetProximityValue() {
        lastProximitySensorValue = -1;
    }

    protected void startRecognize() {
        hideMenu();
        TtsService.getDefaultService().stop(null);
        OneTouchApplication.getInstance().disableWakeupService(TAG, new WakeupService.OnStoppedListener() {
            @Override
            public void onStopped() {
                SpeechRecognizerService.getDefaultService().startRecognize(15000, 1000, new SpeechRecognizerService.OnResultListener() {
                    @Override
                    public void onResult(String result) {
                        if (SystemUtil.isDev()) {
                            Toast.makeText(OneTouchApplication.getInstance(), result, Toast.LENGTH_SHORT).show();
                        }
                        parseInput(result);
                        OneTouchApplication.getInstance().enableWakeupService(TAG);
                    }

                    @Override
                    public void onPartialResult(String partialResult) {
                    }

                    @Override
                    public void onError(ErrorCode error) {
                        if (error.getCode() != ErrorCode.SPEECH_ERROR_TIMEOUT) {
                            OnRoadApplication.getInstance().handleError(error);
                        }
                        OneTouchApplication.getInstance().enableWakeupService(TAG);
                    }
                });
            }
        });
    }

    protected void cancelRecognize() {
        SpeechRecognizerService.getDefaultService().cancelRecognize();
        OneTouchApplication.getInstance().enableWakeupService(TAG);
    }

    protected boolean hideMenu() {
        if (menuPanel.getVisibility() == View.VISIBLE &&
                (!slideInToTopAnim.hasStarted() || slideInToTopAnim.hasEnded())) {
            slideOutFromTopAnim.cancel();
            menuPanel.startAnimation(slideInToTopAnim);
            return true;
        }
        return false;
    }

    protected void parseInput(String input) {
        NluService.getDefaultService().parseIntent(
                input,
                LocationService.getDefaultService().getLastLocation(),
                new NluService.ResultListener() {
                    @Override
                    public void onResult(Map<String, Object> result) {
                        if (!MainActivity.getInstance().processIntent(
                                DataUtil.getInt(result, NluService.RESULT_KEY_INTENT),
                                DataUtil.getMap(result, NluService.RESULT_KEY_RESULT))) {
                            OnRoadApplication.getInstance().tts(R.string.tts_unsupport_intent, TAG);
                        }
                    }

                    @Override
                    public void onError(ErrorCode error) {
                        // TODO: handle error
                        switch (error.getCode()) {
                            case ErrorCode.NLU_ERROR_NO_RESULT:
                            case ErrorCode.NLU_ERROR_UNSUPPORTED_DOMAIN:
                            case ErrorCode.NLU_ERROR_UNSUPPORTED_INTENT:
                                OnRoadApplication.getInstance().tts(R.string.tts_unsupport_intent, TAG);
                                break;
                            default:
                                OnRoadApplication.getInstance().handleError(error);
                        }
                    }
                });
    }

    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == slideOutFromTopAnim) {
            menuPanel.setVisibility(View.VISIBLE);
            menuPanel.requestLayout();
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == slideInToTopAnim) {
            menuPanel.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public void wakeupStarted() {
        if (micView == null) {
            return;
        }
        micButton.setActivated(false);
        micButton.setEnabled(true);
    }

    public void wakeupStopped() {
        if (micView == null || QnATask.getCurrentTask() != null) {
            return;
        }
        micButton.setActivated(false);
        micButton.setEnabled(false);
    }

    public void listeningStarted() {
        micButton.setActivated(true);
        micButton.setEnabled(false);
    }

    public void listeningReady(long timeout) {
        micProgressAnimator.setDuration(timeout);
        micProgressAnimator.start();
        micButton.setEnabled(true);
    }

    public void speechBegan() {
        micProgressAnimator.cancel();
        micProgressBar.setProgress(micProgressBar.getMax());
    }

    public void speechEnded() {
        micButton.setActivated(false);
        micButton.setEnabled(false);
    }

    public void listeningEnded() {
        micProgressAnimator.cancel();
        micProgressBar.setProgress(0);
        micButton.setActivated(false);
        if (!WakeupService.getDefaultService().isActive()) {
            micButton.setEnabled(false);
        } else {
            micButton.setEnabled(true);
        }
    }

    public void setLayout(View view, int x, int y) {
        ViewGroup.MarginLayoutParams margin = new ViewGroup.MarginLayoutParams(view.getLayoutParams());
        margin.setMargins(x, y, x + margin.width, y + margin.height);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(margin);
        view.setLayoutParams(layoutParams);
    }

    public void setMicViewToTabbar() {
        ViewGroup.MarginLayoutParams margin = new ViewGroup.MarginLayoutParams(micView.getLayoutParams());
        margin.setMargins(15, 0, 0, 20);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(margin);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        layoutParams.height = 200;
        layoutParams.width = 200;
        micView.setLayoutParams(layoutParams);
    }
}
