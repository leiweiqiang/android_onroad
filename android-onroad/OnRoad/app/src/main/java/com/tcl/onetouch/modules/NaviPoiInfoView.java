package com.tcl.onetouch.modules;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.onroad.R;

/**
 * Created by leiweiqiang2 on 16/5/23.
 */
public class NaviPoiInfoView extends LinearLayout {

    public NaviPoiInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.navi_poi_info_view, this);
    }

    public NaviPoiInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.navi_poi_info_view, this);
    }

    public NaviPoiInfoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.navi_poi_info_view, this);
    }

    public NaviPoiInfoView(Context context) {
        super(context);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.navi_poi_info_view, this);
    }

    public void setData(PlaceInfo placeInfo) {
        ((TextView)findViewById(R.id.navi_poi_address)).setText(placeInfo.getAddressInfo().getShortAddress());
    }
}
