package com.tcl.onetouch.model;

public class ParkingReservationInfo {
    private String facilityId;
    private String facilityName;
    private String facilityAddress;
    private long startTime;
    private long endTime;
    private String url;
    private String reservationId;
    private float cost;
    private boolean isCanceled;

    public ParkingReservationInfo() {
        startTime = System.currentTimeMillis();
        endTime = startTime;
        url = "";
        reservationId = "";
        cost = 0;
        isCanceled = false;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getFacilityAddress() {
        return facilityAddress;
    }

    public void setFacilityAddress(String facilityAddress) {
        this.facilityAddress = facilityAddress;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public boolean isCanceled() {
        return isCanceled;
    }

    public void setIsCanceled(boolean isCanceled) {
        this.isCanceled = isCanceled;
    }

    @Override
    public String toString() {
        return "ParkingReservationInfo{" +
            "facilityId='" + facilityId + '\'' +
            ", facilityName='" + facilityName + '\'' +
            ", facilityAddress='" + facilityAddress + '\'' +
            ", startTime=" + startTime +
            ", endTime=" + endTime +
            ", url='" + url + '\'' +
            ", reservationId='" + reservationId + '\'' +
            ", cost=" + cost +
            ", isCanceled=" + isCanceled +
            '}';
    }
}
