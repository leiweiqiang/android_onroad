package com.tcl.onetouch.service.skobbler;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Environment;
import android.os.StatFs;
import android.view.View;

import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.SKMaps;
import com.skobbler.ngx.SKPrepareMapTextureListener;
import com.skobbler.ngx.SKPrepareMapTextureThread;
import com.skobbler.ngx.map.SKAnimationSettings;
import com.skobbler.ngx.map.SKAnnotation;
import com.skobbler.ngx.map.SKAnnotationView;
import com.skobbler.ngx.map.SKBoundingBox;
import com.skobbler.ngx.map.SKCoordinateRegion;
import com.skobbler.ngx.map.SKMapCustomPOI;
import com.skobbler.ngx.map.SKMapPOI;
import com.skobbler.ngx.map.SKMapSettings;
import com.skobbler.ngx.map.SKMapSurfaceListener;
import com.skobbler.ngx.map.SKMapSurfaceView;
import com.skobbler.ngx.map.SKMapViewHolder;
import com.skobbler.ngx.map.SKPOICluster;
import com.skobbler.ngx.map.SKScreenPoint;
import com.skobbler.ngx.map.traffic.SKTrafficIncident;
import com.skobbler.ngx.positioner.SKPosition;
import com.skobbler.ngx.positioner.SKPositionerManager;
import com.skobbler.ngx.versioning.SKMapUpdateListener;
import com.skobbler.ngx.versioning.SKVersioningManager;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.MapCoordinate;
import com.tcl.onetouch.model.OnRoadErrorCode;
import com.tcl.onetouch.service.MapService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class SkobblerMapService extends MapService
    implements SKPrepareMapTextureListener, SKMapUpdateListener, SKMapSurfaceListener {
    public static final byte GREEN_PIN_ICON_ID = 0;
    public static final byte RED_PIN_ICON_ID = 1;
    public static final byte GREY_PIN_ICON_ID = 3;
    public static final byte VIA_POINT_ICON_ID = 4;

    public static final float DEFAULT_ZOOM = 17;
    public static final int DEFAULT_DURATION = 500;

    public static final long KILO = 1024;
    public static final long MEGA = KILO * KILO;

    private SkobblerPreference preference;
    private SKMapViewHolder mapViewGroup;
    private SKMapSurfaceView mapView;

    private MapService.Listener listener;

    private boolean isMapReady;

    private String mapResourcesDirPath;

    /**
     * MapService
     */
    @Override
    public void init(Context context, MapService.Listener listener) {
        this.listener = listener;
        preference = new SkobblerPreference(context);
        String applicationPath = chooseStoragePath(context);

        // determine path where map resources should be copied on the device
        if (applicationPath != null) {
            mapResourcesDirPath = applicationPath + "/" + "SKMaps/";
        } else {
            listener.onError(createErrorCode(OnRoadErrorCode.MAP_ERROR_STORAGE));
            return;
        }
        preference.saveStringPreference(SkobblerPreference.MAP_RESOURCE_PATH, mapResourcesDirPath);
        int savedVersionCode = preference.getIntPreference(SkobblerPreference.CURRENT_VERSION_CODE);
        int currentVersionCode = 0;
        try {
            currentVersionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
        }
        if (!new File(mapResourcesDirPath).exists()) {
            new SKPrepareMapTextureThread(context, mapResourcesDirPath, "SKMaps.zip", this).start();
            copyOtherResources(context);
//            prepareMapCreatorFile();
            preference.setCurrentVersionCode(currentVersionCode);
        } else if (savedVersionCode < currentVersionCode) {
            SkobblerUtil.deleteFileOrDirectory(new File(mapResourcesDirPath));
            new SKPrepareMapTextureThread(context, mapResourcesDirPath, "SKMaps.zip", this).start();
            copyOtherResources(context);
//            prepareMapCreatorFile();
            preference.setCurrentVersionCode(currentVersionCode);
        } else {
//            prepareMapCreatorFile();
            onMapTexturesPrepared(true);
        }
    }

    /**
     * MapService
     */
    @Override
    public void pause() {
        if (mapViewGroup != null) {
            mapViewGroup.onPause();
        }
    }

    /**
     * MapService
     */
    @Override
    public void resume() {
        if (mapViewGroup != null) {
            mapViewGroup.onResume();
        }
    }

    @Override
    public void destroy() {
        SKMaps.getInstance().destroySKMaps();
        releaseMapView();
    }

    /**
     * MapService
     */
    @Override
    public void centerMe(int followingMode, float zoomLevel, int offsetX, int offsetY) {
        if (mapView != null) {
            mapView.centerMapOnCurrentPositionSmooth(DEFAULT_ZOOM, DEFAULT_DURATION);
            if (followingMode == FOLLOWING_MODE_POSITION_AND_HEADING) {
                mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.POSITION_PLUS_HEADING);
            } else {
                mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.POSITION);
            }
        }
    }

    /**
     * MapService
     */
    @Override
    public void centerLocation(Location location) {
        if (mapView != null) {
            mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.NONE);
            SKCoordinate coordinate = new SKCoordinate(location.getLongitude(), location.getLatitude());
            mapView.centerMapOnPositionSmooth(coordinate, DEFAULT_DURATION);
        }
    }

    @Override
    public void setBearing(float bearing, int duration) {
        if (mapView != null) {
            if (duration > 0) {
                mapView.rotateMapWithAngleSmooth(bearing, duration);
            } else {
                mapView.rotateMapWithAngle(bearing);
            }
        }
    }

    @Override
    public float getBearing() {
        if (mapView != null) {
            return mapView.getMapBearing();
        } else {
            return 0;
        }
    }

    /**
     * MapService
     */
    @Override
    public void updateCurrentLocation(Location location) {
        SKPositionerManager.getInstance().reportNewGPSPosition(new SKPosition(location));
        if (!isMapReady && mapView != null) {
            SKBoundingBox bbox = mapView.getBoundingBoxForRegion(mapView.getCurrentMapRegion());
            double lat = location.getLatitude();
            double lon = location.getLongitude();
            if (lat <= bbox.getTopLeftLatitude() && lat >= bbox.getBottomRightLatitude() &&
                lon >= bbox.getTopLeftLongitude() && lon <= bbox.getBottomRightLongitude()) {
                isMapReady = true;
                if (listener != null) {
                    listener.onMapReady();
                }
            }
        }
    }

    @Override
    public void fitRegion(double topLat, double leftLng, double bottomLat, double rightLng) {
        if (mapView != null) {
            mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.NONE);
            SKBoundingBox bb = new SKBoundingBox(topLat, leftLng, bottomLat, rightLng);
            mapView.fitBoundingBox(bb, 0, 0);
        }
    }

    /**
     * MapService
     */
    @Override
    public View getMapView() {
        return mapViewGroup;
    }

    /**
     * MapService
     */
    @Override
    public View createMapView(Context context) {
        if (!SKMaps.getInstance().isSKMapsInitialized()) {
            return null;
        }
        mapViewGroup = new SKMapViewHolder(context);
        mapViewGroup.setMapSurfaceListener(this);
        return mapViewGroup;
    }

    /**
     * MapService
     */
    @Override
    public void releaseMapView() {
        if (mapViewGroup != null) {
            mapViewGroup.setMapSurfaceListener(null);
            mapViewGroup.destroyDrawingCache();
            mapViewGroup = null;
        }
        mapView = null;
    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onActionPan() {
        if (mapView != null) {
            mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.NONE);
        }
    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onActionZoom() {
        if (mapView != null) {
            mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.NONE);
        }
    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onSurfaceCreated(SKMapViewHolder skMapViewHolder) {
        isMapReady = false;
        mapView = skMapViewHolder.getMapSurfaceView();
        SkobblerUtil.applySettingsOnMapView(mapView);
    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onMapRegionChanged(SKCoordinateRegion skCoordinateRegion) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onMapRegionChangeStarted(SKCoordinateRegion skCoordinateRegion) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onMapRegionChangeEnded(SKCoordinateRegion skCoordinateRegion) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onDoubleTap(SKScreenPoint skScreenPoint) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onSingleTap(SKScreenPoint skScreenPoint) {
    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onRotateMap() {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onLongPress(SKScreenPoint skScreenPoint) {
        if (mapView != null) {
//            SKCoordinate poiCoordinates = mapView.pointToCoordinate(skScreenPoint);
//            final SKSearchResult place = SKReverseGeocoderManager.getInstance().reverseGeocodePosition(poiCoordinates);
//            onLongPressPoint = place.getLocation();
//            if (mapView != null && currentPosition != null) {
//                startPoint = currentPosition.getCoordinate();
//            }
//            navigationManager = MapUtil.calculateRouteFromSKTools(startPoint, onLongPressPoint, app, this, mapViewGroup);
        }
    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onInternetConnectionNeeded() {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onMapActionDown(SKScreenPoint skScreenPoint) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onMapActionUp(SKScreenPoint skScreenPoint) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onPOIClusterSelected(SKPOICluster skpoiCluster) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onMapPOISelected(SKMapPOI skMapPOI) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onAnnotationSelected(SKAnnotation skAnnotation) {
        if (skAnnotation.getAnnotationView() != null && skAnnotation.getAnnotationView().getView() != null) {
            skAnnotation.getAnnotationView().getView().performClick();
        }
    }

    /**
     * SKMapSurfaceListener
     */

    @Override
    public void onCustomPOISelected(SKMapCustomPOI skMapCustomPOI) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onCompassSelected() {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onCurrentPositionSelected() {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onObjectSelected(int i) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onTrafficIncidentSelected(SKTrafficIncident skTrafficIncident) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onTrafficIncidentsClusterSelected(List<SKTrafficIncident> list) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onTrafficInfoRefreshed() {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onInternationalisationCalled(int i) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onBoundingBoxImageRendered(int i) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onGLInitializationError(String s) {

    }

    /**
     * SKMapSurfaceListener
     */
    @Override
    public void onScreenshotReady(Bitmap bitmap) {

    }

    /**
     * SKMapUpdateListener
     */
    @Override
    public void onNewVersionDetected(int i) {

    }

    /**
     * SKMapUpdateListener
     */
    @Override
    public void onMapVersionSet(int i) {

    }

    /**
     * SKMapUpdateListener
     */
    @Override
    public void onVersionFileDownloadTimeout() {

    }

    /**
     * SKMapUpdateListener
     */
    @Override
    public void onNoNewVersionDetected() {

    }

    /**
     * SKPrepareMapTextureListener
     */
    @Override
    public void onMapTexturesPrepared(boolean prepared) {
        SKVersioningManager.getInstance().setMapUpdateListener(this);

        if (!SkobblerUtil.initializeLibrary(OneTouchApplication.getInstance(), mapResourcesDirPath)) {
            if (listener != null) {
                listener.onError(createErrorCode(OnRoadErrorCode.MAP_ERROR_INITIALIZATION));
            }
            return;
        }

        if (listener != null) {
            listener.onServiceReady();
        }
    }

    /**
     * Copy some additional resources from assets
     */
    private void copyOtherResources(final Context context) {
        new Thread() {
            public void run() {
                try {
                    String tracksPath = mapResourcesDirPath + "GPXTracks";
                    File tracksDir = new File(tracksPath);
                    if (!tracksDir.exists()) {
                        tracksDir.mkdirs();
                    }
                    SkobblerUtil.copyAssetsToFolder(context.getAssets(), "GPXTracks", mapResourcesDirPath + "GPXTracks");

                    String imagesPath = mapResourcesDirPath + "images";
                    File imagesDir = new File(imagesPath);
                    if (!imagesDir.exists()) {
                        imagesDir.mkdirs();
                    }
                    SkobblerUtil.copyAssetsToFolder(context.getAssets(), "images", mapResourcesDirPath + "images");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * Copies the map creator file and logFile from assets to a storage.
     */
    private void prepareMapCreatorFile() {
        final OneTouchApplication app = OneTouchApplication.getInstance();
        final Thread prepareGPXFileThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

                    final String mapCreatorFolderPath = mapResourcesDirPath + "MapCreator";
                    final File mapCreatorFolder = new File(mapCreatorFolderPath);
                    // create the folder where you want to copy the json file
                    if (!mapCreatorFolder.exists()) {
                        mapCreatorFolder.mkdirs();
                    }
//                    app.setMapCreatorFilePath(mapCreatorFolderPath + "/mapcreatorFile.json");
                    SkobblerUtil.copyAsset(app.getAssets(), "MapCreator", mapCreatorFolderPath, "mapcreatorFile.json");
                    // Copies the log file from assets to a storage.
                    final String logFolderPath = mapResourcesDirPath + "logFile";
                    final File logFolder = new File(logFolderPath);
                    if (!logFolder.exists()) {
                        logFolder.mkdirs();
                    }
                    SkobblerUtil.copyAsset(app.getAssets(), "logFile", logFolderPath, "Seattle.log");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        prepareGPXFileThread.start();
    }

    private static String chooseStoragePath(Context context) {
        if (getAvailableMemorySize(Environment.getDataDirectory().getPath()) >= 50 * MEGA) {
            if (context.getFilesDir() != null) {
                return context.getFilesDir().getPath();
            }
        } else if (context.getExternalFilesDir(null) != null) {
            if (getAvailableMemorySize(context.getExternalFilesDir(null).toString()) >= 50 * MEGA) {
                return context.getExternalFilesDir(null).toString();
            }
        }

        if (context.getFilesDir() != null) {
            return context.getFilesDir().getPath();
        } else if (context.getExternalFilesDir(null) != null) {
            return context.getExternalFilesDir(null).toString();
        } else {
            return null;
        }
    }

    /**
     * get the available internal memory size
     *
     * @return available memory size in bytes
     */
    public static long getAvailableMemorySize(String path) {
        try {
            StatFs statFs = new StatFs(path);
            return statFs.getAvailableBytes();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public void addAnnotation(int id, MapCoordinate position, View view, float offsetX, float offsetY) {
        if (mapView != null) {
            SKAnnotation annotation = new SKAnnotation(id);
            annotation.setLocation(new SKCoordinate(position.getLongitude(), position.getLatitude()));
            SKAnnotationView annotationView = new SKAnnotationView();
            annotationView.setView(view);
            annotation.setAnnotationView(annotationView);
            annotation.setOffset(new SKScreenPoint(offsetX, offsetY));
            mapView.addAnnotation(annotation, SKAnimationSettings.ANIMATION_NONE);
        }
    }

    private void createAnnotation(int id, int type, double longitude, double latitude,
                                  SKAnimationSettings annotationAnimationType) {
        if (mapView != null) {
            SKAnnotation annotation = new SKAnnotation(id);
            annotation.setAnnotationType(type);
            annotation.setLocation(new SKCoordinate(longitude, latitude));
            mapView.addAnnotation(annotation, annotationAnimationType);
        }
    }

    public void deleteAnnotation(int id) {
        if (mapView != null) {
            mapView.deleteAnnotation(id);
        }
    }
}
