package com.tcl.onetouch.onroad;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.service.UserService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.view.FacebookLoginPreference;
import com.tcl.onetouch.view.GoogleLoginPreference;

import java.util.Map;

public class SettingsFragment extends PreferenceFragment
    implements UserService.Listener, SharedPreferences.OnSharedPreferenceChangeListener {

    private PreferenceGroup userSettingsGroup;
    private GoogleLoginPreference googleLoginPreference;
    private FacebookLoginPreference facebookLoginPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        userSettingsGroup = (PreferenceGroup) findPreference(
            getActivity().getResources().getString(R.string.PREF_KEY_USER_SETTINGS));
        googleLoginPreference = (GoogleLoginPreference)
            userSettingsGroup.findPreference(getActivity().getResources().getString(R.string.PREF_KEY_GOOGLE_LOGIN));
        facebookLoginPreference = (FacebookLoginPreference)
            userSettingsGroup.findPreference(getActivity().getResources().getString(R.string.PREF_KEY_FACEBOOK_LOGIN));

        if (UserService.getInstance().getGoogleSignInResult() != null) {
            userSettingsGroup.removePreference(facebookLoginPreference);
        }
        if (UserService.getInstance().getFacebookSignInResult() != null) {
            userSettingsGroup.removePreference(googleLoginPreference);
        }

        UserService.getInstance().registerListener(this);
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup content = (ViewGroup) inflater.inflate(R.layout.fragment_settings, container, false);
        content.addView(view);
        return content;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        UserService.getInstance().unregisterListener(this);
    }

    @Override
    public void onSignInChanged(int signInType, Map<String, Object> result) {
        LogUtil.d("onSignInChanged: " + signInType + ", " + result);
        switch (signInType) {
            case UserService.SIGN_IN_TYPE_ONE_TOUCH: {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(OneTouchApplication.getInstance());
                SharedPreferences.Editor editor = pref.edit();
                if (result != null) {
                    editor.putString(
                        OneTouchApplication.getInstance().getResources().getString(R.string.PREF_KEY_ONE_TOUCH_LOGIN),
                        DataUtil.toJson(result));
                } else {
                    editor.remove(OneTouchApplication.getInstance().getResources().getString(R.string.PREF_KEY_ONE_TOUCH_LOGIN));
                }
                editor.commit();
                break;
            }
            case UserService.SIGN_IN_TYPE_GOOGLE:
                if (result != null) {
                    userSettingsGroup.removePreference(facebookLoginPreference);
                    googleLoginPreference.updateUi(true);
                } else {
                    userSettingsGroup.addPreference(facebookLoginPreference);
                    googleLoginPreference.updateUi(false);
                }
                break;
            case UserService.SIGN_IN_TYPE_FACEBOOK:
                if (result != null) {
                    userSettingsGroup.removePreference(googleLoginPreference);
                } else {
                    userSettingsGroup.addPreference(googleLoginPreference);
                }
                break;
        }
    }

    @Override
    public void onError(ErrorCode error) {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (OnRoadApplication.getInstance().getString(R.string.PREF_KEY_NAVIGATION_MOCK_GPS).equals(key)) {
            LocationService.getDefaultService().enableMockLocation(sharedPreferences.getBoolean(key, false));
        }
    }
}
