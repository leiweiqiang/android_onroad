package com.tcl.onetouch.model;

import java.util.ArrayList;
import java.util.List;

public class AlbumInfo {
    private String uri;
    private String name;
    private String imageUrl;
    private String artistUri;
    private String releaseDate;
    private List<String> tracks;

    public AlbumInfo(String uri) {
        this.uri = uri;
        name = "";
        imageUrl = null;
        artistUri = "";
        releaseDate = "";
        tracks = new ArrayList<>();
    }

    public String getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getArtistUri() {
        return artistUri;
    }

    public void setArtistUri(String artistUri) {
        this.artistUri = artistUri;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<String> getTracks() {
        return tracks;
    }

    public void addTrack(String trackUri) {
        tracks.add(trackUri);
    }
}
