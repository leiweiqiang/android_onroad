package com.tcl.onetouch.model;

public class TrackInfo {
    private String uri;
    private String name;
    private int duration; // in milliseconds
    private String artistUri;
    private String albumUri;

    public TrackInfo(String uri) {
        this.uri = uri;
        name = "";
        duration = 0;
        artistUri = "";
        albumUri = "";
    }

    public String getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getArtistUri() {
        return artistUri;
    }

    public void setArtistUri(String artistUri) {
        this.artistUri = artistUri;
    }

    public String getAlbumUri() {
        return albumUri;
    }

    public void setAlbumUri(String albumUri) {
        this.albumUri = albumUri;
    }
}
