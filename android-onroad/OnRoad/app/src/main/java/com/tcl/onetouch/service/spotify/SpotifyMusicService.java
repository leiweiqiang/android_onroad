package com.tcl.onetouch.service.spotify;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.util.LruCache;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Connectivity;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;
import com.spotify.sdk.android.player.PlayerStateCallback;
import com.spotify.sdk.android.player.Spotify;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.AlbumInfo;
import com.tcl.onetouch.model.ArtistInfo;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.OnRoadErrorCode;
import com.tcl.onetouch.model.PlaylistInfo;
import com.tcl.onetouch.model.TrackInfo;
import com.tcl.onetouch.service.MusicService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.HttpUtil;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SpotifyMusicService extends MusicService
    implements PlayerNotificationCallback, ConnectionStateCallback, PlayerStateCallback {
    private static final String CLIENT_ID = "5aba910a2e6e4a03b3ea6acf804f536d";
    //private static final String CLIENT_SECRET = "fb2069f332564762bb0e5870ae160416";
    private static final String CLIENT_SECRET = "78631b64794748a1910caeddd0d7b4d9";
    //private static final String REDIRECT_URI = "http://54.165.202.192:8080/spotify/callback";
    private static final String REDIRECT_URI = "http://54.197.12.95:8080/spotify/callback";

    private static final String TOKEN_URL = "https://accounts.spotify.com/api/token";
    private static final String USER_ID_URL = "https://api.spotify.com/v1/me";
    private static final String SEARCH_URL = "https://api.spotify.com/v1/search?";
    private static final String TRACK_URL = "https://api.spotify.com/v1/tracks/";
    private static final String ALBUM_URL = "https://api.spotify.com/v1/albums/";
    private static final String ARTIST_URL = "https://api.spotify.com/v1/artists/";
    private static final String FEATURED_PLAYLIST_URL = "https://api.spotify.com/v1/browse/featured-playlists?";
    private static final String MY_PLAYLIST_URL = "https://api.spotify.com/v1/me/playlists";

    private static final String PREFS_NAME = "SpotifyPrefs";
    private static final String ACCESS_TOKEN_KEY = "access_token";
    private static final String REFRESH_TOKEN_KEY = "refresh_token";
    private static final String EXPIRES_TIME_KEY = "expires_time";

    private static final String ERROR_KEY = "error";
    private static final String ERROR_DESCRIPTION_KEY = "error_description";
    private static final String ERROR_STATUS_KEY = "status";
    private static final String ERROR_MESSAGE_KEY = "message";

    public static final String KEY_SPOTIFY_LOGIN_STATUS = "com.tcl.onetouch.KEY_SPOTIFY_LOGIN_STATUS";

    private SharedPreferences prefs;
    private Player player;
    private SpotifyAudioController audioController;
    private BroadcastReceiver networkStateReceiver;
    private State currentState;
    private OnStateChangeListener listener;

    private Map<String, String> authenticationHeader;
    private String userId;

    private LruCache<String, TrackInfo> trackInfoLruCache;
    private LruCache<String, ArtistInfo> artistInfoLruCache;
    private LruCache<String, AlbumInfo> albumInfoLruCache;

    @Override
    public void init(final Context context, OnStateChangeListener listener) {
        this.listener = listener;
        player = null;
        audioController = new SpotifyAudioController();

        currentState = new State();
        currentState.isLoggedIn = OneTouchApplication.getInstance().getBooleanPreference(KEY_SPOTIFY_LOGIN_STATUS, false);
        currentState.isPlaying = false;
        currentState.isRepeating = false;
        currentState.isShuffling = false;
        currentState.currentTrackUri = null;
        currentState.currentPlaylistUri = null;
        currentState.duration = 0;
        currentState.position = 0;

        networkStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context ctx, Intent intent) {
                if (player != null) {
                    Connectivity connectivity = getNetworkConnectivity(OneTouchApplication.getInstance());
                    player.setConnectivityStatus(connectivity);
                }
            }
        };

        authenticationHeader = new HashMap<>();
        userId = "";

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        OneTouchApplication.getInstance().registerReceiver(networkStateReceiver, filter);
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        long expiresTime = prefs.getLong(EXPIRES_TIME_KEY, 0);
        if (expiresTime > 0) {
            long delay = expiresTime - System.currentTimeMillis() - 300000;
            if (delay <= 0) {
                refreshToken();
            } else {
                authenticate();
                SystemUtil.postToMainThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshToken();
                    }
                }, delay);
            }
        }

        trackInfoLruCache = new LruCache<>(100);
        artistInfoLruCache = new LruCache<>(100);
        albumInfoLruCache = new LruCache<>(100);
    }

    @Override
    public void destroy() {
        stop();
        OneTouchApplication.getInstance().unregisterReceiver(networkStateReceiver);
    }

    @Override
    public State getState() {
        return currentState;
    }

    @Override
    public void refreshState() {
        if (player != null) {
            player.getPlayerState(this);
        }
    }

    @Override
    public boolean isAuthenticated() {
        return userId != null && !userId.isEmpty();
    }

    @Override
    public void login(Activity activity) {
        AuthenticationRequest request = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.CODE, REDIRECT_URI)
            .setScopes(new String[]{"user-read-private", "playlist-read", "playlist-read-private", "user-follow-read", "user-library-read", "streaming"})
            .build();
        AuthenticationClient.openLoginActivity(activity, LOGIN_REQUST_CODE, request);
    }

    @Override
    public void logout() {
        if (player != null && player.isLoggedIn()) {
            player.logout();
        }
        OneTouchApplication.getInstance().savePreference(KEY_SPOTIFY_LOGIN_STATUS, false);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(ACCESS_TOKEN_KEY);
        editor.remove(REFRESH_TOKEN_KEY);
        editor.remove(EXPIRES_TIME_KEY);
        editor.commit();
    }

    @Override
    public void onLoginResult(int resultCode, Intent intent) {
        AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
        switch (response.getType()) {
            // Response was successful and contains auth token
            case CODE:
                String code = response.getCode();
                Map<String, String> query = new HashMap<>();
                query.put("grant_type", "authorization_code");
                query.put("code", code);
                query.put("redirect_uri", REDIRECT_URI);
                query.put("client_id", CLIENT_ID);
                query.put("client_secret", CLIENT_SECRET);
                HttpUtil.doPost(TOKEN_URL, query, new HttpUtil.RequestListener() {
                    @Override
                    public void onResult(HttpUtil.HttpRequestResult result) {
                        ErrorCode error = handleHttpError(result);
                        if (error == null) {
                            Map<String, Object> data = DataUtil.parseJson(result.result);
                            String accessToken = DataUtil.getString(data, "access_token");
                            final String refreshToken = DataUtil.getString(data, "refresh_token");
                            int expiresIn = DataUtil.getInt(data, "expires_in");
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString(ACCESS_TOKEN_KEY, accessToken);
                            editor.putString(REFRESH_TOKEN_KEY, refreshToken);
                            editor.putLong(EXPIRES_TIME_KEY, System.currentTimeMillis() + expiresIn * 1000);
                            editor.commit();
                            authenticate();
                            if (player != null) {
                                player.login(accessToken);
                            }
                            SystemUtil.postToMainThread(new Runnable() {
                                @Override
                                public void run() {
                                    refreshToken();
                                }
                            }, (expiresIn - 300) * 1000);
                        } else if (listener != null) {
                            listener.onError(error);
                        }
                    }
                });
                break;
            case TOKEN:
                String accessToken = response.getAccessToken();
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(ACCESS_TOKEN_KEY, accessToken);
                editor.commit();
                authenticate();
                if (player != null) {
                    player.login(accessToken);
                }
                break;
            // Auth flow returned an error
            case ERROR:
                LogUtil.d("spotify auth error: " + response.getError());
                break;
            // Most likely auth flow was cancelled
            default:
                LogUtil.d("spotify auth result: " + response.getType());
        }
    }

    private void refreshToken() {
        String refreshToken = prefs.getString(REFRESH_TOKEN_KEY, "");
        if (refreshToken.isEmpty()) {
            return;
        }
        Map<String, String> query = new HashMap<>();
        query.put("grant_type", "refresh_token");
        query.put("refresh_token", refreshToken);
        query.put("client_id", CLIENT_ID);
        query.put("client_secret", CLIENT_SECRET);
        HttpUtil.doPost(TOKEN_URL, query, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    String accessToken = DataUtil.getString(data, "access_token");
                    int expiresIn = DataUtil.getInt(data, "expires_in");
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(ACCESS_TOKEN_KEY, accessToken);
                    editor.putLong(EXPIRES_TIME_KEY, System.currentTimeMillis() + expiresIn * 1000);
                    editor.commit();
                    authenticate();
                    if (player != null) {
                        player.login(accessToken);
                    }
                    SystemUtil.postToMainThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshToken();
                        }
                    }, (expiresIn - 300) * 1000);
                } else if (listener != null) {
                    listener.onError(error);
                }
            }
        });
    }

    private void authenticate() {
        authenticationHeader.put("Authorization", "Bearer " + prefs.getString(ACCESS_TOKEN_KEY, ""));
        HttpUtil.doGet(USER_ID_URL, authenticationHeader, null, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    userId = DataUtil.getString(data, "id");
                    LogUtil.d("spotify userId: " + userId);
                } else {
                    userId = "";
                    if (listener != null) {
                        listener.onError(amendErrorCode(error));
                    }
                }
            }
        });
    }

    private Connectivity getNetworkConnectivity(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return Connectivity.fromNetworkType(activeNetwork.getType());
        } else {
            return Connectivity.OFFLINE;
        }
    }

    @Override
    public void playTracks(List<String> tracks) {
        if (player != null) {
            player.play(tracks);
        }
    }

    @Override
    public void playTrack(String trackUri) {
        currentState.currentTrackUri = null;
        currentState.currentPlaylistUri = null;
        if (player != null) {
            player.play(trackUri);
        }
    }

    @Override
    public void playPlayList(String playListUri) {
        currentState.currentPlaylistUri = playListUri;
        if (player != null) {
            player.play(playListUri);
        }
    }

    @Override
    public void queueTrack(String trackUri) {
        if (player != null) {
            player.queue(trackUri);
        }
    }

    @Override
    public void queuePlayList(String playListUri) {
        if (player != null) {
            player.queue(playListUri);
        }
    }

    @Override
    public void clearQueue() {
        if (player != null) {
            player.clearQueue();
        }
    }

    @Override
    public void start() {
        if (player != null) {
            return;
        }
        String accessToken = prefs.getString(ACCESS_TOKEN_KEY, "");
        LogUtil.d("spotify access token: " + accessToken);
        if (accessToken.isEmpty()) {
            return;
        }
        Config playerConfig = new Config(OneTouchApplication.getInstance(), accessToken, CLIENT_ID);
        Player.Builder builder = new Player.Builder(playerConfig);
        builder.setAudioController(audioController);
        player = Spotify.getPlayer(builder, this, new Player.InitializationObserver() {
            @Override
            public void onInitialized(Player player) {
                player.setConnectivityStatus(getNetworkConnectivity(OneTouchApplication.getInstance()));
                player.addPlayerNotificationCallback(SpotifyMusicService.this);
                player.addConnectionStateCallback(SpotifyMusicService.this);
                if (player.isLoggedIn()) {
                    onLoggedIn();
                }
            }

            @Override
            public void onError(Throwable error) {
            }
        });
    }

    @Override
    public void pause() {
        if (player != null) {
            player.pause();
        }
    }

    @Override
    public void resume() {
        if (player != null) {
            player.resume();
        }
    }

    @Override
    public void stop() {
        if (player != null) {
            player.logout();
        }
        //currentState.isLoggedIn = false;
        currentState.isPlaying = false;
        currentState.isRepeating = false;
        currentState.isShuffling = false;
        currentState.currentTrackUri = null;
        currentState.currentPlaylistUri = null;
        currentState.duration = 0;
        currentState.position = 0;
    }

    @Override
    public void setDuck(boolean isDuck) {
        audioController.setVolumeGain(isDuck ? 0.1f : 1.0f);
    }

    @Override
    public void setMute(boolean isMute) {
        audioController.setVolumeGain(isMute ? 0.0f : 1.0f);
    }

    @Override
    public void skipToNext() {
        if (player != null) {
            player.skipToNext();
        }
    }

    @Override
    public void skipToPrevious() {
        if (player != null) {
            player.skipToPrevious();
        }
    }

    @Override
    public void setShuffle(boolean enabled) {
        if (player != null) {
            player.setShuffle(enabled);
        }
    }

    @Override
    public void setRepeat(boolean enabled) {
        if (player != null) {
            player.setRepeat(enabled);
        }
    }

    private String getImageUrl(Collection<Object> images) {
        if (images == null) {
            return "";
        }
        Iterator<Object> it = images.iterator();
        String url = "";
        while (it.hasNext()) {
            Map<String, Object> image = (Map<String, Object>) it.next();
            url = DataUtil.getString(image, "url");
            int height = DataUtil.getInt(image, "height");
            int width = DataUtil.getInt(image, "width");
            if (height < 1000 && width < 1000) {
                return url;
            }
        }
        return url;
    }

    private String parseId(String uri) {
        int index = uri.lastIndexOf(':');
        if (index == -1) {
            return uri;
        }
        return uri.substring(index + 1);
    }

    private TrackInfo createTrackInfo(String trackUri, Map<String, Object> track) {
        TrackInfo trackInfo = new TrackInfo(trackUri);
        trackInfo.setName(DataUtil.getString(track, "name"));
        trackInfo.setDuration(DataUtil.getInt(track, "duration_ms"));
        Map<String, Object> album = DataUtil.getMap(track, "album");
        if (album != null) {
            trackInfo.setAlbumUri(DataUtil.getString(album, "uri"));
        }
        Collection<Object> artists = DataUtil.getCollection(track, "artists");
        if (artists != null && artists.size() > 0) {
            Map<String, Object> artist = (Map<String, Object>) artists.iterator().next();
            trackInfo.setArtistUri(DataUtil.getString(artist, "uri"));
        }
        return trackInfo;
    }

    private AlbumInfo createAlbumInfo(String albumUri, Map<String, Object> album) {
        AlbumInfo albumInfo = new AlbumInfo(albumUri);
        albumInfo.setName(DataUtil.getString(album, "name"));
        albumInfo.setReleaseDate(DataUtil.getString(album, "release_date"));
        albumInfo.setImageUrl(getImageUrl(DataUtil.getCollection(album, "images")));
        Collection<Object> artists = DataUtil.getCollection(album, "artists");
        if (artists != null && artists.size() > 0) {
            Map<String, Object> artist = (Map<String, Object>) artists.iterator().next();
            albumInfo.setArtistUri(DataUtil.getString(artist, "uri"));
        }
        Map<String, Object> tracks = DataUtil.getMap(album, "tracks");
        if (tracks == null) {
            return albumInfo;
        }
        Collection<Object> items = DataUtil.getCollection(tracks, "items");
        if (items == null || items.size() == 0) {
            return albumInfo;
        }
        Iterator<Object> it = items.iterator();
        while (it.hasNext()) {
            albumInfo.addTrack(DataUtil.getString((Map<String, Object>) it.next(), "uri"));
        }
        return albumInfo;
    }

    private ArtistInfo createArtistInfo(String artistUri, Map<String, Object> artist) {
        ArtistInfo artistInfo = new ArtistInfo(artistUri);
        artistInfo.setName(DataUtil.getString(artist, "name"));
        artistInfo.setImageUrl(getImageUrl(DataUtil.getCollection(artist, "images")));
        return artistInfo;
    }

    @Override
    public void searchTrack(String name, final OnObjectLoadedListener listener) {
        Map<String, String> query = new HashMap<>();
        query.put("q", name);
        query.put("market", "from_token");
        query.put("type", "track");
        query.put("limit", "1");
        HttpUtil.doGet(SEARCH_URL, authenticationHeader, query, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> tracks = DataUtil.getMap(DataUtil.parseJson(result.result), "tracks");
                    if (tracks == null) {
                        listener.onLoaded(null);
                        return;
                    }
                    Collection<Object> items = DataUtil.getCollection(tracks, "items");
                    if (items == null || items.size() == 0) {
                        listener.onLoaded(null);
                        return;
                    }
                    Map<String, Object> track = (Map<String, Object>) items.iterator().next();
                    String trackUri = DataUtil.getString(track, "uri");
                    TrackInfo trackInfo = trackInfoLruCache.get(trackUri);
                    if (trackInfo != null) {
                        listener.onLoaded(trackInfo);
                        return;
                    }
                    trackInfo = createTrackInfo(trackUri, track);
                    trackInfoLruCache.put(trackUri, trackInfo);
                    listener.onLoaded(trackInfo);
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void searchTracks(String queryStr, final OnObjectLoadedListener listener) {
        Map<String, String> query = new HashMap<>();
        query.put("q", queryStr);
        query.put("market", "from_token");
        query.put("type", "track");
        HttpUtil.doGet(SEARCH_URL, authenticationHeader, query, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> tracks = DataUtil.getMap(DataUtil.parseJson(result.result), "tracks");
                    if (tracks == null) {
                        listener.onLoaded(null);
                        return;
                    }
                    Collection<Object> items = DataUtil.getCollection(tracks, "items");
                    if (items == null || items.size() == 0) {
                        listener.onLoaded(null);
                        return;
                    }
                    List<TrackInfo> trackList = new ArrayList<TrackInfo>();
                    Iterator<Object> iit = items.iterator();
                    while (iit.hasNext()) {
                        Map<String, Object> track = (Map<String, Object>) iit.next();
                        String trackUri = DataUtil.getString(track, "uri");
                        TrackInfo trackInfo = trackInfoLruCache.get(trackUri);
                        if (trackInfo != null) {
                            trackList.add(trackInfo);
                        } else {
                            trackInfo = createTrackInfo(trackUri, track);
                            trackInfoLruCache.put(trackUri, trackInfo);
                            trackList.add(trackInfo);
                        }
                    }
                    listener.onLoaded(trackList);
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void searchAlbum(String name, OnObjectLoadedListener listener) {

    }

    @Override
    public void getTrack(final String trackUri, final OnObjectLoadedListener listener) {
        TrackInfo trackInfo = trackInfoLruCache.get(trackUri);
        if (trackInfo != null) {
            listener.onLoaded(trackInfo);
            return;
        }
        HttpUtil.doGet(TRACK_URL + parseId(trackUri), null, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    TrackInfo ti = createTrackInfo(trackUri, DataUtil.parseJson(result.result));
                    trackInfoLruCache.put(trackUri, ti);
                    listener.onLoaded(ti);
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void getAlbum(final String albumUri, final OnObjectLoadedListener listener) {
        AlbumInfo albumInfo = albumInfoLruCache.get(albumUri);
        if (albumInfo != null) {
            listener.onLoaded(albumInfo);
            return;
        }
        HttpUtil.doGet(ALBUM_URL + parseId(albumUri), null, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    AlbumInfo ai = createAlbumInfo(albumUri, DataUtil.parseJson(result.result));
                    albumInfoLruCache.put(albumUri, ai);
                    listener.onLoaded(ai);
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void getArtist(final String artistUri, final OnObjectLoadedListener listener) {
        ArtistInfo artistInfo = artistInfoLruCache.get(artistUri);
        if (artistInfo != null) {
            listener.onLoaded(artistInfo);
            return;
        }
        HttpUtil.doGet(ARTIST_URL + parseId(artistUri), null, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    ArtistInfo ai = createArtistInfo(artistUri, DataUtil.parseJson(result.result));
                    artistInfoLruCache.put(artistUri, ai);
                    listener.onLoaded(ai);
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void getFeaturedPlayLists(final OnObjectLoadedListener listener) {
        Map<String, String> query = new HashMap<>();
        query.put("country", "US");
        query.put("limit", "10");
        HttpUtil.doGet(FEATURED_PLAYLIST_URL, authenticationHeader, query, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> playlists = DataUtil.getMap(DataUtil.parseJson(result.result), "playlists");
                    if (playlists == null) {
                        listener.onLoaded(null);
                        return;
                    }
                    Collection<Object> items = DataUtil.getCollection(playlists, "items");
                    if (items == null || items.size() == 0) {
                        listener.onLoaded(null);
                        return;
                    }
                    ArrayList<PlaylistInfo> infos = new ArrayList<>();
                    Iterator<Object> it = items.iterator();
                    while (it.hasNext()) {
                        Map<String, Object> playlist = (Map<String, Object>) it.next();
                        PlaylistInfo info = new PlaylistInfo(DataUtil.getString(playlist, "uri"));
                        info.setName(DataUtil.getString(playlist, "name"));
                        info.setImageUrl(getImageUrl(DataUtil.getCollection(playlist, "images")));
                        infos.add(info);
                    }
                    listener.onLoaded(infos);
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void getMyPlayLists(final OnObjectLoadedListener listener) {
        HttpUtil.doGet(MY_PLAYLIST_URL, authenticationHeader, null, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Collection<Object> items = DataUtil.getCollection(DataUtil.parseJson(result.result), "items");
                    if (items == null || items.size() == 0) {
                        listener.onLoaded(null);
                        return;
                    }
                    ArrayList<PlaylistInfo> infos = new ArrayList<>();
                    Iterator<Object> it = items.iterator();
                    while (it.hasNext()) {
                        Map<String, Object> playlist = (Map<String, Object>) it.next();
                        PlaylistInfo info = new PlaylistInfo(DataUtil.getString(playlist, "uri"));
                        info.setName(DataUtil.getString(playlist, "name"));
                        info.setImageUrl(getImageUrl(DataUtil.getCollection(playlist, "images")));
                        infos.add(info);
                    }
                    listener.onLoaded(infos);
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void onLoggedIn() {
        if (currentState.isLoggedIn) {
            return;
        }
        OneTouchApplication.getInstance().savePreference(KEY_SPOTIFY_LOGIN_STATUS, true);
        currentState.isLoggedIn = true;
        if (listener != null) {
            listener.onStateChanged(currentState);
        }

        authenticate();
    }

    @Override
    public void onLoggedOut() {
        LogUtil.d("spotify.onLoggedOut");
        currentState.isLoggedIn = false;
        if (listener != null) {
            listener.onStateChanged(currentState);
        }
        try {
            Spotify.destroyPlayer(this);
        } catch (Throwable ignore) {
            ignore.printStackTrace();
        }
        player = null;
    }

    @Override
    public void onLoginFailed(Throwable throwable) {
        LogUtil.d("spotify.onLoginFailed: " + throwable.getMessage());
    }

    @Override
    public void onTemporaryError() {

    }

    @Override
    public void onConnectionMessage(String s) {

    }

    private void updatePlayerState(PlayerState playerState) {
        currentState.isPlaying = playerState.playing;
        currentState.isShuffling = playerState.shuffling;
        currentState.isRepeating = playerState.repeating;
        currentState.currentTrackUri = playerState.trackUri;
        currentState.duration = playerState.durationInMs;
        currentState.position = playerState.positionInMs;
    }

    @Override
    public void onPlaybackEvent(EventType eventType, PlayerState playerState) {
        updatePlayerState(playerState);
        switch (eventType) {
            case PLAY:
                currentState.isPlaying = true;
                break;
            case PAUSE:
                currentState.isPlaying = false;
                break;
            case TRACK_CHANGED:
                currentState.position = 0;
                break;
            case SKIP_NEXT:
                break;
            case SKIP_PREV:
                break;
            case SHUFFLE_ENABLED:
                currentState.isShuffling = true;
                break;
            case SHUFFLE_DISABLED:
                currentState.isShuffling = false;
                break;
            case REPEAT_ENABLED:
                currentState.isRepeating = true;
                break;
            case REPEAT_DISABLED:
                currentState.isRepeating = false;
                break;
            case BECAME_ACTIVE:
                break;
            case BECAME_INACTIVE:
                break;
            case LOST_PERMISSION:
                break;
            case AUDIO_FLUSH:
            case END_OF_CONTEXT:
            case EVENT_UNKNOWN:
            default:
                return;
        }
        if (listener != null) {
            listener.onStateChanged(currentState);
        }
    }

    @Override
    public void onPlaybackError(ErrorType errorType, String s) {
        ErrorCode error;
        switch (errorType) {
            case TRACK_UNAVAILABLE:
                error = createErrorCode(OnRoadErrorCode.MUSIC_ERROR_PLAYBACK_NO_TRACK, s);
                break;
            default:
                error = createErrorCode(OnRoadErrorCode.MUSIC_ERROR_PLAYBACK, s);
                break;
        }
        if (listener != null) {
            listener.onError(error);
        }
    }

    @Override
    public void onPlayerState(PlayerState playerState) {
        updatePlayerState(playerState);
        if (listener != null) {
            listener.onStateChanged(currentState);
        }
    }

    protected ErrorCode handleHttpError(HttpUtil.HttpRequestResult result) {
        if (result.error == null) {
            return null;
        }
        Map<String, Object> data = DataUtil.parseJson(result.result);
        if (data != null) {
            String msg;
            Map<String, Object> errorData = DataUtil.getMap(data, ERROR_KEY);
            if (errorData != null) {
                msg = DataUtil.getString(errorData, ERROR_STATUS_KEY) + ": " + DataUtil.getString(errorData, ERROR_MESSAGE_KEY);
            } else {
                msg = DataUtil.getString(data, ERROR_KEY) + ": " + DataUtil.getString(data, ERROR_DESCRIPTION_KEY);
            }
            result.error.setMessage(msg);
        }
        return amendErrorCode(result.error);

    }
}
