package com.tcl.onetouch.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.tcl.onetouch.onroad.R;

public class CutoutView extends View {

    private int windowX;
    private int windowY;
    private int windowWidth;
    private int windowHeight;

    private Paint fillPaint;
    private Paint windowPaint;

    public CutoutView(Context context) {
        super(context);
        init(null);
    }

    public CutoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CutoutView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public CutoutView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        windowX = 0;
        windowY = 0;
        windowWidth = 0;
        windowHeight = 0;
        fillPaint = new Paint();
        windowPaint = new Paint();
        windowPaint.setColor(getResources().getColor(android.R.color.transparent));
        windowPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        if (attrs == null) {
            return;
        }

        TypedArray a = getContext().getTheme().obtainStyledAttributes(
            attrs,
            R.styleable.CutoutView,
            0, 0);

        try {
            windowX = a.getDimensionPixelSize(R.styleable.CutoutView_windowX, 0);
            windowY = a.getDimensionPixelSize(R.styleable.CutoutView_windowY, 0);
            windowWidth = a.getDimensionPixelSize(R.styleable.CutoutView_windowWidth, 0);
            windowHeight = a.getDimensionPixelSize(R.styleable.CutoutView_windowHeight, 0);
            fillPaint.setColor(a.getColor(R.styleable.CutoutView_fillColor, 0));
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPaint(fillPaint);
        if (windowWidth != 0 && windowHeight != 0) {
            int right = windowX + windowWidth;
            if (windowWidth < 0) {
                right = getWidth();
            }
            int bottom = windowY + windowHeight;
            if (windowHeight < 0) {
                bottom = getHeight();
            }
            canvas.drawRect(new Rect(windowX, windowY, right, bottom), windowPaint);
        }
    }

    public void setWindow(int windowX, int windowY, int windowWidth, int windowHeight) {
        this.windowX = windowX;
        this.windowY = windowY;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        invalidate();
    }
}
