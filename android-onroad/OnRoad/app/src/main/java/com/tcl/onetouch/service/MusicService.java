package com.tcl.onetouch.service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.spotify.SpotifyMusicService;

import java.util.List;

public abstract class MusicService extends OnRoadService {
    public static final int LOGIN_REQUST_CODE = 1337;

    public static class State {
        public boolean isLoggedIn;
        public boolean isPlaying;
        public boolean isShuffling;
        public boolean isRepeating;
        public String currentTrackUri;
        public String currentPlaylistUri;
        public int duration;
        public int position;
    }

    public static interface OnStateChangeListener {
        public void onStateChanged(State state);
        public void onError(ErrorCode error);
    }

    public static interface OnObjectLoadedListener {
        public void onLoaded(Object result);
        public void onError(ErrorCode error);
    }

    public abstract void init(final Context context, OnStateChangeListener listener);
    public abstract void destroy();

    public abstract State getState();
    public abstract void refreshState();

    public abstract boolean isAuthenticated();
    public abstract void login(Activity activity);
    public abstract void logout();
    public abstract void onLoginResult(int resultCode, Intent intent);

    public abstract void playTracks(List<String> tracks);
    public abstract void playTrack(String trackUri);
    public abstract void playPlayList(String playListUri);

    public abstract void queueTrack(String trackUri);
    public abstract void queuePlayList(String playListUri);
    public abstract void clearQueue();

    public abstract void start();
    public abstract void pause();
    public abstract void resume();
    public abstract void stop();

    public abstract void setDuck(boolean isDuck);
    public abstract void setMute(boolean isMute);

    public abstract void skipToNext();
    public abstract void skipToPrevious();

    public abstract void setShuffle(boolean enabled);
    public abstract void setRepeat(boolean enabled);

    public abstract void searchTrack(String name, OnObjectLoadedListener listener);
    public abstract void searchTracks(String query, OnObjectLoadedListener listener);
    public abstract void searchAlbum(String name, OnObjectLoadedListener listener);
    public abstract void getTrack(String trackUri, OnObjectLoadedListener listener);
    public abstract void getAlbum(String albumUri, OnObjectLoadedListener listener);
    public abstract void getArtist(String artistUri, OnObjectLoadedListener listener);
    public abstract void getFeaturedPlayLists(OnObjectLoadedListener listener);
    public abstract void getMyPlayLists(OnObjectLoadedListener listener);

    public static MusicService sDefaultService;

    public static MusicService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new SpotifyMusicService();
        }
        return sDefaultService;
    }

}
