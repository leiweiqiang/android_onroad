package com.tcl.onetouch.onroad;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.location.places.AutocompletePrediction;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.base.QnATask;
import com.tcl.onetouch.model.ActionLog;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.MapCoordinate;
import com.tcl.onetouch.model.NavigationInfo;
import com.tcl.onetouch.model.ParkingInfo;
import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.service.GeoService;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.service.MapService;
import com.tcl.onetouch.service.NavigationService;
import com.tcl.onetouch.service.NluService;
import com.tcl.onetouch.service.TtsService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.StringUtil;
import com.tcl.onetouch.util.SystemUtil;
import com.tcl.onetouch.view.CustomEditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class NavigationFragment extends ContentFragment implements View.OnClickListener {
    public static final String TAG = NavigationFragment.class.getName();
    private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
    private static final int AUTOCOMPLETE_LIMIT = 3;
    private static final int POI_RESULT_LIMIT = 6;

    private static final int DESTINATION_LIST_NONE = 0;
    private static final int DESTINATION_LIST_AUTOCOMPLETE = 1;
    private static final int DESTINATION_LIST_HISTORY = 2;
    private static final int DESTINATION_LIST_BOOKMARKS = 3;

    private View searchContainer;
    private View searchHeader;
    private CustomEditText searchText;
    private ListView destinationList;
    private View poiListPanel;
    private ListView poiListView;

    private View favoritePanel;
    private View historyButton;
    private View bookmarksButton;

    private View routingContainer;
    private View routingHeader;
    private View routingInfoPanel;
    private View startNavigationButton;
    private View routingSearchHeader;

    private View navigationContainer;
    private View navigationInfoPanel;
    private View arrivalInfoPanel;

//    private WebView webView;

    private QnATask startNavigationQnATask;
    private QnATask selectDestinationQnATask;

    private int destinationListContent = DESTINATION_LIST_NONE;
    private List<AutocompletePrediction> autocompletePredictions;
    private List<ActionLog> actionLogs;
    private List<DestinationItem> destinationItems;

    private List<PlaceInfo> destinations;
    private PlaceInfo destination;
    private int routeType = NavigationInfo.ROUTE_TYPE_CAR_FASTEST;

    private static class DestinationItem {
        int iconResId = 0;
        CharSequence primaryText = null;
        CharSequence secondaryText = null;
        String placeId = null;
        int logId = 0;
    }

    private TextWatcher autocompleteTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            poiListPanel.setVisibility(View.GONE);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            autocompletePredictions = null;
            if (s.length() >= 2) {
                destinationListContent = DESTINATION_LIST_AUTOCOMPLETE;
                destinationList.setVisibility(View.VISIBLE);
                refreshAutocompleteItems();
                GeoService.getDefaultService().getAutocompletePredictions(
                    s.toString(),
                    LocationService.getDefaultService().getLastLocation(),
                    new GeoService.AutocompleteResultListener() {
                        @Override
                        public void onResult(List<AutocompletePrediction> results) {
                            if (destinationListContent == DESTINATION_LIST_AUTOCOMPLETE) {
                                autocompletePredictions = results;
                                refreshAutocompleteItems();
                            }
                        }

                        @Override
                        public void onError(ErrorCode error) {
                            if (destinationListContent == DESTINATION_LIST_AUTOCOMPLETE) {
                                OneTouchApplication.getInstance().handleError(error);
                            }
                        }
                    });
            } else {
                destinationListContent = DESTINATION_LIST_NONE;
                destinationList.setVisibility(View.INVISIBLE);
                if (s.length() == 0 && searchText.isKeyboardShown()) {
                    showFavoritePanel();
                } else {
                    favoritePanel.setVisibility(View.GONE);
                }
            }
        }
    };

    private BaseAdapter poiListAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return destinations != null ? destinations.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return destinations.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = NavigationFragment.this.getActivity().getLayoutInflater().inflate(
                    R.layout.element_poi_list_item, parent, false);
            }
            PlaceInfo place = destinations.get(position);
            Location currentLocation = LocationService.getDefaultService().getLastLocation();
            Location placeLocation = place.getLocation();
            float[] rs = new float[1];
            Location.distanceBetween(currentLocation.getLatitude(), currentLocation.getLongitude(),
                placeLocation.getLatitude(), placeLocation.getLongitude(), rs);
            convertView.setTag(place);
            ((TextView) convertView.findViewById(R.id.item_text)).setText(String.valueOf(position + 1));
            ((TextView) convertView.findViewById(R.id.poi_name_text)).setText(place.getName());
            ((TextView) convertView.findViewById(R.id.poi_location_text)).setText(place.getAddressInfo().getShortAddress());
            ((TextView) convertView.findViewById(R.id.poi_distance_text)).setText(StringUtil.formatDistance((int) rs[0]));
            RatingBar ratingBar = (RatingBar) convertView.findViewById(R.id.poi_rating_bar);
            ratingBar.setRating(place.getRating());
            LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
            stars.getDrawable(0).setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.poi_star_rating), PorterDuff.Mode.SRC_ATOP);
            convertView.setOnClickListener(destinationAnnotationListener);
            return convertView;
        }
    };

    private int deletingLogId = -1;
    private BaseAdapter destinationListAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return destinationItems.size();
        }

        @Override
        public Object getItem(int position) {
            return destinationItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = NavigationFragment.this.getActivity().getLayoutInflater().inflate(
                    R.layout.element_destination_list_item, parent, false);
            }
            View deleteButton = convertView.findViewById(R.id.delete_button);
            TextView itemText = (TextView) convertView.findViewById(R.id.item_text);
            itemText.setText(String.valueOf(position + 1));
            DestinationItem item = destinationItems.get(position);
            ImageView iconImg = (ImageView) convertView.findViewById(R.id.item_icon);
            if (item.iconResId == 0) {
                iconImg.setVisibility(View.GONE);
            } else {
                iconImg.setVisibility(View.VISIBLE);
                iconImg.setImageResource(item.iconResId);
            }
            CharSequence primaryText = item.primaryText;
            TextView textView = (TextView) convertView.findViewById(R.id.primary_text);
            if (primaryText != null && primaryText.length() > 0) {
                textView.setVisibility(View.VISIBLE);
                textView.setText(primaryText);
            } else {
                textView.setVisibility(View.GONE);
                textView.setText("");
            }
            CharSequence secondaryText = item.secondaryText;
            textView = (TextView) convertView.findViewById(R.id.secondary_text);
            if (secondaryText != null && secondaryText.length() > 0) {
                textView.setVisibility(View.VISIBLE);
                textView.setText(secondaryText);
            } else {
                textView.setVisibility(View.GONE);
                textView.setText("");
            }
            if (item.logId == deletingLogId) {
                deleteButton.setVisibility(View.VISIBLE);
                itemText.setVisibility(View.GONE);
                deleteButton.setTag(deletingLogId);
                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int logId = (Integer) v.getTag();
                        ActionLogManager.getInstance().deleteLog(logId, new ActionLogManager.UpdateResultListener() {
                            @Override
                            public void onResult(int result) {
                                if (result > 0) {
                                    Iterator<DestinationItem> it = destinationItems.iterator();
                                    while (it.hasNext()) {
                                        if (it.next().logId == result) {
                                            it.remove();
                                            destinationListAdapter.notifyDataSetChanged();
                                            return;
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onError(ErrorCode errorCode) {

                            }
                        });
                    }
                });
            } else {
                deleteButton.setVisibility(View.GONE);
                itemText.setVisibility(View.VISIBLE);
                deleteButton.setOnClickListener(null);
            }
            View detailsPanel = convertView.findViewById(R.id.details_panel);
            detailsPanel.setTag(item);
            detailsPanel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (destinationListContent == DESTINATION_LIST_AUTOCOMPLETE) {
                        destinationList.setVisibility(View.INVISIBLE);
                    }
                    searchText.hideKeyboard();
                    DestinationItem item = (DestinationItem) v.getTag();
                    autocompletePredictions = null;
                    if (item.placeId != null) {
                        searchPlace(item.placeId);
                    } else {
                        searchQuery(item.primaryText.toString());
                    }
                }
            });
            if (item.logId > 0) {
                detailsPanel.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        DestinationItem item = (DestinationItem) v.getTag();
                        if (item.logId == deletingLogId) {
                            deletingLogId = -1;
                            destinationListAdapter.notifyDataSetChanged();
                        } else {
                            deletingLogId = item.logId;
                            destinationListAdapter.notifyDataSetChanged();
                        }
                        return true;
                    }
                });
            } else {
                detailsPanel.setOnLongClickListener(null);
                detailsPanel.setLongClickable(false);
            }
            return convertView;
        }
    };

    private View.OnClickListener destinationAnnotationListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            selectDestinationQnATask.end();
            PlaceInfo place = (PlaceInfo) v.getTag();
            if (place != null) {
                selectDestination(place);
            }
        }
    };

    private int firstItem;
    private int lastItem;
    private Runnable addDestinationAnnotationsTask = new Runnable() {
        @Override
        public void run() {
            if (destinations == null || destinations.isEmpty()) {
                deleteDestinationAnnotations();
                return;
            }
            int newFirstItem = poiListView.getFirstVisiblePosition();
            int newLastItem = poiListView.getLastVisiblePosition();
            View firstView = poiListView.getChildAt(0);
            if (((firstView.getHeight() + firstView.getY()) / firstView.getHeight()) < 0.7) {
                ++newFirstItem;
            }
            View lastView = poiListView.getChildAt(poiListView.getChildCount() - 1);
            if (((poiListView.getHeight() - lastView.getY()) / lastView.getHeight()) < 0.4) {
                --newLastItem;
            }
            if (newFirstItem == firstItem && newLastItem == lastItem) {
                return;
            }
            deleteDestinationAnnotations();
            firstItem = newFirstItem;
            lastItem = newLastItem;
            Location currLoc = LocationService.getDefaultService().getLastLocation();
            double southLat = currLoc.getLatitude(),
                northLat = currLoc.getLatitude(),
                westLng = currLoc.getLongitude(),
                eastLng = currLoc.getLongitude();
            for (int i = firstItem; i <= lastItem; ++i) {
                PlaceInfo place = destinations.get(i);
                MainActivity.getInstance().getMapFragment().addDestinationOptionAnnotation(
                    place, place.getId().hashCode(), i, R.drawable.icon_destination_option,
                    destinationAnnotationListener);
                double placeLat = place.getLocation().getLatitude(),
                    placeLng = place.getLocation().getLongitude();
                if (placeLat < southLat) {
                    southLat = placeLat;
                } else if (placeLat > northLat) {
                    northLat = placeLat;
                }
                if (placeLng < westLng) {
                    westLng = placeLng;
                } else if (placeLng > eastLng) {
                    eastLng = placeLng;
                }
            }
            if (southLat == northLat && westLng == eastLng) {
                return;
            }
            Resources rs = getActivity().getResources();
            int margin = rs.getDimensionPixelSize(R.dimen.map_fit_padding);
            int destinationAnnotationHeight = rs.getDimensionPixelSize(R.dimen.map_annotation_destination_height),
                startAnnotationWidth = rs.getDimensionPixelSize(R.dimen.map_annotation_start_width),
                startAnnotationHeight = rs.getDimensionPixelSize(R.dimen.map_annotation_start_height);
            int marginTop = margin + destinationAnnotationHeight,
                marginBottom = margin + startAnnotationHeight / 2,
                marginLeft = margin + startAnnotationWidth / 2,
                marginRight = margin + startAnnotationWidth / 2;
            int x = poiListView.getRight(), y = searchHeader.getBottom(),
                viewWidth = getView().getWidth() - x - marginLeft - marginRight,
                viewHeight = getView().getHeight() - y - marginTop - marginBottom;
            int[] viewLoc = new int[2];
            getView().getLocationInWindow(viewLoc);
            x += viewLoc[0];
            y += viewLoc[1];
            double[] scales = new double[2];
            MainActivity.getInstance().getMapFragment().calculateScale(
                southLat, westLng, northLat, eastLng, viewWidth, viewHeight, scales);
            double centerLat = (southLat + northLat) / 2.0d, centerLng = (westLng + eastLng) / 2.0d;
            double leftLng = centerLng - scales[1] * (viewWidth / 2.0d + marginLeft + x),
                rightLng = centerLng + scales[1] * (viewWidth / 2.0d + marginRight),
                bottomLat = centerLat - scales[0] * (viewHeight / 2.0d + marginBottom),
                topLat = centerLat + scales[0] * (viewHeight / 2.0d + marginTop + y);
            MapService.getDefaultService().setBearing(0f, 0);
            MapService.getDefaultService().fitRegion(topLat, leftLng, bottomLat, rightLng);
        }
    };

    private int mapLeftPadding;
    private boolean hasPromptedNavigation;

    private CharSequence findMatch(String text, String query) {
        if (text.isEmpty() || query.isEmpty()) {
            return null;
        }
        String lowerText = text.toLowerCase();
        if (lowerText.startsWith(query)) {
            SpannableString match = new SpannableString(text);
            match.setSpan(STYLE_BOLD, 0, query.length(), 0);
            return match;
        }
        int index = lowerText.indexOf(" " + query);
        if (index != -1) {
            SpannableString match = new SpannableString(text);
            match.setSpan(STYLE_BOLD, index + 1, index + 1 + query.length(), 0);
            return match;
        }
        return null;
    }

    private int getBookmarkDrawableResId(String bookmark, boolean color) {
        if (bookmark == null) {
            return R.drawable.icon_bookmark_blank;
        } else if (bookmark.equalsIgnoreCase(getResources().getString(R.string.home))) {
            return color ? R.drawable.icon_home_color : R.drawable.icon_home_black;
        } else if (bookmark.equalsIgnoreCase(getResources().getString(R.string.work))) {
            return color ? R.drawable.icon_work_color : R.drawable.icon_work_black;
        } else {
            return color ? R.drawable.icon_bookmark_color : R.drawable.icon_bookmark_black;
        }
    }

    private void refreshAutocompleteItems() {
        destinationItems.clear();
        deletingLogId = -1;
        String query = searchText.getText().toString().toLowerCase();
        Set<String> addedPlaceIds = new HashSet<>();
        if (actionLogs != null) {
            for (ActionLog actionLog : actionLogs) {
                if (actionLog.getType() != ActionLog.ACTION_TYPE_BOOKMARK) {
                    continue;
                }
                String bookmark = DataUtil.getString(actionLog.getLog(), "bookmark");
                CharSequence match = findMatch(bookmark, query);
                if (match != null) {
                    DestinationItem item = new DestinationItem();
                    item.iconResId = getBookmarkDrawableResId(bookmark, false);
                    item.placeId = DataUtil.getString(actionLog.getLog(), "id");
                    item.primaryText = match;
                    item.secondaryText = DataUtil.getString(actionLog.getLog(), "address");
                    destinationItems.add(item);
                    addedPlaceIds.add(item.placeId);
                }
            }
            for (ActionLog actionLog : actionLogs) {
                if (actionLog.getType() != ActionLog.ACTION_TYPE_GEO_QUERY) {
                    continue;
                }
                String logQuery = DataUtil.getString(actionLog.getLog(), "query");
                CharSequence match = findMatch(logQuery, query);
                if (match != null) {
                    DestinationItem item = new DestinationItem();
                    item.iconResId = R.drawable.icon_history_black;
                    item.primaryText = match;
                    item.logId = actionLog.getId();
                    destinationItems.add(item);
                }
            }
        }
        if (autocompletePredictions != null) {
            if (actionLogs != null) {
                for (ActionLog actionLog : actionLogs) {
                    if (actionLog.getType() != ActionLog.ACTION_TYPE_DESTINATION &&
                        actionLog.getType() != ActionLog.ACTION_TYPE_BOOKMARK) {
                        continue;
                    }
                    String placeId = DataUtil.getString(actionLog.getLog(), "id");
                    if (addedPlaceIds.contains(placeId)) {
                        continue;
                    }
                    for (AutocompletePrediction prediction : autocompletePredictions) {
                        if (placeId.equals(prediction.getPlaceId())) {
                            DestinationItem item = new DestinationItem();
                            if (actionLog.getType() == ActionLog.ACTION_TYPE_BOOKMARK) {
                                String bookmark = DataUtil.getString(actionLog.getLog(), "bookmark");
                                item.iconResId = getBookmarkDrawableResId(bookmark, false);
                            }
                            item.primaryText = prediction.getPrimaryText(STYLE_BOLD);
                            item.secondaryText = prediction.getSecondaryText(STYLE_BOLD);
                            item.placeId = prediction.getPlaceId();
                            destinationItems.add(item);
                            addedPlaceIds.add(placeId);
                            break;
                        }
                    }
                }
            }
            for (AutocompletePrediction prediction : autocompletePredictions) {
                if (!addedPlaceIds.contains(prediction.getPlaceId())) {
                    DestinationItem item = new DestinationItem();
                    item.primaryText = prediction.getPrimaryText(STYLE_BOLD);
                    item.secondaryText = prediction.getSecondaryText(STYLE_BOLD);
                    item.placeId = prediction.getPlaceId();
                    destinationItems.add(item);
                }
            }
        }
        destinationListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        destinationItems = new ArrayList<>();
        firstItem = -1;
        lastItem = -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation, container, false);

        searchContainer = view.findViewById(R.id.search_container);
        searchHeader = searchContainer.findViewById(R.id.search_header);
        searchText = (CustomEditText) searchHeader.findViewById(R.id.search_text);
        searchText.addTextChangedListener(autocompleteTextWatcher);
        searchText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
                    doSearch();
                    return true;
                }
                return false;
            }
        });
        searchText.setKeyboardListener(new CustomEditText.KeyboardListener() {
            @Override
            public void onKeyboardShown() {
                OneTouchApplication.getInstance().disableWakeupService(TAG, null);
                ActionLogManager.getInstance().getLog(
                    new int[]{
                        ActionLog.ACTION_TYPE_GEO_QUERY,
                        ActionLog.ACTION_TYPE_DESTINATION,
                        ActionLog.ACTION_TYPE_BOOKMARK},
                    new ActionLogManager.QueryResultListener() {
                        @Override
                        public void onResult(List<ActionLog> results) {
                            actionLogs = results;
                        }

                        @Override
                        public void onError(ErrorCode errorCode) {

                        }
                    });
                if (searchText.getText().length() == 0) {
                    showFavoritePanel();
                    destinationList.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onKeyboardHidden() {
                SystemUtil.postToMainThread(new Runnable() {
                    @Override
                    public void run() {
                        OneTouchApplication.getInstance().enableWakeupService(TAG);
                    }
                });
            }
        });

        searchHeader.findViewById(R.id.search_button).setOnClickListener(this);
        searchHeader.findViewById(R.id.cancel_search_button).setOnClickListener(this);

        destinationList = (ListView) view.findViewById(R.id.item_list);
        destinationList.setAdapter(destinationListAdapter);

        poiListPanel = searchContainer.findViewById(R.id.poi_list_panel);
        poiListView = (ListView) poiListPanel.findViewById(R.id.poi_list);
        poiListView.setAdapter(poiListAdapter);
        poiListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState != SCROLL_STATE_IDLE) {
                    return;
                }
                SystemUtil.postToMainThread(addDestinationAnnotationsTask);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        favoritePanel = searchContainer.findViewById(R.id.favorite_panel);
        historyButton = favoritePanel.findViewById(R.id.history_button);
        historyButton.setOnClickListener(this);
        bookmarksButton = favoritePanel.findViewById(R.id.bookmark_button);
        bookmarksButton.setOnClickListener(this);

        routingContainer = view.findViewById(R.id.routing_container);
        routingHeader = routingContainer.findViewById(R.id.routing_header);
        routingHeader.findViewById(R.id.cancel_routing_button).setOnClickListener(this);

        routingInfoPanel = routingContainer.findViewById(R.id.route_info_panel);
        routingInfoPanel.findViewById(R.id.prev_route_button).setOnClickListener(this);
        routingInfoPanel.findViewById(R.id.next_route_button).setOnClickListener(this);
        startNavigationButton = routingInfoPanel.findViewById(R.id.start_navigation_button);
        startNavigationButton.setOnClickListener(this);
        routingSearchHeader = routingContainer.findViewById(R.id.routing_search_header);
        routingSearchHeader.findViewById(R.id.routing_search_button).setOnClickListener(this);
        routingSearchHeader.findViewById(R.id.routing_search_text).setOnClickListener(this);
        routingSearchHeader.findViewById(R.id.cancel_routing_search_button).setOnClickListener(this);
        routingContainer.findViewById(R.id.find_parking_button).setOnClickListener(this);


        navigationContainer = view.findViewById(R.id.navigation_container);
        navigationContainer.findViewById(R.id.cancel_navigation_button).setOnClickListener(this);
        navigationInfoPanel = navigationContainer.findViewById(R.id.navigation_info_panel);
        arrivalInfoPanel = navigationContainer.findViewById(R.id.arrival_info_panel);

//        webView = (WebView) navigationContainer.findViewById(R.id.web_view);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.setWebViewClient(new WebViewClient());

        searchContainer.setVisibility(View.VISIBLE);
//        webView.setVisibility(View.GONE);
        searchText.setText("");
        poiListPanel.setVisibility(View.GONE);
        destinationList.setVisibility(View.INVISIBLE);
        favoritePanel.setVisibility(View.GONE);
        routingContainer.setVisibility(View.GONE);
        navigationContainer.setVisibility(View.GONE);

//        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            boolean poiListVisible = false;
//            @Override
//            public void onGlobalLayout() {
//                if (poiListPanel != null) {
//                    if (poiListPanel.getVisibility() == View.VISIBLE) {
//                        if (!poiListVisible) {
//                            poiListVisible = true;
//                            int[] panelLoc = new int[2];
//                            poiListPanel.getLocationInWindow(panelLoc);
//                            int[] listLoc = new int[2];
//                            poiListView.getLocationInWindow(listLoc);
//                            MainActivity.getInstance().getMapFragment().showCutout(
//                                panelLoc[0] + poiListView.getWidth(), panelLoc[1],
//                                poiListPanel.getWidth() - poiListView.getWidth() - 10,
//                                poiListPanel.getHeight());
//                        }
//                    } else {
//                        if (poiListVisible) {
//                            poiListVisible = false;
//                            MainActivity.getInstance().getMapFragment().hideCutout();
//                        }
//                    }
//                }
//            }
//        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        int[] loc = new int[2];
        getView().getLocationInWindow(loc);
        mapLeftPadding = loc[0];
        updateNavigationInfo(NavigationService.getDefaultService().getCurrentNavigationInfo());
        startNavigationQnATask = new QnATask().
            setSource(TAG).
            setQuestion(R.string.tts_start_navigation).
            setYesNoResultHandler(NluService.YES_NO_MODE_NAVIGATION, new QnATask.ResultHandler() {
                @Override
                public void onResult(Map<String, Object> result) {
                    if (DataUtil.getBoolean(result, NluService.RESULT_KEY_RESULT)) {
                        startNavigationButton.setPressed(true);
                        startNavigationButton.invalidate();
                        startNavigationButton.post(new Runnable() {
                            @Override
                            public void run() {
                                startNavigationButton.setPressed(false);
                                startNavigationButton.invalidate();
                                startNavigationButton.performClick();
                            }
                        });
                    } else {
                        MainActivity.getInstance().cancelNavigation();
                    }
                }
            });
        selectDestinationQnATask = new QnATask().
            setSource(TAG).
            setQuestion(R.string.tts_choose_destination).
            setSelectionResultHandler(new QnATask.ResultHandler() {
                @Override
                public void onResult(Map<String, Object> result) {
                    int selected = DataUtil.getInt(result, NluService.RESULT_KEY_RESULT);
                    final View selectedView = poiListView.getChildAt((selected - 1) - poiListView.getFirstVisiblePosition());
                    if (selectedView != null) {
                        selectedView.setPressed(true);
                        selectedView.invalidate();
                        selectedView.post(new Runnable() {
                            @Override
                            public void run() {
                                selectedView.setPressed(false);
                                selectedView.invalidate();
                                selectedView.performClick();
                            }
                        });
                    }
                }
            });
        hasPromptedNavigation = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        searchText.hideKeyboard();
    }

    @Override
    public void onStop() {
        super.onStop();
        deleteDestinationAnnotations();
        destinations = null;
        destination = null;
        startNavigationQnATask = null;
        selectDestinationQnATask = null;
        autocompletePredictions = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        searchContainer = null;
        searchHeader = null;
        searchText = null;
        destinationList = null;
        poiListPanel = null;
        poiListView = null;

        favoritePanel = null;
        historyButton = null;
        bookmarksButton = null;

        routingContainer = null;
        routingHeader = null;
        routingInfoPanel = null;
        startNavigationButton = null;
        routingSearchHeader = null;

        navigationContainer = null;
        navigationInfoPanel = null;
        arrivalInfoPanel = null;
    }

    private boolean showParkingOption(NavigationInfo navigationInfo) {
        return !navigationInfo.isToParking() &&
            navigationInfo.getRouteType() != NavigationInfo.ROUTE_TYPE_PEDESTRIAN &&
            navigationInfo.isParkingAvailable();
    }

    private static void setDirectionImage(ImageView imageView, int direction) {
        switch (direction) {
            case NavigationInfo.DIRECTION_STRAIGHT_AHEAD:
                imageView.setImageResource(R.drawable.img_direction_straight_ahead);
                break;
            case NavigationInfo.DIRECTION_SLIGHT_RIGHT:
                imageView.setImageResource(R.drawable.img_direction_slight_right);
                break;
            case NavigationInfo.DIRECTION_SLIGHT_LEFT:
                imageView.setImageResource(R.drawable.img_direction_slight_left);
                break;
            case NavigationInfo.DIRECTION_LEFT:
                imageView.setImageResource(R.drawable.img_direction_left);
                break;
            case NavigationInfo.DIRECTION_RIGHT:
                imageView.setImageResource(R.drawable.img_direction_right);
                break;
            case NavigationInfo.DIRECTION_HARD_RIGHT:
                imageView.setImageResource(R.drawable.img_direction_right);
                break;
            case NavigationInfo.DIRECTION_HARD_LEFT:
                imageView.setImageResource(R.drawable.img_direction_left);
                break;
            case NavigationInfo.DIRECTION_U_TURN:
                imageView.setImageResource(R.drawable.img_direction_u_turn);
                break;
            case NavigationInfo.DIRECTION_T_STREET:
                imageView.setImageResource(R.drawable.img_direction_t_street);
                break;
            case NavigationInfo.DIRECTION_BIFURCATION:
                imageView.setImageResource(R.drawable.img_direction_bifurcation);
                break;
            case NavigationInfo.DIRECTION_ROUNDABOUT:
                imageView.setImageResource(R.drawable.img_direction_roundabout);
                break;
            default:
                imageView.setImageBitmap(null);
        }
    }

    public void updateNavigationInfo(final NavigationInfo navigationInfo) {
        if (getView() == null) {
            return;
        }
        int[] loc = new int[2];
        if (navigationInfo == null) {
            deleteDestinationAnnotations();
            searchContainer.setVisibility(View.VISIBLE);
            routingContainer.setVisibility(View.GONE);
            navigationContainer.setVisibility(View.GONE);
            destination = null;
            if (poiListPanel.getVisibility() == View.VISIBLE) {
                SystemUtil.postToMainThread(addDestinationAnnotationsTask);
            }
            getView().getLocationInWindow(loc);
            hasPromptedNavigation = false;
        } else if (navigationInfo.isRouteReady()) {
            searchContainer.setVisibility(View.GONE);
            routingContainer.setVisibility(View.VISIBLE);
            navigationContainer.setVisibility(View.GONE);
            routingHeader.setVisibility(View.GONE);
            routingInfoPanel.setVisibility(View.VISIBLE);
            routingSearchHeader.setVisibility(View.VISIBLE);

            if (showParkingOption(navigationInfo)) {
                routingContainer.findViewById(R.id.find_parking_button).setVisibility(View.VISIBLE);
            } else {
                routingContainer.findViewById(R.id.find_parking_button).setVisibility(View.GONE);
            }

            String query = searchText.getText().toString();
            if (query.isEmpty()) {
                query = navigationInfo.getDestination().getDisplayName();
            }
            if (query.isEmpty()) {
                query = navigationInfo.getDestination().getAddressInfo().getShortAddress();
            }
            ((TextView) routingSearchHeader.findViewById(R.id.routing_search_text)).setText(query);

            int selectedRoute = NavigationService.getDefaultService().getSelectedRoute() + 1;
            int numRoutes = NavigationService.getDefaultService().getNumberOfRoutes();
            ((TextView) routingInfoPanel.findViewById(R.id.selected_route_text)).setText(
                selectedRoute + "/" + numRoutes);

            View prevRouteButton = routingInfoPanel.findViewById(R.id.prev_route_button);
            if (selectedRoute <= 1) {
                prevRouteButton.setEnabled(false);
                prevRouteButton.setAlpha(0.2f);
            } else {
                prevRouteButton.setEnabled(true);
                prevRouteButton.setAlpha(1.0f);
            }
            View nextRouteButton = routingInfoPanel.findViewById(R.id.next_route_button);
            if (selectedRoute >= numRoutes) {
                nextRouteButton.setEnabled(false);
                nextRouteButton.setAlpha(0.2f);
            } else {
                nextRouteButton.setEnabled(true);
                nextRouteButton.setAlpha(1.0f);
            }

            TextView routeInfoTextView = ((TextView) routingInfoPanel.findViewById(R.id.route_info_text));
            routeInfoTextView.setText("via " + navigationInfo.getRouteSummary());
            routeInfoTextView.setSelected(true);

            String travelTime = StringUtil.formatDuration(navigationInfo.getTravelTimeInSec());
            int idx = travelTime.indexOf(' ');
            ((TextView) routingInfoPanel.findViewById(R.id.travel_time_text)).
                setText(travelTime.substring(0, idx));
            ((TextView) routingInfoPanel.findViewById(R.id.travel_time_unit_text)).
                setText(travelTime.substring(idx + 1));
            String distance = StringUtil.formatDistance(navigationInfo.getDistanceToDestinationInMeter());
            idx = distance.indexOf(' ');
            ((TextView) routingInfoPanel.findViewById(R.id.travel_distance_text)).
                setText(distance.substring(0, idx));
            ((TextView) routingInfoPanel.findViewById(R.id.travel_distance_unit_text)).
                setText(distance.substring(idx + 1));
            ((TextView) routingInfoPanel.findViewById(R.id.arrival_time_text)).
                setText(StringUtil.formatArrivalTime(navigationInfo.getArrivalTime()));

            SystemUtil.postToMainThread(new Runnable() {
                @Override
                public void run() {
                    double[][] boundingBox = NavigationService.getDefaultService().getRouteBoundingBox();
                    if (boundingBox == null) {
                        return;
                    }
                    double southLat = boundingBox[0][0],
                        northLat = boundingBox[1][0],
                        westLng = boundingBox[0][1],
                        eastLng = boundingBox[1][1];
                    Location startLoc = LocationService.getDefaultService().getLastLocation();
                    Location destinationLoc = navigationInfo.getNavigationDestination().getLocation();
                    Resources rs = getActivity().getResources();
                    int margin = rs.getDimensionPixelSize(R.dimen.map_fit_padding);
                    int destinationAnnotationWidth = rs.getDimensionPixelSize(R.dimen.map_annotation_destination_width),
                        destinationAnnotationHeight = rs.getDimensionPixelSize(R.dimen.map_annotation_destination_height),
                        startAnnotationWidth = rs.getDimensionPixelSize(R.dimen.map_annotation_start_width),
                        startAnnotationHeight = rs.getDimensionPixelSize(R.dimen.map_annotation_start_height);
                    int x = routingInfoPanel.getRight(), y = routingSearchHeader.getBottom(),
                        viewWidth = getView().getWidth() - x - 2 * margin,
                        viewHeight = getView().getHeight() - y - 2 * margin;
                    int[] viewLoc = new int[2];
                    getView().getLocationInWindow(viewLoc);
                    x += viewLoc[0];
                    y += viewLoc[1];
                    double[] scales = new double[2];
                    MainActivity.getInstance().getMapFragment().calculateScale(
                        southLat, westLng, northLat, eastLng, viewWidth, viewHeight, scales);
                    boolean refit = false;
                    double destinationLat = destinationLoc.getLatitude() + destinationAnnotationHeight * scales[0],
                        destinationWestLng = destinationLoc.getLongitude() - destinationAnnotationWidth * scales[1] / 2.0d,
                        destinationEastLng = destinationLoc.getLongitude() + destinationAnnotationWidth * scales[1] / 2.0d,
                        startNorthLat = startLoc.getLatitude() + startAnnotationHeight * scales[0] / 2.0d,
                        startSouthLat = startLoc.getLatitude() - startAnnotationHeight * scales[0] / 2.0d,
                        startWestLng = startLoc.getLongitude() - startAnnotationWidth * scales[1] / 2.0d,
                        startEastLng = startLoc.getLongitude() + startAnnotationWidth * scales[1] / 2.0d;
                    if (destinationLat > northLat) {
                        northLat = destinationLat;
                        refit = true;
                    }
                    if (destinationWestLng < westLng) {
                        westLng = destinationWestLng;
                        refit = true;
                    }
                    if (destinationEastLng > eastLng) {
                        eastLng = destinationEastLng;
                        refit = true;
                    }
                    if (startNorthLat > northLat) {
                        northLat = startNorthLat;
                        refit = true;
                    }
                    if (startSouthLat < southLat) {
                        southLat = startSouthLat;
                        refit = true;
                    }
                    if (startWestLng < westLng) {
                        westLng = startWestLng;
                        refit = true;
                    }
                    if (startEastLng > eastLng) {
                        eastLng = startEastLng;
                        refit = true;
                    }
                    if (refit) {
                        MainActivity.getInstance().getMapFragment().calculateScale(
                            southLat, westLng, northLat, eastLng, viewWidth, viewHeight, scales);
                    }
                    double centerLat = (southLat + northLat) / 2.0d, centerLng = (westLng + eastLng) / 2.0d;
                    double leftLng = centerLng - scales[1] * (viewWidth / 2.0d + margin + x),
                        rightLng = centerLng + scales[1] * (viewWidth / 2.0d + margin),
                        bottomLat = centerLat - scales[0] * (viewHeight / 2.0d + margin),
                        topLat = centerLat + scales[0] * (viewHeight / 2.0d + margin + y);
                    MapService.getDefaultService().setBearing(0f, 0);
                    MapService.getDefaultService().fitRegion(topLat, leftLng, bottomLat, rightLng);
                }
            });
            routingInfoPanel.getLocationInWindow(loc);
            loc[0] += routingInfoPanel.getWidth();
            promptNavigation();
        } else if (navigationInfo.isNavigating()) {
            searchContainer.setVisibility(View.GONE);
            routingContainer.setVisibility(View.GONE);
            navigationContainer.setVisibility(View.VISIBLE);
            navigationInfoPanel.setVisibility(View.VISIBLE);
            arrivalInfoPanel.setVisibility(View.GONE);

            if (showParkingOption(navigationInfo)) {
                navigationContainer.findViewById(R.id.find_parking_button).setVisibility(View.VISIBLE);
            } else {
                navigationContainer.findViewById(R.id.find_parking_button).setVisibility(View.GONE);
            }

            String travelTime = StringUtil.formatDuration(navigationInfo.getTravelTimeInSec());
            int idx = travelTime.indexOf(' ');
            ((TextView) navigationInfoPanel.findViewById(R.id.travel_time_text)).
                setText(travelTime.substring(0, idx));
            ((TextView) navigationInfoPanel.findViewById(R.id.travel_time_unit_text)).
                setText(travelTime.substring(idx + 1));
            String distance = StringUtil.formatDistance(navigationInfo.getDistanceToDestinationInMeter());
            idx = distance.indexOf(' ');
            ((TextView) navigationInfoPanel.findViewById(R.id.travel_distance_text)).
                setText(distance.substring(0, idx));
            ((TextView) navigationInfoPanel.findViewById(R.id.travel_distance_unit_text)).
                setText(distance.substring(idx + 1));
            ((TextView) navigationInfoPanel.findViewById(R.id.arrival_time_text)).
                setText(StringUtil.formatArrivalTime(navigationInfo.getArrivalTime()));

            NavigationInfo.Advice advice = navigationInfo.getCurrentAdvice();
            setDirectionImage((ImageView) navigationInfoPanel.findViewById(R.id.advice_img), advice.direction);

            ((TextView) navigationInfoPanel.findViewById(R.id.advice_distance_text)).
                setText(StringUtil.convertAndFormatDistance(advice.distance, StringUtil.DISTANCE_UNIT_MILES_FEET));
            StringBuilder sb = new StringBuilder();
            if (advice.exitNumber != null && !advice.exitNumber.isEmpty()) {
                sb.append(advice.exitNumber);
            }
            if (advice.streetName != null && !advice.streetName.isEmpty()) {
                if (sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(advice.streetName);
            }
            ((TextView) navigationInfoPanel.findViewById(R.id.advice_street_text)).setText(sb.toString());

            NavigationInfo.Advice nextAdvice = navigationInfo.getNextAdvice();
            View nextAdvicePanel = navigationInfoPanel.findViewById(R.id.next_advice_panel);
            if (nextAdvice.distance > 0) {
                setDirectionImage((ImageView) nextAdvicePanel.findViewById(R.id.next_advice_img), nextAdvice.direction);
                ((TextView) nextAdvicePanel.findViewById(R.id.next_advice_distance_text)).
                    setText(StringUtil.convertAndFormatDistance(advice.distance + nextAdvice.distance, StringUtil.DISTANCE_UNIT_MILES_FEET));
                sb = new StringBuilder();
                if (nextAdvice.exitNumber != null && !nextAdvice.exitNumber.isEmpty()) {
                    sb.append(nextAdvice.exitNumber);
                }
                if (nextAdvice.streetName != null && !nextAdvice.streetName.isEmpty()) {
                    if (sb.length() > 0) {
                        sb.append(" ");
                    }
                    sb.append(nextAdvice.streetName);
                }
                ((TextView) nextAdvicePanel.findViewById(R.id.next_advice_street_text)).setText(sb.toString());
                nextAdvicePanel.setVisibility(View.VISIBLE);
            } else {
                nextAdvicePanel.setVisibility(View.GONE);
            }
            navigationInfoPanel.getLocationInWindow(loc);
            loc[0] += navigationInfoPanel.getWidth();
        } else if (navigationInfo.isDestinationReached()) {
            searchContainer.setVisibility(View.GONE);
            routingContainer.setVisibility(View.GONE);
            navigationContainer.setVisibility(View.VISIBLE);
            navigationInfoPanel.setVisibility(View.GONE);
            arrivalInfoPanel.setVisibility(View.VISIBLE);
            arrivalInfoPanel.getLocationInWindow(loc);
            navigationContainer.findViewById(R.id.find_parking_button).setVisibility(View.GONE);
            loc[0] += arrivalInfoPanel.getWidth();
        } else {
            searchContainer.setVisibility(View.GONE);
            routingContainer.setVisibility(View.VISIBLE);
            routingHeader.setVisibility(View.VISIBLE);
            routingInfoPanel.setVisibility(View.GONE);
            routingSearchHeader.setVisibility(View.GONE);
            navigationContainer.setVisibility(View.GONE);

            routingContainer.findViewById(R.id.find_parking_button).setVisibility(View.GONE);

            String displayAddress = navigationInfo.getNavigationDestination().getDisplayString();
            ((TextView) routingHeader.findViewById(R.id.routing_to_address_text)).
                setText(displayAddress);

            String destinationName = navigationInfo.getNavigationDestination().getDisplayName();
            String destinationAddress = navigationInfo.getNavigationDestination().getAddressInfo().getShortAddress();
            ((TextView) routingInfoPanel.findViewById(R.id.destination_name_text)).setText(destinationName);
            routingInfoPanel.findViewById(R.id.destination_name_text).setSelected(true);
            ((TextView) routingInfoPanel.findViewById(R.id.destination_address_text)).setText(destinationAddress);

            ((TextView) navigationInfoPanel.findViewById(R.id.destination_name_text)).setText(destinationName);
            navigationInfoPanel.findViewById(R.id.destination_name_text).setSelected(true);
            ((TextView) navigationInfoPanel.findViewById(R.id.destination_address_text)).setText(destinationAddress);

            ((TextView) arrivalInfoPanel.findViewById(R.id.destination_name_text)).setText(destinationName);
            arrivalInfoPanel.findViewById(R.id.destination_name_text).setSelected(true);
            ((TextView) arrivalInfoPanel.findViewById(R.id.destination_address_text)).setText(destinationAddress);

            getView().getLocationInWindow(loc);

            hasPromptedNavigation = false;
        }
        mapLeftPadding = loc[0];
    }

    protected int getMapLeftPadding() {
        return mapLeftPadding;
    }

    protected void doSearch() {
        destinationListContent = DESTINATION_LIST_NONE;
        destinationList.setVisibility(View.INVISIBLE);
        searchText.hideKeyboard();
        autocompletePredictions = null;
        String query = searchText.getText().toString();
        searchQuery(query);
    }

    private void collapseFavoritePanel(int selectedButton) {
        int padding = getResources().getDimensionPixelSize(R.dimen.favorite_button_collapse_padding);
        historyButton.setPadding(padding, padding, padding, padding);
        bookmarksButton.setPadding(padding, padding, padding, padding);
        if (selectedButton == R.id.history_button) {
            historyButton.setBackgroundResource(R.color.poi_category_selected);
            historyButton.setAlpha(1.0f);
            ImageView icon = (ImageView) historyButton.findViewById(android.R.id.icon);
            icon.setImageResource(R.drawable.icon_history_white);
            icon.setAlpha(1.0f);
            TextView title = (TextView) historyButton.findViewById(android.R.id.title);
            title.setTextColor(getResources().getColor(android.R.color.white));
            title.setAlpha(1.0f);
            bookmarksButton.setBackgroundResource(R.color.poi_category_default);
            bookmarksButton.setAlpha(0.2f);
            icon = (ImageView) bookmarksButton.findViewById(android.R.id.icon);
            icon.setImageResource(R.drawable.icon_bookmark_black);
            icon.setAlpha(0.3f);
            title = (TextView) bookmarksButton.findViewById(android.R.id.title);
            title.setTextColor(getResources().getColor(android.R.color.black));
            title.setAlpha(0.3f);
        } else {
            historyButton.setBackgroundResource(R.color.poi_category_default);
            historyButton.setAlpha(0.2f);
            ImageView icon = (ImageView) historyButton.findViewById(android.R.id.icon);
            icon.setImageResource(R.drawable.icon_history_black);
            icon.setAlpha(0.3f);
            TextView title = (TextView) historyButton.findViewById(android.R.id.title);
            title.setTextColor(getResources().getColor(android.R.color.black));
            title.setAlpha(0.3f);
            bookmarksButton.setBackgroundResource(R.color.poi_category_selected);
            bookmarksButton.setAlpha(1.0f);
            icon = (ImageView) bookmarksButton.findViewById(android.R.id.icon);
            icon.setImageResource(R.drawable.icon_bookmark_white);
            icon.setAlpha(1.0f);
            title = (TextView) bookmarksButton.findViewById(android.R.id.title);
            title.setTextColor(getResources().getColor(android.R.color.white));
            title.setAlpha(1.0f);
        }
    }

    private void showFavoritePanel() {
        favoritePanel.setVisibility(View.VISIBLE);
        int padding = getResources().getDimensionPixelSize(R.dimen.favorite_button_expand_padding);
        historyButton.setPadding(padding, padding, padding, padding);
        historyButton.setBackgroundResource(R.color.poi_category_default);
        historyButton.setAlpha(1.0f);
        ImageView icon = (ImageView) historyButton.findViewById(android.R.id.icon);
        icon.setImageResource(R.drawable.icon_history_black);
        icon.setAlpha(1.0f);
        TextView title = (TextView) historyButton.findViewById(android.R.id.title);
        title.setTextColor(getResources().getColor(android.R.color.black));
        title.setAlpha(1.0f);
        bookmarksButton.setPadding(padding, padding, padding, padding);
        bookmarksButton.setBackgroundResource(R.color.poi_category_default);
        bookmarksButton.setAlpha(1.0f);
        icon = (ImageView) bookmarksButton.findViewById(android.R.id.icon);
        icon.setImageResource(R.drawable.icon_bookmark_black);
        icon.setAlpha(1.0f);
        title = (TextView) bookmarksButton.findViewById(android.R.id.title);
        title.setTextColor(getResources().getColor(android.R.color.black));
        title.setAlpha(1.0f);
    }

    private DestinationItem createDestinationItem(ActionLog actionLog, String bookmark) {
        DestinationItem item = new DestinationItem();
        String name = DataUtil.getString(actionLog.getLog(), "name");
        String address = DataUtil.getString(actionLog.getLog(), "address");
        String primaryText = name;
        item.logId = actionLog.getId();
        item.placeId = DataUtil.getString(actionLog.getLog(), "id");
        item.secondaryText = address;
        if (bookmark != null) {
            item.iconResId = getBookmarkDrawableResId(bookmark, false);
            if (!bookmark.isEmpty()) {
                primaryText = bookmark;
                if (!name.isEmpty() && bookmark.toLowerCase().indexOf(name.toLowerCase()) == -1) {
                    item.secondaryText = name + ", " + address;
                }
            }
        }
        SpannableString boldPrimaryText = new SpannableString(primaryText);
        boldPrimaryText.setSpan(STYLE_BOLD, 0, primaryText.length(), 0);
        item.primaryText = primaryText;
        return item;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_button:
                doSearch();
                break;
            case R.id.history_button:
                searchText.hideKeyboard();
                destinationListContent = DESTINATION_LIST_HISTORY;
                destinationItems.clear();
                deletingLogId = -1;
                destinationListAdapter.notifyDataSetChanged();
                destinationList.setVisibility(View.VISIBLE);
                ActionLogManager.getInstance().getLog(
                    new int[]{
                        ActionLog.ACTION_TYPE_GEO_QUERY,
                        ActionLog.ACTION_TYPE_DESTINATION,
                        ActionLog.ACTION_TYPE_BOOKMARK},
                    new ActionLogManager.QueryResultListener() {
                        @Override
                        public void onResult(List<ActionLog> results) {
                            actionLogs = results;
                            if (destinationListContent == DESTINATION_LIST_HISTORY) {
                                Map<String, String> bookmarks = new HashMap<String, String>();
                                for (ActionLog actionLog : actionLogs) {
                                    if (actionLog.getType() != ActionLog.ACTION_TYPE_BOOKMARK) {
                                        continue;
                                    }
                                    bookmarks.put(DataUtil.getString(actionLog.getLog(), "id"),
                                        DataUtil.getString(actionLog.getLog(), "bookmark"));
                                }
                                for (ActionLog actionLog : actionLogs) {
                                    if (actionLog.getType() != ActionLog.ACTION_TYPE_DESTINATION) {
                                        continue;
                                    }
                                    String bookmark = bookmarks.get(DataUtil.getString(actionLog.getLog(), "id"));
                                    destinationItems.add(createDestinationItem(actionLog, bookmark));
                                    destinationListAdapter.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onError(ErrorCode errorCode) {

                        }
                    });
                collapseFavoritePanel(v.getId());
                break;
            case R.id.bookmark_button:
                searchText.hideKeyboard();
                destinationListContent = DESTINATION_LIST_BOOKMARKS;
                destinationItems.clear();
                deletingLogId = -1;
                destinationListAdapter.notifyDataSetChanged();
                destinationList.setVisibility(View.VISIBLE);
                ActionLogManager.getInstance().getLog(
                    new int[]{
                        ActionLog.ACTION_TYPE_GEO_QUERY,
                        ActionLog.ACTION_TYPE_DESTINATION,
                        ActionLog.ACTION_TYPE_BOOKMARK},
                    new ActionLogManager.QueryResultListener() {
                        @Override
                        public void onResult(List<ActionLog> results) {
                            actionLogs = results;
                            if (destinationListContent == DESTINATION_LIST_BOOKMARKS) {
                                for (ActionLog actionLog : actionLogs) {
                                    if (actionLog.getType() != ActionLog.ACTION_TYPE_BOOKMARK) {
                                        continue;
                                    }
                                    String bookmark = DataUtil.getString(actionLog.getLog(), "bookmark");
                                    destinationItems.add(createDestinationItem(actionLog, bookmark));
                                    destinationListAdapter.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onError(ErrorCode errorCode) {

                        }
                    });
                collapseFavoritePanel(v.getId());
                break;
            case R.id.cancel_search_button:
                deleteDestinationAnnotations();
                destinationList.setVisibility(View.INVISIBLE);
                selectDestinationQnATask.end();
                if (searchText.getText().length() > 0) {
                    searchText.setText("");
                } else if (searchText.isKeyboardShown()) {
                    searchText.hideKeyboard();
                    favoritePanel.setVisibility(View.GONE);
                } else if (destinationListContent != DESTINATION_LIST_NONE) {
                    destinationItems.clear();
                    destinationList.setVisibility(View.INVISIBLE);
                    destinationListContent = DESTINATION_LIST_NONE;
                } else {
                    MainActivity.getInstance().hideNavigation();
                }
                break;
            case R.id.prev_route_button:
                NavigationService.getDefaultService().selectRoute(NavigationService.getDefaultService().getSelectedRoute() - 1);
                break;
            case R.id.next_route_button:
                NavigationService.getDefaultService().selectRoute(NavigationService.getDefaultService().getSelectedRoute() + 1);
                break;
            case R.id.set_bookmark_button:
                startNavigationQnATask.end();
                final PlaceInfo destination = NavigationService.getDefaultService().
                    getCurrentNavigationInfo().getDestination();
                SetBookmarkDialogFragment setBookmarkDialogFragment = new SetBookmarkDialogFragment();
                setBookmarkDialogFragment.setOnBookmarkSelectedListener(new SetBookmarkDialogFragment.OnBookmarkSelectedListener() {
                    @Override
                    public void onBookmarkSelected(SetBookmarkDialogFragment dialog, CharSequence bookmark) {
                        String key;
                        final String savedBookmark;
                        if (bookmark.length() > 0 && !bookmark.toString().equalsIgnoreCase(destination.getName())) {
                            key = bookmark.toString().toLowerCase();
                            savedBookmark = bookmark.toString();
                        } else {
                            key = destination.getId();
                            savedBookmark = "";
                        }
                        ActionLogManager.getInstance().insertLog(
                            ActionLog.ACTION_TYPE_BOOKMARK,
                            key,
                            DataUtil.newMapData().
                                add("bookmark", savedBookmark).
                                add("id", destination.getId()).
                                add("name", destination.getName()).
                                add("address", destination.getAddressInfo().getFormattedAddress()),
                            new ActionLogManager.UpdateResultListener() {
                                @Override
                                public void onResult(int result) {
                                }

                                @Override
                                public void onError(ErrorCode errorCode) {
                                }
                            });
                    }
                });
                setBookmarkDialogFragment.show(getActivity().getFragmentManager(), "choose_bookmark_name");
                break;
            case R.id.cancel_routing_search_button:
                deleteDestinationAnnotations();
                startNavigationQnATask.end();
                destinationListContent = DESTINATION_LIST_NONE;
                destinationList.setVisibility(View.INVISIBLE);
                MainActivity.getInstance().cancelNavigation();
                MainActivity.getInstance().hideNavigation();
                break;
            case R.id.routing_search_button:
            case R.id.routing_search_text:
                deleteDestinationAnnotations();
                searchText.setText("");
            case R.id.cancel_navigation_button:
            case R.id.cancel_routing_button:
                destinationListContent = DESTINATION_LIST_NONE;
                TtsService.getDefaultService().stop(TAG);
                startNavigationQnATask.end();
                if (OnRoadApplication.getInstance().getBooleanPreference(R.string.PREF_KEY_NAVIGATION_SIMULATION, false)) {
                    LocationService.getDefaultService().stopSimulation();
                }
                MainActivity.getInstance().cancelNavigation();
                break;
            case R.id.find_parking_button:
                startNavigationQnATask.end();
                MainActivity.getInstance().showParking();
                break;
            case R.id.start_navigation_button:
                startNavigationQnATask.end();
                int[] loc = new int[2];
                navigationInfoPanel.getLocationInWindow(loc);
                NavigationService.getDefaultService().startNavigation();
                break;
//            case R.id.receipt_button:
//                if (webView.getVisibility() != View.VISIBLE) {
//                    webView.setVisibility(View.VISIBLE);
//                    NavigationInfo navigationInfo = NavigationService.getDefaultService().getCurrentNavigationInfo();
//                    if (navigationInfo != null && navigationInfo.getParkingReservationInfo() != null) {
//                        webView.loadUrl(navigationInfo.getParkingReservationInfo().getUrl());
//                    }
//                } else {
//                    webView.setVisibility(View.GONE);
//                }
//                break;
        }
    }

    protected void searchQuery(final String query) {
        ActionLogManager.getInstance().insertLog(
            ActionLog.ACTION_TYPE_GEO_QUERY,
            query.toLowerCase(),
            DataUtil.newMapData().add("query", query),
            new ActionLogManager.UpdateResultListener() {
                @Override
                public void onResult(int result) {
                }

                @Override
                public void onError(ErrorCode errorCode) {
                    OnRoadApplication.getInstance().handleError(errorCode);
                }
            });
        GeoService.getDefaultService().poiQuery(
            query,
            LocationService.getDefaultService().getLastLocation(),
            new GeoService.PoiResultListener() {
                @Override
                public void onResult(List<PlaceInfo> places) {
                    if (places.size() > 0) {
                        setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                        setDestinations(query, places);
                    }
                }

                @Override
                public void onError(ErrorCode error) {
                    OnRoadApplication.getInstance().handleError(error);
                }
            });
    }

    protected void searchPlace(String placeId) {
        GeoService.getDefaultService().placeQuery(placeId, new GeoService.PlaceResultListener() {
            @Override
            public void onResult(PlaceInfo place) {
                setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                setDestination(place);
            }

            @Override
            public void onError(ErrorCode error) {
                OnRoadApplication.getInstance().handleError(error);
            }
        });
    }

    public void promptNavigation() {
        if (hasPromptedNavigation) {
            return;
        }
        startNavigationQnATask.start();
        hasPromptedNavigation = true;
    }

    public void setRouteType(int routeType) {
        this.routeType = routeType;
    }

    public void setDestination(PlaceInfo place) {
        deleteDestinationAnnotations();
        destinations = null;
        destination = place;
        if (destination == null) {
            return;
        }
        addDestinationAnnotation();
        initNavigation(!destination.hasDetailedInfo(), destination.getBookmark() == null);
    }

    public void setDestinations(String query, List<PlaceInfo> places) {
        if (!query.equals(searchText.getText().toString())) {
            searchText.removeTextChangedListener(autocompleteTextWatcher);
            searchText.setText(query);
            searchText.post(new Runnable() {
                @Override
                public void run() {
                    searchText.setSelection(searchText.getText().length());
                }
            });
            searchText.addTextChangedListener(autocompleteTextWatcher);
        }
        deleteDestinationAnnotations();
        destinations = null;
        destination = null;
        if (places == null && places.size() == 0) {
            return;
        }
        if (places.size() == 1) {
            destination = places.get(0);
            addDestinationAnnotation();
            initNavigation(!destination.hasDetailedInfo(), destination.getBookmark() == null);
        } else {
            destinations = places;
            firstItem = -1;
            lastItem = -1;
            poiListAdapter.notifyDataSetChanged();
            poiListPanel.setVisibility(View.VISIBLE);
            List<String> selectItems = new ArrayList<>(destinations.size());
            for (PlaceInfo place : destinations) {
                selectItems.add(place.getName() + ", " + place.getAddressInfo().getShortAddress());
                if (!place.hasDetailedInfo()) {
                    GeoService.getDefaultService().updatePlace(place, new GeoService.PlaceResultListener() {
                        @Override
                        public void onResult(PlaceInfo place) {
                            poiListAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onError(ErrorCode error) {
                            OnRoadApplication.getInstance().handleError(error);
                        }
                    });
                }
            }
            selectDestinationQnATask.setSelectionItems(selectItems).start();
            SystemUtil.postToMainThread(addDestinationAnnotationsTask);
        }
    }

    protected void deleteDestinationAnnotations() {
        if (destinations != null) {
            for (PlaceInfo placeInfo : destinations) {
                MapService.getDefaultService().deleteAnnotation(placeInfo.getId().hashCode());
            }
        } else if (destination != null) {
            MapService.getDefaultService().deleteAnnotation(destination.getId().hashCode());
        }
    }

    protected void selectDestination(PlaceInfo place) {
        if (place == destination) {
            return;
        }
        MainActivity.getInstance().getMapFragment().hideCutout();
        deleteDestinationAnnotations();
        destination = place;
        addDestinationAnnotation();
        initNavigation(!destination.hasDetailedInfo(), destination.getBookmark() == null);
    }

    protected void addDestinationAnnotation() {
        MapService.getDefaultService().deleteAnnotation(destination.getId().hashCode());
        View annotationView =
            ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.annotation_destination, null, false);
        annotationView.findViewById(R.id.name_text).setVisibility(View.GONE);
        Resources rs = getActivity().getResources();
        float offsetY = rs.getDimension(R.dimen.map_annotation_destination_height) / 2.0f;
        MapService.getDefaultService().addAnnotation(
            destination.getId().hashCode(),
            new MapCoordinate(destination.getLocation().getLatitude(), destination.getLocation().getLongitude()),
            annotationView, 0, offsetY);
    }

    protected void addAltDestinationAnnotation(PlaceInfo place) {
        MapService.getDefaultService().deleteAnnotation(place.getId().hashCode());
        View annotationView =
            ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.annotation_destination_alt, null, false);
        ((TextView) annotationView.findViewById(R.id.name_text)).setText(place.getName());
        annotationView.setTag(place);
        annotationView.setOnClickListener(destinationAnnotationListener);
        annotationView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Resources rs = getActivity().getResources();
        float offsetX = annotationView.getMeasuredWidth() / 2.0f -
            (rs.getDimension(R.dimen.map_annotation_padding) +
                rs.getDimension(R.dimen.map_annotation_destination_alt_width) / 2.0f);
        MapService.getDefaultService().addAnnotation(
            place.getId().hashCode(),
            new MapCoordinate(place.getLocation().getLatitude(), place.getLocation().getLongitude()),
            annotationView, offsetX, 0);
    }

    protected void initNavigation(final boolean updateDetail, final boolean updateBookmark) {
        if (updateDetail) {
            GeoService.getDefaultService().updatePlace(destination, new GeoService.PlaceResultListener() {
                @Override
                public void onResult(PlaceInfo place) {
                    initNavigation(false, updateBookmark);
                }

                @Override
                public void onError(ErrorCode error) {
                    initNavigation(false, updateBookmark);
                    OnRoadApplication.getInstance().handleError(error);
                }
            });
        } else if (updateBookmark) {
            ActionLogManager.getInstance().getLog(
                new int[]{ActionLog.ACTION_TYPE_BOOKMARK},
                new ActionLogManager.QueryResultListener() {
                    @Override
                    public void onResult(List<ActionLog> results) {
                        for (ActionLog actionLog : results) {
                            if (destination.getId().equals(DataUtil.getString(actionLog.getLog(), "id"))) {
                                destination.setBookmark(DataUtil.getString(actionLog.getLog(), "bookmark"));
                                break;
                            }
                        }
                        initNavigation(updateDetail, false);
                    }

                    @Override
                    public void onError(ErrorCode errorCode) {
                        initNavigation(updateDetail, false);
                    }
                });
        } else {
            initNavigation(destination, routeType);
        }
    }

    protected static void initNavigation(PlaceInfo destination, int routeType) {
        if (!(destination instanceof ParkingInfo)) {
            ActionLogManager.getInstance().insertLog(
                ActionLog.ACTION_TYPE_DESTINATION,
                destination.getId(),
                DataUtil.newMapData().
                    add("id", destination.getId()).
                    add("name", destination.getName()).
                    add("address", destination.getAddressInfo().getFormattedAddress()),
                new ActionLogManager.UpdateResultListener() {
                    @Override
                    public void onResult(int result) {
                    }

                    @Override
                    public void onError(ErrorCode errorCode) {
                        OnRoadApplication.getInstance().handleError(errorCode);
                    }
                });
        }

        // stop current navigation
        TtsService.getDefaultService().stop(NavigationFragment.TAG);
        NavigationInfo navigationInfo = NavigationService.getDefaultService().getCurrentNavigationInfo();
        if (navigationInfo != null && (destination instanceof ParkingInfo)) {
            // navigate to parking location
            navigationInfo.setNavigationDestination(destination);
        } else {
            // else start new navigation
            if (navigationInfo != null) {
                if (navigationInfo.isDestinationReached() || navigationInfo.isNavigating()) {
                    NavigationService.getDefaultService().stopNavigation();
                } else {
                    NavigationService.getDefaultService().cancelRouteCalculation();
                }
            }
            navigationInfo = new NavigationInfo();
            navigationInfo.setStartCoordinate(LocationService.getDefaultService().getLastLocation());
            navigationInfo.setDestination(destination);
            navigationInfo.setNavigationDestination(destination);
        }
        navigationInfo.setRouteType(routeType);
        navigationInfo.setNavigationType(
            OnRoadApplication.getInstance().getIntPreference(
                NavigationService.PREF_KEY_NAVIGATION_TYPE, NavigationInfo.NAVIGATION_TYPE_SIMULATION));
        NavigationService.getDefaultService().initNavigation(navigationInfo, MapService.getDefaultService().getMapView());
        OneTouchApplication.getInstance().tts("routing to " + destination.getDisplayString(), NavigationFragment.TAG, TtsService.SPEECH_PRIORITY_NORMAL, new TtsService.OnSpeechListener() {
            @Override
            public void onFinished() {
                NavigationService.getDefaultService().calculateRoute();
            }

            @Override
            public void onStopped() {
                NavigationService.getDefaultService().calculateRoute();
            }

            @Override
            public void onError(ErrorCode error) {
                NavigationService.getDefaultService().calculateRoute();
            }
        });
    }

    public void setVisibility(boolean visibility) {
        if (getView() != null) {
            getView().setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
        }
        NavigationService.getDefaultService().setRoutesVisibility(visibility);
    }
}
