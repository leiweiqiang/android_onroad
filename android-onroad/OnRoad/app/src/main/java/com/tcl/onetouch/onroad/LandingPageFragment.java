package com.tcl.onetouch.onroad;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by leiweiqiang2 on 16/5/19.
 */
public class LandingPageFragment extends ContentFragment{

    private static final String TAG = LandingPageFragment.class.getName();
    private View view;
    View.OnClickListener listener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.panel_landing_page, container, false);
        view.findViewById(R.id.btn_navi).setOnClickListener(listener);
        view.findViewById(R.id.btn_parking).setOnClickListener(listener);
        view.findViewById(R.id.btn_music).setOnClickListener(listener);
        view.findViewById(R.id.btn_call).setOnClickListener(listener);
        view.findViewById(R.id.btn_message).setOnClickListener(listener);
        return view;
    }

    public void setTabbarClickListener(View.OnClickListener listener){
      this.listener = listener;
    }

//    @Override
//    public void onClick(View v) {
//
//        switch (v.getId()) {
//            case R.id.btn_navi:
//                break;
//
//            case R.id.btn_parking:
//                break;
//        }
//    }

}
