package com.tcl.onetouch.service;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.OnRoadErrorCode;

public class OnRoadService extends BaseService {
    public ErrorCode createErrorCode(int error) {
        return new OnRoadErrorCode(error, null, this);
    }
}
