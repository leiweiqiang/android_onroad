package com.tcl.onetouch.service.skobbler;

import android.content.Context;
import android.location.Location;
import android.view.View;

import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.SKMaps;
import com.skobbler.ngx.map.SKMapSettings;
import com.skobbler.ngx.map.SKMapSurfaceView;
import com.skobbler.ngx.map.SKMapViewHolder;
import com.skobbler.ngx.map.SKScreenPoint;
import com.skobbler.ngx.map.traffic.SKTrafficListener;
import com.skobbler.ngx.map.traffic.SKTrafficUpdateData;
import com.skobbler.ngx.navigation.SKAdvisorSettings;
import com.skobbler.ngx.navigation.SKNavigationListener;
import com.skobbler.ngx.navigation.SKNavigationManager;
import com.skobbler.ngx.navigation.SKNavigationSettings;
import com.skobbler.ngx.navigation.SKNavigationState;
import com.skobbler.ngx.positioner.SKCurrentPositionListener;
import com.skobbler.ngx.positioner.SKPosition;
import com.skobbler.ngx.routing.SKRouteInfo;
import com.skobbler.ngx.routing.SKRouteJsonAnswer;
import com.skobbler.ngx.routing.SKRouteListener;
import com.skobbler.ngx.routing.SKRouteManager;
import com.skobbler.ngx.routing.SKRouteSettings;
import com.skobbler.ngx.trail.SKTrailType;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.MapCoordinate;
import com.tcl.onetouch.model.NavigationInfo;
import com.tcl.onetouch.model.OnRoadErrorCode;
import com.tcl.onetouch.service.NavigationService;
import com.tcl.onetouch.util.SystemUtil;

import java.util.ArrayList;
import java.util.List;

public class SkobblerNavigationService
    extends NavigationService
    implements SKNavigationListener, SKRouteListener, SKTrafficListener, SKCurrentPositionListener {

    private static final double FULL_SCREEN_MINIMAL_SCREENSIZE = 3.85;
    private static final SKMaps.SKDistanceUnitType DISTANCE_UNIT_TYPE = SKMaps.SKDistanceUnitType.DISTANCE_UNIT_MILES_YARDS;

    private SkobblerPreference preference;
    private NavigationService.Listener listener;

    private SKMapViewHolder mapViewHolder;
    private NavigationInfo navigationInfo;
    private List<SKRouteInfo> routeList;

    @Override
    public void init(Context context, NavigationService.Listener listener) {
        this.listener = listener;
        preference = new SkobblerPreference(context);

        routeList = new ArrayList();
        mapViewHolder = null;

        SKAdvisorSettings advisorSettings = new SKAdvisorSettings();
        advisorSettings.setLanguage(SKAdvisorSettings.SKAdvisorLanguage.LANGUAGE_EN);
        String mapResourcesDirPath = preference.getStringPreference(SkobblerPreference.MAP_RESOURCE_PATH);
        advisorSettings.setAdvisorConfigPath(mapResourcesDirPath + "/Advisor");
        advisorSettings.setResourcePath(mapResourcesDirPath + "/Advisor/Languages");
        advisorSettings.setAdvisorVoice("en");
        advisorSettings.setAdvisorType(SKAdvisorSettings.SKAdvisorType.TEXT_TO_SPEECH);
        SKRouteManager.getInstance().setAudioAdvisorSettings(advisorSettings);
    }

    @Override
    public void initNavigation(NavigationInfo info, View mapView) {
        if (info == null) {
            // TODO: throw exception
            return;
        }

        navigationInfo = info;
        navigationInfo.setIsRouting(false);
        navigationInfo.setIsRouteReady(false);
        navigationInfo.setIsNavigating(false);
        navigationInfo.setIsReRouting(false);
        navigationInfo.setIsDestinationReached(false);

        SKRouteManager.getInstance().clearCurrentRoute();
        SKRouteManager.getInstance().clearAllRoutesFromCache();
        routeList.clear();

        mapViewHolder = null;
        if (mapView instanceof SKMapViewHolder) {
            mapViewHolder = (SKMapViewHolder) mapView;
        }

        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onNavigationInitialized(info);
                }
            });
        }
    }

    @Override
    public void calculateRoute() {
        if (navigationInfo == null) {
            return;
        }

        SKRouteManager.getInstance().clearCurrentRoute();
        SKRouteManager.getInstance().clearAllRoutesFromCache();
        routeList.clear();
        SKRouteSettings route = new SKRouteSettings();
        route.setStartCoordinate(SkobblerUtil.convertFrom(navigationInfo.getStartCoordinate()));
        route.setDestinationCoordinate(SkobblerUtil.convertFrom(navigationInfo.getNavigationDestination().getLocation()));

        switch (navigationInfo.getRouteType()) {
            case NavigationInfo.ROUTE_TYPE_CAR_FASTEST:
                route.setRouteMode(SKRouteSettings.SKRouteMode.CAR_FASTEST);
                break;
            case NavigationInfo.ROUTE_TYPE_CAR_SHORTEST:
                route.setRouteMode(SKRouteSettings.SKRouteMode.CAR_SHORTEST);
                break;
            default:
                route.setRouteMode(SKRouteSettings.SKRouteMode.PEDESTRIAN);
                SKRouteManager.getInstance().enablePedestrianTrail(true, 5);
        }
        route.setNoOfRoutes(1);

        route.setRouteExposed(true);
        route.setTollRoadsAvoided(navigationInfo.isTollRoadsAvoided());
        route.setAvoidFerries(navigationInfo.isFerriesAvoided());
        route.setHighWaysAvoided(navigationInfo.isHighWaysAvoided());
        route.setUseLiveTraffic(true);
        route.setUseLiveTrafficETA(true);
        SKRouteManager.getInstance().setTrafficRoutingMode(SKMapSettings.SKTrafficMode.FLOW_AND_INCIDENTS);

        SKRouteManager.getInstance().setRouteListener(this);
        SKRouteManager.getInstance().calculateRoute(route);

        navigationInfo.setIsRouting(true);
        navigationInfo.setRoute(null);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onRouteCalculationStarted(info);
                }
            });
        }
    }

    @Override
    public void cancelRouteCalculation() {
        if (navigationInfo == null || navigationInfo.isNavigating() || navigationInfo.isDestinationReached()) {
            return;
        }

        SKRouteManager.getInstance().clearCurrentRoute();
        SKRouteManager.getInstance().clearAllRoutesFromCache();
        routeList.clear();
        navigationInfo.setIsRouting(false);
        navigationInfo.setIsRouteReady(false);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onRouteCalculationCanceled(info);
                }
            });
        }
        navigationInfo = null;
    }

    @Override
    public int getNumberOfRoutes() {
        return 0;
    }

    @Override
    public int getSelectedRoute() {
        return 0;
    }

    @Override
    public void selectRoute(int index) {

    }

    @Override
    public double[][] getRouteBoundingBox() {
        return null;
    }

    @Override
    public void setRoutesVisibility(boolean visibility) {
    }

    @Override
    public void onRouteCalculationCompleted(SKRouteInfo skRouteInfo) {
        if (navigationInfo == null || !navigationInfo.isRouting() || !skRouteInfo.isCorridorDownloaded()) {
            return;
        }
        routeList.add(skRouteInfo);
        if (skRouteInfo.getMode() == SKRouteSettings.SKRouteMode.PEDESTRIAN.getValue()) {
            SKRouteManager.getInstance().renderRouteAsPedestrian(skRouteInfo.getRouteID());
        }
    }

    @Override
    public void onRouteCalculationFailed(SKRoutingErrorCode skRoutingErrorCode) {
        if (navigationInfo == null || !navigationInfo.isRouting()) {
            return;
        }

        if (listener != null) {
            final ErrorCode error;
            switch (skRoutingErrorCode) {
                case SAME_START_AND_DESTINATION:
                    error = createErrorCode(OnRoadErrorCode.NAVIGATION_ERROR_SAME_START_AND_DESTINATION);
                    break;
                case INVALID_START:
                    error = createErrorCode(OnRoadErrorCode.NAVIGATION_ERROR_INVALID_START);
                    break;
                case INVALID_DESTINATION:
                    error = createErrorCode(OnRoadErrorCode.NAVIGATION_ERROR_INVALID_DESTINATION);
                    break;
                case ROUTE_CANNOT_BE_CALCULATED:
                    error = createErrorCode(OnRoadErrorCode.NAVIGATION_ERROR_NO_ROUTE);
                    break;
                case NO_RESULTS_FOUND:
                    error = createErrorCode(OnRoadErrorCode.NAVIGATION_ERROR_NO_RESULT);
                    break;
                default:
                    error = createErrorCode(OnRoadErrorCode.NAVIGATION_ERROR);
                    break;
            }
            navigationInfo.setIsRouting(false);
            if (listener != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    NavigationInfo info = navigationInfo;

                    @Override
                    public void run() {
                        listener.onRouteCalculationFailed(info, error);
                    }
                });
            }
            navigationInfo = null;
        }
    }

    @Override
    public void onAllRoutesCompleted() {
        if (navigationInfo == null || !navigationInfo.isRouting()) {
            return;
        }
        if (!routeList.isEmpty()) {
            SKRouteInfo routeInfo = routeList.get(0);
            navigationInfo.setTravelTimeInSec(routeInfo.getEstimatedTime());
            navigationInfo.setTravelDistanceInMeter(routeInfo.getDistance());
            String[] summary = routeInfo.getRouteSummary();
            if (summary != null) {
                if (summary.length > 1) {
                    navigationInfo.setRouteSummary(summary[1]);
                } else if (summary.length > 0) {
                    navigationInfo.setRouteSummary(summary[0]);
                }
            }
            int routeId = routeInfo.getRouteID();
            SKRouteManager.getInstance().setCurrentRouteByUniqueId(routeId);
            List<SKCoordinate> points = SKRouteManager.getInstance().getCoordinatesForRoute(routeId);
            if (points != null) {
                List<MapCoordinate> route = new ArrayList<>(points.size());
                for (SKCoordinate point : points) {
                    route.add(new MapCoordinate(point.getLatitude(), point.getLongitude()));
                }
                navigationInfo.setRoute(route);
            }
            navigationInfo.setIsRouting(false);
            navigationInfo.setIsRouteReady(true);
            if (listener != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    NavigationInfo info = navigationInfo;
                    @Override
                    public void run() {
                        listener.onRouteCalculationCompleted(info);
                    }
                });
            }
        } else {
            navigationInfo.setIsRouting(false);
            if (listener != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    NavigationInfo info = navigationInfo;

                    @Override
                    public void run() {
                        listener.onRouteCalculationFailed(
                            info,
                            createErrorCode(OnRoadErrorCode.NAVIGATION_ERROR_NO_RESULT));
                    }
                });
            }
            navigationInfo = null;
        }
    }

    @Override
    public void onServerLikeRouteCalculationCompleted(SKRouteJsonAnswer skRouteJsonAnswer) {
        // TODO: figure out what's this
    }

    @Override
    public void onOnlineRouteComputationHanging(int i) {
        // TODO: fire onError
    }

    @Override
    public void startNavigation() {
        if (navigationInfo == null || !navigationInfo.isRouteReady()) {
            // TODO: throw exception
            return;
        }

        SKNavigationSettings navigationSettings = new SKNavigationSettings();
        if (mapViewHolder != null) {
            SKMapSurfaceView mapView = mapViewHolder.getMapSurfaceView();
            SKMapSettings.SKMapDisplayMode currentUserDisplayMode;
            if (navigationInfo.getRouteType() == NavigationInfo.ROUTE_TYPE_PEDESTRIAN) {
                currentUserDisplayMode = SKMapSettings.SKMapDisplayMode.MODE_2D;
                mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.HISTORIC_POSITION);
                SKTrailType trailType = new SKTrailType();
                trailType.setPedestrianTrailEnabled(true, 1);
                navigationSettings.setTrailType(trailType);
                navigationSettings.setCcpAsCurrentPosition(true);
                mapView.getMapSettings().setCompassPosition(new SKScreenPoint(10, 70));
                mapView.getMapSettings().setCompassShown(true);
                navigationSettings.setNavigationMode(SKNavigationSettings.SKNavigationMode.PEDESTRIAN);
            } else {
                currentUserDisplayMode = SKMapSettings.SKMapDisplayMode.MODE_3D;
                mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.NAVIGATION);
            }
            mapView.getMapSettings().setMapDisplayMode(currentUserDisplayMode);
            mapView.getMapSettings().setStreetNamePopupsShown(true);
            mapView.getMapSettings().setMapZoomingEnabled(false);
            SKNavigationManager.getInstance().setMapView(mapView);
        }

        if (this.navigationInfo.getNavigationType() == NavigationInfo.NAVIGATION_TYPE_SIMULATION) {
            navigationSettings.setNavigationType(SKNavigationSettings.SKNavigationType.SIMULATION);
        } else {
            navigationSettings.setNavigationType(SKNavigationSettings.SKNavigationType.REAL);
        }
        navigationSettings.setPositionerVerticalAlignment(-0.25f);
        navigationSettings.setShowRealGPSPositions(false);
        navigationSettings.setDistanceUnit(DISTANCE_UNIT_TYPE);

        SKNavigationManager.getInstance().setNavigationListener(this);
        SKNavigationManager.getInstance().startNavigation(navigationSettings);

        navigationInfo.setIsNavigating(true);
        navigationInfo.setIsRouteReady(false);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onNavigationStarted(info);
                }
            });
        }

    }

    @Override
    public void stopNavigation() {
        if (navigationInfo == null || (!navigationInfo.isNavigating() && !navigationInfo.isDestinationReached())) {
            return;
        }

        if (mapViewHolder != null) {
            SKMapSurfaceView mapView = mapViewHolder.getMapSurfaceView();
            mapView.getMapSettings().setCompassShown(false);
            SKRouteManager.getInstance().clearCurrentRoute();
            SKRouteManager.getInstance().clearAllRoutesFromCache();
            routeList.clear();
            SKNavigationManager.getInstance().stopNavigation();
            mapView.rotateTheMapToNorth();
            mapView.getMapSettings().setMapDisplayMode(SKMapSettings.SKMapDisplayMode.MODE_2D);
            mapView.getMapSettings().setFollowerMode(SKMapSettings.SKMapFollowerMode.POSITION);
            mapView.getMapSettings().setStreetNamePopupsShown(false);
            mapView.getMapSettings().setMapZoomingEnabled(true);
        }

        navigationInfo.setIsNavigating(false);
        navigationInfo.setIsDestinationReached(false);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onNavigationEnded(info);
                }
            });
        }
        navigationInfo = null;
    }

    @Override
    public void updateCurrentLocation(Location location) {

    }

    @Override
    public NavigationInfo getCurrentNavigationInfo() {
        return navigationInfo;
    }


    @Override
    public void onCurrentPositionUpdate(SKPosition skPosition) {

    }

    @Override
    public void onDestinationReached() {
        if (navigationInfo == null || !navigationInfo.isNavigating()) {
            return;
        }
        navigationInfo.setIsNavigating(false);
        navigationInfo.setIsDestinationReached(true);
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onDestinationReached(info);
                }
            });
        }
    }

    @Override
    public void onSignalNewAdviceWithInstruction(String s) {
        if (navigationInfo == null || !navigationInfo.isNavigating() ||
            navigationInfo.getRouteType() == NavigationInfo.ROUTE_TYPE_PEDESTRIAN) {
            return;
        }
        NavigationInfo.Advice advice = navigationInfo.getCurrentAdvice();
        advice.instruction = s;
        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onNavigationInstruction(info);
                }
            });
        }
    }

    @Override
    public void onSignalNewAdviceWithAudioFiles(String[] audioFiles, boolean b) {
    }

    @Override
    public void onSpeedExceededWithAudioFiles(String[] strings, boolean b) {

    }

    @Override
    public void onSpeedExceededWithInstruction(String s, boolean b) {

    }

    @Override
    public void onUpdateNavigationState(SKNavigationState state) {
        if (navigationInfo == null || !navigationInfo.isNavigating()) {
            return;
        }

        navigationInfo.setIsReRouting(false);

        NavigationInfo.Advice advice = navigationInfo.getCurrentAdvice();
        advice.distance = state.getCurrentAdviceDistanceToAdvice();
        advice.exitNumber = state.getCurrentAdviceExitNumber();
        advice.direction = state.getCurrentStreetDirection().getValue();
        advice.streetName = state.getCurrentAdviceNextStreetName();
        if (advice.streetName != null) {
            advice.streetName =
                advice.streetName.replace("\u021B", "\u0163").replace("\u021A", "\u0162")
                    .replace("\u0218", "\u015E").replace("\u0219", "\u015F");
        }
        advice.instruction = state.getAdviceInstruction();
        navigationInfo.setDistanceToDestinationInMeter(state.getCurrentAdviceDistanceToDestination());
        navigationInfo.setTimeToDestinationInSec(state.getCurrentAdviceTimeToDestination());
        String street = state.getCurrentAdviceCurrentStreetName();
        if (street != null) {
            street =
                street.replace("\u021B", "\u0163").replace("\u021A", "\u0162")
                    .replace("\u0218", "\u015E").replace("\u0219", "\u015F");
        }
        navigationInfo.setCurrentStreetName(street);

        if (listener != null) {
            SystemUtil.postToMainThread(new Runnable() {
                NavigationInfo info = navigationInfo;

                @Override
                public void run() {
                    listener.onNavigationUpdated(info);
                }
            });
        }
    }

    @Override
    public void onReRoutingStarted() {
        if (navigationInfo == null || !navigationInfo.isNavigating()) {
            return;
        }

        navigationInfo.setIsReRouting(true);
        navigationInfo.setRoute(null);
    }

    @Override
    public void onFreeDriveUpdated(String s, String s1, String s2, SKNavigationState.SKStreetType skStreetType, double v, double v1) {

    }

    @Override
    public void onViaPointReached(int i) {

    }

    @Override
    public void onVisualAdviceChanged(boolean b, boolean b1, SKNavigationState skNavigationState) {

    }

    @Override
    public void onTunnelEvent(boolean b) {

    }

    @Override
    public void onTrafficUpdated(SKTrafficUpdateData skTrafficUpdateData) {
        if (navigationInfo == null || !navigationInfo.isNavigating()) {
            return;
        }
        SKNavigationManager.getInstance().rerouteWithTraffic();
    }
}
