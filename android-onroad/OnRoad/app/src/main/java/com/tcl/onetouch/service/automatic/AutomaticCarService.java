package com.tcl.onetouch.service.automatic;

import android.content.Context;

import com.automatic.android.sdk.Automatic;
import com.automatic.android.sdk.LogLevel;
import com.automatic.android.sdk.events.ConnectionStateListener;
import com.automatic.android.sdk.events.DriveStateListener;
import com.automatic.android.sdk.events.IgnitionEventListener;
import com.automatic.android.sdk.events.LocationEventListener;
import com.automatic.net.Scope;
import com.automatic.net.events.IgnitionEvent;
import com.automatic.net.events.Location;
import com.automatic.net.responses.ResultSet;
import com.automatic.net.responses.Trip;
import com.automatic.net.responses.Vehicle;
import com.automatic.net.servicebinding.DriveState;
import com.tcl.onetouch.model.OnRoadErrorCode;
import com.tcl.onetouch.service.CarService;
import com.tcl.onetouch.util.LogUtil;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AutomaticCarService extends CarService
    implements DriveStateListener, ConnectionStateListener, IgnitionEventListener, LocationEventListener {
    private static final Scope[] scopes = {
        Scope.Public,
        Scope.Trips,
        Scope.Location,
        Scope.CurrentLocation,
        Scope.VehicleEvents,
        Scope.VehicleProfile,
        Scope.UserProfile,
        Scope.Behavior
    };

    private CarService.Listener listener;
    private int driveState;
    private boolean isConnected;
    private boolean isIgnitionOn;
    private Location location;
    private Vehicle vehicle;

    @Override
    public void init(Context context, CarService.Listener listener) {
        this.listener = listener;
        driveState = DRIVE_STATE_UNKNOWN;
        isConnected = false;
        isIgnitionOn = false;
        location = null;
        vehicle = null;
        Automatic.initialize(
            new Automatic.Builder(context)
                .addScopes(scopes)
                .useServiceBinding()
                .logLevel(LogLevel.Basic)
                .autoLogIn(true));

        Automatic.get().addDriveStateListener(this);
        Automatic.get().addConnectionStateListener(this);
        Automatic.get().addIgnitionEventListener(this);
        Automatic.get().addLocationEventListener(this);
    }

    @Override
    public void destroy() {
        Automatic.get().removeDriveStateListener(this);
        Automatic.get().removeConnectionStateListener(this);
        Automatic.get().removeIgnitionEventListener(this);
        Automatic.get().removeLocationEventListener(this);
    }

    @Override
    public void findMyCar(final CarService.LocationListener locationListener) {
        if (locationListener == null) {
            return;
        }
        if (location != null) {
            android.location.Location loc = new android.location.Location("");
            loc.setLatitude(location.lat);
            loc.setLongitude(location.lon);
            locationListener.onLocation(loc);
        } else if (vehicle == null) {
            Automatic.get().restApi().getMyVehicles(new Callback<ResultSet<Vehicle>>() {
                @Override
                public void success(ResultSet<Vehicle> vehicles, Response response) {
                    if ((vehicles != null) && (vehicles.results != null) && !vehicles.results.isEmpty()) {
                        vehicle = vehicles.results.get(0);
                        LogUtil.d(vehicle.year + ", " + vehicle.make + ", " + vehicle.model + ", " + vehicle.display_name);
                        findMyCar(locationListener);
                    } else {
                        locationListener.onError(createErrorCode(OnRoadErrorCode.CAR_ERROR_NO_CAR));
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    locationListener.onError(createErrorCode(
                        OnRoadErrorCode.CAR_ERROR,
                        retrofitError.getResponse().getReason()));
                }
            });
        } else {
            Automatic.get().restApi().getTrips(vehicle.id, 1, 1, new Callback<ResultSet<Trip>>() {
                @Override
                public void success(ResultSet<Trip> trips, Response response) {
                    if (trips.results != null && trips.results.size() > 0) {
                        Trip trip = trips.results.get(0);
                        Trip.LocationInfo locationInfo = trip.end_location;
                        LogUtil.d("last trip: " + trip.getEndTimeFormatted() + ", " + trip.end_location.lat + ", " + trip.end_location.lon);
                        android.location.Location loc = new android.location.Location("");
                        loc.setLatitude(locationInfo.lat);
                        loc.setLongitude(locationInfo.lon);
                        locationListener.onLocation(loc);
                    } else {
                        locationListener.onError(createErrorCode(OnRoadErrorCode.CAR_ERROR_NO_LOCATION));
                    }
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    locationListener.onError(createErrorCode(OnRoadErrorCode.CAR_ERROR, retrofitError.getResponse().getReason()));
                }
            });
        }

    }

    @Override
    public int getCurrentDriveState() {
        return driveState;
    }

    @Override
    public boolean isIgnitionOn() {
        return isIgnitionOn;
    }

    @Override
    public boolean isConnected() {
        return isConnected;
    }

    @Override
    public void onDisconnect() {
        LogUtil.d("AutomaticCarService.onDisconnect");
        isConnected = false;
        if (listener != null) {
            listener.onConnectionChanged(isConnected);
        }
    }

    @Override
    public void onConnect() {
        LogUtil.d("AutomaticCarService.onConnect");
        isConnected = false;
        if (listener != null) {
            listener.onConnectionChanged(isConnected);
        }
    }

    @Override
    public void onLogout() {
        LogUtil.d("AutomaticCarService.onLogout");
    }

    @Override
    public void onDriveStateChanged(DriveState state) {
        LogUtil.d("AutomaticCarService.onDriveStateChanged: " + state);
        switch (state) {
            case CURRENTLY_DRIVING:
                driveState = DRIVE_STATE_DRIVING;
                break;
            case END_DRIVE:
                driveState = DRIVE_STATE_END_DRIVE;
                break;
            case IGNITION_WAIT:
                driveState = DRIVE_STATE_IGNITION_WAIT;
                break;
            case DISCONNECTED:
                driveState = DRIVE_STATE_DISCONNECTED;
                break;
            default:
                driveState = DRIVE_STATE_UNKNOWN;
        }
        if (listener != null) {
            listener.onDriveStateChanged(driveState);
        }
    }

    @Override
    public void onIgnitionEvent(IgnitionEvent event) {
        LogUtil.d("AutomaticCarService.onIgnitionEvent: " + event.toString());
        isIgnitionOn = event.ignitionStatus;
        if (listener != null) {
            listener.onIgnitionEvent(isIgnitionOn);
        }
    }

    @Override
    public void onLocationEvent(Location location) {
        LogUtil.d("AutomaticCarService.onLocationEvent: " + location.toString());
        this.location = location;
    }
}
