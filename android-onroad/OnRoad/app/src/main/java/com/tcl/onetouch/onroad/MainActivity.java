package com.tcl.onetouch.onroad;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Location;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.ActionLog;
import com.tcl.onetouch.model.AddressInfo;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.NavigationInfo;
import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.modules.IntentHandler;
import com.tcl.onetouch.modules.NaviFragment;
import com.tcl.onetouch.service.CarService;
import com.tcl.onetouch.service.GeoService;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.service.MapService;
import com.tcl.onetouch.service.MusicService;
import com.tcl.onetouch.service.NavigationService;
import com.tcl.onetouch.service.NluService;
import com.tcl.onetouch.service.ParkingService;
import com.tcl.onetouch.service.SpeechRecognizerService;
import com.tcl.onetouch.service.TtsService;
import com.tcl.onetouch.service.UserService;
import com.tcl.onetouch.service.WakeupService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getName();

    private SplashFragment splashFragment;
    private MapFragment mapFragment;
    private NavigationFragment navigationFragment;
    private NaviFragment naviFragment;
    private ParkingFragment parkingFragment;
    private MusicFragment musicFragment;
    private ControlFragment controlFragment;
    private SettingsFragment settingsFragment;
    private LandingPageFragment landingPageFragment;

    private ViewGroup contentContainer;
    private int controlBarWidth;


    private static boolean NAVI_DEBUG = true;

    private LocationService.Listener locationListener = new LocationService.Listener() {
        @Override
        public void onLocationChanged(Location location) {
            MapService.getDefaultService().updateCurrentLocation(location);
            NavigationService.getDefaultService().updateCurrentLocation(location);
        }
    };

    private MapService.Listener mapListener = new MapService.Listener() {
        @Override
        public void onServiceReady() {

            landingPageFragment = new LandingPageFragment();
            getFragmentManager().beginTransaction().add(R.id.ladingpage_layer, landingPageFragment).commit();
            landingPageFragment.setTabbarClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.btn_navi: {
                            if (landingPageFragment.isAdded()) {
                                getFragmentManager().beginTransaction().remove(landingPageFragment).commit();
                                controlFragment.setMicViewToTabbar();
                            }
                        }
                        break;

                        case R.id.btn_parking:
                            break;
                    }
                }
            });

            mapFragment = new MapFragment();
            mapFragment.setLeftPadding(controlBarWidth);
            getFragmentManager().beginTransaction().add(R.id.map_layer, mapFragment).commit();

            navigationFragment = new NavigationFragment();
            naviFragment = new NaviFragment();
            parkingFragment = new ParkingFragment();

            musicFragment = new MusicFragment();
            MusicService.getDefaultService().init(MainActivity.this, musicFragment);


        }

        @Override
        public void onMapReady() {
            if (splashFragment.isAdded()) {
                getFragmentManager().beginTransaction().remove(splashFragment).commit();
            }
        }

        @Override
        public void onError(ErrorCode error) {
            OnRoadApplication.getInstance().handleError(error);
        }
    };

    private NavigationService.Listener navigationListener = new NavigationService.Listener() {
        @Override
        public void onNavigationInitialized(NavigationInfo navigationInfo) {
            navigationFragment.updateNavigationInfo(navigationInfo);
            checkParkingAvailability(navigationInfo);
        }

        @Override
        public void onRouteCalculationStarted(NavigationInfo navigationInfo) {
            LogUtil.d("onRouteCalculationStarted");
            navigationFragment.updateNavigationInfo(navigationInfo);
        }

        @Override
        public void onRouteCalculationCompleted(NavigationInfo navigationInfo) {
            LogUtil.d("onRouteCalculationCompleted");
            navigationFragment.updateNavigationInfo(navigationInfo);
        }

        @Override
        public void onRouteCalculationCanceled(NavigationInfo navigationInfo) {
            LogUtil.d("onRouteCalculationCanceled");
            MapService.getDefaultService().centerMe();
        }

        @Override
        public void onRouteCalculationFailed(NavigationInfo navigationInfo, ErrorCode error) {
            LogUtil.d("onRouteCalculationFailed");
            hideNavigation();
            MapService.getDefaultService().centerMe();
            OnRoadApplication.getInstance().handleError(error);
        }

        @Override
        public void onNavigationStarted(final NavigationInfo navigationInfo) {
            LogUtil.d("onNavigationStarted");
            String speech = navigationInfo.getCurrentAdvice().instruction;
            if (speech == null || speech.isEmpty()) {
                speech = "navigating to " + navigationInfo.getNavigationDestination().getDisplayString();
            }
            OnRoadApplication.getInstance().tts(
                    speech,
                    NavigationFragment.TAG,
                    TtsService.SPEECH_PRIORITY_NORMAL,
                    new TtsService.OnSpeechListener() {
                        @Override
                        public void onFinished() {
                            startSimulation();
                        }

                        @Override
                        public void onStopped() {
                            startSimulation();
                        }

                        @Override
                        public void onError(ErrorCode error) {
                            startSimulation();
                        }

                        private void startSimulation() {
                            if (OnRoadApplication.getInstance().getBooleanPreference(R.string.PREF_KEY_NAVIGATION_SIMULATION, false)) {
                                LocationService.getDefaultService().startSimulation(navigationInfo.getRoute(), 14);
                            }
                        }
                    });
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            navigationFragment.updateNavigationInfo(navigationInfo);
            mapFragment.setLeftPadding(navigationFragment.getMapLeftPadding());
            MapService.getDefaultService().centerMe(
                    MapService.FOLLOWING_MODE_POSITION_AND_HEADING, 0,
                    navigationFragment.getMapLeftPadding() / 2,
                    getResources().getDimensionPixelSize(R.dimen.map_navigation_offset));
        }

        @Override
        public void onNavigationUpdated(NavigationInfo navigationInfo) {
            navigationFragment.updateNavigationInfo(navigationInfo);
            mapFragment.setLeftPadding(navigationFragment.getMapLeftPadding());
        }

        @Override
        public void onNavigationInstruction(NavigationInfo navigationInfo) {
            OnRoadApplication.getInstance().tts(navigationInfo.getCurrentAdvice().instruction, NavigationFragment.TAG, TtsService.SPEECH_PRIORITY_LOW);
        }

        @Override
        public void onNavigationEnded(NavigationInfo navigationInfo) {
            LogUtil.d("onNavigationEnded");
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            if (OnRoadApplication.getInstance().getBooleanPreference(R.string.PREF_KEY_NAVIGATION_SIMULATION, false)) {
                LocationService.getDefaultService().stopSimulation();
            }
            mapFragment.setLeftPadding(navigationFragment.getMapLeftPadding());
            MapService.getDefaultService().centerMe(MapService.FOLLOWING_MODE_POSITION_AND_HEADING_NORTH, 0, controlBarWidth / 2, 0);
        }

        @Override
        public void onDestinationReached(NavigationInfo navigationInfo) {
            LogUtil.d("onDestinationReached");
            navigationFragment.updateNavigationInfo(navigationInfo);
            if (navigationInfo.getParkingReservationInfo() == null) {
                SystemUtil.postToMainThread(new Runnable() {
                    @Override
                    public void run() {
                        NavigationService.getDefaultService().stopNavigation();
                        hideNavigation();
                    }
                }, 5000);
            }
        }
    };

    private CarService.Listener carListener = new CarService.Listener() {
        @Override
        public void onDriveStateChanged(int state) {
            String status = "";
            switch (state) {
                case CarService.DRIVE_STATE_DRIVING:
                    status = "driving";
                    controlFragment.startRecognize();
                    break;
                case CarService.DRIVE_STATE_END_DRIVE:
                    status = "driving end";
                    break;
                case CarService.DRIVE_STATE_IGNITION_WAIT:
                    status = "ignition wait";
                    break;
                case CarService.DRIVE_STATE_DISCONNECTED:
                    status = "disconnected";
                    break;
                default:
                    status = "unknown";
            }
            LogUtil.d("drive state changed: " + status);
            Toast.makeText(MainActivity.this, "drive state changed: " + status, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onConnectionChanged(boolean isConnected) {
            LogUtil.d(isConnected ? "connected" : "disconnected");
            Toast.makeText(MainActivity.this, isConnected ? "connected" : "disconnected", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onIgnitionEvent(boolean status) {
            LogUtil.d(status ? "ignition on" : "ignition off");
            Toast.makeText(MainActivity.this, status ? "ignition on" : "ignition off", Toast.LENGTH_LONG).show();
        }
    };

    protected boolean isSpeechRecognizing = false;
    private SpeechRecognizerService.OnStateChangeListener speechRecognizerStateChangeListener = new SpeechRecognizerService.OnStateChangeListener() {
        @Override
        public void onRecognitionStarted() {
            isSpeechRecognizing = true;
            setVolumeControlStream(AudioManager.STREAM_NOTIFICATION);
            controlFragment.listeningStarted();
        }

        @Override
        public void onReadyToSpeech(long timeout, long endPause) {
            controlFragment.listeningReady(timeout);
        }

        @Override
        public void onBeginningOfSpeech() {
            controlFragment.speechBegan();
        }

        @Override
        public void onEndOfSpeech() {
            controlFragment.speechEnded();
        }

        @Override
        public void onRecognitionEnded() {
            isSpeechRecognizing = false;
            if (ttsSpeakingPriority != TtsService.SPEECH_PRIORITY_HIGH) {
                if (ttsSpeakingPriority == 0) {
                    if (musicFragment.isAdded()) {
                        setVolumeControlStream(AudioManager.STREAM_MUSIC);
                    } else {
                        setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
                    }
                }
            }
            controlFragment.listeningEnded();
        }
    };

    private WakeupService.OnStateChangeListener wakeupServiceStateChangeListener = new WakeupService.OnStateChangeListener() {
        @Override
        public void onWakeupStarted() {
            controlFragment.wakeupStarted();
        }

        @Override
        public void onWakeupStopped() {
            controlFragment.wakeupStopped();
        }
    };

    protected int ttsSpeakingPriority = 0;
    private TtsService.OnStateChangeListener ttsStateChangeListener = new TtsService.OnStateChangeListener() {
        @Override
        public void onSpeechStarted(String text, String source, int priority) {
            setVolumeControlStream(TtsService.STREAM_TYPE);
            ttsSpeakingPriority = priority;
        }

        @Override
        public void onSpeechStopped() {
            ttsSpeakingPriority = 0;
            if (!isSpeechRecognizing) {
                if (musicFragment.isAdded()) {
                    setVolumeControlStream(AudioManager.STREAM_MUSIC);
                } else {
                    setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
                }
            }
        }

        @Override
        public void onError(ErrorCode error) {
            OnRoadApplication.getInstance().handleError(error);
        }
    };

    private SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                    if (key.equals(getResources().getString(R.string.PREF_KEY_NAVIGATION_SIMULATION))) {
                        if (!sharedPreferences.getBoolean(key, false)) {
                            LocationService.getDefaultService().stopSimulation();
                        }
                    }
                }
            };

    private static MainActivity sInstance;

    public static MainActivity getInstance() {
        return sInstance;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sInstance = this;

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SystemUtil.setDisplayMetrics(metrics);

        setContentView(R.layout.activity_main);
        controlFragment = new ControlFragment();
        getFragmentManager().beginTransaction().add(R.id.control_layer, controlFragment).commit();

        splashFragment = new SplashFragment();
        getFragmentManager().beginTransaction().add(R.id.splash_layer, splashFragment).commit();

        settingsFragment = new SettingsFragment();

        contentContainer = (ViewGroup) findViewById(R.id.content_container);
//        controlBarWidth = getResources().getDimensionPixelSize(R.dimen.control_button_size) +
//                2 * getResources().getDimensionPixelSize(R.dimen.control_button_margin);
//        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) contentContainer.getLayoutParams();
//        lp.leftMargin = controlBarWidth;
//        contentContainer.setLayoutParams(lp);

        UserService.getInstance().init(this);
        MapService.getDefaultService().init(this, mapListener);
        NavigationService.getDefaultService().init(this, navigationListener);
        GeoService.getDefaultService().init(this);
        WakeupService.getDefaultService().init(this, wakeupServiceStateChangeListener);
        WakeupService.getDefaultService().startListening();
        SpeechRecognizerService.getDefaultService().init(this, speechRecognizerStateChangeListener);
        NluService.getDefaultService().init(this);
        ParkingService.getDefaultService().init(this);
        TtsService.getDefaultService().init(this, ttsStateChangeListener);
        CarService.getDefaultService().init(this, carListener);
        LocationService.getDefaultService().init(this, locationListener);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationService.getDefaultService().resume();
//        settingsFragment.getPreferenceScreen().getSharedPreferences().
//            registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocationService.getDefaultService().pause();
    }

    @Override
    protected void onDestroy() {
        UserService.getInstance().destroy();
        LocationService.getDefaultService().destroy();
        MapService.getDefaultService().destroy();
        SpeechRecognizerService.getDefaultService().destroy();
        WakeupService.getDefaultService().destroy();
        TtsService.getDefaultService().destroy();
        CarService.getDefaultService().destroy();
        MusicService.getDefaultService().destroy();
        sInstance = null;
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LogUtil.d("onConfigurationChanged: " + newConfig);
//
//        if (newConfig.keyboardHidden == Configuration.KEYBOARDHIDDEN_NO ||
//            newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
//            OneTouchApplication.getInstance().disableWakeupService(TAG, null);
//        } else {
//            OneTouchApplication.getInstance().enableWakeupService(TAG);
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        LogUtil.d("onActivityResult: " + requestCode + ", " + resultCode + ", " + intent.getAction());

        if (requestCode == MusicService.LOGIN_REQUST_CODE) {
            MusicService.getDefaultService().onLoginResult(resultCode, intent);
        } else {
            UserService.getInstance().onActivityResult(requestCode, resultCode, intent);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getActionMasked() == MotionEvent.ACTION_DOWN) {
            if (controlFragment != null) {
                controlFragment.resetProximityValue();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        LogUtil.d("dispatchKeyEvent: " + event);
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent) {
        return super.onKeyDown(keyCode, keyEvent);
    }

    @Override
    public void onBackPressed() {
        if (settingsFragment.isAdded()) {
            hideSettings();
            return;
        }

        if (musicFragment.isAdded() && musicFragment.goBack()) {
            return;
        }

        if (parkingFragment.isAdded() && parkingFragment.goBack()) {
            return;
        }

        if (navigationFragment.isAdded() && navigationFragment.goBack()) {
            return;
        }

        super.onBackPressed();
    }

    public void onContentFragmentAdded(ContentFragment fragment) {
        LogUtil.d("MainActivity.onContentFragmentAdded: " + fragment.getClass().getName());
    }

    public void onContentFragmentRemoved(ContentFragment fragment) {
        LogUtil.d("MainActivity.onContentFragmentRemoved: " + fragment.getClass().getName());
    }

    public boolean addContentFragment(ContentFragment fragment) {
        if (!fragment.isAdded()) {
            LogUtil.d("MainActivity.addContentFragment: " + fragment.getClass().getName());
            int layerResId = 0;
            if (fragment == navigationFragment) {
                layerResId = R.id.navigation_layer;
            } else if (fragment == naviFragment) {
                layerResId = R.id.navi_layer;
            } else if (fragment == parkingFragment) {
            layerResId = R.id.parking_layer;
        } else {
                layerResId = R.id.music_layer;
            }
            FragmentManager fm = getFragmentManager();
            fm.beginTransaction().add(layerResId, fragment).commit();
            fm.executePendingTransactions();
            return true;
        }
        return false;
    }

    public boolean removeContentFragment(ContentFragment fragment) {
        if (fragment.isAdded()) {
            LogUtil.d("MainActivity.removeContentFragment: " + fragment.getClass().getName());
            FragmentManager fm = getFragmentManager();
            fm.beginTransaction().remove(fragment).commit();
            fm.executePendingTransactions();
            return true;
        }
        return false;
    }

    public boolean processIntent(int intent, Map<String, Object> data) {
        LogUtil.d("MainActivity processIntent: " + intent);

        if (NAVI_DEBUG) {
            IntentHandler intentHandler = new IntentHandler(getApplication(), this, naviFragment);
            if (intentHandler.processIntent(intent, data) == true){
                return true;
            }
        }

        if (intent == NluService.GEO_ROUTE_INTENT || intent == NluService.GEO_SEARCH_INTENT) {
            String address = "";
            String poi = "";
            if (DataUtil.getBoolean(data, "is_poi")) {
                poi = DataUtil.getString(data, "destination_address");
            } else {
                poi = DataUtil.getString(data, "poi_name");
                address = DataUtil.getString(data, "destination_address");
                if (address.isEmpty()) {
                    address = DataUtil.getString(data, "location");
                }
                if (address.isEmpty()) {
                    address = DataUtil.getString(data, "poi_address");
                }
            }
            final Location currentLocation = LocationService.getDefaultService().getLastLocation();
            if (!address.isEmpty()) {
                if (address.equalsIgnoreCase("your home")) {
                    ActionLogManager.getInstance().getLog(
                            ActionLog.ACTION_TYPE_BOOKMARK,
                            getResources().getString(R.string.home).toLowerCase(),
                            new ActionLogManager.QueryResultListener() {
                                @Override
                                public void onResult(List<ActionLog> results) {
                                    if (results.size() > 0) {
                                        ActionLog actionLog = results.get(0);
                                        PlaceInfo destination = new PlaceInfo();
                                        destination.setBookmark(DataUtil.getString(actionLog.getLog(), "bookmark"));
                                        destination.setId(DataUtil.getString(actionLog.getLog(), "id"));
                                        showNavigation();
                                        navigationFragment.setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                                        navigationFragment.setDestination(destination);
                                    } else {
                                        // notify home is not set
                                    }
                                }

                                @Override
                                public void onError(ErrorCode errorCode) {
                                }
                            });
                } else if (address.equalsIgnoreCase("your work")) {
                    ActionLogManager.getInstance().getLog(
                            ActionLog.ACTION_TYPE_BOOKMARK,
                            getResources().getString(R.string.work).toLowerCase(),
                            new ActionLogManager.QueryResultListener() {
                                @Override
                                public void onResult(List<ActionLog> results) {
                                    if (results.size() > 0) {
                                        ActionLog actionLog = results.get(0);
                                        PlaceInfo destination = new PlaceInfo();
                                        destination.setBookmark(DataUtil.getString(actionLog.getLog(), "bookmark"));
                                        destination.setId(DataUtil.getString(actionLog.getLog(), "id"));
                                        showNavigation();
                                        navigationFragment.setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                                        navigationFragment.setDestination(destination);
                                    } else {
                                        // notify home is not set
                                    }
                                }

                                @Override
                                public void onError(ErrorCode errorCode) {
                                }
                            });
                } else {
                    GeoService.getDefaultService().geocodeQuery(address, currentLocation, new GeoService.GeocodeResultListener() {
                        @Override
                        public void onResult(PlaceInfo place) {
                            showNavigation();
                            navigationFragment.setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                            navigationFragment.setDestination(place);
                        }

                        @Override
                        public void onError(ErrorCode error) {
                            OnRoadApplication.getInstance().handleError(error);
                        }
                    });
                }
            } else if (!poi.isEmpty()) {
                final String query = poi;
                GeoService.getDefaultService().poiQuery(poi, currentLocation, new GeoService.PoiResultListener() {
                    @Override
                    public void onResult(List<PlaceInfo> places) {
                        if (places.size() > 0) {
                            showNavigation();
                            navigationFragment.setRouteType(NavigationInfo.ROUTE_TYPE_CAR_FASTEST);
                            navigationFragment.setDestinations(query, places.subList(0, Math.min(3, places.size())));
                        }
                    }

                    @Override
                    public void onError(ErrorCode error) {
                        OnRoadApplication.getInstance().handleError(error);
                    }
                });
            }
            return true;
        } else if (intent == NluService.CAR_FIND_LOCATION_INTENT) {
            CarService.getDefaultService().findMyCar(new CarService.LocationListener() {
                @Override
                public void onLocation(Location location) {
                    PlaceInfo place = new PlaceInfo();
                    place.setLocation(location);
                    AddressInfo addressInfo = new AddressInfo();
                    place.setAddressInfo(addressInfo);
                    place.setName("My Car");
                    place.addType("car_location");
                    navigationFragment.setRouteType(NavigationInfo.ROUTE_TYPE_PEDESTRIAN);
                    navigationFragment.setDestination(place);
                }

                @Override
                public void onError(ErrorCode error) {
                    OnRoadApplication.getInstance().handleError(error);
                }
            });
            return true;
        } else if (intent == NluService.MUSIC_PLAY_TRACK_INTENT) {
            String query = DataUtil.getString(data, "QueryEntity");
            if (query.isEmpty()) {
                query = DataUtil.getString(data, "TrackName");
            }
            addContentFragment(musicFragment);
            musicFragment.searchAndPlay(query);
            return true;
        } else if (intent == NluService.MUSIC_PLAY_INTENT) {
            addContentFragment(musicFragment);
            MusicService.State state = MusicService.getDefaultService().getState();
            if (state.currentTrackUri != null && !state.currentTrackUri.isEmpty()) {
                musicFragment.resumeMusic();
            } else {
                musicFragment.searchAndPlay("music");
            }
            return true;
        } else if (intent == NluService.MUSIC_NEXT_INTENT) {
            MusicService.getDefaultService().skipToNext();
            return true;
        } else if (intent == NluService.MUSIC_PREV_INTENT) {
            MusicService.getDefaultService().skipToPrevious();
            return true;
        } else if (intent == NluService.MUSIC_PAUSE_INTENT || intent == NluService.MUSIC_STOP_INTENT) {
            musicFragment.pauseMusic();
            return true;
        } else {
            return false;
        }
    }

    public void onMapViewTapped() {
        musicFragment.togglePlayBar();
    }

    public void showMusic() {
        addContentFragment(musicFragment);
        musicFragment.showPlaylist();
    }

    public void hideMusic() {
        removeContentFragment(musicFragment);
    }

    public void showNavigation() {
        if (NAVI_DEBUG) {
            addContentFragment(naviFragment);
        }else {
            addContentFragment(navigationFragment);
        }
    }

    public boolean hideNavigation() {
        mapFragment.setLeftPadding(controlBarWidth);
        TtsService.getDefaultService().stop(NavigationFragment.TAG);
        return removeContentFragment(navigationFragment);
    }

    public void navigateTo(PlaceInfo destination) {
        showNavigation();
        navigationFragment.setDestination(destination);
    }

    public void cancelNavigation() {
        NavigationInfo navigationInfo = NavigationService.getDefaultService().getCurrentNavigationInfo();
        if (navigationInfo != null) {
            if (navigationInfo.isDestinationReached() || navigationInfo.isNavigating()) {
                NavigationService.getDefaultService().stopNavigation();
            } else {
                NavigationService.getDefaultService().cancelRouteCalculation();
            }
        }
        navigationFragment.updateNavigationInfo(null);
    }

    public void showParking() {
        addContentFragment(parkingFragment);
        navigationFragment.setVisibility(false);
    }

    public boolean hideParking() {
        navigationFragment.setVisibility(true);
        parkingFragment.deleteParkingAnnotations();
        return removeContentFragment(parkingFragment);
    }

    public void cancelParking() {
        if (hideParking()) {
            navigationFragment.promptNavigation();
        }
    }

    protected void checkParkingAvailability(final NavigationInfo info) {
        if (info.getRouteType() == NavigationInfo.ROUTE_TYPE_PEDESTRIAN || info.isToParking()) {
            return;
        }
        ParkingService.getDefaultService().checkParkingAvailability(
                info.getDestination().getLocation(),
                info.getArrivalTime(),
                new ParkingService.Listener() {
                    @Override
                    public void onResult(Object data) {
                        Map<String, Object> result = (Map<String, Object>) data;
                        info.setIsParkingAvailable(false);
                        if (DataUtil.getInt(result, "FacilityCount") + DataUtil.getInt(result, "MeterCount") > 0) {
                            info.setIsParkingAvailable(true);
                        }
                        navigationFragment.updateNavigationInfo(info);
                    }

                    @Override
                    public void onError(ErrorCode error) {
                        LogUtil.d("checkParkingAvailability: " + error);
                        OnRoadApplication.getInstance().handleError(error);
                    }
                }

        );
    }

    public void updateRouteAndParking(NavigationInfo info) {
//        navigationFragment.updateNavigationInfo(info);
//        if (info.isRouteReady()) {
//            if (info.getRouteType() == NavigationInfo.ROUTE_TYPE_PEDESTRIAN || info.isToParking()) {
//                navigationFragment.promptNavigation();
//            } else if (info.isParkingOptionsLoaded()) {
//                if (info.isParkingAvailable()) {
//                    showParking();
//                } else {
//                    navigationFragment.promptNavigation();
//                }
//            }
//        }
    }

    public void cancelRecognize() {
        if (controlFragment != null) {
            controlFragment.cancelRecognize();
        }
    }

    private void checkGooglePlayServices() {
        int googlePlayServicesCheck =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        switch (googlePlayServicesCheck) {
            case ConnectionResult.SUCCESS:
                return;
            case ConnectionResult.SERVICE_DISABLED:
            case ConnectionResult.SERVICE_INVALID:
            case ConnectionResult.SERVICE_MISSING:
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(googlePlayServicesCheck,
                        this, 0);
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        MainActivity.this.finish();
                    }
                });
                dialog.show();
                break;
            default:
        }
    }

    public void showSettings() {
        OneTouchApplication.getInstance().disableWakeupService(TAG, null);
        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().add(R.id.settings_layer, settingsFragment).commit();
        fm.executePendingTransactions();
    }

    public void hideSettings() {
        if (settingsFragment.isAdded()) {
            FragmentManager fm = getFragmentManager();
            fm.beginTransaction().remove(settingsFragment).commit();
            fm.executePendingTransactions();
        }
        OneTouchApplication.getInstance().enableWakeupService(TAG);
    }

    public MapFragment getMapFragment() {
        return mapFragment;
    }



}
