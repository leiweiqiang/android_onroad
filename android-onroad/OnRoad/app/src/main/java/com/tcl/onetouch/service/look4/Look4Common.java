package com.tcl.onetouch.service.look4;

import com.locationtoolkit.common.LTKContext;
import com.locationtoolkit.common.data.Location;
import com.locationtoolkit.common.data.Maneuver;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.NavigationInfo;

public class Look4Common {
	public final static String SERVERTOKEN = "gzzaaLMY7DyeKplR5KP6Q8JKrkKizRtt4GwqfIQs.navbuilder.nimlbs.net";

	private static LTKContext sLtkContext;

	public static LTKContext getLTKContext() {
		if (sLtkContext == null) {
			sLtkContext = new LTKContext(OneTouchApplication.getInstance(), Look4Common.SERVERTOKEN, "en", "US");
		}
		return sLtkContext;
	}

    public static com.locationtoolkit.common.data.Location convertLocation(android.location.Location location) {
        if (location == null) {
            return null;
        }
        com.locationtoolkit.common.data.Location loc = new com.locationtoolkit.common.data.Location((location));
        loc.setGpsTime(location.getElapsedRealtimeNanos());
        int valid = com.locationtoolkit.location.Location.VALID_LATITUDE |
            com.locationtoolkit.location.Location.VALID_LONGITUDE;
        if (location.hasAccuracy()) {
            valid |= com.locationtoolkit.location.Location.VALID_ACCURACY;
        }
        if (location.hasBearing()) {
            valid |= com.locationtoolkit.location.Location.VALID_HEADING;
        }
        if (location.hasAltitude()) {
            valid |= com.locationtoolkit.location.Location.VALID_ALTITUDE;
        }
        if (location.hasSpeed()) {
            valid |= com.locationtoolkit.location.Location.VALID_HORIZONTAL_VELOCITY;
        }
        if (location.getTime() > 0L) {
            valid |= com.locationtoolkit.location.Location.VALID_UTC_OFFSET;
        }
        loc.setValid(valid);
        return loc;
    }

    public static int convertDirection(Maneuver.ManeuverCode code) {
        switch (code) {
            case DESTINATION_STRAIGHT_AHEAD:
            case TAKE_RAMP_HIGHWAY_STRAIGHT_AHEAD:
            case EXIT_HIGHWAY_STRAIGHT_AHEAD:
            case CROSS_BRIDGE_STRAIGHT_AHEAD:
            case ENTER_COUNTRY_STRAIGHT_AHEAD:
            case ENTER_HIGHWAY_STRAIGHT_AHEAD:
            case ENTER_PRIVATEROAD_STRAIGHT_AHEAD:
            case ENTER_TUNNEL_STRAIGHT_AHEAD:
            case ESCALATOR_STRAIGHT_AHEAD:
            case STAIRS_STRAIGHT_AHEAD:
            case CONTINUE_ON:
                return NavigationInfo.DIRECTION_STRAIGHT_AHEAD;
            case DESTINATION_LEFT:
            case TURN_LEFT:
            case ORIGIN_LEFT:
                return NavigationInfo.DIRECTION_LEFT;
            case TAKE_RAMP_HIGHWAY_LEFT:
            case EXIT_HIGHWAY_LEFT:
            case SLIGHT_LEFT:
                return NavigationInfo.DIRECTION_SLIGHT_LEFT;
            case HARD_LEFT:
                return NavigationInfo.DIRECTION_HARD_LEFT;
            case DESTINATION_RIGHT:
            case TURN_RIGHT:
            case ORIGIN_RIGHT:
                return NavigationInfo.DIRECTION_RIGHT;
            case TAKE_RAMP_HIGHWAY_RIGHT:
            case EXIT_HIGHWAY_RIGHT:
            case SLIGHT_RIGHT:
                return NavigationInfo.DIRECTION_SLIGHT_RIGHT;
            case HARD_RIGHT:
                return NavigationInfo.DIRECTION_HARD_RIGHT;
            case ENTER_ROUNDABOUT:
            case CONTINUE_STRAIGHT_THROUGH_ROUNDABOUT:
            case ROUNDABOUT_EXIT1:
            case ROUNDABOUT_EXIT2:
            case ROUNDABOUT_EXIT3:
            case ROUNDABOUT_EXIT4:
            case ROUNDABOUT_EXIT5:
            case ROUNDABOUT_EXIT6:
            case ROUNDABOUT_EXIT7:
            case ROUNDABOUT_EXIT8:
            case ROUNDABOUT_EXIT9:
            case ROUNDABOUT_EXIT10:
                return NavigationInfo.DIRECTION_ROUNDABOUT;
            case U_TURN:
                return NavigationInfo.DIRECTION_U_TURN;
            case KEEP_HIGHWAY_LEFT:
            case KEEP_HIGHWAY_RIGHT:
            case KEEP_STREET_LEFT:
            case KEEP_STREET_RIGHT:
            case MERGE_LEFT:
            case MERGE_RIGHT:
            case STAY_HIGHWAY_LEFT:
            case STAY_HIGHWAY_RIGHT:
            case CROSS_BRIDGE_LEFT:
            case CROSS_BRIDGE_RIGHT:
            case ENTER_COUNTRY_LEFT:
            case ENTER_COUNTRY_RIGHT:
            case ENTER_HIGHWAY_LEFT:
            case ENTER_HIGHWAY_RIGHT:
            case ENTER_FERRY:
            case EXIT_FERRY:
            case KEEP_LEFT:
            case KEEP_RIGHT:
            case NAME_CHANGE:
            case ENTER_PRIVATEROAD_LEFT:
            case ENTER_PRIVATEROAD_RIGHT:
            case ORIGIN:
            case CONTINUE_BY_FOOT:
            case STAY_LEFT:
            case STAY_RIGHT:
            case ENTER_TUNNEL_LEFT:
            case ENTER_TUNNEL_RIGHT:
            case ESCALATOR_LEFT:
            case ESCALATOR_RIGHT:
            case STAIRS_LEFT:
            case STAIRS_RIGHT:
            default:
                return NavigationInfo.DIRECTION_INVALID;
        }
    }
}
