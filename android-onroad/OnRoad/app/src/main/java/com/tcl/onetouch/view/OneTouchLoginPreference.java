package com.tcl.onetouch.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.CheckBoxPreference;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.tcl.onetouch.onroad.OnRoadApplication;
import com.tcl.onetouch.onroad.R;
import com.tcl.onetouch.service.UserService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.HttpUtil;
import com.tcl.onetouch.util.LogUtil;

import java.util.HashMap;
import java.util.Map;

public class OneTouchLoginPreference extends Preference {

    private Dialog loginDialog;

    public OneTouchLoginPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public OneTouchLoginPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public OneTouchLoginPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OneTouchLoginPreference(Context context) {
        super(context);
    }


    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        view.findViewById(R.id.one_touch_sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginDialog = new AlertDialog.Builder(getContext()).
                    setView(R.layout.dialog_onetouch_login).
                    setTitle(getContext().getResources().getString(R.string.one_touch_login)).
                    setPositiveButton(R.string.login, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String userName = ((TextView) loginDialog.findViewById(R.id.user_name_text)).getText().toString();
                            String password = ((TextView) loginDialog.findViewById(R.id.password_text)).getText().toString();
                            UserService.getInstance().oneTouchSignIn(userName, password);
                            dialog.dismiss();
                        }
                    }).
                    setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }).
                    create();

                String auth = PreferenceManager.getDefaultSharedPreferences(getContext()).
                    getString(getContext().getResources().getString(com.tcl.onetouch.R.string.PREF_KEY_ONE_TOUCH_LOGIN), "");
                if (!auth.isEmpty()) {
                    Map<String, Object> data = DataUtil.parseJson(auth);
                    ((TextView) loginDialog.findViewById(R.id.user_name_text)).setText(DataUtil.getString(data, "user_name"));
                    ((TextView) loginDialog.findViewById(R.id.password_text)).setText(DataUtil.getString(data, "password"));
                }

                loginDialog.show();
            }
        });
    }
}
