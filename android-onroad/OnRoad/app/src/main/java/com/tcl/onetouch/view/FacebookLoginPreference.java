package com.tcl.onetouch.view;

import android.app.Fragment;
import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.login.widget.LoginButton;
import com.tcl.onetouch.onroad.MainActivity;
import com.tcl.onetouch.onroad.R;
import com.tcl.onetouch.util.LogUtil;

public class FacebookLoginPreference extends Preference {

    public FacebookLoginPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public FacebookLoginPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public FacebookLoginPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FacebookLoginPreference(Context context) {
        super(context);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        LoginButton loginButton = (LoginButton) view.findViewById(R.id.facebook_login_button);
        Fragment fragment = MainActivity.getInstance().getFragmentManager().findFragmentById(R.id.settings_layer);
//        loginButton.setFragment(fragment);
        loginButton.setReadPermissions("public_profile", "user_friends", "email");
//
//        LogUtil.d("onBindView: " + fragment);
    }
}
