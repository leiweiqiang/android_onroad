package com.tcl.onetouch.service.spotify;

import android.annotation.TargetApi;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Build;
import android.util.Log;

import com.spotify.sdk.android.player.AudioController;
import com.spotify.sdk.android.player.AudioRingBuffer;
import com.tcl.onetouch.onroad.BuildConfig;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public class SpotifyAudioController implements AudioController {
    private static final int DEFAULT_CHANNEL_COUNT = 2;
    private static final int AUDIO_BUFFER_SIZE_SAMPLES = 2048;
    private static final int BUFFER_LATENCY_FACTOR = 2;
    private static final int AUDIO_BUFFER_SIZE_FRAMES = 4096;
    private final int AUDIO_BUFFER_CAPACITY = 131072;

    private AudioRingBuffer audioBuffer;
    private ExecutorService executorService;
    private AudioTrack audioTrack;
    private int sampleRate;
    private int channels;
    private float volumeGain;

    private final Runnable audioRunnable = new Runnable() {
        final short[] pendingFrames = new short[AUDIO_BUFFER_SIZE_FRAMES];
        public void run() {
            int itemsRead = audioBuffer.peek(pendingFrames);
            if (itemsRead > 0) {
                int itemsWritten = writeFramesToAudioOutput(pendingFrames, itemsRead);
                audioBuffer.remove(itemsWritten);
            }
        }
    };

    public SpotifyAudioController() {
        sampleRate = 0;
        channels = 0;
        volumeGain = 1.0f;
    }

    @Override
    public int onAudioDataDelivered(final short[] frames, final int numFrames, int sampleRate, int channels) {
        if (audioTrack != null && (this.sampleRate != sampleRate || this.channels != channels)) {
            synchronized (this) {
                audioTrack.release();
                audioTrack = null;
            }
        }

        this.sampleRate = sampleRate;
        this.channels = channels;
        if (audioTrack == null) {
            createAudioTrack(sampleRate, channels);
        }

        try {
            executorService.execute(audioRunnable);
        } catch (RejectedExecutionException ignore) {
        }

        return audioBuffer.write(frames, numFrames);
    }

    @Override
    public void onAudioFlush() {
        audioBuffer.clear();
        if (audioTrack != null) {
            synchronized (this) {
                audioTrack.pause();
                audioTrack.flush();
                audioTrack.release();
                audioTrack = null;
            }
        }
    }

    @Override
    public void onAudioPaused() {
        if (audioTrack != null) {
            audioTrack.pause();
        }
    }

    @Override
    public void onAudioResumed() {
        if (audioTrack != null) {
            audioTrack.play();
        }
    }

    @Override
    public void start() {
        audioBuffer = new AudioRingBuffer(AUDIO_BUFFER_CAPACITY);
        executorService = Executors.newSingleThreadExecutor();
    }

    @Override
    public void stop() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ignore) {
        } finally {
            audioBuffer = null;
            executorService = null;
        }
    }

    @TargetApi(21)
    public void setVolumeGain(float gain) {
        volumeGain = gain;
        if (audioTrack != null) {
            float volume = AudioTrack.getMaxVolume() * volumeGain;
            if (Build.VERSION.SDK_INT >= 21) {
                audioTrack.setVolume(volume);
            } else {
                audioTrack.setStereoVolume(volume, volume);
            }
        }
    }

    @TargetApi(21)
    private void createAudioTrack(int sampleRate, int channels) {
        byte channelConfig;
        switch (channels) {
            case 0:
                throw new IllegalStateException("Input source has 0 channels");
            case 1:
                channelConfig = AudioFormat.CHANNEL_OUT_MONO;
                break;
            case 2:
                channelConfig = AudioFormat.CHANNEL_OUT_STEREO;
                break;
            default:
                throw new IllegalArgumentException("Unsupported input source has " + channels + " channels");
        }

        int bufferSize = AudioTrack.getMinBufferSize(sampleRate, channelConfig, AudioFormat.ENCODING_PCM_16BIT) * BUFFER_LATENCY_FACTOR;
        float volume = AudioTrack.getMaxVolume() * volumeGain;
        synchronized (this) {
            audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, channelConfig, AudioFormat.ENCODING_PCM_16BIT, bufferSize, 1);
            if (audioTrack.getState() == AudioTrack.STATE_INITIALIZED) {
                if (Build.VERSION.SDK_INT >= 21) {
                    audioTrack.setVolume(volume);
                } else {
                    audioTrack.setStereoVolume(volume, volume);
                }
                audioTrack.play();
            } else {
                audioTrack.release();
                audioTrack = null;
            }
        }
    }

    private int writeFramesToAudioOutput(short[] frames, int numFrames) {
        int itemsWritten = 0;
        synchronized (this) {
            if (isAudioTrackPlaying()) {
                itemsWritten = audioTrack.write(frames, 0, numFrames);
            }
        }
        return itemsWritten;
    }

    private boolean isAudioTrackPlaying() {
        return audioTrack != null && audioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING;
    }
}
