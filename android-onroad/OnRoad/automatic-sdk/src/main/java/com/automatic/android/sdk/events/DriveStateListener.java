package com.automatic.android.sdk.events;

import com.automatic.net.servicebinding.DriveState;

/**
 * Created by duncancarroll on 6/23/15.
 */
public interface DriveStateListener {
    void onDriveStateChanged(DriveState state);
}
