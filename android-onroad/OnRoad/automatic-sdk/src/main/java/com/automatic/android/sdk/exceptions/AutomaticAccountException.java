package com.automatic.android.sdk.exceptions;

/**
 * Copyright (c) 2015 Automatic Labs. All rights reserved.
 */
public class AutomaticAccountException extends RuntimeException {
    public AutomaticAccountException(String message) {
        super(message);
    }
}
