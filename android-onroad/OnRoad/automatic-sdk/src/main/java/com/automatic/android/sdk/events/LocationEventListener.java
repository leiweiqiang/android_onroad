package com.automatic.android.sdk.events;

import com.automatic.net.events.Location;

/**
 * Created by duncancarroll on 5/20/15.
 */
public interface LocationEventListener {
    void onLocationEvent(Location location);
}
