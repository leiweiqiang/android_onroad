package com.automatic.android.sdk;

import com.automatic.android.sdk.exceptions.AutomaticAccountException;

import retrofit.RetrofitError;

/**
 * Created by duncancarroll on 2/12/15.
 * Copyright (c) 2015 Automatic Labs. All rights reserved.
 */
public interface AutomaticLoginCallbacks {

    void onLoginSuccess();

    /**
     * @param exception Either {@link AutomaticAccountException} on auto login error
     *                  or {@link RetrofitError} on web login error.
     */
    void onLoginFailure(RuntimeException exception);

}
