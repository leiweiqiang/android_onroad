package com.automatic.android.sdk.events;

import com.automatic.net.events.TripEnded;

/**
 * Created by duncancarroll on 5/20/15.
 */
public interface TripEndedListener {
    void onTripEnded(TripEnded event);
}
