package com.automatic.android.sdk.events;

import com.automatic.net.events.IgnitionEvent;

/**
 * Created by duncancarroll on 5/20/15.
 */
public interface IgnitionEventListener {
    void onIgnitionEvent(IgnitionEvent event);
}

