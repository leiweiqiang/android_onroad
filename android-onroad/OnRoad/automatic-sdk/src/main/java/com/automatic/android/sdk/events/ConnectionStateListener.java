package com.automatic.android.sdk.events;

/**
 * Created by duncancarroll on 5/20/15.
 */
public interface ConnectionStateListener {
    void onDisconnect();
    void onConnect();
    void onLogout();
}
