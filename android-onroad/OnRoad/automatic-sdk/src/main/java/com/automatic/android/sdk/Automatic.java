package com.automatic.android.sdk;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

import com.automatic.android.sdk.events.ConnectionStateListener;
import com.automatic.android.sdk.events.DriveStateListener;
import com.automatic.android.sdk.events.HardEventListener;
import com.automatic.android.sdk.events.IgnitionEventListener;
import com.automatic.android.sdk.events.LocationEventListener;
import com.automatic.android.sdk.events.MILEventListener;
import com.automatic.android.sdk.events.TripEndedListener;
import com.automatic.android.sdk.exceptions.AutomaticSdkException;
import com.automatic.net.AutomaticClientPublic;
import com.automatic.net.LogInterface;
import com.automatic.net.NetworkHandler;
import com.automatic.net.ResponsesPublic;
import com.automatic.net.Scope;
import com.automatic.net.ServiceRequest;
import com.automatic.net.events.Auth;
import com.automatic.net.events.Disconnect;
import com.automatic.net.events.Handshake;
import com.automatic.net.events.HardEvent;
import com.automatic.net.events.IgnitionEvent;
import com.automatic.net.events.Location;
import com.automatic.net.events.MILEvent;
import com.automatic.net.events.State;
import com.automatic.net.events.TripEnded;
import com.automatic.net.responses.User;
import com.automatic.net.servicebinding.DriveState;
import com.automatic.net.servicebinding.ServiceEvents;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Entry point for SDK.  Call initialize() to begin a session.
 * <p/>
 * Created by duncancarroll on 2/12/15.
 * Copyright (c) 2015 Automatic Labs. All rights reserved.
 */
public class Automatic {

    public static final String TAG = "AutomatidSDK";
    public static final int REQUEST_CODE = 0x5afe;

    private static Automatic sAutomatic;
    private Context mContext;
    private AutomaticClientPublic mClient;
    private LoginButton mLoginButton;
    private Activity mLoginButtonParentActivity;
    private AutomaticLoginCallbacks mLoginCallbacks;
    private OAuthHandlerSDK mOAuthHandler = new OAuthHandlerSDK();
    private NetworkHandler mNetworkHandler;
    private List<Scope> mScopes = new ArrayList<>();
    private boolean mUseServiceBinding = false;
    private Boolean mIsServiceAuthenticated = false;
    private boolean mAutoLogIn = true;
    private RestAdapter.LogLevel mLogLevel = RestAdapter.LogLevel.NONE;

    // service event listeners
    private List<DriveStateListener> driveStateListeners = new ArrayList<>();
    private List<IgnitionEventListener> ignitionEventlisteners = new ArrayList<>();
    private List<ConnectionStateListener> connectionStateListeners = new ArrayList<>();
    private List<HardEventListener> hardEventListeners = new ArrayList<>();
    private List<LocationEventListener> locationEventListeners = new ArrayList<>();
    private List<MILEventListener> milEventListeners = new ArrayList<>();
    private List<TripEndedListener> tripEndedListeners = new ArrayList<>();

    // the messenger representing the client
    private Messenger mMessenger = new Messenger(new ServiceHandler());
    private Messenger mService = null;
    private boolean mIsBound = false;

    private LogInterface mLogInterface = new LogInterface() {
        @Override
        public void logDebug(String s) {
            Log.d("AutomaticSDK", s);
        }

        @Override
        public void logException(String s) {
            Log.e("AutomaticSDK", s);
        }
    };

    private List<IgnitionEvent> ignitionEventHistory = new ArrayList<>();

    /**
     * Entry point to the SDK.  Pass in a Builder object and the client will be created.
     * Note that before API calls can be made, the user must be logged in.
     *
     * @param builder Builder instance
     */
    public static void initialize(Builder builder) {
        sAutomatic = builder.build();

        sAutomatic.mClient = new AutomaticClientPublic(sAutomatic.mOAuthHandler, sAutomatic.mLogLevel, sAutomatic.mLogInterface);
        sAutomatic.mNetworkHandler = new NetworkHandler(sAutomatic.mClient);
        // initialize the Internal context
        Internal.init(sAutomatic.mContext);

        // set initial button state post-init
        if (sAutomatic.mLoginButton != null) {
            if (sAutomatic.mLoginButtonParentActivity != null) {
                sAutomatic.addLoginButton(sAutomatic.mLoginButton, sAutomatic.mLoginButtonParentActivity);
            }
            sAutomatic.mLoginButton.updateStyles();
        }
    }

    private Automatic() {
        /* private constructor to prevent instance creation */
    }

    /**
     * Builder class to create an SDK instance.
     */
    public static class Builder {

        private Automatic mAutomatic;
        private String clientSecret;

        public Builder(Context context) {
            mAutomatic = new Automatic();
            mAutomatic.mContext = context.getApplicationContext();
        }

        /**
         * Add multiple scopes here, e.g.:
         * addScopes(Scope.Public, Scope.Location, Scope.Vin);
         *
         * @param scopes varargs of scopes to add.  Old scopes will not be deleted.  If a scope is already added, it will be skipped.
         * @return instance of Builder
         */
        public Builder addScopes(Scope[] scopes) {
            for (Scope scope : scopes) {
                addScope(scope);
            }
            return this;
        }

        /**
         * Add an individual scope to the API context
         *
         * @param scope the scope to add
         * @return Builder instance
         */
        public Builder addScope(Scope scope) {
            // only add if we haven't added it already
            if (!mAutomatic.mScopes.contains(scope)) {
                mAutomatic.mScopes.add(scope);
            }
            return this;
        }

        /**
         * Add a LoginButton instance and set the default click handler
         *
         * @param loginButton your LoginButton
         * @param activity    your Activity
         * @return Builder instance
         */
        public Builder useLoginButton(LoginButton loginButton, final Activity activity) {
            mAutomatic.mLoginButton = loginButton;
            mAutomatic.mLoginButtonParentActivity = activity;
            return this;
        }

        /**
         * Set whether to auto log in using local Automatic account
         *
         * @param autoLogIn true to log in using local Automatic account
         * @return Builder instance
         */
        public Builder autoLogIn(boolean autoLogIn) {
            mAutomatic.mAutoLogIn = autoLogIn;
            return this;
        }

        /**
         * Set the log level of the SDK
         *
         * @param level Retrofit log level
         * @return Builder instance
         */
        public Builder logLevel(LogLevel level) {
            switch (level) {
                case None:
                    mAutomatic.mLogLevel = RestAdapter.LogLevel.NONE;
                    break;
                case Basic:
                    mAutomatic.mLogLevel = RestAdapter.LogLevel.BASIC;
                    break;
                case Full:
                    mAutomatic.mLogLevel = RestAdapter.LogLevel.FULL;
                    break;
            }
            return this;
        }

        /**
         * Called within the Builder to allow
         * usage of the AutomaticService binding.
         * Note:  Before the service can be bound, the user
         * must be logged in with OAuth.
         *
         * @return Builder instance
         */
        public Builder useServiceBinding() {
            mAutomatic.mUseServiceBinding = true;
            return this;
        }

        private Automatic build() {
            return mAutomatic;
        }
    }

    public static Automatic get() {
        if (sAutomatic == null) {
            throw new AutomaticSdkException("SDK has not been initialized.");
        }

        return sAutomatic;
    }

    protected LoginButton getButton() {
        return mLoginButton;
    }

    public void addLoginButton(LoginButton loginButton, final Activity activity) {
        mLoginButton = loginButton;
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // not logged in
                if (!Internal.get().hasToken()) {
                    loginWithAutomatic(activity);
                } else {
                    // already logged-in
                    // show "really log out?"
                    mLoginButton.showLogoutConfirmDialog(activity);
                }

            }
        });
    }

    /**
     * Method to manually invoke the OAuth login flow (for example if you are creating your own login button)
     *
     * @param activity Activity context
     */
    public void loginWithAutomatic(Activity activity) {
        // try to launch
        launchActivityHandler(activity);
    }

    private void launchActivityHandler(Activity activity) {
        Intent launchIntent = new Intent(activity, LoginActivity.class);
        launchIntent.putExtra(LoginActivity.KEY_AUTO_LOGIN, Automatic.get().mAutoLogIn);

        try {
            activity.startActivityForResult(launchIntent, REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "Error: You must add com.automatic.sdk.LoginActivity to your AndroidManifest");
            throw e;
        }
    }

    public AutomaticClientPublic restApi() {
        return mClient;
    }

    public boolean isLoggedIn() {
        return Internal.get().hasToken();
    }

    public void logout() {
        // remove token
        Internal.get().reset();
        // also update button state (if it's not null)
        if (mLoginButton != null) {
            mLoginButton.updateStyles();
        }
        // unbind
        doUnbindService();
    }

    protected String getScopesString() {
        String scopes = "";
        for (Scope scope : mScopes) {
            scopes += scope.serverName() + "%20";
        }
        return scopes;
    }

    // Scopes array to primitive String[] array
    protected String[] getScopes() {
        String[] out = new String[mScopes.size()];
        for (int i = 0; i < mScopes.size(); i++) {
            out[i] = mScopes.get(i).name();
        }
        return out;
    }

    boolean isServiceAuthenticated() {
        return mIsServiceAuthenticated;
    }

    boolean isServiceBound() {
        return mIsBound;
    }

    public void setLoginCallbackListener(AutomaticLoginCallbacks callbacks) {
        mLoginCallbacks = callbacks;
    }

    private void invokeCallback(Boolean success, RetrofitError error) {
        if (success) {
            mLoginCallbacks.onLoginSuccess();
        } else {
            mLoginCallbacks.onLoginFailure(error);
        }
    }


    // used to keep access protected
    protected void getTokenApi(String code, Callback<ResponsesPublic.OAuthResponse> callback) {
        if (mNetworkHandler != null) {
            mNetworkHandler.getTokenApi(code, callback);
        }
    }

    // used to keep access protected
    protected ResponsesPublic.OAuthResponse refreshToken(String refreshToken) {
        if (mNetworkHandler != null) {
            return mNetworkHandler.refreshToken(refreshToken);
        }
        return null;
    }

    boolean usesServiceBinding() {
        return mUseServiceBinding;
    }

    /**
     * Public method to bind to the AutomaticService class
     */
    public void bindService() {
        if (!isLoggedIn()) {
            throw new AutomaticSdkException("Must be logged in before service can be bound!");
        }
        // bind but do not authenticate (wait until we receive the LoginButton token)
        doBindService();
    }

    /**
     * Unbind from the service
     */
    public void unbindService() {
        doUnbindService();
    }

    /**
     * Establish a connection with the service.  We use an explicit
     * class name because there is no reason to be able to let other
     * applications replace our component.
     */
    private void doBindService() {

        if (mService != null && mService.getBinder().isBinderAlive()) {
            Log.e(TAG, "Service is already bound!");
            return;
        }

        if (mContext == null) {
            throw new AutomaticSdkException("Fatal error: SDK Context was null.  Need to re-initializs()");
        }

        Intent intent = Utils.createExplicitFromImplicitIntent(mContext, new Intent("com.automatic.service.AutomaticService.BIND"));
        //intent.setPackage("com.automatic.service");
        if (intent != null) {
            mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            Log.d(TAG, "Binding...");
        }else {
            Log.e(TAG, "Error: app requested bindService(), but no matching Intent could be found.  Is Automatic not installed?");
        }
    }


    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mIsBound = true;
            mService = new Messenger(service);
            Log.i(TAG, "Attached.");

            // The first thing we do before we bind is check if we have an authorization
            // by sending a Handshake packet
            try {
                // send a handshake packet
                send(new ServiceRequest.Handshake(mOAuthHandler.getClientId()));
            } catch (RemoteException e) {
                e.printStackTrace();
                return;
            }

            // As part of the sample, tell the user what happened.
            //Toast.makeText(mContext, "Service Connected", Toast.LENGTH_SHORT).show();
        }

        public void onServiceDisconnected(ComponentName className) {
            mIsBound = false;
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;
            Log.w(TAG, "Disconnected.");
            // As part of the sample, tell the user what happened.
            //Toast.makeText(mContext, "Service Disconnected", Toast.LENGTH_SHORT).show();
        }
    };


    /**
     * Handle messages received from the bound Service
     */
    protected class ServiceHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {

            // is message valid?
            if (msg == null || msg.obj == null || !(msg.obj instanceof Bundle)) {
                Log.e(TAG, "Error: Message was null or malformed.");
                return;
            }

            try {
                Bundle payload = (Bundle) msg.obj;

                switch (msg.what) {
                    case ServiceEvents.MSG_HANDSHAKE:
                        // parse out message
                        Handshake handshake = (Handshake) Utils.parse(payload, Handshake.class);

                        // check for null
                        if (handshake == null) {
                            Log.e(TAG, "Couldn't parse Handshake!");
                            return;
                        }

                        // failure case; send a re-auth in this case
                        if (!handshake.success) {
                            Log.e(TAG, "Handshake failed.  Requesting authorization");
                            sendAuthRequest();
                        }else {
                            mIsServiceAuthenticated = true;
                            // drive state is passed along with handshake, so send that along.
                            notifyDriveStateListeners(handshake.state);
                        }
                        break;
                    case ServiceEvents.MSG_AUTH_RESPONSE:
                        // parse out message
                        Auth authResponse = (Auth) Utils.parse(payload, Auth.class);

                        // check for null
                        if (authResponse == null) {
                            Log.e(TAG, "Couldn't parse Auth Response!");
                            mIsServiceAuthenticated = false;
                            unbindService();
                            return;
                        }

                        // failure case
                        if (!authResponse.success) {
                            Log.e(TAG, "Error obtaining Service authorization: " + authResponse.failureMessage);
                            if (authResponse.failureMessage != null && authResponse.failureMessage.equals("401")) {
                                // trigger a token refresh, and re-auth when done
                                triggerTokenRefresh(true);
                            }else {
                                // if this is a non-401 failure then it means we should unbind
                                unbindService();
                            }
                            mIsServiceAuthenticated = false;
                            return;
                        }
                        Log.d(TAG, "Successfully authenticated with Service.");

                        // drive state is passed along with auth, so send that along.
                        notifyDriveStateListeners(authResponse.state);

                        // notify connection state listeners
                        for (ConnectionStateListener listener: connectionStateListeners) {
                            listener.onConnect();
                        }
                        break;
                    case ServiceEvents.MSG_UNAUTHORIZED:
                        mIsServiceAuthenticated = false;
                        // if we get an unauthorized, send an auth request
                        sendAuthRequest();
                        break;
                    case ServiceEvents.MSG_DISCONNECT:
                        Disconnect disconnect = (Disconnect) Utils.parse(payload, Disconnect.class);

                        // check for null
                        if (disconnect == null) {
                            Log.e(TAG, "Couldn't parse Disconnect!");
                            return;
                        }

                        if (disconnect.success) {
                            Log.d(TAG, "Successfully disconnected from Service");
                            for (ConnectionStateListener listener: connectionStateListeners) {
                                listener.onDisconnect();
                            }
                        }
                        break;
                    case ServiceEvents.MSG_STATE:
                        State state = (State) Utils.parse(payload, State.class);

                        // check for null
                        if (state == null) {
                            Log.e(TAG, "Couldn't parse State!");
                            return;
                        }

                        notifyDriveStateListeners(state.state);
                        break;
                    case ServiceEvents.MSG_IGNITION_EVENT:
                        // parse message
                        IgnitionEvent ignitionEvent = (IgnitionEvent) Utils.parse(payload, IgnitionEvent.class);

                        // check for null
                        if (ignitionEvent == null) {
                            Log.e(TAG, "Couldn't parse IgnitionEvent!");
                            return;
                        }

                        // don't give us duplicate events
                        if (isDuplicateEvent(ignitionEvent)) {
                            break;
                        }else {
                            ignitionEventHistory.add(ignitionEvent);
                        }

                        // notify listeners
                        for (IgnitionEventListener listener : ignitionEventlisteners) {
                                listener.onIgnitionEvent(ignitionEvent);
                        }
                        break;
                    case ServiceEvents.MSG_TRIP_FINISHED:
                        // parse message
                        TripEnded tripEndedEvent = (TripEnded) Utils.parse(payload, TripEnded.class);

                        // check for null
                        if (tripEndedEvent == null) {
                            Log.e(TAG, "Couldn't parse TripEnded!");
                            return;
                        }

                        // notify listeners
                        for (TripEndedListener listener: tripEndedListeners) {
                            listener.onTripEnded(tripEndedEvent);
                        }
                        break;
                    case ServiceEvents.MSG_MIL_EVENT:
                        // parse message
                        MILEvent milEvent = (MILEvent) Utils.parse(payload, MILEvent.class);

                        // check for null
                        if (milEvent == null) {
                            Log.e(TAG, "Couldn't parse MIL Event!");
                            return;
                        }

                        // notify listeners
                        for (MILEventListener listener: milEventListeners) {
                            listener.onMILEvent(milEvent);
                        }
                        break;
                    case ServiceEvents.MSG_LOCATION_CHANGED:
                        // parse message
                        Location locationEvent = (Location) Utils.parse(payload, Location.class);

                        // check for null
                        if (locationEvent == null) {
                            Log.e(TAG, "Couldn't parse Location!");
                            return;
                        }

                        // notify listeners
                        for (LocationEventListener listener: locationEventListeners) {
                            listener.onLocationEvent(locationEvent);
                        }
                        break;
                    case ServiceEvents.MSG_HARD_EVENT:
                        // parse message
                        HardEvent hardEvent = (HardEvent) Utils.parse(payload, HardEvent.class);

                        // check for null
                        if (hardEvent == null) {
                            Log.e(TAG, "Couldn't parse hard event!");
                            return;
                        }

                        // notify listeners
                        for (HardEventListener listener: hardEventListeners) {
                            listener.onHardEvent(hardEvent);
                        }
                        break;
                    case ServiceEvents.MSG_LOGOUT:
                        // notify listeners
                        for (ConnectionStateListener listener: connectionStateListeners) {
                            listener.onLogout();
                        }
                        break;
                    default:
                        super.handleMessage(msg);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
                // disconnect?
                //doUnbindService();
            }
        }
    }

    /**
     * Method to trigger a token refresh
     */
    private void triggerTokenRefresh(final Boolean reAuthOnSuccess) {
        // have to do this off the main thread
        new Thread(new Runnable() {
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                // refresh and pass pack to the main thread
                handleTokenRefresh(mOAuthHandler.refreshToken(Internal.get().getToken()), reAuthOnSuccess);
            }
        }).start();
    }

    private void handleTokenRefresh(final boolean success, final boolean reAuthOnSuccess) {
        // get back on main thread
        if (mContext != null) {
            new Handler(mContext.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    if (!success) {
                        // do nothing.  probably should log an error though.
                        Log.e(TAG, "Error refreshing token:  app will try again later.");
                    } else if (success && reAuthOnSuccess) {
                        try {
                            sendAuthRequest();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            // failed. will try again.
                            Log.e(TAG, "Error refreshing token:  app will try again later.");
                        }
                    }
                }
            });
        }
    }

    /**
     * Don't send through duplicate ignition events.
     * @param newEvent
     * @return
     */
    private boolean isDuplicateEvent(IgnitionEvent newEvent) {
        for (IgnitionEvent lastEvent : ignitionEventHistory) {
            Long delta = newEvent.created - lastEvent.created;
            if (delta < TimeUnit.SECONDS.toMillis(2)) {
                Log.w(TAG, "Ignition event was duplicate! Did not send through to client " + "( delta " + delta + " ms)");
                return true;
            }
        }
        return false;
    }

    private void notifyDriveStateListeners(DriveState state) {
        for (DriveStateListener listener: driveStateListeners) {
            listener.onDriveStateChanged(state);
        }
    }

    protected void sendHandshake() throws RemoteException {
        send(new ServiceRequest.Handshake(mOAuthHandler.getClientId()));
    }

    protected void sendAuthRequest() throws RemoteException {
        send(new ServiceRequest.Auth(mOAuthHandler.getClientId(), getScopes(), Internal.get().getToken()));
    }

    /**
     * Send a message to the service
     * @param request
     * @throws RemoteException
     */
    protected void send(ServiceRequest.BaseRequest request) throws RemoteException {
        Bundle payload = new Bundle();
        payload.putString(ServiceEvents.FIELD_DATA, Utils.getGson().toJson(request));
        Message msg = Message.obtain(null, request.CMD, payload);
        msg.replyTo = mMessenger;
        if (mService != null) {
            mService.send(msg);
        }else {
            Log.e(TAG, "Error: Service binding was null.  Didn't send message.");
        }
    }

    protected void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                try {
                    send(new ServiceRequest.Disconnect());
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }

            // Detach our existing connection.
            mContext.unbindService(mConnection);
            mIsBound = false;

            Log.i(TAG, "Unbinding.");
        } else {
            Log.i(TAG, "Service is already unbound.");
        }
    }

    public User getUser() {
        if (isLoggedIn()) {
            return Internal.get().getUser();
        }else {
            throw new AutomaticSdkException("Must be logged in!");
        }
    }

    public void setUser(User user) {
        if (isLoggedIn()) {
            Internal.get().setUser(user);
        }else {
            throw new AutomaticSdkException("Must be logged in!");
        }
    }

    /**
     * Sends drive state request to core app. Client must implement and
     * add {@link DriveStateListener} to Automatic SDK instance in order
     * to receive callback.
     *
     * @throws RemoteException Throws DeadObjectException if the target
     * Handler no longer exists.
     */
    public void queryCurrentDriveState() throws RemoteException {
        send(new ServiceRequest.State());
    }


    /**
     * LISTENER ADD / REMOVE
     */


    // Add a listener TODO genericize
    /**
     * Listen for connection state
     * @param listener the listener to invoke
     */
    public void addConnectionStateListener(ConnectionStateListener listener) {
        if (!connectionStateListeners.contains(listener)) {
            connectionStateListeners.add(listener);
        }else {
            Log.w(TAG, "Not adding listener that was already added.");
        }
    }

    /**
     * Listen for ignition on / off
     * @param listener the listener to invoke
     */
    public void addIgnitionEventListener(IgnitionEventListener listener) {
        if (!ignitionEventlisteners.contains(listener)) {
            ignitionEventlisteners.add(listener);
        }else {
            Log.w(TAG, "Not adding listener that was already added.");
        }
    }

    /**
     * Listen for hard events
     * @param listener the listener to invoke
     */
    public void addHardEventListener(HardEventListener listener) {
        if (!hardEventListeners.contains(listener)) {
            hardEventListeners.add(listener);
        }else {
            Log.w(TAG, "Not adding listener that was already added.");
        }
    }

    /**
     * Listen for location changes
     * @param listener the listener to invoke
     */
    public void addLocationEventListener(LocationEventListener listener) {
        if (!locationEventListeners.contains(listener)) {
            locationEventListeners.add(listener);
        }else {
            Log.w(TAG, "Not adding listener that was already added.");
        }
    }

    /**
     * Listen for MIL events
     * @param listener the listener to invoke
     */
    public void addMILEventListener(MILEventListener listener) {
        if (!milEventListeners.contains(listener)) {
            milEventListeners.add(listener);
        }else {
            Log.w(TAG, "Not adding listener that was already added.");
        }
    }

    /**
     * Listen for trip ended
     * @param listener the listener to invoke
     */
    public void addTripEndedListener(TripEndedListener listener) {
        if (!tripEndedListeners.contains(listener)) {
            tripEndedListeners.add(listener);
        }else {
            Log.w(TAG, "Not adding listener that was already added.");
        }
    }

    /**
     * Listen for trip ended
     * @param listener the listener to invoke
     */
    public void addDriveStateListener(DriveStateListener listener) {
        if (!driveStateListeners.contains(listener)) {
            driveStateListeners.add(listener);
        }else {
            Log.w(TAG, "Not adding listener that was already added.");
        }
    }

    // Remove Listeners TODO genericize

    /**
     * Remove a listener
     * @param listener the listener to remove
     */
    public void removeIgnitionEventListener(IgnitionEventListener listener) {
        if (ignitionEventlisteners.contains(listener)) {
            ignitionEventlisteners.remove(listener);
        }else {
            Log.w(TAG, "Listener wasn't added; can't remove.");
        }
    }

    /**
     * Remove a listener
     * @param listener the listener to remove
     */
    public void removeHardEventListener(HardEventListener listener) {
        if (hardEventListeners.contains(listener)) {
            hardEventListeners.remove(listener);
        }else {
            Log.w(TAG, "Listener wasn't added; can't remove.");
        }
    }

    /**
     * Remove a listener
     * @param listener the listener to remove
     */
    public void removeConnectionStateListener(ConnectionStateListener listener) {
        if (connectionStateListeners.contains(listener)) {
            connectionStateListeners.remove(listener);
        }else {
            Log.w(TAG, "Listener wasn't added; can't remove.");
        }
    }

    /**
     * Remove a listener
     * @param listener the listener to remove
     */
    public void removeLocationEventListener(LocationEventListener listener) {
        if (locationEventListeners.contains(listener)) {
            locationEventListeners.remove(listener);
        }else {
            Log.w(TAG, "Listener wasn't added; can't remove.");
        }
    }

    /**
     * Remove a listener
     * @param listener the listener to remove
     */
    public void removeMILEventListener(MILEventListener listener) {
        if (milEventListeners.contains(listener)) {
            milEventListeners.remove(listener);
        }else {
            Log.w(TAG, "Listener wasn't added; can't remove.");
        }
    }

    /**
     * Remove a listener
     * @param listener the listener to remove
     */
    public void removeTripEndedListener(TripEndedListener listener) {
        if (tripEndedListeners.contains(listener)) {
            tripEndedListeners.remove(listener);
        }else {
            Log.w(TAG, "Listener wasn't added; can't remove.");
        }
    }

    /**
     * Remove a listener
     * @param listener the listener to remove
     */
    public void removeDriveStateListener(DriveStateListener listener) {
        if (driveStateListeners.contains(listener)) {
            driveStateListeners.remove(listener);
        }else {
            Log.w(TAG, "Listener wasn't added; can't remove.");
        }
    }
}