package com.automatic.android.sdk;

import android.util.Log;

import com.automatic.net.OAuthHandler;
import com.automatic.net.ResponsesPublic;

import retrofit.RetrofitError;

/**
 * Created by duncancarroll on 5/17/15.
 */
public class OAuthHandlerSDK implements OAuthHandler {

    @Override
    public ResponsesPublic.OAuthResponse getToken() {
//            if (SET_BAD_TOKEN != null) {
//                ResponsesPublic.OAuthResponse token = Internal.getToken();
//                token.access_token = SET_BAD_TOKEN;
//                token.expires_in = 0;
//                SET_BAD_TOKEN = null; // reset flag
//                return token;
//            }
        return Internal.get().getToken();
    }

    @Override
    public void setToken(ResponsesPublic.OAuthResponse token) {
        Internal.get().setToken(token);
    }

    @Override
    public boolean refreshToken(ResponsesPublic.OAuthResponse oldToken) {
        ResponsesPublic.OAuthResponse newToken = null;

        // Can't refresh without an old token
        if (oldToken == null) {
            return false;
        }

        // synchronous requests in retrofit use try...catch blocks to handle failure cases
        try {
            newToken = Automatic.get().refreshToken(oldToken.refresh_token);
        }catch (RetrofitError error) {
            // If the request returns any 4xx response, that is a complete failure.
            if ((error.getResponse() != null) && String.valueOf(error.getResponse().getStatus()).startsWith("4")) {
                return false;
            }else {
                // any other error response is benign (network issue, intermittent server 500, etc.), so we return true, which allows us to try again later
                return true;
            }
        }

        // Handle success case
        if (newToken != null) {
            // Store the new token
            Internal.get().setToken(newToken);
            return true;
        }
        return false;
    }

    @Override
    public String getClientId() {
        return Internal.get().getClientId();
    }

    @Override
    public void onRefreshFailed() {
        Log.e("Automatic SDK", "Fatal error: Couldn't refresh token!  Logging out the user rather than failing all subsequent network requests");
        Automatic.get().logout();
    }
}
