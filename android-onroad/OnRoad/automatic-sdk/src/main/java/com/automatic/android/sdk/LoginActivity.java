package com.automatic.android.sdk;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.automatic.android.sdk.exceptions.AutomaticAccountException;
import com.automatic.android.sdk.exceptions.AutomaticSdkException;
import com.automatic.net.ResponsesPublic;
import com.automatic.net.responses.User;

import java.util.UUID;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by duncancarroll on 2/12/15.
 * Copyright (c) 2015 Automatic Labs. All rights reserved.
 */
public class LoginActivity extends Activity {

    private static final int REQ_GET_ACCOUNTS = 42;
    private final String TAG = LoginActivity.this.getClass().getSimpleName();

    private static final String AUTOMATIC_ACCOUNT = "com.automatic.account";
    private static final String AUTOMATIC_TOKEN = "com.automatic.user_account";
    private static final String KEY_AUTOMATIC_CLIENT_ID = "com.automatic.client_id";
    private static final String KEY_AUTOMATIC_CLIENT_PACKAGE = "com.automatic.client.package";

    private static final String AUTH_URL_BASE = "https://www.automatic.com/oauth/";
    public static final String KEY_AUTO_LOGIN = "key_auto_login";
    private WebView mWebView;
    private ProgressDialog mLoadingSpinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        mLoadingSpinner = new ProgressDialog(this, ProgressDialog.THEME_HOLO_LIGHT);
        mWebView = (WebView) findViewById(R.id.auth_webview);

        Intent intent = getIntent();
        boolean autoLogIn = true;
        if (intent != null) {
            autoLogIn = intent.getBooleanExtra(KEY_AUTO_LOGIN, true);
        }

        if (autoLogIn) {
            getAuthTokenFromCoreApp(this);
        } else {
            initWebView();
        }
    }

    private void initWebView() {
        showLoadingSpinner();
        // this is the only thing that will remove the auth creds in webview
        CookieSyncManager.createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        // Paranoidly also use old-school cachebuster to avoid caching
        String cacheBuster = "#" + UUID.randomUUID().toString();

        mWebView.clearCache(true);
        mWebView.clearHistory();
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        // don't save form data b/c that's bad.
        webSettings.setSaveFormData(false);
        mWebView.setWebViewClient(new SdkWebViewClient());

        // load the url!
        mWebView.loadUrl(AUTH_URL_BASE + "authorize/" + "?response_type=code&client_id=" +
            Internal.get().getClientId() + "&scope=" + Automatic.get().getScopesString() + cacheBuster);
    }

    @Override
    public void onResume() {
        super.onResume();
        // always clear the cache
        mWebView.clearCache(true);
        mWebView.clearHistory();
    }

    // might not need this
    @Override
    public void onStop() {
        super.onStop();
        mWebView.clearCache(true);
        mWebView.clearHistory();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQ_GET_ACCOUNTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission granted!  Accessing account credentials");
                    getAuthTokenFromCoreApp(this);
                } else {
                    Log.e(TAG, "Permission NOT GRANTED.  Falling back on WebView");
                    initWebView();
                }
            }
        }
    }

    /**
     * Intercepts webview URL loads and allows us to grab the code
     */
    private class SdkWebViewClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            hideLoadingSpinner();
            view.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(TAG, "Inside shouldOverrideUrlLoading() with url: " + url);
            // is our code present in the URL?  If so, pull it out.
            final String MARKER = "code=";
            if (url.toLowerCase().contains(MARKER.toLowerCase())) {
                int index = url.indexOf(MARKER) + MARKER.length(); // indexOf(MARKER) will get me the beginning of the marker, but we want the end, so add the marker length.
                // token is always a 40-character string preceded by "&code="
                String code;
                try {
                    code = url.substring(index, index + 40);
                    showLoadingSpinner();
                    Log.i(TAG, "Got token: " + code);
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                    throw new AutomaticSdkException("Error: No authorize code was returned or could be found within the response.  Check your client id and try again.");
                }
                // got the initial token, now turn around and get our auth_token
                getToken(code);
                return true;
            }
            return false;
        }
    }

    private void getToken(String mCode) {
        final Automatic automaticSdkInstance = Automatic.get();

        automaticSdkInstance.getTokenApi(mCode, new Callback<ResponsesPublic.OAuthResponse>() {
            @Override
            public void success(ResponsesPublic.OAuthResponse oAuthResponse, Response response) {

                Internal.get().setToken(oAuthResponse);

                // always pull in userinfo get User info
                automaticSdkInstance.restApi().getUser(new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        automaticSdkInstance.setUser(user);
                        proceed();
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, "Failed to get user!");
                        proceed();
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                // TODO if the user has defined a failure callback, we call that, otherwise we fail out with a Toast
                hideLoadingSpinner();
                Log.e(TAG, "Error: " + error.getResponse().getStatus() + ", " + error.getMessage());

                Boolean userHandledCallback = automaticSdkInstance.getButton().invokeCallbacks(false, error);

                // show a toast error to the user by default
                if (!userHandledCallback) {
                    Toast.makeText(LoginActivity.this, "Error communicating with Automatic\'s servers.  Please try again later.", Toast.LENGTH_LONG).show();
                }

                Internal.get().reset();
                // did I mention we always clear the cache?
                mWebView.clearCache(true);
                mWebView.clearHistory();
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    /**
     * Get the authToken from the main app. Since this process may take some time, the
     * token is returned via a callback.
     *
     * @param activity The activity that launches this call.
     */
    private synchronized void getAuthTokenFromCoreApp(final Activity activity) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // ask for permission to access accounts
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "Permission has not been granted to access accounts!  Requesting permissions.");
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.GET_ACCOUNTS)) {
                    Log.e(TAG, "The system thinks we should show a rationale.");
                }
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, REQ_GET_ACCOUNTS);
                return;
            }
        }

        AccountManager mgr = AccountManager.get(activity);
        Account[] accounts = mgr.getAccountsByType(AUTOMATIC_ACCOUNT);
        Log.d(TAG, String.format("getAuthTokenFromCoreApp accounts.length = %d", accounts.length));
        if (accounts.length == 0) {
            handleAccountFailure(getString(R.string.account_error_no_account), new AutomaticAccountException("No Automatic account"));
            // TODO should fall back on web-based login instead of failing out
        } else if (accounts.length == 1) {
            showLoadingSpinner();
            Bundle options = new Bundle();
            options.putString(KEY_AUTOMATIC_CLIENT_ID, Internal.get().getClientId());
            options.putString(KEY_AUTOMATIC_CLIENT_PACKAGE, getPackageName());

            mgr.confirmCredentials(accounts[0], options, activity,
                new AccountManagerCallback<Bundle>() {
                    @Override
                    public void run(AccountManagerFuture<Bundle> future) {
                        try {
                            Bundle result = future.getResult();
                            String grantToken = result.getString(AccountManager.KEY_USERDATA);
                            getToken(grantToken);
                        } catch (Exception e) {
                            Log.e(TAG, "cannot get auth result", e);
                        }
                    }
                }, null);
        } else {
            handleAccountFailure(getString(R.string.account_error_more_than_one_account), new AutomaticAccountException("More than one Automatic account"));
            // TODO should fall back on web-based login instead of failing out
        }
    }

    private void handleAccountFailure(String errorMessage, AutomaticAccountException exception) {
        Log.e(TAG, errorMessage);
        boolean userHandledCallback = Automatic.get().getButton().invokeCallbacks(false, exception);
        // show a toast error to the user by default
        if (!userHandledCallback) {
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    private void proceed() {
        hideLoadingSpinner();

        final LoginButton loginButton = Automatic.get().getButton();
        if (loginButton != null) {
            loginButton.invokeCallbacks(true);
            // set the button state as soon as we can
            loginButton.updateStyles();
        }
        // technically we're done, so finish us out and call our success handler
        setResult(Activity.RESULT_OK);
        finish();
    }

    private void showLoadingSpinner() {
        // show loading dialog
        if (mLoadingSpinner != null) {
            mLoadingSpinner.setMessage("Loading...");
        }
        mLoadingSpinner.show();
    }

    private void hideLoadingSpinner() {
        if (mLoadingSpinner != null) {
            mLoadingSpinner.dismiss();
        }
    }
}
