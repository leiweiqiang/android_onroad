package com.automatic.android.sdk.events;

import com.automatic.net.events.MILEvent;

/**
 * Created by duncancarroll on 5/20/15.
 */
public interface MILEventListener {
    void onMILEvent(MILEvent event);
}
