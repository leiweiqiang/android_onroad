package com.automatic.android.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;

import com.automatic.android.sdk.exceptions.AutomaticSdkException;
import com.automatic.net.servicebinding.ServiceEvents;

/**
 * This broadcast receiver receives pings from the core Automatic App, the point of which
 * is to wake up any app that is listening to these events, and (re)establish the Service Binding.
 * Note: To receive events, clients will need to add this receiver to the manifest.
 *
 * Created by duncancarroll on 5/11/15.
 */
public class ServiceBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return;
        }

        if (intent.getAction().equals(ServiceEvents.ACTION_PING)) {
            try {
                final Automatic automaticSdkInstance = Automatic.get();
                // if we're not logged in,or we are not registered to use the service binding, ignore this
                if (!automaticSdkInstance.isLoggedIn() || !automaticSdkInstance.usesServiceBinding()) {
                    return;
                }
                handlePing();
            } catch (AutomaticSdkException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * A ping request is a systemwide broadcast that AutomaticService sends which
     * causes any unbound clients to re-bind.  In the event that any clients tokens have
     * expired, we attempt to re_auth.
     */
    private void handlePing() {
        Log.d("ServiceBroadcast", "Got ACTION_PING!");
        final Automatic automaticSdkInstance = Automatic.get();
        if (!automaticSdkInstance.isServiceBound()) {
            // if we aren't bound we need to re-bind
            automaticSdkInstance.bindService();
        } else if (!automaticSdkInstance.isServiceAuthenticated()) {
            // bound but not yet authenticated, start authentication process.
            try {
                automaticSdkInstance.sendHandshake();
            } catch (RemoteException e) {
                e.printStackTrace();
                // TODO unbind here?
            }
        }
    }
}
