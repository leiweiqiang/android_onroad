package com.automatic.android.sdk.events;

import com.automatic.net.events.HardEvent;

/**
 * Created by duncancarroll on 5/20/15.
 */
public interface HardEventListener {
    void onHardEvent(HardEvent event);
}
