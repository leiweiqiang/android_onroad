package com.tcl.onetouch.research;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import com.tcl.onetouch.util.LogUtil;

public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtil.d("SmsReceiver SMS received");
        SmsMessage[] smsMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
        if (smsMessages != null) {
            for (SmsMessage smsMessage : smsMessages) {
                LogUtil.d("SMS From: " + smsMessage.getOriginatingAddress() + " - " + smsMessage.getMessageBody());
            }
        }
    }
}
