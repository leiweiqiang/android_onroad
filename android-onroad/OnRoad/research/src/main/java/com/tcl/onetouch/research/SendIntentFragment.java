package com.tcl.onetouch.research;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SendIntentFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_send_intent, container, false);

        final TextView intentText = (TextView) view.findViewById(R.id.intent_text);
        final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        view.findViewById(R.id.send_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = intentText.getText().toString();
                try {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(intent);
                } catch (Exception e) {
                    AlertDialog ad = adb.create();
                    ad.setMessage("Failed to Launch: " + e.getMessage());
                    ad.show();
                }
            }
        });

        return view;
    }
}
