package com.tcl.onetouch.research;

import android.app.Activity;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tcl.onetouch.util.LogUtil;

public class SmsFragment extends Fragment implements View.OnClickListener {
    private static final String ACTION_SENT = "ONROAD_SMS_SENT";
    private static final String ACTION_DELIVERED = "ONROAD_SMS_DELIVERED";
    private static final String SMS_BUNDLE = "pdus";

    private TextView phoneNumberText;
    private TextView messageText;

    private PendingIntent sentIntent;
    private PendingIntent deliveredIntent;

    private BroadcastReceiver sendReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    LogUtil.d("SMS sent");
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    LogUtil.d("Generic failure");
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    LogUtil.d("No service");
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    LogUtil.d("Null PDU");
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    LogUtil.d("Radio off");
                    break;
            }
        }
    };

    private BroadcastReceiver deliveredReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    LogUtil.d("SMS delivered");
                    break;
                case Activity.RESULT_CANCELED:
                    LogUtil.d("SMS not delivered");
                    break;
            }
        }
    };

    private BroadcastReceiver incomingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtil.d("SMS received");
            SmsMessage[] smsMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            if (smsMessages != null) {
                for (SmsMessage smsMessage : smsMessages) {
                    LogUtil.d("SMS From: " + smsMessage.getOriginatingAddress() + " - " + smsMessage.getMessageBody());
                }
            }
        }
    };

    private class InboxObserver extends ContentObserver {
        private ContentResolver contentResolver;

        public InboxObserver(ContentResolver contentResolver) {
            super(new Handler());
            this.contentResolver = contentResolver;
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            LogUtil.d(uri + " content changed");
            String[] cols = new String[]{Telephony.Sms.Inbox._ID, Telephony.Sms.Inbox.ADDRESS, Telephony.Sms.Inbox.BODY};
            Cursor cursor = contentResolver.query(Telephony.Sms.Inbox.CONTENT_URI, cols, null, null, null);
            while (cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex(Telephony.Sms.Inbox._ID));
                String address = cursor.getString(cursor.getColumnIndex(Telephony.Sms.Inbox.ADDRESS));
                String body = cursor.getString(cursor.getColumnIndex(Telephony.Sms.Inbox.BODY));
                LogUtil.d(id + ", " + address + ", " + body);
            }
        }
    }

    ;
    private InboxObserver inboxObserver;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        sentIntent = PendingIntent.getBroadcast(
            getActivity(), 0, new Intent(ACTION_SENT), PendingIntent.FLAG_UPDATE_CURRENT);
        deliveredIntent = PendingIntent.getBroadcast(
            getActivity(), 0, new Intent(ACTION_DELIVERED), PendingIntent.FLAG_UPDATE_CURRENT);

        inboxObserver = new InboxObserver(getActivity().getContentResolver());
        inboxObserver.onChange(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sms, container, false);

        phoneNumberText = (TextView) view.findViewById(R.id.phone_number_text);
        phoneNumberText.setText("4087574410");
        messageText = (TextView) view.findViewById(R.id.message_text);
        messageText.setText("Hello!");

        view.findViewById(R.id.send_button).setOnClickListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        getActivity().registerReceiver(sendReceiver, new IntentFilter(ACTION_SENT));
        getActivity().registerReceiver(deliveredReceiver, new IntentFilter(ACTION_DELIVERED));

        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(Integer.MAX_VALUE);
        getActivity().registerReceiver(incomingReceiver, intentFilter);

        getActivity().getContentResolver().registerContentObserver(Telephony.Sms.CONTENT_URI, true, inboxObserver);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(sendReceiver);
        getActivity().unregisterReceiver(deliveredReceiver);
        getActivity().unregisterReceiver(incomingReceiver);
        getActivity().getContentResolver().unregisterContentObserver(inboxObserver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_button:
                String phoneNo = phoneNumberText.getText().toString();
                String msg = messageText.getText().toString();
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNo, null, messageText.getText().toString(), sentIntent, deliveredIntent);
                    LogUtil.d(msg + " sent to " + phoneNo);
                } catch (Exception e) {
                    LogUtil.d("", e);
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
