package com.tcl.onetouch.research;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PhoneFragment extends Fragment implements View.OnClickListener {
    public static final int PERMISSIONS_REQUEST_CALL_PHONE = 1;

    private TextView phoneNumberText;
    private Object telephonyService;
    private Method startCallMethod;
    private Method answerCallMethod;
    private Method endCallMethod;

    private boolean callInitiated;
    private boolean callStarted;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        LogUtil.d("PhoneFragment.onCreate");
        TelephonyManager tm = (TelephonyManager) getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        tm.listen(new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                LogUtil.d("callStateChanged: " + state + ", " + incomingNumber);
                if (state == TelephonyManager.CALL_STATE_OFFHOOK && callInitiated) {
                    LogUtil.d("phone offhook");
                    callInitiated = false;
                    SystemUtil.postToMainThread(new Runnable() {
                        @Override
                        public void run() {
                            LogUtil.d("startActivity");
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            getActivity().startActivity(intent);
                        }
                    }, 100);
                }
            }
        }, PhoneStateListener.LISTEN_CALL_STATE);
        Class c = null;
        try {
            c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = m.invoke(tm); // Get the internal ITelephony object
            c = Class.forName(telephonyService.getClass().getName()); // Get its class
            startCallMethod = c.getMethod("call", String.class, String.class);
            startCallMethod.setAccessible(true);
            answerCallMethod = c.getMethod("answerRingingCall");
            answerCallMethod.setAccessible(true);
            endCallMethod = c.getDeclaredMethod("endCall"); // Get the "endCall()" method
            endCallMethod.setAccessible(true); // Make it accessible
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phone, container, false);

        phoneNumberText = (TextView) view.findViewById(R.id.phone_number_text);
        phoneNumberText.setText("4087574410");
        view.findViewById(R.id.call_button).setOnClickListener(this);
        view.findViewById(R.id.pickup_button).setOnClickListener(this);
        view.findViewById(R.id.end_button).setOnClickListener(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtil.d("PhoneFragment onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtil.d("PhoneFragment onPause");
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.call_button:
                try {
                    LogUtil.d("start calling");
                    callInitiated = true;
                    startCallMethod.invoke(
                        telephonyService,
                        getActivity().getApplication().getPackageName(),
                        phoneNumberText.getText().toString());
                    LogUtil.d("call initiated");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

//                int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
//                    Manifest.permission.CALL_PHONE);
//                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                        Manifest.permission.CALL_PHONE)) {
//
//                        // Show an expanation to the user *asynchronously* -- don't block
//                        // this thread waiting for the user's response! After the user
//                        // sees the explanation, try again to request the permission.
//
//                    } else {
//
//                        // No explanation needed, we can request the permission.
//
//                        ActivityCompat.requestPermissions(getActivity(),
//                            new String[] { Manifest.permission.CALL_PHONE },
//                            PERMISSIONS_REQUEST_CALL_PHONE);
//
//                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                        // app-defined int constant. The callback method gets the
//                        // result of the request.
//                    }
//                } else {
//                    Intent dialIntent = new Intent(Intent.ACTION_CALL);
//                    dialIntent.setData(Uri.parse("tel:4087574410"));
//                    startActivity(dialIntent);
//                }
                break;
            case R.id.pickup_button:
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            Runtime.getRuntime().exec("input keyevent " +
                                Integer.toString(KeyEvent.KEYCODE_HEADSETHOOK));
                        } catch (IOException e) {
                            // Runtime.exec(String) had an I/O problem, try to fall back
                            String enforcedPerm = "android.permission.CALL_PRIVILEGED";
                            Intent btnDown = new Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                                Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN,
                                    KeyEvent.KEYCODE_HEADSETHOOK));
                            Intent btnUp = new Intent(Intent.ACTION_MEDIA_BUTTON).putExtra(
                                Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP,
                                    KeyEvent.KEYCODE_HEADSETHOOK));

                            getActivity().sendOrderedBroadcast(btnDown, enforcedPerm);
                            getActivity().sendOrderedBroadcast(btnUp, enforcedPerm);
                        }
                    }

                }).start();
//                int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
//                    Manifest.permission.MODIFY_PHONE_STATE);
//                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                        Manifest.permission.MODIFY_PHONE_STATE)) {
//
//                        // Show an expanation to the user *asynchronously* -- don't block
//                        // this thread waiting for the user's response! After the user
//                        // sees the explanation, try again to request the permission.
//
//                    } else {
//
//                        // No explanation needed, we can request the permission.
//
//                        ActivityCompat.requestPermissions(getActivity(),
//                            new String[] { Manifest.permission.MODIFY_PHONE_STATE },
//                            PERMISSIONS_REQUEST_CALL_PHONE);
//
//                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                        // app-defined int constant. The callback method gets the
//                        // result of the request.
//                    }
//                }
//                try {
//                    answerCallMethod.invoke(telephonyService);
//                } catch (IllegalAccessException e) {
//                    e.printStackTrace();
//                } catch (InvocationTargetException e) {
//                    e.printStackTrace();
//                }
//                Log.d(BuildConfig.LOG_TAG, "send button down");
//                Intent buttonDown = new Intent(Intent.ACTION_MEDIA_BUTTON);
//                buttonDown.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
//                getActivity().sendOrderedBroadcast(buttonDown, "android.permission.CALL_PRIVILEGED");
//
//                // froyo and beyond trigger on buttonUp instead of buttonDown
//                Log.d(BuildConfig.LOG_TAG, "send button up");
//                Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);
//                buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
//                getActivity().sendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED");
                break;
            case R.id.end_button:
                try {
                    endCallMethod.invoke(telephonyService);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
