package com.tcl.onetouch.research;

import android.app.Fragment;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.NluService;
import com.tcl.onetouch.service.SpeechRecognizerService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.Metaphone3;
import com.tcl.onetouch.util.StringUtil;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class VoiceSelectFragment extends Fragment implements View.OnClickListener {

    private List<List<String>> tests;
    private int currTest;
    private int selectedIndex;

    private BaseAdapter listAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return currTest < 0 ? 0: tests.get(currTest).size();
        }

        @Override
        public Object getItem(int position) {
            return currTest < 0? null : tests.get(currTest).get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = VoiceSelectFragment.this.getActivity().getLayoutInflater().inflate(
                    R.layout.voice_select_item, parent, false);
            }
            ((TextView) convertView.findViewById(R.id.item_text)).setText(tests.get(currTest).get(position));
            if (selectedIndex == position) {
                ((CheckBox) convertView.findViewById(R.id.selected_check)).setChecked(true);
            } else {
                ((CheckBox) convertView.findViewById(R.id.selected_check)).setChecked(false);
            }
            return convertView;
        }
    };

    private ListView listView;
    private TextView answerText;
    private View prevButton;
    private View nextButton;
    private View micButton;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        tests = new ArrayList<>();
        currTest = -1;

        AssetManager as = getActivity().getAssets();
        try {
            InputStream is = as.open("multiple_choice.txt", AssetManager.ACCESS_STREAMING);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = reader.readLine();
            List<String> test = null;
            while (line != null) {
                if (line.startsWith("**********")) {
                    test = new ArrayList<>();
                    tests.add(test);
                } else if (test != null) {
                    int index = line.indexOf(':');
                    if (index >= 0) {
                        String s1 = line.substring(0, index);
                        String s2 = line.substring(index + 1);
                        test.add(s2);
                    }
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (tests.size() > 0) {
            currTest = 0;
        }
        selectedIndex = -1;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_voice_select, container, false);

        listView = (ListView) view.findViewById(R.id.item_list);
        listView.setAdapter(listAdapter);

        answerText = (TextView) view.findViewById(R.id.answer_text);
        prevButton = view.findViewById(R.id.prev_button);
        prevButton.setOnClickListener(this);
        nextButton = view.findViewById(R.id.next_button);
        nextButton.setOnClickListener(this);
        micButton = view.findViewById(R.id.mic_button);
        micButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listView = null;
        answerText = null;
        prevButton = null;
        nextButton = null;
        micButton = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prev_button:
                if (currTest > 0) {
                    --currTest;
                    selectedIndex = -1;
                    listAdapter.notifyDataSetChanged();
                    answerText.setText("");
                }
                break;
            case R.id.next_button:
                if (currTest < tests.size() - 1) {
                    ++currTest;
                    selectedIndex = -1;
                    listAdapter.notifyDataSetChanged();
                    answerText.setText("");
                }
                break;
            case R.id.mic_button:
                selectedIndex = -1;
                listAdapter.notifyDataSetChanged();
                SpeechRecognizerService.getDefaultService().startRecognize(10000, 1000, new SpeechRecognizerService.AbstractOnResultListener() {
                    @Override
                    public void onResult(final String answer) {
                        answerText.setText(answer);
                        NluService.getDefaultService().parseSelection(answer, tests.get(currTest), new NluService.ResultListener() {
                            @Override
                            public void onResult(Map<String, Object> result) {
                                selectedIndex = DataUtil.getInt(result, NluService.RESULT_KEY_RESULT) - 1;
                                listAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onError(ErrorCode error) {
                                ResearchApplication.getInstance().handleError(error);
                            }
                        });
                    }

                    @Override
                    public void onPartialResult(String partialResult) {
                        answerText.setText(partialResult);
                    }

                    @Override
                    public void onError(ErrorCode error) {
                        ResearchApplication.getInstance().handleError(error);
                    }
                });
                break;
        }
    }


}
