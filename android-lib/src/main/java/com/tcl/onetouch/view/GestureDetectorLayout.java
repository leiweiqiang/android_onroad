package com.tcl.onetouch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import com.tcl.onetouch.util.SystemUtil;

public class GestureDetectorLayout extends RelativeLayout implements GestureDetector.OnGestureListener {
    public static class OnSwipeListener {
        public void onSwipeLeft() {
        }

        public void onSwipeRight() {
        }

        public void onSwipeUp() {
        }

        public void onSwipeDown() {
        }
    }

    public static interface OnTapListener {
        public void onTap(float x, float y);
    }

    private GestureDetector detector;
    private OnSwipeListener onSwipeListener;
    private OnTapListener onTapListener;
    private boolean blockTap;

    public GestureDetectorLayout(Context context) {
        this(context, null);
    }

    public GestureDetectorLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GestureDetectorLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        detector = new GestureDetector(context, this);
        blockTap = false;
    }

    public void setOnSwipeListener(OnSwipeListener listener) {
        onSwipeListener = listener;
    }

    public void setOnTapListener(OnTapListener listener) {
        setOnTapListener(listener, true);
    }

    public void setOnTapListener(OnTapListener listener, boolean blockTap) {
        onTapListener = listener;
        this.blockTap = blockTap;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (detector.onTouchEvent(ev)) {
            return true;
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (ev.getActionMasked() != MotionEvent.ACTION_DOWN) {
            detector.onTouchEvent(ev);
        }
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        if (onTapListener != null) {
            onTapListener.onTap(e.getX(), e.getY());
        }
        return blockTap;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (onSwipeListener == null) {
            return false;
        }
        float xDist = (e2.getRawX() - e1.getRawX()) / SystemUtil.getDisplayMetrics().xdpi;
        float yDist = (e2.getRawY() - e1.getRawY()) / SystemUtil.getDisplayMetrics().ydpi;
        if (Math.abs(xDist) >= 0.5 || Math.abs(yDist) >= 0.5) {
            if (Math.abs(xDist) > Math.abs(yDist)) {
                if (xDist > 0) {
                    onSwipeListener.onSwipeRight();
                } else {
                    onSwipeListener.onSwipeLeft();
                }
            } else {
                if (yDist > 0) {
                    onSwipeListener.onSwipeDown();
                } else {
                    onSwipeListener.onSwipeUp();
                }
            }
        }
        return true;
    }
}
