package com.tcl.onetouch.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.util.ImageUtil;

public class AsyncImageView extends ImageView implements ImageUtil.OnImageLoadedListener {
    public AsyncImageView(Context context) {
        super(context);
    }

    public AsyncImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AsyncImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AsyncImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setImageUri(String uri) {
        ImageUtil.getInstance().loadImage(uri, this);
    }

    @Override
    public void onLoaded(Bitmap bitmap) {
        setImageBitmap(bitmap);
    }

    @Override
    public void onError(ErrorCode error) {
    }
}
