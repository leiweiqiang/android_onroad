package com.tcl.onetouch.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.tcl.onetouch.R;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.ErrorCode;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

public class HttpUtil {
    private static final int HTTP_READ_TIMEOUT = 60000;
    private static final int HTTP_CONNECT_TIMEOUT = 60000;

    private static SSLContext sslContext;
    private static HostnameVerifier hostnameVerifier;
    static {
        if (SystemUtil.isDev()) {
            try {
                InputStream caInput = new BufferedInputStream(
                    OneTouchApplication.getInstance().getResources().openRawResource(R.raw.onetouch));
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                Certificate ca = cf.generateCertificate(caInput);
                caInput.close();

                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);

                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                tmf.init(keyStore);

                sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, tmf.getTrustManagers(), null);
            } catch (CertificateException e) {
                LogUtil.d("", e);
            } catch (IOException e) {
                LogUtil.d("", e);
            } catch (NoSuchAlgorithmException e) {
                LogUtil.d("", e);
            } catch (KeyStoreException e) {
                LogUtil.d("", e);
            } catch (KeyManagementException e) {
                LogUtil.d("", e);
            }

            hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
        }
    }

    private static ErrorCode handleResponse(HttpURLConnection conn) throws IOException {
        int response = conn.getResponseCode();
        if (response >= 200 && response < 300) {
            return null;
        } else if (response == HttpURLConnection.HTTP_CLIENT_TIMEOUT) {
            return new ErrorCode(ErrorCode.NETWORK_ERROR_HTTP_TIMEOUT_CLIENT, conn.getResponseMessage());
        } else if (response == HttpURLConnection.HTTP_GATEWAY_TIMEOUT) {
            return new ErrorCode(ErrorCode.NETWORK_ERROR_HTTP_TIMEOUT_GATEWAY, conn.getResponseMessage());
        } else if (response >= 400 && response < 500) {
            return new ErrorCode(ErrorCode.NETWORK_ERROR_HTTP_REQUEST, conn.getResponseMessage());
        } else if (response >= 500) {
            return new ErrorCode(ErrorCode.NETWORK_ERROR_HTTP_SERVER, conn.getResponseMessage());
        } else {
            return new ErrorCode(ErrorCode.NETWORK_ERROR, conn.getResponseMessage());
        }
    }

    public static class HttpRequestResult {
        public String result;
        public ErrorCode error;
    }

    public static interface RequestListener {
        public void onResult(HttpRequestResult result);
    }

    private static class HttpRequestTask extends AsyncTask<Void, Void, HttpRequestResult> {
        private String requestUrl;
        private Map<String, String> header;
        private Map<String, String> query;
        private byte[] postBody;
        private HttpUtil.RequestListener listener;
        private String method;

        HttpRequestTask(String url, Map<String, String> header, Map<String, String> query, HttpUtil.RequestListener listener) {
            this("GET", url, header, query, listener);
        }

        HttpRequestTask(String url, Map<String, String> header, byte[] postBody, HttpUtil.RequestListener listener) {
            this(url, header, null, postBody, listener);
        }

        HttpRequestTask(String url, Map<String, String> header, Map<String, String> query, byte[] postBody, HttpUtil.RequestListener listener) {
            requestUrl = url;
            this.header = header;
            this.query = query;
            this.postBody = postBody;
            this.listener = listener;
            method = "POST";
        }

        HttpRequestTask(String method, String url, Map<String, String> header, Map<String, String> query, HttpUtil.RequestListener listener) {
            requestUrl = url;
            this.header = header;
            this.query = query;
            postBody = null;
            this.listener = listener;
            this.method = method;
        }

        @Override
        protected HttpRequestResult doInBackground(Void... params) {
            HttpRequestResult result = new HttpRequestResult();
            ConnectivityManager connMgr = (ConnectivityManager)
                OneTouchApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()) {
                result.error = new ErrorCode(ErrorCode.NETWORK_ERROR_NO_NETWORK);
                return result;
            }
            try {
                // build query string
                StringBuilder builder = new StringBuilder();
                if (query != null) {
                    for (Map.Entry<String, String> entry : query.entrySet()) {
                        if (builder.length() > 0) {
                            builder.append("&");
                        }
                        builder.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                        builder.append("=");
                        builder.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                    }
                }
                String queryString = builder.toString();

                // create http connection
                final URL url;
                if ("GET".equals(method) || postBody != null) {
                    url = new URL(requestUrl + queryString);
                } else {
                    url = new URL(requestUrl);
                }
                LogUtil.d("http request: " + url.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                if (SystemUtil.isDev() && url.getHost().endsWith("onetouch.rideo.io") &&
                    conn instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) conn).setSSLSocketFactory(sslContext.getSocketFactory());
                    ((HttpsURLConnection) conn).setHostnameVerifier(hostnameVerifier);
                }
                if (header != null) {
                    for (Map.Entry<String, String> entry : header.entrySet()) {
                        conn.setRequestProperty(entry.getKey(), entry.getValue());
                    }
                }
                conn.setReadTimeout(HttpUtil.HTTP_READ_TIMEOUT);
                conn.setConnectTimeout(HttpUtil.HTTP_CONNECT_TIMEOUT);
                conn.setRequestMethod(method);
                conn.setDoInput(true);

                // do the request
                if ("GET".equals(method)) {
                    conn.connect();
                } else if (postBody != null) {
                    if (postBody.length > 0) {
                        conn.setDoOutput(true);
                        OutputStream os = conn.getOutputStream();
                        BufferedOutputStream bos = new BufferedOutputStream(os);
                        bos.write(postBody);
                        bos.flush();
                        bos.close();
                        os.close();
                    }
                } else if (!queryString.isEmpty()) {
                    LogUtil.d("post query: " + queryString);
                    conn.setDoOutput(true);
                    OutputStream os = conn.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(queryString);
                    writer.flush();
                    writer.close();
                    os.close();
                }

                // get response
                result.error = handleResponse(conn);
                InputStream is = conn.getInputStream();
                Reader reader = new InputStreamReader(is, "UTF-8");
                StringWriter writer = new StringWriter();
                char[] buffer = new char[1024];
                int n = 0;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
                result.result = writer.toString();
                is.close();
                reader.close();
                writer.close();
            } catch (SocketTimeoutException e) {
                result.error = new ErrorCode(ErrorCode.NETWORK_ERROR_HTTP_TIMEOUT, e.getMessage());
            } catch (IOException e) {
                result.error = new ErrorCode(ErrorCode.NETWORK_ERROR, e.getMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(HttpRequestResult result) {
            if (listener != null) {
                listener.onResult(result);
            }
        }
    }

    public static class HttpDownloadResult {
        public byte[] result;
        public ErrorCode error;
    }

    public static interface DownloadListener {
        public void onResult(HttpDownloadResult result);
    }

    private static class HttpDownloadTask extends AsyncTask<Void, Void, HttpDownloadResult> {
        private String downloadUrl;
        private Map<String, String> header;
        private Map<String, String> query;
        private HttpUtil.DownloadListener listener;

        HttpDownloadTask(String url, Map<String, String> header, Map<String, String> query, HttpUtil.DownloadListener listener) {
            downloadUrl = url;
            this.header = header;
            this.query = query;
            this.listener = listener;
        }

        @Override
        protected HttpDownloadResult doInBackground(Void... params) {
            HttpDownloadResult result = new HttpDownloadResult();
            ConnectivityManager connMgr = (ConnectivityManager)
                OneTouchApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()) {
                result.error = new ErrorCode(ErrorCode.NETWORK_ERROR_NO_NETWORK);
                return result;
            }
            try {
                // build query string
                StringBuilder builder = new StringBuilder();
                if (query != null) {
                    for (Map.Entry<String, String> entry : query.entrySet()) {
                        if (builder.length() > 0) {
                            builder.append("&");
                        }
                        builder.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                        builder.append("=");
                        builder.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                    }
                }
                String queryString = builder.toString();

                // create http connection
                URL url = new URL(downloadUrl + queryString);
                LogUtil.d("download: " + url.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                if (header != null) {
                    for (Map.Entry<String, String> entry : header.entrySet()) {
                        conn.setRequestProperty(entry.getKey(), entry.getValue());
                    }
                }
                conn.setReadTimeout(HttpUtil.HTTP_READ_TIMEOUT);
                conn.setConnectTimeout(HttpUtil.HTTP_CONNECT_TIMEOUT);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);

                // do the request
                conn.connect();

                // get response
                result.error = handleResponse(conn);
                InputStream is = conn.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] buffer = new byte[8192];
                int n = 0;
                while ((n = bis.read(buffer, 0, 8192)) != -1) {
                    bos.write(buffer, 0, n);
                }
                result.result = bos.toByteArray();
                is.close();
                bis.close();
                bos.close();
            } catch (SocketTimeoutException e) {
                result.error = new ErrorCode(ErrorCode.NETWORK_ERROR_HTTP_TIMEOUT, e.getMessage());
            } catch (IOException e) {
                result.error = new ErrorCode(ErrorCode.NETWORK_ERROR, e.getMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(HttpDownloadResult result) {
            if (listener != null) {
                listener.onResult(result);
            }
        }
    }

    public static void doGet(String url, Map<String, String> query, HttpUtil.RequestListener listener) {
        HttpRequestTask task = new HttpRequestTask(url, null, query, listener);
        task.executeOnExecutor(SystemUtil.getExecutor());
    }

    public static void doGet(String url, Map<String, String> header, Map<String, String> query, HttpUtil.RequestListener listener) {
        HttpRequestTask task = new HttpRequestTask(url, header, query, listener);
        task.executeOnExecutor(SystemUtil.getExecutor());
    }

    public static void doPost(String url, Map<String, String> header, Map<String, String> query, HttpUtil.RequestListener listener) {
        HttpRequestTask task = new HttpRequestTask("POST", url, header, query, listener);
        task.executeOnExecutor(SystemUtil.getExecutor());
    }

    public static void doPost(String url, Map<String, String> query, HttpUtil.RequestListener listener) {
        HttpRequestTask task = new HttpRequestTask("POST", url, null, query, listener);
        task.executeOnExecutor(SystemUtil.getExecutor());
    }

    public static void doPost(String url, Map<String, String> header, byte[] postBody, HttpUtil.RequestListener listener) {
        HttpRequestTask task = new HttpRequestTask(url, header, postBody, listener);
        task.executeOnExecutor(SystemUtil.getExecutor());
    }

    public static void doPost(String url, Map<String, String> header, Map<String, String> query, byte[] postBody, HttpUtil.RequestListener listener) {
        HttpRequestTask task = new HttpRequestTask(url, header, query, postBody, listener);
        task.executeOnExecutor(SystemUtil.getExecutor());
    }

    public static void doPost(String url, byte[] postBody, HttpUtil.RequestListener listener) {
        HttpRequestTask task = new HttpRequestTask(url, null, postBody, listener);
        task.executeOnExecutor(SystemUtil.getExecutor());
    }

    public static void downloadData(String url, HttpUtil.DownloadListener listener) {
        HttpDownloadTask task = new HttpDownloadTask(url, null, null, listener);
        task.executeOnExecutor(SystemUtil.getExecutor());
    }
}
