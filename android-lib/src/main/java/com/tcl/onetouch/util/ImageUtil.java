package com.tcl.onetouch.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.LruCache;

import com.tcl.onetouch.model.ErrorCode;

public class ImageUtil {
    private LruCache<String, byte[]> dataCache;
    private LruCache<String, Bitmap> bitmapCache;

    private static ImageUtil sInstance;

    public static ImageUtil getInstance() {
        if (sInstance == null) {
            sInstance = new ImageUtil();
        }
        return sInstance;
    }

    protected ImageUtil() {
        // TODO: make maxSize be memory size
        dataCache = new LruCache<>(20);
        bitmapCache = new LruCache<>(10);
    }

    public interface OnImageLoadedListener {
        public void onLoaded(Bitmap bitmap);
        public void onError(ErrorCode error);
    }

    protected class DecodeImageTask extends AsyncTask<byte[], Void, Bitmap> {
        String uri;
        OnImageLoadedListener listener;

        DecodeImageTask(String uri, OnImageLoadedListener listener) {
            this.uri = uri;
            this.listener = listener;
        }

        @Override
        protected Bitmap doInBackground(byte[]... params) {
            byte[] data = params[0];
            return BitmapFactory.decodeByteArray(data, 0, data.length);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            bitmapCache.put(uri, bitmap);
            listener.onLoaded(bitmap);
        }
    }

    public void loadImage(final String uri, final OnImageLoadedListener listener) {
        // TODO: handle other kinds of uri
        Bitmap bitmap = bitmapCache.get(uri);
        if (bitmap != null) {
            listener.onLoaded(bitmap);
            return;
        }
        byte[] data = dataCache.get(uri);
        if (data != null) {
            (new DecodeImageTask(uri, listener)).execute(data);
            return;
        }
        HttpUtil.downloadData(uri, new HttpUtil.DownloadListener() {
            @Override
            public void onResult(HttpUtil.HttpDownloadResult result) {
                if (result.error == null) {
                    dataCache.put(uri, result.result);
                    (new DecodeImageTask(uri, listener)).execute(result.result);
                } else {
                    listener.onError(result.error);
                }
            }
        });
    }
}
