package com.tcl.onetouch.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.SoundPool;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;

import com.tcl.onetouch.BuildConfig;

import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class SystemUtil {
    private static DisplayMetrics displayMetrics;

    private static final int AUDIO_RECORD_BASE_SAMPLE_RATE = 44100;
    private static final int AUDIO_RECORD_CHANNEL = AudioFormat.CHANNEL_IN_MONO;
    private static final int AUDIO_RECORD_FORMAT = AudioFormat.ENCODING_PCM_16BIT;
    private static final int AUDIO_RECORD_BUFFER_SIZE;

    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    private static Handler mainThreadHandler;
    private static ExecutorService threadPoolExecutor;

    private static Thread bgThread;
    private static Handler bgHandler;

    private static boolean isDev;

    static {
        isDev = "dev".equals(BuildConfig.FLAVOR);
        AUDIO_RECORD_BUFFER_SIZE = AudioRecord.getMinBufferSize(
            AUDIO_RECORD_BASE_SAMPLE_RATE, AUDIO_RECORD_CHANNEL, AUDIO_RECORD_FORMAT);
        mainThreadHandler = new Handler(Looper.getMainLooper());
        threadPoolExecutor = Executors.newFixedThreadPool(NUMBER_OF_CORES);
        bgThread = new Thread() {
            public void run() {
                Looper.prepare();
                bgHandler = new Handler();
                Looper.loop();
            }
        };
        bgThread.start();
    }

    public static boolean isDev() {
        return isDev;
    }

    /**
     * Tells if internet is currently available on the device
     *
     * @param currentContext
     * @return
     */
    public static boolean isInternetAvailable(Context currentContext) {
        ConnectivityManager conectivityManager =
            (ConnectivityManager) currentContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = conectivityManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                if (networkInfo.isConnected()) {
                    return true;
                }
            } else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                if (networkInfo.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if the current device has a GPS module (hardware)
     *
     * @return true if the current device has GPS
     */
    public static boolean hasGpsModule(final Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        for (final String provider : locationManager.getAllProviders()) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the current device has a  NETWORK module (hardware)
     *
     * @return true if the current device has NETWORK
     */
    public static boolean hasNetworkModule(final Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        for (final String provider : locationManager.getAllProviders()) {
            if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if microphone is ready
     *
     * @return true if microphone is ready
     */
    @TargetApi(Build.VERSION_CODES.M)
    public static boolean isAudioSourceReady(int source) {
        long ts = System.currentTimeMillis();
        AudioRecord audio = null;
        boolean ready = false;
        try {
            audio = new AudioRecord(
                source, AUDIO_RECORD_BASE_SAMPLE_RATE, AUDIO_RECORD_CHANNEL,
                AUDIO_RECORD_FORMAT, AUDIO_RECORD_BUFFER_SIZE);
            audio.startRecording();
            ready = audio.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING;
        } catch (Exception e) {
        } finally {
            try {
                if (audio != null) {
                    audio.stop();
                    audio.release();
                }
            } catch (Exception e) {
            }
        }

        LogUtil.d("SystemUtil.isAudioSourceReady: " + ready + ", total time " + (System.currentTimeMillis() - ts));
        return ready;
    }

    public static void setDisplayMetrics(DisplayMetrics dm) {
        displayMetrics = dm;
    }

    public static DisplayMetrics getDisplayMetrics() {
        return displayMetrics;
    }

    public static int convertDpToPixel(int dp) {
        return (int) (dp * displayMetrics.density);
    }

    public static final void runOnMainThread(Runnable action) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            action.run();
        } else {
            mainThreadHandler.post(action);
        }
    }

    public static final void postToMainThread(Runnable action) {
        if (action == null) {
            return;
        }
        mainThreadHandler.post(action);
    }

    public static final void postToMainThread(Runnable action, long delay) {
        if (action == null) {
            return;
        }
        mainThreadHandler.postDelayed(action, delay);
    }

    public static final void cancelMainThreadAction(Runnable action) {
        if (action == null) {
            return;
        }
        mainThreadHandler.removeCallbacks(action);
    }

    public static final void postToBgThread(Runnable action) {
        if (action == null) {
            return;
        }
        bgHandler.post(action);
    }

    public static final void postToBgThread(Runnable action, long delay) {
        if (action == null) {
            return;
        }
        bgHandler.postDelayed(action, delay);
    }

    public static final void cancelBgThreadAction(Runnable action) {
        if (action == null) {
            return;
        }
        bgHandler.removeCallbacks(action);
    }

    public static final Handler getBgHandler() {
        return bgHandler;
    }

    public static Executor getExecutor() {
        return threadPoolExecutor;
    }
}
