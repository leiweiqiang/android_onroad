package com.tcl.onetouch.util;

import android.os.Environment;
import android.util.Log;

import com.tcl.onetouch.BuildConfig;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogUtil {
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static String logFileName = "OneTouch";
    private static String logTag = "OneTouch";

    private static File getLogFile() throws IOException {
        File file = new File(Environment.getExternalStorageDirectory(), logFileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    public static void setPackageName(String packageName) {
        logFileName = packageName + ".log";
        String[] tokens = packageName.split("\\.");
        logTag = tokens[tokens.length - 1];
    }

    public static void v(String msg) {
        Log.d(logTag, msg);
    }

    public static void v(String msg, Throwable tr) {
        Log.i(logTag, msg, tr);
    }

    public static void i(String msg) {
        Log.d(logTag, msg);
    }

    public static void i(String msg, Throwable tr) {
        Log.i(logTag, msg, tr);
    }

    public static void d(String msg) {
        Log.i(logTag, msg);
    }

    public static void d(String msg, Throwable tr) {
        Log.d(logTag, msg, tr);
    }

    public static void w(String msg) {
        Log.i(logTag, msg);
    }

    public static void w(String msg, Throwable tr) {
        Log.d(logTag, msg, tr);
    }

    public static void e(String msg) {
        Log.i(logTag, msg);
    }

    public static void e(String msg, Throwable tr) {
        Log.d(logTag, msg, tr);
    }

    public static void f(String msg) {
        Log.d(logTag, msg);
        try {
            File file = getLogFile();
            FileWriter fw = new FileWriter(file, true);
            fw.write(timeFormat.format(new Date()) + ": " + msg + "\n");
            fw.close();
        } catch (Throwable ignore) {
        }
    }

    public static void f(String msg, Throwable e) {
        Log.d(logTag, msg, e);
        try {
            File file = getLogFile();
            FileWriter fw = new FileWriter(file, true);
            fw.write(timeFormat.format(new Date()) + ": " + msg + "\n");
            PrintWriter pw = new PrintWriter(fw);
            e.printStackTrace(pw);
            fw.write("\n");
            pw.close();
            fw.close();
        } catch (Throwable ignore) {
        }
    }

    public static void reset() {
        try {
            File file = getLogFile();
            file.delete();
        } catch (Throwable ignore) {
        }
    }
}
