package com.tcl.onetouch.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DataUtil {
    private static Gson gson;
    private static Type parseType;

    static {
        gson = new GsonBuilder().create();
        parseType = new TypeToken<Map<String, Object>>(){}.getType();
    }

    public static class MapData extends HashMap<String, Object> {
        public MapData add(String key, boolean value) {
            put(key, value);
            return this;
        }

        public MapData add(String key, int value) {
            put(key, value);
            return this;
        }

        public MapData add(String key, long value) {
            put(key, value);
            return this;
        }

        public MapData add(String key, float value) {
            put(key, value);
            return this;
        }

        public MapData add(String key, double value) {
            put(key, value);
            return this;
        }

        public MapData add(String key, String value) {
            put(key, value);
            return this;
        }

        public MapData add(String key, Object value) {
            put(key, value);
            return this;
        }

        public String toJson() {
            return gson.toJson(this);
        }
    }

    public static MapData newMapData() {
        return new MapData();
    }

    public static Map<String, Object> parseJson(String jsonStr) {
        return gson.fromJson(jsonStr, parseType);
    }

    public static String toJson(Map<String, Object> map) {
        return gson.toJson(map);
    }

    public static String toJson(Collection<Object> list) {
        return gson.toJson(list);
    }

    public static boolean getBoolean(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value != null) {
            return Boolean.parseBoolean(value.toString());
        }
        return false;
    }

    public static int getInt(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value != null) {
            try {
                return (int) Double.parseDouble(value.toString());
            } catch (NumberFormatException e) {
                return 0;
            }
        }
        return 0;
    }

    public static long getLong(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value != null) {
            try {
                return (long) Double.parseDouble(value.toString());
            } catch (NumberFormatException e) {
                return 0l;
            }
        }
        return 0l;
    }

    public static float getFloat(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value != null) {
            try {
                return Float.parseFloat(value.toString());
            } catch (NumberFormatException e) {
                return 0;
            }
        }
        return 0;
    }

    public static double getDouble(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value != null) {
            try {
                return Double.parseDouble(value.toString());
            } catch (NumberFormatException e) {
                return 0D;
            }
        }
        return 0D;
    }

    public static String getString(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value != null) {
            return value.toString();
        }
        return "";
    }

    public static Map<String, Object> getMap(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value != null && value instanceof Map) {
            return (Map<String, Object>)value;
        }
        return null;
    }

    public static Collection<Object> getCollection(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value != null && value instanceof Collection) {
            return (Collection<Object>)value;
        }
        return null;
    }
}
