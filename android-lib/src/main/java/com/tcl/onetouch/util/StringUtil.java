package com.tcl.onetouch.util;

import android.org.apache.commons.codec.language.DoubleMetaphone;

import com.nuance.dragon.toolkit.data.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtil {

    public static final int DISTANCE_UNIT_KILOMETER_METERS = 1;
    public static final int DISTANCE_UNIT_MILES_FEET = 2;
    public static final int DISTANCE_UNIT_MILES_YARDS = 3;


    /**
     * the number of km/h in 1 m/s
     */
    private static final double SPEED_IN_KILOMETRES = 3.6;

    /**
     * number of mi/h in 1 m/s
     */
    private static final double SPEED_IN_MILES = 2.2369;

    /**
     * the number of meters in a km
     */
    private static final int METERS_IN_KM = 1000;

    /**
     * the number of meters in a mile
     */
    private static final double METERS_IN_MILE = 1609.34;

    /**
     * converter from meters to feet
     */
    private static final double METERS_TO_FEET = 3.2808399;

    /**
     * converter from meters to yards
     */
    private static final double METERS_TO_YARDS = 1.0936133;

    /**
     * the number of yards in a mile
     */
    private static final int YARDS_IN_MILE = 1760;

    /**
     * the number of feet in a yard
     */
    private static final int FEET_IN_YARD = 3;

    /**
     * the number of feet in a mile
     */
    private static final int FEET_IN_MILE = 5280;

    /**
     * the limit of feet where the distance should be converted into miles
     */
    private static final int LIMIT_TO_MILES = 1500;

    private static SimpleDateFormat arrivalTimeFormatter = new SimpleDateFormat("h:mma");

    private static Metaphone3 metaphone3 = new Metaphone3();
    private static DoubleMetaphone doubleMetaphone = new DoubleMetaphone();

    /**
     * Gets formatted time from a given number of seconds
     *
     * @param timeInSec
     * @return
     */
    public static String formatTime(int timeInSec) {
        StringBuilder builder = new StringBuilder();
        int hours = timeInSec / 3600;
        int minutes = (timeInSec - hours * 3600) / 60;
        int seconds = timeInSec - hours * 3600 - minutes * 60;
        builder.insert(0, seconds + "s");
        if (minutes > 0 || hours > 0) {
            builder.insert(0, minutes + "m ");
        }
        if (hours > 0) {
            builder.insert(0, hours + "h ");
        }
        return builder.toString();
    }

    public static String formatDuration(int timeInSec) {
        int hours = timeInSec / 3600;
        int minutes = (timeInSec - hours * 3600) / 60;
        if (hours > 1) {
            if (minutes == 0) {
                return String.format("%d hrs", hours);
            } else {
                return String.format("%d:%02d hrs", hours, minutes);
            }
        } else if (hours > 0) {
            if (minutes == 0) {
                return String.format("%d hr", hours);
            } else {
                return String.format("%d:%02d hr", hours, minutes);
            }
        } else if (minutes > 1) {
            return String.format("%d mins", minutes);
        } else {
            return String.format("%d min", 1);
        }
    }

    public static String formatTrackTime(int timeInSec) {
        StringBuilder sb = new StringBuilder();
        int hours = timeInSec / 3600;
        if (hours > 0) {
            sb.append(hours + ":");
        }
        timeInSec -= hours * 3600;
        int minutes = timeInSec / 60;
        int seconds = timeInSec - minutes * 60;
        sb.append(String.format("%02d:%02d", minutes, seconds));
        return sb.toString();
    }

    public static String formatArrivalTime(Date arrivalTime) {
        return arrivalTimeFormatter.format(arrivalTime);
    }

    /**
     * Formats a given distance value (given in meters)
     *
     * @param distInMeters
     * @return
     */
    public static String formatDistance(int distInMeters) {
        return convertAndFormatDistance(distInMeters, DISTANCE_UNIT_MILES_FEET);
    }

    /**
     * converts a distance given in meters to the according distance in yards
     *
     * @param distanceInMeters
     * @return
     */
    private static double distanceInYards(double distanceInMeters) {
        if (distanceInMeters != -1) {
            return distanceInMeters *= METERS_TO_YARDS;
        } else {
            return distanceInMeters;
        }
    }

    /**
     * converts a distance given in meters to the according distance in feet
     *
     * @param distanceInMeters
     * @return
     */
    private static double distanceInFeet(double distanceInMeters) {
        if (distanceInMeters != -1) {
            return distanceInMeters * METERS_TO_YARDS * FEET_IN_YARD;
        } else {
            return distanceInMeters;
        }
    }

    /**
     * Converts (to imperial units if necessary) and formats as string a
     * distance value given in meters.
     *
     * @param distanceValue    distance value in meters
     * @param distanceUnitType unit type to convert
     * @return
     */
    public static String convertAndFormatDistance(double distanceValue, int distanceUnitType) {

        if (distanceUnitType != DISTANCE_UNIT_KILOMETER_METERS) {
            // convert meters to feet or yards if needed
            if (distanceUnitType == DISTANCE_UNIT_MILES_FEET) {
                distanceValue = (float) distanceInFeet(distanceValue);
            } else {
                distanceValue = (float) distanceInYards(distanceValue);
            }
        }

        String distanceValueText;

        if (distanceUnitType == DISTANCE_UNIT_KILOMETER_METERS) {
            if (distanceValue >= METERS_IN_KM) {
                distanceValue /= METERS_IN_KM;
                if (distanceValue >= 10) {
                    // if distance is >= 10 km => display distance without any
                    // decimals
                    distanceValueText =
                        (Math.round(distanceValue) + " km");
                } else {
                    // distance displayed in kilometers
                    distanceValueText =
                        (((float) Math.round(distanceValue * 10) / 10)) + " km";
                }
            } else {
                // distance displayed in meters
                distanceValueText =
                    ((int) distanceValue) + " m";
            }
        } else if (distanceUnitType == DISTANCE_UNIT_MILES_FEET) {
            // if the distance in feet > 1500 => convert it in miles (FMA-2577)
            if (distanceValue >= LIMIT_TO_MILES) {
                distanceValue /= FEET_IN_MILE;
                if (distanceValue >= 10) {
                    // for routing if the distance is > 10 should be rounded to
                    // be an int; rounded distance displayed in miles
                    distanceValueText =
                        (Math.round(distanceValue) + " mi");

                } else {
                    // distance displayed in miles
                    distanceValueText =
                        (((float) Math.round(distanceValue * 10) / 10)) + " mi";
                }
            } else {
                // distance displayed in feet
                distanceValueText =
                    ((int) distanceValue) + " ft";
            }
        } else {
            if (distanceValue >= METERS_IN_KM) {
                distanceValue /= YARDS_IN_MILE;
                if (distanceValue >= 10) {
                    distanceValueText =
                        (Math.round(distanceValue) + " mi");
                } else {
                    // distance displayed in miles
                    distanceValueText =
                        (((float) Math.round(distanceValue * 10) / 10)) + " mi";
                }
            } else {
                // distance displayed in yards
                distanceValueText =
                    ((int) distanceValue) + " yd";
            }
        }

        return distanceValueText;
    }

    public static String formatCost(double cost) {
        if (cost < 0) {
            cost = 0;
        }
        return "$" + String.format("%.2f", cost);
    }

    // this method is NOT thread safe
    public static String getMetaphone3Encode(String s) {
        String[] tokens = s.split("\\s");
        StringBuilder sb = new StringBuilder();
        for (String token : tokens) {
            try {
                Double.parseDouble(token);
                sb.append(token);
            } catch (NumberFormatException e) {
                metaphone3.SetWord(token.replaceAll("\\W", ""));
                metaphone3.Encode();
                sb.append(metaphone3.GetMetaph());
            }
        }
        return sb.toString();
    }

    // this method is NOT thread safe
    public static String getDoubleMetaphoneEncode(String s) {
        return doubleMetaphone.doubleMetaphone(s);
    }

    protected static final char[] HEX_INDEX = "0123456789abcdef".toCharArray();

    public static byte[] stringToByteArray(String appKey) {
        String trimmedAppKey = appKey.trim();
        byte[] keyInBytes = new byte[trimmedAppKey.length() / 2];

        for (int i = 0; i < trimmedAppKey.length() / 2; i++) {
            String key = trimmedAppKey.substring(i * 2, i * 2 + 2);
            keyInBytes[i] = (byte) Integer.parseInt(key, 16);
        }

        return keyInBytes;
    }

    public static String byteArrayToString(byte[] appKey) {
        char[] hexChars = new char[appKey.length * 2];
        for (int i = 0; i < appKey.length; ++i) {
            int v = appKey[i] & 0xFF;
            hexChars[i * 2] = HEX_INDEX[v >>> 4];
            hexChars[i * 2 + 1] = HEX_INDEX[v & 0x0F];
        }
        return new String(hexChars);
    }

}