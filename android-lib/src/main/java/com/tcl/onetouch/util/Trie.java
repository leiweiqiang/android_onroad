package com.tcl.onetouch.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Trie {
    protected class Result {
        String str;
        int count;

        Result(String str, int count) {
            this.str = str;
            this.count = count;
        }
    }

    TrieNode root = new TrieNode();

    public class TrieNode {
        int count;
        Map<Character, TrieNode> map;

        public TrieNode() {
            count = 0;
            map = new HashMap<Character, TrieNode>();
        }
    }

    public void buildTrie(String s) {
        if (s == null || s.length() == 0) {
            return;
        }
        TrieNode node = root;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            TrieNode nextNode = node.map.get(c);
            if (nextNode == null) {
                nextNode = new TrieNode();
                node.map.put(c, nextNode);
            }
            node = nextNode;
        }

        ++node.count;
    }

    public List<String> searchTrie(String s) {
        if (s == null || s.length() == 0) {
            return null;
        }
        TrieNode node = root;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (node.map.containsKey(c)) {
                node = node.map.get(c);
            } else {
                break;
            }
        }
        ++node.count;
        List<Trie.Result> list = new ArrayList<Trie.Result>();
        StringBuilder sb = new StringBuilder();

        findRemained(list, node, sb);

        Collections.sort(list, new Comparator<Result>() {
            public int compare(Trie.Result r1, Trie.Result r2) {
                return r2.count - r1.count;
            }
        });
        List<String> res = new ArrayList<String>();
        for (Trie.Result r : list) {
            res.add(r.str);
        }
        return res;
    }

    public void findRemained(List<Result> res, TrieNode node, StringBuilder sb) {
        if (node == null) {
            return;
        }
        if (node.count != 0) {
            res.add(new Result(sb.toString(), node.count));
        }
        for (Character c : node.map.keySet()) {
            sb.append(c);
            findRemained(res, node.map.get(c), sb);
            sb.deleteCharAt(sb.length() - 1);
        }
    }
}