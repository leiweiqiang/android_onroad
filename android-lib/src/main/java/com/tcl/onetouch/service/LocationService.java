package com.tcl.onetouch.service;

import android.content.Context;
import android.location.Location;
import android.util.Pair;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.tcl.onetouch.BuildConfig;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.MapCoordinate;
import com.tcl.onetouch.service.google.AndroidLocationService;
import com.tcl.onetouch.service.google.GoogleLocationService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public abstract class LocationService extends BaseService {
    public final static String VEHICLE_LOCATION_KEY = "VEHICLE_LOCATION";
    private final static float DEFAULT_SIMULATION_SPEED = 14.0f; // about 50km/h or 30miles/h

    public static interface Listener {
        public void onLocationChanged(Location location);
    }

    protected LocationService.Listener listener;
    protected Queue<Pair<Integer, MapCoordinate>> simulationRoute;
    protected Location simulationLocation;
    protected Location mockLocation;

    protected Runnable simulationTask = new Runnable() {
        @Override
        public void run() {
            if (simulationRoute == null) {
                return;
            }
            Pair<Integer, MapCoordinate> current = simulationRoute.poll();
            if (current == null) {
                return;
            }
            if (simulationLocation != null) {
                simulationLocation.setLatitude(current.second.getLatitude());
                simulationLocation.setLongitude(current.second.getLongitude());
                simulationLocation.setBearing((float) current.second.getBearing());
                simulationLocation.setSpeed(DEFAULT_SIMULATION_SPEED);
                simulationLocation.setElapsedRealtimeNanos(System.nanoTime());
                if (listener != null) {
                    listener.onLocationChanged(new Location(simulationLocation));
                }
            }
            Pair<Integer, MapCoordinate> next = simulationRoute.peek();
            if (next != null) {
                SystemUtil.postToMainThread(simulationTask, next.first);
            }
        }
    };

    public void init(Context context, LocationService.Listener listener) {
        this.listener = listener;
    }

    public Location getLastLocation() {
        if (simulationLocation != null) {
            return simulationLocation;
        }
        if (mockLocation != null) {
            return mockLocation;
        }
        return getRealLocation();
    }

    protected abstract Location getRealLocation();

    public abstract Location getLastVehicleLocation();

    public abstract void pause();

    public abstract void resume();

    public abstract void destroy();

    public void enableMockLocation(boolean enabled) {
        if (enabled) {
            pause();
            int idx = BuildConfig.MOCK_GPS.indexOf(',');
            mockLocation = new Location("");
            mockLocation.setLatitude(Double.parseDouble(BuildConfig.MOCK_GPS.substring(0, idx)));
            mockLocation.setLongitude(Double.parseDouble(BuildConfig.MOCK_GPS.substring(idx + 1)));
            if (listener != null && simulationLocation == null) {
                listener.onLocationChanged(mockLocation);
            }
        } else {
            mockLocation = null;
            if (simulationLocation == null) {
                resume();
            }
        }
    }

    public void startSimulation(List<MapCoordinate> route, float speed) {
        pause();
        if (route == null || route.isEmpty()) {
            return;
        }
        if (speed <= 0) {
            speed = DEFAULT_SIMULATION_SPEED;
        }
        simulationRoute = new LinkedList<>();
        simulationLocation = new Location("");
        Location currentLocation = getRealLocation();
        MapCoordinate lastLoc = new MapCoordinate(
            currentLocation.getLatitude(),
            currentLocation.getLongitude(),
            currentLocation.getBearing());
        float[] rs = new float[3];
        for (MapCoordinate loc : route) {
            int interval = 0;
            Location.distanceBetween(lastLoc.getLatitude(), lastLoc.getLongitude(),
                loc.getLatitude(), loc.getLongitude(), rs);
            if (rs[0] < 2) {
                continue;
            }
            float time = rs[0] / speed;
            lastLoc.setBearing((lastLoc.getBearing() + rs[1]) / 2);
            loc.setBearing(rs[2]);
            int seg = Math.round(time);
            if (seg > 1) {
                double brg = rs[1];
                double lat = lastLoc.getLatitude();
                double lng = lastLoc.getLongitude();
                double dBrg = (rs[2] - rs[1]) / seg;
                double dLat = (loc.getLatitude() - lat) / seg;
                double dLng = (loc.getLongitude() - lng) / seg;
                interval = Math.round(time * 1000 / seg);
                while (seg-- > 1) {
                    brg += dBrg;
                    lat += dLat;
                    lng += dLng;
                    simulationRoute.add(new Pair<Integer, MapCoordinate>(interval, new MapCoordinate(lat, lng, brg)));
                }
            } else {
                interval = Math.round(time * 1000);
            }
            lastLoc = loc;
            simulationRoute.add(new Pair<Integer, MapCoordinate>(interval, loc));
        }
        simulationTask.run();
    }

    public void stopSimulation() {
        SystemUtil.cancelMainThreadAction(simulationTask);
        simulationRoute = null;
        simulationLocation = null;
        if (mockLocation == null) {
            resume();
        }
    }

    private static LocationService sDefaultService;

    public static LocationService getDefaultService() {
        if (sDefaultService == null) {
            if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(OneTouchApplication.getInstance()) ==
                ConnectionResult.SUCCESS) {
                LogUtil.d("Use GoogleLocationService");
                sDefaultService = new GoogleLocationService();
            } else {
                LogUtil.d("Use AndroidLocationService");
                sDefaultService = new AndroidLocationService();
            }
        }
        return sDefaultService;
    }

}
