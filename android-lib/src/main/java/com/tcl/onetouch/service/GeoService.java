package com.tcl.onetouch.service;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.tcl.onetouch.model.AddressInfo;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.service.google.GoogleGeoService;

import java.util.List;

public abstract class GeoService extends BaseService {
    public static interface GeocodeResultListener {
        public void onResult(PlaceInfo place);
        public void onError(ErrorCode error);
    }

    public static interface PoiResultListener {
        public void onResult(List<PlaceInfo> places);
        public void onError(ErrorCode error);
    }

    public static interface PlaceResultListener {
        public void onResult(PlaceInfo place);
        public void onError(ErrorCode error);
    }

    public static interface AutocompleteResultListener {
        public void onResult(List<AutocompletePrediction> results);
        public void onError(ErrorCode error);
    }

    public abstract void init(Context context);
    public abstract void geocodeQuery(String query, Location currentLocation, GeocodeResultListener listener);
    public abstract void poiQuery(String query, Location currentLocation, PoiResultListener listener);
    public abstract void placeQuery(String placeId, PlaceResultListener listener);
    public abstract void updatePlace(PlaceInfo placeInfo, PlaceResultListener listener);

    // Available through Google GeoDataApi
    public void getAutocompletePredictions(String query, Location currentLocation, AutocompleteResultListener listener) {
        listener.onError(createErrorCode(ErrorCode.GEO_ERROR_NO_SERVICE));
    }

    // Available through Google GeoDataApi, blocking call
    public List<AutocompletePrediction> getAutocompletePredictions(String query, Location currentLocation) {
        return null;
    }

    private static GeoService sDefaultService;

    public static GeoService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new GoogleGeoService();
        }
        return sDefaultService;
    }
}
