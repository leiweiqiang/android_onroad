package com.tcl.onetouch.service;

import com.tcl.onetouch.model.ErrorCode;

public class BaseService {
    public ErrorCode amendErrorCode(ErrorCode error) {
        error.setService(this);
        return error;
    }

    public ErrorCode createErrorCode(int error) {
        return new ErrorCode(error, null, this);
    }

    public ErrorCode createErrorCode(int error, String message) {
        return new ErrorCode(error, message, this);
    }
}
