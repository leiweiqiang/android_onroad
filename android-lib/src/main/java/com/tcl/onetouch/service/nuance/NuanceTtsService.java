package com.tcl.onetouch.service.nuance;

import android.content.Context;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSource;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.pipes.ConverterPipe;
import com.nuance.dragon.toolkit.audio.pipes.SpeexDecoderPipe;
import com.nuance.dragon.toolkit.audio.sinks.PlayerSink;
import com.nuance.dragon.toolkit.audio.sinks.SpeakerPlayerSink;
import com.nuance.dragon.toolkit.cloudservices.CloudConfig;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.TransactionError;
import com.nuance.dragon.toolkit.cloudservices.vocalizer.CloudVocalizer;
import com.nuance.dragon.toolkit.cloudservices.vocalizer.TtsSpec;
import com.nuance.dragon.toolkit.data.Data;
import com.nuance.dragon.toolkit.util.Logger;
import com.tcl.onetouch.BuildConfig;
import com.tcl.onetouch.service.TtsService;
import com.tcl.onetouch.util.StringUtil;

/**
 * Created by jianxin on 3/15/16.
 */
public class NuanceTtsService extends TtsService {

    private CloudServices cloudServices;
    private CloudVocalizer cloudVocalizer;
    private TtsService.OnStateChangeListener onStateChangeListener;
    private TtsService.OnSpeechListener onSpeechListener;
    private Context context;
    private int lastSpeechPriority;
    private String lastSpeechText;
    private String lastSpeechSource;
    private boolean isSpeaking;
    private boolean isReady;

    private ConverterPipe<AudioChunk, AudioChunk> decoder;
    private PlayerSink player;
    private static final AudioType audioTypePlayback = AudioType.PCM_16k;
    private static final AudioType AUDIO_TYPE = AudioType.SPEEX_WB;

    @Override
    public void init(Context context, OnStateChangeListener listener) {
        this.context = context;
        onStateChangeListener = listener;

        // Cloud services initialization
        createNuanceServices(context);

        lastSpeechPriority = 0;
        isSpeaking = false;
        isReady = true;
    }

    private void createNuanceServices(Context context) {
        cloudServices = CloudServices.createCloudServices(context,
                new CloudConfig(
                        BuildConfig.NUANCE_HOST,
                        BuildConfig.NUANCE_PORT,
                        BuildConfig.NUANCE_APP_ID,
                        StringUtil.stringToByteArray(BuildConfig.NUANCE_APP_KEY),
                        AUDIO_TYPE,
                        AUDIO_TYPE));
        cloudVocalizer = new CloudVocalizer(cloudServices);
    }

    @Override
    public void speak(String text, String source, int priority, OnSpeechListener listener) {
        if (isSpeaking && priority < lastSpeechPriority) {
            return;
        }

        isSpeaking = true;
        lastSpeechPriority = priority;
        lastSpeechSource = source;
        lastSpeechText = text;
        onSpeechListener = listener;

        if (cloudServices == null || cloudServices.getConnectionState() == CloudServices.ConnectionState.DISCONNECTED) {
            createNuanceServices(context);
        }

        generateTts(getTtsSpec(text));
    }

    @Override
    public void stop(String source) {
        if (!isReady) {
            return;
        }

        if ((source == null || source.isEmpty() || source.equals(lastSpeechSource)) && player != null) {
            player.stopPlaying();
        }
    }


    private TtsSpec getTtsSpec(String sentence) {
        Data.Dictionary ttsParamData = new Data.Dictionary();
        ttsParamData.put("tts_input", sentence);
        ttsParamData.put("tts_type", "text");
        Data.Dictionary settings = new Data.Dictionary();
        settings.put("tts_voice", "samantha");
        return new TtsSpec("NVC_TTS_CMD", settings, "TEXT_TO_READ", ttsParamData, AUDIO_TYPE);
    }


    private void generateTts(TtsSpec ttsSpec) {
        AudioSource<AudioChunk> audioSource = cloudVocalizer.generateTts(ttsSpec, new CloudVocalizer.Listener() {
            @Override
            public void onProcessingStarted() {
                Logger.info(context, "TTS processing started.");
            }

            @Override
            public void onSuccess() {
                Logger.info(context, "TTS success.");
            }

            @Override
            public void onError(TransactionError error) {
                Logger.info(context, "TTS error [" + error.getErrorText() + "].");
            }
        });

        if (player != null) {
            player.stopPlaying();
            player.disconnectAudioSource();
            player = null;
        }
        player = new SpeakerPlayerSink(audioTypePlayback);


        if (decoder != null) {
            decoder.disconnectAudioSource();
            decoder.release();
            decoder = null;
        }
        decoder = new SpeexDecoderPipe();

        decoder.connectAudioSource(audioSource);
        player.connectAudioSource(decoder);
        player.startPlaying(new PlayerSink.Listener() {
            @Override
            public void onStarted(PlayerSink playerSink) {
                if (!isSpeaking) {
                    isSpeaking = true;
                }
                if (onStateChangeListener != null) {
                    onStateChangeListener.onSpeechStarted(lastSpeechText, lastSpeechSource, lastSpeechPriority);
                }
            }

            @Override
            public void onStopped(PlayerSink playerSink) {
                isSpeaking = false;
                if (onSpeechListener != null) {
                    onSpeechListener.onFinished();
                }
                if (onStateChangeListener != null) {
                    onStateChangeListener.onSpeechStopped();
                }
            }
        });
    }

    @Override
    public void destroy() {
        if (cloudVocalizer != null) {
            cloudVocalizer.cancel();
            cloudVocalizer = null;
        }

        if (cloudServices != null) {
            cloudServices.release();
            cloudServices = null;
        }

        if (player != null) {
            player.stopPlaying();
            player.disconnectAudioSource();
            player = null;
        }

        if (decoder != null) {
            decoder.disconnectAudioSource();
            decoder.release();
            decoder = null;
        }
    }
}
