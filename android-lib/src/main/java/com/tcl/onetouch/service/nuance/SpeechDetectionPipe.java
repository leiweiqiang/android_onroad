package com.tcl.onetouch.service.nuance;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioSink;
import com.nuance.dragon.toolkit.audio.AudioSource;

public class SpeechDetectionPipe extends AudioSink<AudioChunk> {
    public static interface Listener {
        public void onStartOfSpeech();
        public void onEndOfSpeech();
        public void onTimeout();
    }

    private final Listener listener;
    private final int speechStartSilenceThreshold;
    private final int speechEndSilenceThreshold;
    private boolean isStarted;
    private boolean isRecognized;
    private boolean isEnded;
    private boolean isTimeout;
    private int speechStartSilencePeriod;
    private int speechEndSilencePeriod;
    private long recordStartTime;
    private long speechEndTime;

    public SpeechDetectionPipe(Listener listener, int speechStartSilenceThreshold, int speechEndSilenceThreshold) {
        this.listener = listener;
        this.speechStartSilenceThreshold = speechStartSilenceThreshold;
        this.speechEndSilenceThreshold = speechEndSilenceThreshold;
        isStarted = false;
        isRecognized = false;
        isEnded = false;
        isTimeout = false;
        speechStartSilencePeriod = 0;
        speechEndSilencePeriod = 0;
        recordStartTime = -1;
        speechEndTime = -1;
    }

    public void speechRecognized() {
        isRecognized = true;
        if (!isStarted) {
            isStarted = true;
            listener.onStartOfSpeech();
        }
    }

    public void speechNotRecognized() {
        isStarted = false;
        isEnded = false;
        isRecognized = false;
    }

    @Override
    public void chunksAvailable(AudioSource<AudioChunk> audioSource) {
        if (isEnded || isTimeout) {
            return;
        } else if (recordStartTime < 0) {
            recordStartTime = System.currentTimeMillis();
        } else if (!isRecognized && (System.currentTimeMillis() - recordStartTime) >= speechStartSilenceThreshold) {
            isTimeout = true;
            listener.onTimeout();
        } else {
            AudioChunk audioChunk = audioSource.getAudioChunkForSink(this);
            while (audioChunk != null) {
                if (audioChunk.audioSpeechStatus == AbstractAudioChunk.SpeechStatus.SPEECH) {
                    if (!isStarted) {
                        isStarted = true;
                        listener.onStartOfSpeech();
                    }
                    speechEndTime = -1;
                } else if (audioChunk.audioSpeechStatus == AbstractAudioChunk.SpeechStatus.NO_SPEECH) {
                    if (isStarted) {
                        if (speechEndTime < 0) {
                            speechEndTime = System.currentTimeMillis();
                        } else if (System.currentTimeMillis() - speechEndTime >= speechEndSilenceThreshold) {
                            isEnded = true;
                            listener.onEndOfSpeech();
                        }
                    }
                }
                audioChunk = audioSource.getAudioChunkForSink(this);
            }
        }
    }

    @Override
    public void framesDropped(AudioSource<AudioChunk> audioSource) {

    }

    @Override
    public void sourceClosed(AudioSource<AudioChunk> audioSource) {

    }

    protected void onChunkBuffered(AudioChunk audioChunk) {
        if (!isEnded) {
            if (audioChunk.audioSpeechStatus == AbstractAudioChunk.SpeechStatus.SPEECH) {
                if (!isStarted) {
                    isStarted = true;
                    listener.onStartOfSpeech();
                } else {
                    speechStartSilencePeriod += audioChunk.audioDuration;
                    if (speechStartSilencePeriod >= speechStartSilenceThreshold) {
                        listener.onTimeout();
                        isEnded = true;
                    }
                }
                speechEndSilencePeriod = 0;
            } else if (audioChunk.audioSpeechStatus == AbstractAudioChunk.SpeechStatus.NO_SPEECH) {
                if (isStarted) {
                    speechEndSilencePeriod += audioChunk.audioDuration;
                    if (speechEndSilencePeriod >= speechEndSilenceThreshold) {
                        isEnded = true;
                        listener.onEndOfSpeech();
                    }
                }
            }
        }
    }
}
