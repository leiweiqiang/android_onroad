package com.tcl.onetouch.service.google;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.model.PlaceInfo;
import com.tcl.onetouch.service.GeoService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.HttpUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class GoogleGeoService extends GeoService
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String GEOCODE_URL = "https://maps.googleapis.com/maps/api/geocode/json?";
    private static final String POI_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json?";
    private static final String PLACE_DETAIL_URL = "https://maps.googleapis.com/maps/api/place/details/json?";
    private static final String API_KEY = "com.google.android.geo.API_KEY";
    private static final double EARTH_EQUATORIAL_RADIUS = 6378137.0;
    private static final double EARTH_POLAR_RADIUS = 6356752.3142;
    private static final float QUERY_RADIUS = 5000f;

    private static final String STATUS_OK = "OK";
    private static final String STATUS_ZERO_RESULTS = "ZERO_RESULTS";
    private static final String STATUS_OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT";
    private static final String STATUS_REQUEST_DENIED = "REQUEST_DENIED";
    private static final String STATUS_INVALID_REQUEST = "INVALID_REQUEST";
    private static final String STATUS_UNKNOWN_ERROR = "UNKNOWN_ERROR";

    private GoogleApiClient googleApiClient;
    private String apiKey;

    @Override
    public void init(Context context) {
        googleApiClient = new GoogleApiClient.Builder(context)
            .addApi(Places.GEO_DATA_API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build();
        googleApiClient.connect();
        apiKey = OneTouchApplication.getInstance().getMetaStringValue(API_KEY);
    }

    private static LatLngBounds calculateBounds(Location location, float radius) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        double latDelta = Math.toDegrees(radius / EARTH_POLAR_RADIUS);
        double lngDelta = Math.toDegrees(radius / EARTH_EQUATORIAL_RADIUS / Math.cos(Math.toRadians(lat)));
        return new LatLngBounds(new LatLng(lat - latDelta, lng - lngDelta), new LatLng(lat + latDelta, lng + lngDelta));
    }

    private ErrorCode handleResultError(Map<String, Object> data) {
        String status = DataUtil.getString(data, "status");
        if (!STATUS_OK.equals(status)) {
            String msg = DataUtil.getString(data, "error_message");
            if (STATUS_ZERO_RESULTS.equals(status)) {
                return createErrorCode(ErrorCode.GEO_ERROR_NO_RESULT, msg);
            } else if (STATUS_OVER_QUERY_LIMIT.equals(status)) {
                return createErrorCode(ErrorCode.GEO_ERROR_OVER_QUERY_LIMIT, msg);
            } else if (STATUS_REQUEST_DENIED.equals(status)) {
                return createErrorCode(ErrorCode.GEO_ERROR_REQUEST_DENIED, msg);
            } else if (STATUS_INVALID_REQUEST.equals(status)) {
                return createErrorCode(ErrorCode.GEO_ERROR_INVALID_REQUEST, msg);
            } else {
                return createErrorCode(ErrorCode.GEO_ERROR, msg);
            }
        }
        return null;
    }

    @Override
    public void geocodeQuery(final String query, final Location currentLocation, final GeocodeResultListener listener) {
        if (listener == null) {
            // TODO: throw exception
            return;
        }
        Map<String, String> queryData = new HashMap<String, String>();
        queryData.put("address", query);
        LatLngBounds bounds = calculateBounds(currentLocation, QUERY_RADIUS);
        queryData.put("bounds",
            bounds.southwest.latitude + "," + bounds.southwest.longitude + "|" +
                bounds.northeast.latitude + "," + bounds.northeast.longitude);

        queryData.put("key", apiKey);

        HttpUtil.doGet(GEOCODE_URL, queryData, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                if (result.error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    ErrorCode error = handleResultError(data);
                    if (error != null) {
                        listener.onError(error);
                        return;
                    }
                    Collection<Object> results = DataUtil.getCollection(data, "results");
                    if (results == null || results.size() == 0) {
                        listener.onError(createErrorCode(ErrorCode.GEO_ERROR_NO_RESULT));
                        return;
                    }
                    PlaceInfo place = PlaceInfo.createFromGoogleGeocode((Map<String, Object>) results.iterator().next());
                    place.setHasDetailedInfo(true);
                    listener.onResult(place);
                } else {
                    listener.onError(amendErrorCode(result.error));
                }
            }
        });
    }

    @Override
    public void poiQuery(final String query, final Location currentLocation, final PoiResultListener listener) {
        if (listener == null) {
            // TODO: throw exception
            return;
        }
        Map<String, String> queryData = new HashMap<String, String>();
        queryData.put("query", query);
        queryData.put("location", currentLocation.getLatitude() + "," + currentLocation.getLongitude());
        queryData.put("radius", String.valueOf(QUERY_RADIUS));
        queryData.put("key", apiKey);

        HttpUtil.doGet(POI_URL, queryData, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                if (result.error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    ErrorCode error = handleResultError(data);
                    if (error != null) {
                        listener.onError(error);
                        return;
                    }
                    Collection<Object> results = DataUtil.getCollection(data, "results");
                    if (results == null || results.size() == 0) {
                        listener.onError(createErrorCode(ErrorCode.GEO_ERROR_NO_RESULT));
                        return;
                    }
                    Iterator<Object> rit = results.iterator();
                    List<PlaceInfo> places = new ArrayList<PlaceInfo>();
                    while (rit.hasNext()) {
                        places.add(PlaceInfo.createFromGoogleGeocode((Map<String, Object>) rit.next()));
                    }
                    listener.onResult(places);
                } else {
                    listener.onError(amendErrorCode(result.error));
                }
            }
        });
    }

    @Override
    public void placeQuery(String placeId, final PlaceResultListener listener) {
        if (listener == null) {
            // TODO: throw exception
            return;
        }
        Map<String, String> queryData = new HashMap<String, String>();
        queryData.put("placeid", placeId);
        queryData.put("key", apiKey);

        HttpUtil.doGet(PLACE_DETAIL_URL, queryData, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                if (result.error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    ErrorCode error = handleResultError(data);
                    if (error != null) {
                        listener.onError(error);
                        return;
                    }
                    PlaceInfo place = PlaceInfo.createFromGoogleGeocode(DataUtil.getMap(data, "result"));
                    place.setHasDetailedInfo(true);
                    listener.onResult(place);
                } else {
                    listener.onError(amendErrorCode(result.error));
                }
            }
        });
    }

    @Override
    public void updatePlace(final PlaceInfo placeInfo, final PlaceResultListener listener) {
        if (placeInfo == null || listener == null) {
            // TODO: throw exception
            return;
        }
        if (!placeInfo.getId().isEmpty()) {
            Map<String, String> queryData = new HashMap<String, String>();
            queryData.put("placeid", placeInfo.getId());
            queryData.put("key", apiKey);
            HttpUtil.doGet(PLACE_DETAIL_URL, queryData, new HttpUtil.RequestListener() {
                @Override
                public void onResult(HttpUtil.HttpRequestResult result) {
                    if (result.error == null) {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        ErrorCode error = handleResultError(data);
                        if (error != null) {
                            listener.onError(error);
                            return;
                        }
                        placeInfo.updateFromGoogleGeocode(DataUtil.getMap(data, "result"));
                        placeInfo.setHasDetailedInfo(true);
                        listener.onResult(placeInfo);
                    } else {
                        listener.onError(amendErrorCode(result.error));
                    }
                }
            });
        } else if (placeInfo.getAddressInfo() != null &&
            !placeInfo.getAddressInfo().getFormattedAddress().isEmpty()) {
            Map<String, String> queryData = new HashMap<String, String>();
            queryData.put("address", placeInfo.getAddressInfo().getFormattedAddress());
            LatLngBounds bounds = calculateBounds(placeInfo.getLocation(), QUERY_RADIUS);
            queryData.put("bounds",
                bounds.southwest.latitude + "," + bounds.southwest.longitude + "|" +
                    bounds.northeast.latitude + "," + bounds.northeast.longitude);

            queryData.put("key", apiKey);

            HttpUtil.doGet(GEOCODE_URL, queryData, new HttpUtil.RequestListener() {
                @Override
                public void onResult(HttpUtil.HttpRequestResult result) {
                    if (result.error == null) {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        ErrorCode error = handleResultError(data);
                        if (error != null) {
                            listener.onError(error);
                            return;
                        }
                        Collection<Object> results = DataUtil.getCollection(data, "results");
                        if (results == null || results.size() == 0) {
                            listener.onError(createErrorCode(ErrorCode.GEO_ERROR_NO_RESULT));
                            return;
                        }
                        placeInfo.updateFromGoogleGeocode((Map<String, Object>) results.iterator().next());
                        placeInfo.setHasDetailedInfo(true);
                        listener.onResult(placeInfo);
                    } else {
                        listener.onError(amendErrorCode(result.error));
                    }
                }
            });
        } else {
            listener.onError(createErrorCode(ErrorCode.GEO_ERROR_INVALID_REQUEST));
        }
    }

    @Override
    public void getAutocompletePredictions(String query, Location currentLocation, final AutocompleteResultListener listener) {
        if (googleApiClient.isConnected()) {
            PendingResult<AutocompletePredictionBuffer> results =
                Places.GeoDataApi.getAutocompletePredictions(
                    googleApiClient, query,
                    calculateBounds(currentLocation, QUERY_RADIUS),
                    null);
            results.setResultCallback(new ResultCallback<AutocompletePredictionBuffer>() {
                @Override
                public void onResult(AutocompletePredictionBuffer autocompletePredictions) {
                    if (autocompletePredictions.getStatus().isSuccess()) {
                        ArrayList<AutocompletePrediction> result = new ArrayList(autocompletePredictions.getCount());
                        Iterator<AutocompletePrediction> it = autocompletePredictions.iterator();
                        while (it.hasNext()) {
                            result.add(it.next().freeze());
                        }
                        listener.onResult(result);
                    } else {
                        listener.onError(createErrorCode(ErrorCode.GEO_ERROR_SERVICE_ERROR,
                            autocompletePredictions.getStatus().getStatusMessage()));
                    }
                    autocompletePredictions.release();
                }
            }, 60, TimeUnit.SECONDS);
        } else {
            ConnectionResult cr = googleApiClient.getConnectionResult(Places.GEO_DATA_API);
            String msg = "GoogleApiClient connection error";
            if (!cr.isSuccess()) {
                msg = cr.getErrorMessage();
            }
            listener.onError(createErrorCode(ErrorCode.GEO_ERROR_SERVICE_ERROR, msg));
        }
    }

    @Override
    public List<AutocompletePrediction> getAutocompletePredictions(String query, Location currentLocation) {
        if (googleApiClient.isConnected()) {
            PendingResult<AutocompletePredictionBuffer> results =
                Places.GeoDataApi.getAutocompletePredictions(
                    googleApiClient, query,
                    calculateBounds(currentLocation, QUERY_RADIUS),
                    null);
            AutocompletePredictionBuffer autocompletePredictions = results.await(60, TimeUnit.SECONDS);
            ArrayList<AutocompletePrediction> result = null;
            if (autocompletePredictions.getStatus().isSuccess()) {
                result = new ArrayList(autocompletePredictions.getCount());
                Iterator<AutocompletePrediction> it = autocompletePredictions.iterator();
                while (it.hasNext()) {
                    result.add(it.next().freeze());
                }
            }
            autocompletePredictions.release();
            return result;
        } else {
            return null;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
