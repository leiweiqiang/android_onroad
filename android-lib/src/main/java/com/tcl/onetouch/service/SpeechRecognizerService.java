package com.tcl.onetouch.service;

import android.app.Application;
import android.content.Context;
import android.media.AudioManager;
import android.speech.SpeechRecognizer;

import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.google.AndroidSpeechRecognizer;
import com.tcl.onetouch.service.nuance.NuanceCloudRecognizer;
import com.tcl.onetouch.util.LogUtil;

public abstract class SpeechRecognizerService extends BaseService {
    private static final String DEFAULT_SERVICE_KEY = SpeechRecognizerService.class.getName() + ".DEFAULT_SERVICE";
    private static final String NUANCE_SERVICE_VALUE = "nuance";

    public static interface OnStateChangeListener {
        public void onRecognitionStarted();
        public void onReadyToSpeech(long timeout, long endPause);
        public void onBeginningOfSpeech();
        public void onEndOfSpeech();
        public void onRecognitionEnded();
    }

    public interface OnResultListener {
        public void onResult(String result);

        public void onPartialResult(String partialResult);

        public void onError(ErrorCode error);
    }

    public static abstract class AbstractOnResultListener implements OnResultListener {
        public void onPartialResult(String partialResult) {
        }

        public void onError(ErrorCode error) {
        }
    }

    protected OnStateChangeListener onStateChangeListener;
    protected AudioManager audioManager;
    protected AudioManager.OnAudioFocusChangeListener afChangeListener;
    protected int afRequestResult;

    public void init(Context context, SpeechRecognizerService.OnStateChangeListener listener) {
        onStateChangeListener = listener;
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            public void onAudioFocusChange(int focusChange) {
                switch (focusChange) {
                    case AudioManager.AUDIOFOCUS_GAIN:
                        LogUtil.d("SpeechRecognizerService audiofocus change: AUDIOFOCUS_GAIN");
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                        LogUtil.d("SpeechRecognizerService audiofocus change: AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                        LogUtil.d("SpeechRecognizerService audiofocus change: AUDIOFOCUS_LOSS_TRANSIENT");
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS:
                        LogUtil.d("SpeechRecognizerService audiofocus change: AUDIOFOCUS_LOSS");
                        break;
                    default:
                        LogUtil.d("SpeechRecognizerService audiofocus change: unknown " + focusChange);
                        break;
                }
            }
        };
        afRequestResult = AudioManager.AUDIOFOCUS_REQUEST_FAILED;
    }

    public abstract void startRecognize(OnResultListener listener);

    public abstract void startRecognize(long timeout, OnResultListener listener);

    public abstract void startRecognize(long timeout, long endPause, OnResultListener listener);

    public abstract void stopRecognize();

    public abstract void cancelRecognize();

    public abstract boolean isRecognizing();

    public void destroy() {
        onStateChangeListener = null;
        audioManager = null;
        afChangeListener = null;
    }

    protected void requestAudioFocus() {
        if (afRequestResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            afRequestResult = audioManager.requestAudioFocus(
                afChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE);
        }
    }

    protected void abandonAudioFocus() {
        if (afRequestResult == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            audioManager.abandonAudioFocus(afChangeListener);
            afRequestResult = AudioManager.AUDIOFOCUS_REQUEST_FAILED;
        }
    }

    private static SpeechRecognizerService sDefaultService;

    public static SpeechRecognizerService getDefaultService() {
        if (sDefaultService == null) {
            Application app = OneTouchApplication.getInstance();
            boolean useNuance = NUANCE_SERVICE_VALUE.equalsIgnoreCase(
                OneTouchApplication.getInstance().getMetaStringValue(DEFAULT_SERVICE_KEY));
            if (useNuance || !SpeechRecognizer.isRecognitionAvailable(app)) {
                sDefaultService = new NuanceCloudRecognizer();
            } else {
                sDefaultService = new AndroidSpeechRecognizer();
            }
        }
        return sDefaultService;
    }
}
