package com.tcl.onetouch.service.google;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.util.LogUtil;

public class GoogleLocationService extends LocationService
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;

    @Override
    public void init(Context context, final LocationService.Listener listener) {
        super.init(context, listener);
        locationRequest = LocationRequest.create().
            setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY).
            setInterval(3000).
            setFastestInterval(1000);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult result) {
                if (GoogleLocationService.this.listener != null) {
                    GoogleLocationService.this.listener.onLocationChanged(result.getLastLocation());
                }
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                LogUtil.d("LocationAvailability changed: " + locationAvailability);
            }
        };
        googleApiClient = new GoogleApiClient.Builder(context)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build();
        googleApiClient.connect();
    }

    @Override
    protected Location getRealLocation() {
        if (googleApiClient.isConnected()) {
            try {
                return LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            } catch (SecurityException e) {
            }
        }
        return null;
    }

    @Override
    public Location getLastVehicleLocation() {
        String locationJson = OneTouchApplication.getInstance().getStringPreference(VEHICLE_LOCATION_KEY, "");
        if (!locationJson.isEmpty()) {
            Gson gson = new GsonBuilder().create();
            return gson.fromJson(locationJson, Location.class);
        } else {
            return null;
        }
    }

    @Override
    public void pause() {
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, locationCallback);
        }
    }

    @Override
    public void resume() {
        if (googleApiClient.isConnected()) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationCallback, Looper.getMainLooper());
            } catch (SecurityException e) {
            }
        }
    }

    @Override
    public void destroy() {
        listener = null;
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
        googleApiClient = null;
        locationRequest = null;
        locationCallback = null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        LogUtil.d("GoogleApiClient onConnected");
        resume();
    }

    @Override
    public void onConnectionSuspended(int i) {
        LogUtil.d("GoogleApiClient onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        LogUtil.d("GoogleApiClient onConnectionFailed: " + connectionResult);
    }
}
