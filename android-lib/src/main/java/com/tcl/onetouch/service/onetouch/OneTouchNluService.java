package com.tcl.onetouch.service.onetouch;

import android.content.Context;
import android.location.Location;

import com.google.gson.Gson;
import com.tcl.onetouch.BuildConfig;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.NluService;
import com.tcl.onetouch.service.UserService;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.HttpUtil;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.StringUtil;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OneTouchNluService extends NluService {
//    private static final String NLP_PATH = "https://" + BuildConfig.GATEWAY_HOST + "/proxy/api/NLP";
    private static final String NLP_PATH = "http://" + BuildConfig.NLP_HOST + "/query_parser_api";
    private static final String PARSE_URL = NLP_PATH + "/parse?";
    private static final String PLACE_URL = NLP_PATH + "/place?";
    private static final String YES_NO_URL = NLP_PATH + "/yes_no?";
    private static final String TIME_URL = NLP_PATH + "/time?";
    private static final String NUMBER_URL = NLP_PATH + "/number?";
    private static final String SELECT_URL = NLP_PATH + "/select";

    private static final String ERROR_CODE_KEY = "errorCode";
    private static final String ERROR_MESSAGE_KEY = "errorMessage";
    private static final String RESULT_KEY = "result";

    private static final float PHONETIC_SEARCH_THRESHOLD = 0.5f;

    @Override
    public void init(Context context) {

    }

    private ErrorCode handleHttpError(HttpUtil.HttpRequestResult result) {
        if (result.error == null) {
            return null;
        }
        Map<String, Object> data = DataUtil.parseJson(result.result);
        if (data != null) {
            int errorCode = DataUtil.getInt(data, ERROR_CODE_KEY);
            String errorMessage = DataUtil.getString(data, ERROR_MESSAGE_KEY);
            if (errorCode != 0 && !errorMessage.isEmpty()) {
                result.error.setMessage(errorCode + ": " + errorMessage);
            }
        }
        return amendErrorCode(result.error);
    }

    private Map<String, String> getAuthenticationHeader() {
        Map<String, String> header = new HashMap<>();
        header.put("Authorization", "Bearer " + UserService.getInstance().getAccessToken());
        return header;
    }

    private Map<String, String> getQueryData() {
        Map<String, String> queryData = new HashMap<>();
        queryData.put("userId", UserService.getInstance().getUserId());
        queryData.put("deviceId", UserService.getInstance().getDeviceId());
        return queryData;
    }

    @Override
    public void parseIntent(final String query, final Location currentLocation, final NluService.ResultListener listener) {
        if (listener == null) {
            // TODO: throw exception
            return;
        }

        LogUtil.d("OneTouchNluService.parseIntent: " + query);

        Map<String, String> queryData = getQueryData();
        queryData.put("query", query);
        queryData.put("lon", String.valueOf(currentLocation.getLongitude()));
        queryData.put("lat", String.valueOf(currentLocation.getLatitude()));
        HttpUtil.doGet(PARSE_URL, getAuthenticationHeader(), queryData, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    Map<String, Object> resultData = DataUtil.getMap(data, RESULT_KEY);
                    if (resultData == null || resultData.isEmpty()) {
                        listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                        return;
                    }
                    String domain = DataUtil.getString(resultData, "domain");
                    String intent = DataUtil.getString(resultData, "intent");
                    if ("geo".equals(domain)) {
                        int entityType = DataUtil.getInt(resultData, "entity_type");
                        if ("route".equals(intent) && entityType == 1) {
                            listener.onResult(DataUtil.newMapData().
                                add(RESULT_KEY_INTENT, NluService.GEO_ROUTE_INTENT).
                                add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")).
                                add(RESULT_KEY_RESULT, DataUtil.getMap(resultData, "entity")));
                        } else if ("search".equals(intent) && (entityType == 2 || entityType == 3)) {
                            listener.onResult(DataUtil.newMapData().
                                add(RESULT_KEY_INTENT, NluService.GEO_SEARCH_INTENT).
                                add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")).
                                add(RESULT_KEY_RESULT, DataUtil.getMap(resultData, "entity")));
                        } else if ("dlg_my_parking".equals(intent)) {
                            listener.onResult(DataUtil.newMapData().
                                add(RESULT_KEY_INTENT, NluService.CAR_FIND_LOCATION_INTENT).
                                add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")).
                                add(RESULT_KEY_RESULT, DataUtil.getMap(resultData, "entity")));
                        } else {
                            listener.onError(createErrorCode(ErrorCode.NLU_ERROR_UNSUPPORTED_INTENT));
                        }
                    } else if ("restaurant".equals(domain)) {
                        int entityType = DataUtil.getInt(resultData, "entity_type");
                        if ("search".equals(intent) && entityType == 2) {
                            listener.onResult(DataUtil.newMapData().
                                add(RESULT_KEY_INTENT, NluService.GEO_SEARCH_INTENT).
                                add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")).
                                add(RESULT_KEY_RESULT, DataUtil.getMap(resultData, "entity")));
                        } else {
                            listener.onError(createErrorCode(ErrorCode.NLU_ERROR_UNSUPPORTED_INTENT));
                        }
                    } else if ("MusicCommand".equals(domain)) {
                        int entityType = DataUtil.getInt(resultData, "entity_type");
                        if ("MusicCommand".equals(intent) && entityType == 9) {
                            Map<String, Object> entity = DataUtil.getMap(resultData, "entity");
                            if (entity != null) {
                                Collection<Object> tracks = DataUtil.getCollection(entity, "tracks");
                                if (tracks != null && tracks.size() > 0) {
                                    String query = DataUtil.getString(resultData, "query");
                                    int idx = query.indexOf(' ');
                                    if (idx >= 0) {
                                        query = query.substring(idx + 1);
                                    }
                                    Map<String, Object> track = (Map<String, Object>) tracks.iterator().next();
                                    track.put("QueryEntity", query);
                                    listener.onResult(DataUtil.newMapData().
                                        add(RESULT_KEY_INTENT, NluService.MUSIC_PLAY_TRACK_INTENT).
                                        add(RESULT_KEY_RESULT, track));
                                } else {
                                    listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                                }
                            } else {
                                listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                            }
                        } else {
                            listener.onError(createErrorCode(ErrorCode.NLU_ERROR_UNSUPPORTED_INTENT));
                        }
                    } else if ("music_action".equals(domain)) {
                        int entityType = DataUtil.getInt(resultData, "entity_type");
                        if ("music_action".equals(intent) && entityType == 10) {
                            Map<String, Object> entity = DataUtil.getMap(resultData, "entity");
                            if (entity != null) {
                                String command = DataUtil.getString(entity, "command");
                                if ("play".equals(command)) {
                                    listener.onResult(DataUtil.newMapData().
                                        add(RESULT_KEY_INTENT, NluService.MUSIC_PLAY_INTENT).
                                        add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")));
                                } else if ("cancel".equals(command)) {
                                    listener.onResult(DataUtil.newMapData().
                                        add(RESULT_KEY_INTENT, NluService.MUSIC_STOP_INTENT).
                                        add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")));
                                } else if ("pause".equals(command)) {
                                    listener.onResult(DataUtil.newMapData().
                                        add(RESULT_KEY_INTENT, NluService.MUSIC_PAUSE_INTENT).
                                        add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")));
                                } else if ("next".equals(command)) {
                                    listener.onResult(DataUtil.newMapData().
                                        add(RESULT_KEY_INTENT, NluService.MUSIC_NEXT_INTENT).
                                        add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")));
                                } else if ("previous".equals(command)) {
                                    listener.onResult(DataUtil.newMapData().
                                        add(RESULT_KEY_INTENT, NluService.MUSIC_PREV_INTENT).
                                        add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")));
                                } else if ("shuffle".equals(command)) {
                                    listener.onResult(DataUtil.newMapData().
                                        add(RESULT_KEY_INTENT, NluService.MUSIC_SHUFFLE_INTENT).
                                        add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")));
                                } else if ("song_detail".equals(command)) {
                                    listener.onResult(DataUtil.newMapData().
                                        add(RESULT_KEY_INTENT, NluService.MUSIC_SHOW_DETAIL_INTENT).
                                        add(RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")));
                                } else {
                                    listener.onError(createErrorCode(ErrorCode.NLU_ERROR_UNSUPPORTED_INTENT));
                                }
                            } else {
                                listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                            }
                        } else {
                            listener.onError(createErrorCode(ErrorCode.NLU_ERROR_UNSUPPORTED_INTENT));
                        }
                    } else {
                        listener.onError(createErrorCode(ErrorCode.NLU_ERROR_UNSUPPORTED_DOMAIN));
                    }
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void parsePlace(String query, final NluService.ResultListener listener) {
        if (listener == null) {
            // TODO: throw exception
            return;
        }

        LogUtil.d("OneTouchNluService.parsePlace: " + query);

        Map<String, String> queryData = getQueryData();
        queryData.put("query", query);
        HttpUtil.doGet(PLACE_URL, getAuthenticationHeader(), queryData, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    int errorCode = DataUtil.getInt(data, ERROR_CODE_KEY);
                    if (errorCode != 2000) {
                        listener.onError(createErrorCode(ErrorCode.NLU_ERROR_SERVER));
                        return;
                    }
                    Map<String, Object> resultData = DataUtil.getMap(data, RESULT_KEY);
                    if (resultData == null || resultData.isEmpty()) {
                        listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                        return;
                    }
                    String address = DataUtil.getString(resultData, "address");
                    if (address.isEmpty()) {
                        listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                        return;
                    }
                    listener.onResult(DataUtil.newMapData().add(RESULT_KEY_RESULT, address));
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void parseYesNo(String query, String mode, final NluService.ResultListener listener) {
        if (listener == null) {
            // TODO: throw exception
            return;
        }

        LogUtil.d("OneTouchNluService.parseYesNo: " + query);

        Map<String, String> queryData = getQueryData();
        queryData.put("query", query);
        queryData.put("mode", mode);
        HttpUtil.doGet(YES_NO_URL, getAuthenticationHeader(), queryData, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    Map<String, Object> resultData = DataUtil.getMap(data, RESULT_KEY);
                    if (resultData == null || resultData.isEmpty()) {
                        listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                        return;
                    }
                    String domain = DataUtil.getString(resultData, "domain");
                    if (!"confirmation".equals(domain)) {
                        listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                        return;
                    }
                    listener.onResult(DataUtil.newMapData().
                        add(NluService.RESULT_KEY_RESULT, "dlg_yes".equals(DataUtil.getString(resultData, "intent"))).
                        add(NluService.RESULT_KEY_INTERPRETATION, DataUtil.getString(resultData, "interpretation")));
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void parseTime(String query, final NluService.ResultListener listener) {
        if (listener == null) {
            // TODO: throw exception
            return;
        }

        LogUtil.d("OneTouchNluService.parseTime: " + query);

        Map<String, String> queryData = getQueryData();
        queryData.put("query", query);
        HttpUtil.doGet(TIME_URL, getAuthenticationHeader(), queryData, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    String resultData = DataUtil.getString(data, RESULT_KEY);
                    if (!resultData.isEmpty()) {
                        listener.onResult(DataUtil.newMapData().add(NluService.RESULT_KEY_RESULT, DataUtil.getInt(data, RESULT_KEY)));
                    } else {
                        listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                    }
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void parseNumber(String query, final NluService.ResultListener listener) {
        if (listener == null) {
            // TODO: throw exception
            return;
        }

        LogUtil.d("OneTouchNluService.parseNumber: " + query);

        Map<String, String> queryData = getQueryData();
        queryData.put("query", query);
        HttpUtil.doGet(NUMBER_URL, getAuthenticationHeader(), queryData, new HttpUtil.RequestListener() {
            @Override
            public void onResult(HttpUtil.HttpRequestResult result) {
                ErrorCode error = handleHttpError(result);
                if (error == null) {
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    String resultData = DataUtil.getString(data, RESULT_KEY);
                    if (!resultData.isEmpty()) {
                        listener.onResult(DataUtil.newMapData().add(NluService.RESULT_KEY_RESULT, DataUtil.getInt(data, RESULT_KEY)));
                    } else {
                        listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                    }
                } else {
                    listener.onError(error);
                }
            }
        });
    }

    @Override
    public void parseSelection(final String query, final List<String> items, final NluService.ResultListener listener) {
        Map<String, Object> data = new LinkedHashMap<>();
        data.put("query", query);
        data.put("items", items);
        data.put("userId", UserService.getInstance().getUserId());
        data.put("deviceId", UserService.getInstance().getDeviceId());
        String dataJson = DataUtil.toJson(data);
        LogUtil.d("data json: " + dataJson);
        try {
            HttpUtil.doPost(SELECT_URL, getAuthenticationHeader(), dataJson.getBytes("UTF-8"), new HttpUtil.RequestListener() {
                @Override
                public void onResult(HttpUtil.HttpRequestResult result) {
                    ErrorCode error = handleHttpError(result);
                    if (error == null) {
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        Map<String, Object> resultData = DataUtil.getMap(data, RESULT_KEY);
                        if (resultData == null || resultData.isEmpty()) {
                            listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                            return;
                        }
                        int index = DataUtil.getInt(resultData, "index");
                        if (index >= 0) {
                            listener.onResult(DataUtil.newMapData().add(NluService.RESULT_KEY_RESULT, index));
                        } else {
                            String queryCode = StringUtil.getMetaphone3Encode(query);
                            if (queryCode.length() >= 3) {
                                LogUtil.d("Phonetic search for " + query + " (" + queryCode + "):");
                                int selected = -1;
                                int score = Integer.MAX_VALUE;
                                for (int i = 0; i < items.size(); ++i) {
                                    String item = items.get(i);
                                    String itemCode = StringUtil.getMetaphone3Encode(item);
                                    int dist = StringUtils.getLevenshteinDistance(queryCode, itemCode) -
                                        Math.abs(itemCode.length() - queryCode.length());
                                    LogUtil.d("  " + item + " (" + itemCode + "): " + dist);
                                    if (dist < score) {
                                        score = dist;
                                        selected = i;
                                    }
                                }
                                if (((float) score) / queryCode.length() < PHONETIC_SEARCH_THRESHOLD) {
                                    LogUtil.d("  selected: " + selected);
                                    index = selected + 1;
                                }
                            }
                            if (index >= 0) {
                                listener.onResult(DataUtil.newMapData().add(NluService.RESULT_KEY_RESULT, index));
                            } else {
                                listener.onError(createErrorCode(ErrorCode.NLU_ERROR_NO_RESULT));
                            }
                        }
                    } else {
                        listener.onError(error);
                    }
                }
            });
        } catch (UnsupportedEncodingException ignore) {
        }
    }
}
