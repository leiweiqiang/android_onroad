package com.tcl.onetouch.service;

import android.content.Context;

import com.tcl.onetouch.base.QnATask;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.nuance.NuanceWakeupService;

public abstract class WakeupService extends BaseService {
    public static final String WAKEUP_PHRASE = "one touch";

    public static interface OnStateChangeListener {
        public void onWakeupStarted();
        public void onWakeupStopped();
    }

    public static interface OnWakeupListener {
        public void onWakeup();
        public void onError(ErrorCode error);
    }

    public static interface OnStoppedListener {
        public void onStopped();
    }

    public abstract void init(Context context, WakeupService.OnStateChangeListener listener);
    public abstract void setOnWakeupListener(OnWakeupListener listener);
    public abstract void startListening();
    public abstract void stopListening(OnStoppedListener listener);
    public abstract boolean isActive();
    public abstract void destroy();

    private static WakeupService sDefaultService;

    public static WakeupService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new NuanceWakeupService();
        }
        return sDefaultService;
    }
}
