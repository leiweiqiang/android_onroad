package com.tcl.onetouch.service;

import android.content.Context;
import android.location.Location;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.onetouch.OneTouchNluService;

import java.util.List;
import java.util.Map;

public abstract class NluService extends BaseService {
    public static final int GEO_SEARCH_INTENT = 1001;
    public static final int GEO_ROUTE_INTENT = 1002;
    public static final int MUSIC_PLAY_TRACK_INTENT = 3001;
    public static final int MUSIC_PLAY_INTENT = 3002;
    public static final int MUSIC_STOP_INTENT = 3003;
    public static final int MUSIC_PAUSE_INTENT = 3004;
    public static final int MUSIC_NEXT_INTENT = 3005;
    public static final int MUSIC_PREV_INTENT = 3006;
    public static final int MUSIC_SHUFFLE_INTENT = 3007;
    public static final int MUSIC_SHOW_DETAIL_INTENT = 3008;
    public static final int CAR_FIND_LOCATION_INTENT = 4001;

    public static final String YES_NO_MODE_PARKING = "parking";
    public static final String YES_NO_MODE_NAVIGATION = "navigation";

    public static final String RESULT_KEY_INTENT = "intent";
    public static final String RESULT_KEY_INTERPRETATION = "interpretation";
    public static final String RESULT_KEY_RESULT = "result";

    public static interface ResultListener {
        public void onResult(Map<String, Object> result);
        public void onError(ErrorCode error);
    }

    public abstract void init(Context context);
    public abstract void parseIntent(String query, Location currentLocation, NluService.ResultListener listener);
    public abstract void parsePlace(String query, NluService.ResultListener listener);
    public abstract void parseYesNo(String query, String mode, NluService.ResultListener listener);
    public abstract void parseTime(String query, NluService.ResultListener listener);
    public abstract void parseNumber(String query, NluService.ResultListener listener);
    public abstract void parseSelection(String query, List<String> items, NluService.ResultListener listener);

    private static NluService sDefaultService;

    public static NluService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new OneTouchNluService();
        }
        return sDefaultService;
    }
}

