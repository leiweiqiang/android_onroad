package com.tcl.onetouch.service.nuance;

import android.content.Context;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.pipes.ConverterPipe;
import com.nuance.dragon.toolkit.audio.pipes.DuplicatorPipe;
import com.nuance.dragon.toolkit.audio.pipes.OpusEncoderPipe;
import com.nuance.dragon.toolkit.audio.pipes.SpeexEncoderPipe;
import com.nuance.dragon.toolkit.audio.sources.RecorderSource;
import com.nuance.dragon.toolkit.cloudservices.CloudConfig;
import com.nuance.dragon.toolkit.cloudservices.CloudServices;
import com.nuance.dragon.toolkit.cloudservices.DictionaryParam;
import com.nuance.dragon.toolkit.cloudservices.TransactionError;
import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognitionError;
import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognitionResult;
import com.nuance.dragon.toolkit.cloudservices.recognizer.CloudRecognizer;
import com.nuance.dragon.toolkit.cloudservices.recognizer.RecogSpec;
import com.nuance.dragon.toolkit.data.Data;
import com.tcl.onetouch.BuildConfig;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.SpeechRecognizerService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.StringUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.UUID;

public class NuanceCloudRecognizer extends SpeechRecognizerService {
    private static final AudioType AUDIO_TYPE = AudioType.OPUS_WB;
    private static final String DICTATION_TYPE = "dictation";
    private static final String DICTATION_LANGUAGE = "eng-USA";
    private static final String RECOG_SPEC_COMMAND = "NVC_ASR_CMD";
//    private static final String RESPONSE_MODE = "UtteranceDetectionWithPartialRecognition";
    private static final String RESPONSE_MODE = "NoUtteranceDetectionWithPartialRecognition";
    private static final long DEFAULT_RECOGNIZE_TIMEOUT = 30000;
    private static final long DEFAULT_SPEECH_END_PAUSE = 500;
    private static final int RETRY_TIMEOUT_INIT = 100;
    private static final int RETRY_TIMEOUT_MAX = 1000;

    private CloudServices cloudServices;
    private CloudRecognizer cloudRecognizer;
    private RecorderSource<AudioChunk> recorder;
    private DuplicatorPipe<AudioChunk> duplicatorPipe;
    private ConverterPipe<AudioChunk, AudioChunk> encoder;
    private ConverterPipe<AudioChunk, AudioChunk> speexEncoder;
    private SpeechDetectionPipe speechDetector;
    private boolean isRecognizing;
    private boolean isPrompted;
    private long recognizeTimeout;
    private long speechEndPause;
    private int retryTimeout;

    private class RetryAction implements Runnable {
        OnResultListener listener;
        @Override
        public void run() {
            startRecognizeInternal(listener);
        }
    };
    private RetryAction retryAction;

    public void init(Context context, SpeechRecognizerService.OnStateChangeListener listener) {
        super.init(context, listener);
        if (cloudServices != null) {
            cloudServices.release();
        }
        cloudServices = CloudServices.createCloudServices(
            context,
            new CloudConfig(BuildConfig.NUANCE_HOST, BuildConfig.NUANCE_PORT,
                BuildConfig.NUANCE_APP_ID, StringUtil.stringToByteArray(BuildConfig.NUANCE_APP_KEY),
                AUDIO_TYPE, AUDIO_TYPE, false));
        if (cloudRecognizer != null) {
            cloudRecognizer.cancel();
        }
        cloudRecognizer = new CloudRecognizer(cloudServices);
        cloudRecognizer.setResultCadence(250);
        isRecognizing = false;
        isPrompted = false;
        recognizeTimeout = 0;
        retryAction = null;
    }

    private RecogSpec createRecogSpec() {
        // Create a sample recognition spec. based on the "NVC_ASR_CMD"
        Data.Dictionary settings = new Data.Dictionary();
        settings.put("dictation_type", DICTATION_TYPE);
        settings.put("dictation_language", DICTATION_LANGUAGE);
        settings.put("application_session_id", String.valueOf(UUID.randomUUID()));
        RecogSpec retRecogSpec = new RecogSpec(RECOG_SPEC_COMMAND, settings, "AUDIO_INFO");

        // Also, add necessary "REQUEST_INFO" parameter
        Data.Dictionary requestInfo = new Data.Dictionary();
        requestInfo.put("start", 0);
        requestInfo.put("end", 0);
        requestInfo.put("text", "");

//        if (resultModeName.equals("Utterance Detection Default"))
//            requestInfo.put("intermediate_response_mode", "UtteranceDetectionWithCompleteRecognition");
//        else if (resultModeName.equals("Utterance Detection Very Aggressive")) {
//            requestInfo.put("intermediate_response_mode", "UtteranceDetectionWithCompleteRecognition");
//            requestInfo.put("utterance_detection_silence_duration", "VeryAggressive");
//        } else if (resultModeName.equals("Utterance Detection Aggressive")) {
//            requestInfo.put("intermediate_response_mode", "UtteranceDetectionWithCompleteRecognition");
//            requestInfo.put("utterance_detection_silence_duration", "Aggressive");
//        } else if (resultModeName.equals("Utterance Detection Average")) {
//            requestInfo.put("intermediate_response_mode", "UtteranceDetectionWithCompleteRecognition");
//            requestInfo.put("utterance_detection_silence_duration", "Average");
//        } else if (resultModeName.equals("Utterance Detection Conservative")) {
//            requestInfo.put("intermediate_response_mode", "UtteranceDetectionWithCompleteRecognition");
//            requestInfo.put("utterance_detection_silence_duration", "Conservative");
//        } else if (resultModeName.equals("Streaming Results")) {
//            requestInfo.put("intermediate_response_mode", "NoUtteranceDetectionWithPartialRecognition");
//            cloudRecognizer.setResultCadence(500);
//        }
        requestInfo.put("intermediate_response_mode", RESPONSE_MODE);
        retRecogSpec.addParam(new DictionaryParam("REQUEST_INFO", requestInfo));

        return retRecogSpec;
    }

    private String parseResults(CloudRecognitionResult cloudResult) {
        Data.Dictionary processedResult = cloudResult.getDictionary();
        if (processedResult == null) {
            return "";
        }

        Data.Sequence transcriptions = processedResult.getSequence("transcriptions");
        if (transcriptions == null) {
            return "";
        }

        int len = transcriptions.size();
        if (len > 0) {
            return transcriptions.getString(0).value;
        } else {
            Data.String prompt = processedResult.getString("prompt");
            if (prompt != null) {
                return prompt.value;
            } else {
                return "";
            }
        }
    }

    @Override
    public void startRecognize(OnResultListener listener) {
        startRecognize(DEFAULT_RECOGNIZE_TIMEOUT, listener);
    }

    @Override
    public void startRecognize(long timeout, OnResultListener listener) {
        startRecognize(timeout, DEFAULT_SPEECH_END_PAUSE, listener);
    }

    @Override
    public void startRecognize(long timeout, long endPause, OnResultListener listener) {
        if (listener == null) {
            // TODO: throw exception
            return;
        }
        LogUtil.d("NuanceCloudRecognizer: startRecognize");

        cancelRecognize();
        isRecognizing = true;
        isPrompted = false;
        if (onStateChangeListener != null) {
            onStateChangeListener.onRecognitionStarted();
        }
        recognizeTimeout = timeout;
        speechEndPause = endPause;
        retryTimeout = 0;
        retryAction = null;
        requestAudioFocus();
        startRecognizeInternal(listener);
    }

    private void startRecognizeInternal(final OnResultListener listener) {
        if (!isRecognizing) {
            return;
        }

        if (retryTimeout == 0) {
            retryTimeout = RETRY_TIMEOUT_INIT;
        } else if (retryTimeout < RETRY_TIMEOUT_MAX) {
            retryTimeout *= 2;
            if (retryTimeout >= RETRY_TIMEOUT_MAX) {
                retryTimeout = RETRY_TIMEOUT_MAX;
            }
        } else {
            endRecognize();
            listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_AUDIO));
            return;
        }

        recorder = new AudioRecordSource(AudioType.PCM_16k, SystemUtil.getBgHandler());
        // Start recording and recognition
        recorder.startRecording(new RecorderSource.Listener<AudioChunk>() {
            @Override
            public void onStarted(RecorderSource<AudioChunk> recorderSource) {
                if (!isRecognizing) {
                    return;
                }
                LogUtil.d("NuanceCloudRecognizer: listening started");
                if (duplicatorPipe != null) {
                    duplicatorPipe.disconnectAudioSource();
                }
                duplicatorPipe = new DuplicatorPipe<>();
                duplicatorPipe.connectAudioSource(recorder);

                if (speexEncoder != null) {
                    speexEncoder.disconnectAudioSource();
                    speexEncoder.release();
                }
                speexEncoder = new SpeexEncoderPipe();
                speexEncoder.connectAudioSource(duplicatorPipe);

                if (speechDetector != null) {
                    speechDetector.disconnectAudioSource();
                }
                speechDetector = new SpeechDetectionPipe(new SpeechDetectionPipe.Listener() {
                    @Override
                    public void onStartOfSpeech() {
                        if (!isRecognizing) {
                            return;
                        }
                        LogUtil.d("NuanceCloudRecognizer: start of speech");
                        if (onStateChangeListener != null) {
                            onStateChangeListener.onBeginningOfSpeech();
                        }
                    }

                    @Override
                    public void onEndOfSpeech() {
                        if (!isRecognizing) {
                            return;
                        }
                        LogUtil.d("NuanceCloudRecognizer: end of speech");
                        if (onStateChangeListener != null) {
                            onStateChangeListener.onEndOfSpeech();
                        }
                        if (encoder != null) {
                            encoder.disconnectAudioSource();
                            encoder.release();
                            encoder = null;
                        }
                        if (cloudRecognizer != null) {
                            cloudRecognizer.processResult();
                        }
                    }

                    @Override
                    public void onTimeout() {
                        if (!isRecognizing) {
                            return;
                        }
                        LogUtil.d("NuanceCloudRecognizer: timeout");
                        cancelRecognize();
                        listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_TIMEOUT));
                    }
                }, (int) recognizeTimeout, (int) speechEndPause);
                speechDetector.connectAudioSource(speexEncoder);

                startCloudRecognizer(listener);
            }

            @Override
            public void onStopped(RecorderSource<AudioChunk> recorderSource) {
            }

            @Override
            public void onError(RecorderSource<AudioChunk> recorderSource) {
                if (!isRecognizing) {
                    return;
                }
                if (retryAction == null) {
                    retryAction = new RetryAction();
                    retryAction.listener = listener;
                }
                SystemUtil.postToMainThread(retryAction, retryTimeout);
            }
        });
    }

    protected void startCloudRecognizer(final OnResultListener listener) {
        if (encoder != null) {
            encoder.disconnectAudioSource();
            encoder.release();
        }
        encoder = new OpusEncoderPipe();
        encoder.connectAudioSource(duplicatorPipe);

        cloudRecognizer.startRecognition(createRecogSpec(), encoder, new CloudRecognizer.Listener() {
            String lastResult = "";

            @Override
            public void onResult(CloudRecognitionResult cloudRecognitionResult) {
                if (!isRecognizing) {
                    return;
                }
                String topResult = parseResults(cloudRecognitionResult);
                if (cloudRecognitionResult.isFinal()) {
                    LogUtil.d("NuanceCloudRecognizer: final result - " + topResult);
                    if (topResult.isEmpty()) {
                        listener.onPartialResult(topResult);
                        speechDetector.speechNotRecognized();
                        startCloudRecognizer(listener);
                    } else {
                        endRecognize();
                        listener.onResult(topResult);
                    }
                } else if (!topResult.equals(lastResult)) {
                    LogUtil.d("NuanceCloudRecognizer: partial result - " + topResult);
                    if (lastResult.isEmpty()) {
                        speechDetector.speechRecognized();
                    }
                    lastResult = topResult;
                    listener.onPartialResult(topResult);
                }
            }

            @Override
            public void onError(CloudRecognitionError cloudRecognitionError) {
                if (!isRecognizing) {
                    return;
                }
                ErrorCode error;
                TransactionError transactionError = cloudRecognitionError.getTransactionError();
                if (transactionError != null) {
                    switch (transactionError.getErrorCode()) {
                        case TransactionError.ConnectionErrorCodes.NETWORK_UNAVAILABLE:
                            error = createErrorCode(
                                ErrorCode.NETWORK_ERROR_NO_NETWORK,
                                transactionError.getErrorText());
                            break;
                        case TransactionError.ConnectionErrorCodes.TIMED_OUT_WAITING_FOR_RESULT:
                            error = createErrorCode(
                                ErrorCode.NETWORK_ERROR_TIMEOUT,
                                transactionError.getErrorText());
                            break;
                        case TransactionError.ConnectionErrorCodes.REMOTE_DISCONNECTION:
                            error = createErrorCode(
                                ErrorCode.SPEECH_ERROR_SERVER,
                                transactionError.getErrorText());
                            break;
                        case TransactionError.ConnectionErrorCodes.COMMAND_IDLE_FOR_TOO_LONG:
                            error = createErrorCode(
                                ErrorCode.SPEECH_ERROR_TIMEOUT,
                                transactionError.getErrorText());
                            break;
                        case TransactionError.ConnectionErrorCodes.COMMAND_ENDED_UNEXPECTEDLY:
                            error = createErrorCode(
                                ErrorCode.SPEECH_ERROR_CLIENT,
                                transactionError.getErrorText());
                            break;
                        default:
                            error = createErrorCode(
                                ErrorCode.SPEECH_ERROR,
                                transactionError.getErrorText());
                            break;
                    }
                } else {
                    error = createErrorCode(
                        ErrorCode.SPEECH_ERROR,
                        transactionError.getErrorText());
                }
                endRecognize();
                listener.onError(error);
            }

            @Override
            public void onTransactionIdGenerated(String s) {
                if (!isRecognizing) {
                    return;
                }
                LogUtil.d("NuanceCloudRecognizer: transaction ID generated: " + s);
                if (!isPrompted) {
                    isPrompted = true;
                    OneTouchApplication.getInstance().playSpeechHintSound();
                    if (onStateChangeListener != null) {
                        onStateChangeListener.onReadyToSpeech(recognizeTimeout, speechEndPause);
                    }
                }
            }
        });
    }

    protected void endRecognize() {
        if (!isRecognizing) {
            return;
        }
        isRecognizing = false;
        if (retryAction != null) {
            SystemUtil.cancelMainThreadAction(retryAction);
            retryAction = null;
        }
        if (encoder != null) {
            encoder.disconnectAudioSource();
            encoder.release();
            encoder = null;
        }

        if (speechDetector != null) {
            speechDetector.disconnectAudioSource();
            speechDetector = null;
        }

        if (speexEncoder != null) {
            speexEncoder.disconnectAudioSource();
            speexEncoder.release();
            speexEncoder = null;
        }

        if (duplicatorPipe != null) {
            duplicatorPipe.disconnectAudioSource();
            duplicatorPipe = null;
        }

        if (recorder != null) {
            recorder.stopRecording();
            recorder = null;
        }

        abandonAudioFocus();

        if (onStateChangeListener != null) {
            onStateChangeListener.onRecognitionEnded();
        }
    }

    public void stopRecognize() {
        endRecognize();
        if (cloudRecognizer != null) {
            cloudRecognizer.processResult();
        }
    }

    public void cancelRecognize() {
        endRecognize();
        if (cloudRecognizer != null) {
            cloudRecognizer.cancel();
        }
    }

    @Override
    public boolean isRecognizing() {
        return isRecognizing;
    }

    public void destroy() {
        if (encoder != null) {
            encoder.disconnectAudioSource();
            encoder.release();
            encoder = null;
        }

        if (speechDetector != null) {
            speechDetector.disconnectAudioSource();
            speechDetector = null;
        }

        if (speexEncoder != null) {
            speexEncoder.disconnectAudioSource();
            speexEncoder.release();
            speexEncoder = null;
        }

        if (duplicatorPipe != null) {
            duplicatorPipe.disconnectAudioSource();
            duplicatorPipe = null;
        }

        if (recorder != null) {
            recorder.stopRecording();
            recorder = null;
        }

        if (cloudRecognizer != null) {
            cloudRecognizer.cancel();
            cloudRecognizer = null;
        }

        if (cloudServices != null) {
            cloudServices.release();
            cloudServices = null;
        }
        super.destroy();
    }
}
