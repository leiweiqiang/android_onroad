package com.tcl.onetouch.service;

import android.content.Context;
import android.media.AudioManager;

import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.google.AndroidTtsService;

public abstract class TtsService extends BaseService {
    public static final int STREAM_TYPE = AudioManager.STREAM_MUSIC;

    public static final int SPEECH_PRIORITY_LOW = 1;
    public static final int SPEECH_PRIORITY_NORMAL = 2;
    public static final int SPEECH_PRIORITY_HIGH = 3;

    public static interface OnStateChangeListener {
        public void onSpeechStarted(String text, String source, int priority);
        public void onSpeechStopped();
        public void onError(ErrorCode error);
    }

    public static interface OnSpeechListener {
        public void onFinished();
        public void onStopped();
        public void onError(ErrorCode error);
    }

    public abstract void init(Context context, TtsService.OnStateChangeListener listener);
    public abstract void speak(String text, String source, int priority, OnSpeechListener listener);
    public abstract void stop(String source);
    public abstract void destroy();

    private static TtsService sDefaultService;

    public static TtsService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new AndroidTtsService();
        }
        return sDefaultService;
    }
}
