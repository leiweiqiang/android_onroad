package com.tcl.onetouch.service;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.iid.InstanceID;
import com.tcl.onetouch.BuildConfig;
import com.tcl.onetouch.R;
import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.util.DataUtil;
import com.tcl.onetouch.util.HttpUtil;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UserService extends BaseService
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final int SIGN_IN_TYPE_ONE_TOUCH = 1;
    public static final int SIGN_IN_TYPE_GOOGLE = 2;
    public static final int SIGN_IN_TYPE_FACEBOOK = 3;

    public static final int GOOGLE_SIGN_IN_REQUEST_CODE = 1;
    private static final String GOOGLE_AUTH_SERVER_CLIENT_ID_KEY = "com.google.android.gms.auth.api.signin.SERVER_CLIENT_ID";
    private static final String ONE_TOUCH_AUTH = "Basic T25lVG91Y2g6T25lVG91Y2hTZWNyZXQ=";

    private static final String SOCIAL_LOGIN_URL = "https://" + BuildConfig.AUTHENTICATION_HOST + "/ui/social/mobile";
    private static final String ONE_TOUCH_LOGIN_URL = "https://" + BuildConfig.AUTHENTICATION_HOST + "/ui/api/authentication";
    private static final String REFRESH_TOKEN_URL = "https://" + BuildConfig.AUTHENTICATION_HOST + "/authserver/oauth/token";

    public static interface Listener {
        public void onSignInChanged(int signInType, Map<String, Object> result);

        public void onError(ErrorCode error);
    }

    private static UserService sInstance;

    public static UserService getInstance() {
        if (sInstance == null) {
            sInstance = new UserService();
        }
        return sInstance;
    }

    private String iid;

    private Map<String, Object> oneTouchSignInResult;

    private GoogleApiClient googleApiClient;
    private Map<String, Object> googleSignInResult;

    private CallbackManager facebookCallbackManager;
    private AccessTokenTracker facebookAccessTokenTracker;
    private Map<String, Object> facebookSignInResult;

    private Set<Listener> listeners;

    private Runnable refreshTokenTask = new Runnable() {
        @Override
        public void run() {
            if (oneTouchSignInResult == null) {
                return;
            }
            final String userId = DataUtil.getString(oneTouchSignInResult, "user_name");
            Map<String, String> header = new HashMap<>();
            header.put("content-type", "application/x-www-form-urlencoded");
            header.put("Authorization", ONE_TOUCH_AUTH);
            Map<String, String> query = new HashMap<>();
            query.put("refresh_token", DataUtil.getString(oneTouchSignInResult, "refresh_token"));
            query.put("grant_type", "refresh_token");
            HttpUtil.doPost(
                REFRESH_TOKEN_URL,
                header,
                query,
                new HttpUtil.RequestListener() {
                    @Override
                    public void onResult(HttpUtil.HttpRequestResult result) {
                        if (result.error != null) {
                            notifyError(amendErrorCode(result.error));
                            return;
                        }
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        if (data != null) {
                            int expires = DataUtil.getInt(data, "expires_in") * 1000;
                            oneTouchSignInResult = DataUtil.newMapData().
                                add("user_name", userId).
                                add("access_token", DataUtil.getString(data, "access_token")).
                                add("refresh_token", DataUtil.getString(data, "refresh_token")).
                                add("expires", System.currentTimeMillis() + expires);
                            SystemUtil.cancelMainThreadAction(refreshTokenTask);
                            SystemUtil.postToMainThread(refreshTokenTask, expires - 300000);
                        }
                    }
                }
            );
        }
    };

    public void init(Context context) {
        iid = InstanceID.getInstance(context).getId();
        LogUtil.d("instanceId: " + iid);
        googleSignInResult = null;
        facebookSignInResult = null;
        oneTouchSignInResult = null;
        listeners = new HashSet<>();

        String auth = PreferenceManager.getDefaultSharedPreferences(context).
            getString(context.getResources().getString(R.string.PREF_KEY_ONE_TOUCH_LOGIN), "");
        if (!auth.isEmpty()) {
            oneTouchSignInResult = DataUtil.parseJson(auth);
            LogUtil.d("oneTouchSignInResult: " + oneTouchSignInResult);
            long expires = DataUtil.getLong(oneTouchSignInResult, "expires");
            long timeout = expires - 300000 - System.currentTimeMillis();
            if (timeout > 0) {
                SystemUtil.cancelMainThreadAction(refreshTokenTask);
                SystemUtil.postToMainThread(refreshTokenTask, timeout);
            } else {
                oneTouchSignInResult.remove("access_token");
                oneTouchSignInResult.remove("refresh_token");
            }
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
            requestIdToken(OneTouchApplication.getInstance().getMetaStringValue(GOOGLE_AUTH_SERVER_CLIENT_ID_KEY)).
            requestEmail().requestId().requestProfile().build();

        googleApiClient = new GoogleApiClient.Builder(context)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build();
        googleApiClient.connect();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            handleGoogleSignIn(opr.get());
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleGoogleSignIn(googleSignInResult);
                }
            });
        }

        FacebookSdk.sdkInitialize(context.getApplicationContext());
        facebookCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(facebookCallbackManager,
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    handleFacebookSignIn(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                }

                @Override
                public void onError(FacebookException exception) {

                }
            });

        facebookAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                AccessToken oldAccessToken,
                AccessToken currentAccessToken) {
                handleFacebookSignIn(currentAccessToken);
            }
        };
    }

    public void registerListener(Listener listener) {
        if (listener != null && listeners != null) {
            listeners.add(listener);
        }
    }

    public void unregisterListener(Listener listener) {
        if (listener != null && listeners != null) {
            listeners.remove(listener);
        }
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == GOOGLE_SIGN_IN_REQUEST_CODE) {
            handleGoogleSignIn(Auth.GoogleSignInApi.getSignInResultFromIntent(intent));
            return true;
        } else if (facebookCallbackManager.onActivityResult(requestCode, resultCode, intent)) {
            handleFacebookSignIn(AccessToken.getCurrentAccessToken());
            return true;
        }
        return false;
    }

    public void oneTouchSignIn(final String userName, final String password) {
        Map<String, String> header = new HashMap<>();
        header.put("content-type", "application/x-www-form-urlencoded");
        Map<String, String> query = new HashMap<>();
        query.put("j_username", userName);
        query.put("j_password", password);
        query.put("remember-me", "true");
        HttpUtil.doPost(
            ONE_TOUCH_LOGIN_URL,
            header,
            query,
            new HttpUtil.RequestListener() {
                @Override
                public void onResult(HttpUtil.HttpRequestResult result) {
                    if (result.error != null) {
                        notifyError(amendErrorCode(result.error));
                        return;
                    }
                    Map<String, Object> data = DataUtil.parseJson(result.result);
                    if (data != null) {
                        int expires = DataUtil.getInt(data, "expires_in") * 1000;
                        oneTouchSignInResult = DataUtil.newMapData().
                            add("user_name", userName).
                            add("access_token", DataUtil.getString(data, "access_token")).
                            add("refresh_token", DataUtil.getString(data, "refresh_token")).
                            add("expires", System.currentTimeMillis() + expires);
                        SystemUtil.cancelMainThreadAction(refreshTokenTask);
                        SystemUtil.postToMainThread(refreshTokenTask, expires - 300000);
                        notifySignInResult(SIGN_IN_TYPE_ONE_TOUCH, oneTouchSignInResult);
                    }
                }
            }
        );
    }

    private void oneTouchSocialLogin(int signInType, Map<String, Object> signInResult) {
        final String userId = DataUtil.getString(signInResult, "email");
        String accessToken = getAccessToken();
        if (TextUtils.equals(userId, getUserId()) && accessToken != null && !accessToken.isEmpty()) {
            return;
        }
        Map<String, String> header = new HashMap<>();
        header.put("content-type", "application/json");
        String providerId = "";
        if (signInType == SIGN_IN_TYPE_GOOGLE) {
            providerId = "google";
        } else {
            providerId = "facebook";
        }
        String body = DataUtil.newMapData().
            add("provider_id", providerId).
            add("provider_user_id", DataUtil.getString(signInResult, "id")).
            add("user_id", userId).
            add("display_name", DataUtil.getString(signInResult, "name")).
            add("email", userId).
            add("access_token", "").
            add("image_url", "").
            add("profile_url", "").
            add("firstname", "").
            add("lastname", "").
            add("expire_time", "").
            toJson();
        try {
            HttpUtil.doPost(
                SOCIAL_LOGIN_URL,
                header,
                body.getBytes("UTF-8"),
                new HttpUtil.RequestListener() {
                    @Override
                    public void onResult(HttpUtil.HttpRequestResult result) {
                        if (result.error != null) {
                            LogUtil.d("social login error: " + result.error + ", " + result.result);
                            notifyError(amendErrorCode(result.error));
                            return;
                        }
                        Map<String, Object> data = DataUtil.parseJson(result.result);
                        if (data != null) {
                            int expires = DataUtil.getInt(data, "expires_in") * 1000;
                            oneTouchSignInResult = DataUtil.newMapData().
                                add("user_name", userId).
                                add("access_token", DataUtil.getString(data, "access_token")).
                                add("refresh_token", DataUtil.getString(data, "refresh_token")).
                                add("expires", System.currentTimeMillis() + expires);
                            SystemUtil.cancelMainThreadAction(refreshTokenTask);
                            SystemUtil.postToMainThread(refreshTokenTask, expires - 300000);
                            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(OneTouchApplication.getInstance());
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString(
                                OneTouchApplication.getInstance().getResources().getString(R.string.PREF_KEY_ONE_TOUCH_LOGIN),
                                DataUtil.toJson(oneTouchSignInResult));
                            editor.commit();
                            notifySignInResult(SIGN_IN_TYPE_ONE_TOUCH, oneTouchSignInResult);
                        }
                    }
                }
            );
        } catch (UnsupportedEncodingException ignore) {
        }
    }

    public String getDeviceId() {
        return iid;
    }

    public String getUserId() {
        if (oneTouchSignInResult == null) {
            return "";
        }
        return DataUtil.getString(oneTouchSignInResult, "user_name");
    }

    public String getAccessToken() {
        if (oneTouchSignInResult == null) {
            return null;
        }
        return DataUtil.getString(oneTouchSignInResult, "access_token");
    }

    public void googleSignIn(Activity activity) {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        activity.startActivityForResult(signInIntent, GOOGLE_SIGN_IN_REQUEST_CODE);
    }

    public void googleSignOut() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
            new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    if (status.isSuccess()) {
                        handleGoogleSignIn(null);
                    }
                }
            });
    }

    private void handleGoogleSignIn(GoogleSignInResult signInResult) {
        LogUtil.d("handleGoogleSignIn: " + signInResult);
        if ((signInResult != null && signInResult.isSuccess()) && googleSignInResult == null) {
            GoogleSignInAccount acct = signInResult.getSignInAccount();
            googleSignInResult = DataUtil.newMapData().
                add("id", acct.getId()).
                add("token", acct.getIdToken()).
                add("email", acct.getEmail()).
                add("name", acct.getDisplayName());
            notifySignInResult(SIGN_IN_TYPE_GOOGLE, googleSignInResult);
            oneTouchSocialLogin(SIGN_IN_TYPE_GOOGLE, googleSignInResult);
        } else if ((signInResult == null || !signInResult.isSuccess()) && googleSignInResult != null) {
            googleSignInResult = null;
            notifySignInResult(SIGN_IN_TYPE_GOOGLE, null);
            if (oneTouchSignInResult != null) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(OneTouchApplication.getInstance());
                SharedPreferences.Editor editor = pref.edit();
                editor.remove(
                    OneTouchApplication.getInstance().getResources().getString(R.string.PREF_KEY_ONE_TOUCH_LOGIN));
                editor.commit();
                oneTouchSignInResult = null;
                notifySignInResult(SIGN_IN_TYPE_ONE_TOUCH, null);
            }
        }
    }

    private void handleFacebookSignIn(AccessToken accessToken) {
        LogUtil.d("handleFacebookSignIn: " + accessToken);
        if (accessToken != null && facebookSignInResult == null) {
            facebookSignInResult = DataUtil.newMapData().
                add("id", accessToken.getUserId()).
                add("token", accessToken.getToken());
            GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (object != null) {
                            try {
                                facebookSignInResult.put("email", object.get("email").toString());
                                facebookSignInResult.put("name", object.get("name").toString());
                                oneTouchSocialLogin(SIGN_IN_TYPE_FACEBOOK, facebookSignInResult);
                            } catch (JSONException e) {
                            }
                        }
                        notifySignInResult(SIGN_IN_TYPE_FACEBOOK, facebookSignInResult);
                    }
                });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email");
            request.setParameters(parameters);
            request.executeAsync();
        } else if (accessToken == null && facebookSignInResult != null) {
            facebookSignInResult = null;
            notifySignInResult(SIGN_IN_TYPE_FACEBOOK, null);
            if (oneTouchSignInResult != null) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(OneTouchApplication.getInstance());
                SharedPreferences.Editor editor = pref.edit();
                editor.remove(
                    OneTouchApplication.getInstance().getResources().getString(R.string.PREF_KEY_ONE_TOUCH_LOGIN));
                editor.commit();
                oneTouchSignInResult = null;
                notifySignInResult(SIGN_IN_TYPE_ONE_TOUCH, null);
            }
        }
    }

    public Map<String, Object> getGoogleSignInResult() {
        return googleSignInResult;
    }

    public Map<String, Object> getFacebookSignInResult() {
        return facebookSignInResult;
    }

    private void notifyError(ErrorCode error) {
        if (listeners == null) {
            return;
        }
        for (Listener listener : listeners) {
            listener.onError(error);
        }
    }

    private void notifySignInResult(int signInType, Map<String, Object> result) {
        if (listeners == null) {
            return;
        }
        for (Listener listener : listeners) {
            listener.onSignInChanged(signInType, result);
        }
    }

    public void destroy() {
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
        googleApiClient = null;
        if (facebookAccessTokenTracker != null) {
            facebookAccessTokenTracker.stopTracking();
        }
        facebookCallbackManager = null;
        iid = null;
        googleSignInResult = null;
        facebookSignInResult = null;
        oneTouchSignInResult = null;
        listeners = null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        LogUtil.d("GoogleApiClient onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        LogUtil.d("GoogleApiClient onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        LogUtil.d("GoogleApiClient onConnectionFailed: " + connectionResult);
    }
}
