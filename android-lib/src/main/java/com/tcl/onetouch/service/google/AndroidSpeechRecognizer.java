package com.tcl.onetouch.service.google;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognitionService;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.SpeechRecognizerService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.ArrayList;
import java.util.List;

public class AndroidSpeechRecognizer extends SpeechRecognizerService {
    private static final long DEFAULT_RECOGNIZE_TIMEOUT = 30000;
    private static final int CHECK_AUDIO_SOURCE_RETRY_TIMEOUT_INIT = 100;
    private static final int CHECK_AUDIO_SOURCE_RETRY_TIMEOUT_MAX = 1000;

    private SpeechRecognizer speechRecognizer;
    private boolean isRecognizing;
    private boolean isCanceled;
    private long recognizeTimeout;
    private long recognizeStartTime;
    private long speechEndPause;

    private class SpeechRecognitionListener implements RecognitionListener {
        OnResultListener listener;

        @Override
        public void onReadyForSpeech(Bundle params) {
            if (isCanceled) {
                return;
            }
            LogUtil.d("AndroidSpeechRecognizer: ready for speech");
            checkAudioSourceAction = null;
            if (recognizeStartTime == 0) {
                recognizeStartTime = System.currentTimeMillis();
                audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
            }
            if (cancelAction == null) {
                cancelAction = new CancelAction();
                cancelAction.listener = listener;
                SystemUtil.postToMainThread(cancelAction, recognizeTimeout);
            }
            if (onStateChangeListener != null) {
                onStateChangeListener.onReadyToSpeech(recognizeTimeout, speechEndPause);
            }
        }

        @Override
        public void onBeginningOfSpeech() {
            if (isCanceled) {
                return;
            }
            LogUtil.d("AndroidSpeechRecognizer: beginning of speech");
            if (cancelAction != null) {
                SystemUtil.cancelMainThreadAction(cancelAction);
                cancelAction = null;
            }
            if (onStateChangeListener != null) {
                onStateChangeListener.onBeginningOfSpeech();
            }
        }

        @Override
        public void onRmsChanged(float rmsdB) {
        }

        @Override
        public void onBufferReceived(byte[] buffer) {
        }

        @Override
        public void onEndOfSpeech() {
            if (isCanceled) {
                return;
            }
            LogUtil.d("AndroidSpeechRecognizer: end of speech");
            if (onStateChangeListener != null) {
                onStateChangeListener.onEndOfSpeech();
            }
        }

        @Override
        public void onError(int error) {
            if (isCanceled) {
                return;
            }
            LogUtil.d("AndroidSpeechRecognizer: error " + error);
            boolean isTimeout = (error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT);
            if (isTimeout &&
                System.currentTimeMillis() - recognizeStartTime < recognizeTimeout) {
                startRecognizeInternal(listener);
            } else {
                if (isTimeout) {
                    listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_TIMEOUT));
                } else {
                    endRecognizeInternal();
                    switch (error) {
                        case SpeechRecognizer.ERROR_NETWORK:
                            listener.onError(createErrorCode(ErrorCode.NETWORK_ERROR));
                            break;
                        case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                            listener.onError(createErrorCode((ErrorCode.NETWORK_ERROR_TIMEOUT)));
                            break;
                        case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                            listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_NO_SERVICE));
                            break;
                        case SpeechRecognizer.ERROR_AUDIO:
                            listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_AUDIO));
                            break;
                        case SpeechRecognizer.ERROR_CLIENT:
                            listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_CLIENT));
                            break;
                        case SpeechRecognizer.ERROR_SERVER:
                            listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_SERVER));
                            break;
                        case SpeechRecognizer.ERROR_NO_MATCH:
                            listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_NO_RESULT));
                            break;
                        default:
                            listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR));
                            break;
                    }
                }
            }
        }

        @Override
        public void onResults(Bundle results) {
            if (isCanceled) {
                return;
            }
            LogUtil.d("AndroidSpeechRecognizer: result");
            endRecognizeInternal();
            ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            if (data.size() > 0) {
                LogUtil.d(data.get(0));
                listener.onResult(data.get(0));
            } else {
                listener.onResult("");
            }
        }

        @Override
        public void onPartialResults(Bundle partialResults) {
            if (isCanceled) {
                return;
            }
            ArrayList<String> data = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            if (data.size() > 0) {
                listener.onPartialResult(data.get(0));
            }
        }

        @Override
        public void onEvent(int eventType, Bundle params) {

        }
    }

    ;
    private SpeechRecognitionListener speechRecognitionListener;

    @Override
    public void init(Context context, SpeechRecognizerService.OnStateChangeListener listener) {
        super.init(context, listener);
        ComponentName cn = null;
        final List<ResolveInfo> list = context.getPackageManager().queryIntentServices(
            new Intent(RecognitionService.SERVICE_INTERFACE), 0);
        for (ResolveInfo ri : list) {
            cn = new ComponentName(ri.serviceInfo.packageName, ri.serviceInfo.name);
            break;
        }
        if (cn == null) {
            // TODO: throw exception
            return;
        }

        speechRecognitionListener = new SpeechRecognitionListener();
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(OneTouchApplication.getInstance(), cn);
        speechRecognizer.setRecognitionListener(speechRecognitionListener);
        recognizeTimeout = DEFAULT_RECOGNIZE_TIMEOUT;
        recognizeStartTime = 0;
        isRecognizing = false;
        isCanceled = false;
    }

    private class CheckAudioSourceAction implements Runnable {
        OnResultListener listener;
        int retryTimeout;

        @Override
        public void run() {
            if (SystemUtil.isAudioSourceReady(MediaRecorder.AudioSource.VOICE_RECOGNITION)) {
                SystemUtil.postToMainThread(new Runnable() {
                    @Override
                    public void run() {
                        startRecognizeInternal(listener);
                    }
                });
            } else if (retryTimeout < CHECK_AUDIO_SOURCE_RETRY_TIMEOUT_MAX) {
                if (retryTimeout == 0) {
                    retryTimeout = CHECK_AUDIO_SOURCE_RETRY_TIMEOUT_INIT;
                } else {
                    retryTimeout *= 2;
                    if (retryTimeout > CHECK_AUDIO_SOURCE_RETRY_TIMEOUT_MAX) {
                        retryTimeout = CHECK_AUDIO_SOURCE_RETRY_TIMEOUT_MAX;
                    }
                }
                SystemUtil.postToBgThread(this, retryTimeout);
            } else {
                SystemUtil.postToMainThread(new Runnable() {
                    @Override
                    public void run() {
                        endRecognizeInternal();
                        listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_AUDIO));
                    }
                });
            }
        }
    }

    private CheckAudioSourceAction checkAudioSourceAction;

    private class CancelAction implements Runnable {
        OnResultListener listener;

        @Override
        public void run() {
            cancelAction = null;
            cancelRecognize();
            listener.onError(createErrorCode(ErrorCode.SPEECH_ERROR_TIMEOUT));
        }
    }

    private CancelAction cancelAction;

    @Override
    public void startRecognize(OnResultListener listener) {
        startRecognize(DEFAULT_RECOGNIZE_TIMEOUT, listener);
    }

    @Override
    public void startRecognize(long timeout, OnResultListener listener) {
        startRecognize(timeout, 0, listener);
    }

    @Override
    public void startRecognize(long timeout, long endPause, OnResultListener listener) {
        if (listener == null) {
            // TODO: throw error
            return;
        }
        LogUtil.d("AndroidSpeechRecognizer:startRecognize");
        cancelRecognize();
        isRecognizing = true;
        isCanceled = false;
        if (onStateChangeListener != null) {
            onStateChangeListener.onRecognitionStarted();
        }
        recognizeTimeout = timeout;
        speechEndPause = endPause;
        recognizeStartTime = 0;
        checkAudioSourceAction = new CheckAudioSourceAction();
        checkAudioSourceAction.listener = listener;
        checkAudioSourceAction.retryTimeout = 0;
        SystemUtil.postToBgThread(checkAudioSourceAction);
    }

    private void startRecognizeInternal(final OnResultListener listener) {
        if (!isRecognizing) {
            return;
        }
        LogUtil.d("AndroidSpeechRecognizer: started");
        requestAudioFocus();
        speechRecognitionListener.listener = listener;
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
        speechRecognizer.startListening(intent);
    }

    private void endRecognizeInternal() {
        if (!isRecognizing) {
            return;
        }
        isRecognizing = false;
        if (checkAudioSourceAction != null) {
            SystemUtil.cancelBgThreadAction(checkAudioSourceAction);
            checkAudioSourceAction = null;
        }
        if (cancelAction != null) {
            SystemUtil.cancelMainThreadAction(cancelAction);
            cancelAction = null;
        }
        if (recognizeStartTime > 0) {
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
            recognizeStartTime = -1;
        }
        abandonAudioFocus();
        if (onStateChangeListener != null) {
            onStateChangeListener.onRecognitionEnded();
        }
    }

    @Override
    public void stopRecognize() {
        endRecognizeInternal();
        speechRecognizer.stopListening();
    }

    @Override
    public void cancelRecognize() {
        isCanceled = true;
        endRecognizeInternal();
        speechRecognizer.cancel();
    }

    @Override
    public boolean isRecognizing() {
        return isRecognizing;
    }

    @Override
    public void destroy() {
        if (speechRecognizer != null) {
            speechRecognizer.destroy();
            speechRecognizer = null;
        }
        speechRecognitionListener = null;
        super.destroy();
    }
}
