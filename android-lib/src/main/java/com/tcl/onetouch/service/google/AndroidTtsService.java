package com.tcl.onetouch.service.google;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;

import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.TtsService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.HashMap;
import java.util.Locale;

public class AndroidTtsService extends TtsService {
    private static final String TAG = AndroidTtsService.class.getName();

    private static class UtteranceInfo {
        OnSpeechListener listener;
        boolean isStopped;
    }

    private TtsService.OnStateChangeListener onStateChangeListener;
    private TextToSpeech speechEngine;
    private boolean isReady;
    private int speechId;
    private HashMap<String, String> params;
    private HashMap<String, UtteranceInfo> utteranceInfos;
    private int lastSpeechPriority;
    private String lastSpeechSource;
    private String currentUtteranceId;

    @Override
    public void init(Context context, TtsService.OnStateChangeListener listener) {
        onStateChangeListener = listener;
        isReady = false;
        speechEngine = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = speechEngine.setLanguage(Locale.ENGLISH);
                    if (result == TextToSpeech.LANG_MISSING_DATA) {
                        if (onStateChangeListener != null) {
                            onStateChangeListener.onError(createErrorCode(ErrorCode.TTS_ERROR_NO_DATA));
                        }
                    } else if (result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        if (onStateChangeListener != null) {
                            onStateChangeListener.onError(createErrorCode(ErrorCode.TTS_ERROR_MISSING_LANG));
                        }
                    } else {
                        isReady = true;
                    }
                }
            }
        });
        speechEngine.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(final String utteranceId) {
                SystemUtil.runOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.d("AndroidTtsService.onStart: " + utteranceId);
                        currentUtteranceId = utteranceId;
                    }
                });
            }

            @Override
            public void onDone(final String utteranceId) {
                SystemUtil.runOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.d("AndroidTtsService.onDone: " + utteranceId);
                        onSpeechStopped();
                        UtteranceInfo utteranceInfo = utteranceInfos.get(utteranceId);
                        if (utteranceInfo != null) {
                            if (utteranceInfo.isStopped) {
                                utteranceInfo.listener.onStopped();
                            } else {
                                utteranceInfo.listener.onFinished();
                            }
                            utteranceInfos.remove(utteranceId);
                        }
                    }
                });
            }

            @Override
            public void onError(String utteranceId) {
                onError(utteranceId, TextToSpeech.ERROR);
            }

            @Override
            public void onError(final String utteranceId, final int errorCode) {
                SystemUtil.runOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.d("AndroidTtsService.onError: " + utteranceId + ", errorCode: " + errorCode);
                        onSpeechStopped();
                        UtteranceInfo utteranceInfo = utteranceInfos.get(utteranceId);
                        if (utteranceInfo != null) {
                            ErrorCode error;
                            switch (errorCode) {
                                case TextToSpeech.ERROR_NETWORK:
                                    error = createErrorCode(ErrorCode.NETWORK_ERROR);
                                    break;
                                case TextToSpeech.ERROR_NETWORK_TIMEOUT:
                                    error = createErrorCode(ErrorCode.NETWORK_ERROR_TIMEOUT);
                                    break;
                                case TextToSpeech.ERROR_SYNTHESIS:
                                    error = createErrorCode(ErrorCode.TTS_ERROR_SYNTHESIS);
                                    break;
                                case TextToSpeech.ERROR_SERVICE:
                                    error = createErrorCode(ErrorCode.TTS_ERROR_SERVICE);
                                    break;
                                case TextToSpeech.ERROR_OUTPUT:
                                    error = createErrorCode(ErrorCode.TTS_ERROR_OUTPUT);
                                    break;
                                case TextToSpeech.ERROR_INVALID_REQUEST:
                                    error = createErrorCode(ErrorCode.TTS_ERROR_INVALID_REQUEST);
                                    break;
                                case TextToSpeech.ERROR_NOT_INSTALLED_YET:
                                    error = createErrorCode(ErrorCode.TTS_ERROR_NO_DATA);
                                    break;
                                default:
                                    error = createErrorCode(ErrorCode.TTS_ERROR);
                                    break;
                            }
                            utteranceInfo.listener.onError(error);
                            utteranceInfos.remove(utteranceId);
                        }
                    }
                });

            }

        });
        params = new HashMap<>();
        params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(STREAM_TYPE));
        utteranceInfos = new HashMap<>();
        speechId = 0;
        lastSpeechPriority = 0;
        lastSpeechSource = "";
        currentUtteranceId = null;
    }

    @Override
    public void speak(String text, String source, int priority, OnSpeechListener listener) {
        if (!isReady || (speechEngine.isSpeaking() && priority < lastSpeechPriority)) {
            return;
        }
        lastSpeechSource = source;
        lastSpeechPriority = priority;
        String utteranceId = String.valueOf(++speechId);
        params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);
        if (listener != null) {
            UtteranceInfo utteranceInfo = new UtteranceInfo();
            utteranceInfo.listener = listener;
            utteranceInfo.isStopped = false;
            utteranceInfos.put(utteranceId, utteranceInfo);
        }
        if (currentUtteranceId != null) {
            UtteranceInfo utteranceInfo = utteranceInfos.get(currentUtteranceId);
            if (utteranceInfo != null) {
                utteranceInfo.isStopped = true;
            }
        }
        if (onStateChangeListener != null) {
            onStateChangeListener.onSpeechStarted(text, source, priority);
        }
        OneTouchApplication.getInstance().requestTtsAudioFocus(TAG);
        LogUtil.d("AndroidTtsService.speak " + utteranceId + ": " + text);
        speechEngine.speak(text, TextToSpeech.QUEUE_FLUSH, params);
    }

    @Override
    public void stop(String source) {
        if (!isReady) {
            return;
        }
        if (source == null || source.isEmpty() || source.equals(lastSpeechSource)) {
            final String utteranceId = currentUtteranceId;
            if (utteranceId != null) {
                UtteranceInfo utteranceInfo = utteranceInfos.get(utteranceId);
                if (utteranceInfo != null) {
                    utteranceInfo.isStopped = true;
                }
            }
            speechEngine.stop();
            onSpeechStopped();
            if (utteranceId != null) {
                SystemUtil.postToMainThread(new Runnable() {
                    @Override
                    public void run() {
                        UtteranceInfo utteranceInfo = utteranceInfos.get(utteranceId);
                        if (utteranceInfo != null) {
                            utteranceInfo.listener.onStopped();
                            utteranceInfos.remove(utteranceId);
                        }
                    }
                });
            }
        }
    }

    protected void onSpeechStopped() {
        currentUtteranceId = null;
        OneTouchApplication.getInstance().releaseTtsAudioFocus(TAG);
        if (onStateChangeListener != null) {
            onStateChangeListener.onSpeechStopped();
        }
    }

    @Override
    public void destroy() {
        speechEngine.stop();
        speechEngine.shutdown();
        speechEngine = null;
        params = null;
        utteranceInfos = null;
    }
}
