package com.tcl.onetouch.service.nuance;

import android.content.Context;

import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.sources.RecorderSource;
import com.nuance.dragon.toolkit.elvis.ElvisConfig;
import com.nuance.dragon.toolkit.elvis.ElvisError;
import com.nuance.dragon.toolkit.elvis.ElvisRecognizer;
import com.nuance.dragon.toolkit.elvis.ElvisResult;
import com.nuance.dragon.toolkit.elvis.WakeupParam;
import com.nuance.dragon.toolkit.file.FileManager;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.WakeupService;
import com.tcl.onetouch.util.LogUtil;
import com.tcl.onetouch.util.SystemUtil;

import java.util.ArrayList;

public class NuanceWakeupService extends WakeupService {
    private static final int RETRY_TIMEOUT_INIT = 100;
    private static final int RETRY_TIMEOUT_MAX = 2000;

    private RecorderSource<AudioChunk> recorder;
    private ElvisRecognizer elvisRecognizer;
    private WakeupParam wakeupParam;
    private boolean isActive;
    private boolean isListening;
    private int retryTimeout;
    private OnStateChangeListener onStateChangeListener;
    private OnWakeupListener onWakeupListener;
    private OnStoppedListener onStoppedListener;

    @Override
    public void init(Context context, WakeupService.OnStateChangeListener listener) {
        onStateChangeListener = listener;
        elvisRecognizer = ElvisRecognizer.createElvisRecognizer(new FileManager(context, ".jpg", "elvis"));
        elvisRecognizer.initialize(
            new ElvisConfig(
                ElvisRecognizer.Languages.UNITED_STATES_ENGLISH,
                ElvisRecognizer.Frequencies.FREQ_16KHZ),
            "elvis.dat");
        ArrayList<String> wuwPhrases = new ArrayList<String>();
        wuwPhrases.add(WAKEUP_PHRASE);
        WakeupParam.Builder paramBuilder = new WakeupParam.Builder(wuwPhrases);
        wakeupParam = paramBuilder.build();
        isActive = false;
        isListening = false;
    }

    @Override
    public void setOnWakeupListener(OnWakeupListener listener) {
        onWakeupListener = listener;
    }

    @Override
    public void startListening() {
        LogUtil.d("NuanceWakeupService: startListening");

        isActive = true;
        retryTimeout = 0;
        onStoppedListener = null;
        if (onStateChangeListener != null) {
            onStateChangeListener.onWakeupStarted();
        }
        startListeningInternal();
    }

    private void startListeningInternal() {
        if (!isActive) {
            return;
        }

        if (recorder != null) {
            recorder.stopRecording();
            recorder = null;
        }

        if (retryTimeout == 0) {
            retryTimeout = RETRY_TIMEOUT_INIT;
        } else if (retryTimeout < RETRY_TIMEOUT_MAX) {
            retryTimeout *= 2;
            if (retryTimeout >= RETRY_TIMEOUT_MAX) {
                retryTimeout = RETRY_TIMEOUT_MAX;
                if (onWakeupListener != null) {
                    onWakeupListener.onError(createErrorCode(ErrorCode.WAKEUP_ERROR_CANT_START));
                }
            }
        }

        recorder = new AudioRecordSource(AudioType.PCM_16k, SystemUtil.getBgHandler());
        recorder.startRecording(new RecorderSource.Listener<AudioChunk>() {
            @Override
            public void onStarted(RecorderSource<AudioChunk> recorderSource) {
                LogUtil.d("NuanceWakeupService: listening started");
                isListening = true;
                elvisRecognizer.startWakeupMode(recorderSource, wakeupParam, null,
                    new ElvisRecognizer.ResultListener() {
                        @Override
                        public void onResult(ElvisResult result) {
                            if (!isActive) {
                                return;
                            }
                            if (onWakeupListener != null) {
                                onWakeupListener.onWakeup();
                            }
                        }

                        @Override
                        public void onError(ElvisError error) {
                            if (!isActive) {
                                return;
                            }
                            if (onWakeupListener != null) {
                                switch (error.getReasonCode()) {
                                    case ElvisError.Reason.CANT_START:
                                        onWakeupListener.onError(createErrorCode(ErrorCode.WAKEUP_ERROR_CANT_START));
                                        break;
                                    case ElvisError.Reason.AUDIO_FRAMES_DROPPED:
                                        onWakeupListener.onError(createErrorCode(ErrorCode.WAKEUP_ERROR_AUDIO));
                                        break;
                                    case ElvisError.Reason.NO_RESULT:
                                        onWakeupListener.onError(createErrorCode(ErrorCode.WAKEUP_ERROR_NO_RESULT));
                                        break;
                                    default:
                                        onWakeupListener.onError(createErrorCode(ErrorCode.WAKEUP_ERROR));
                                        break;
                                }
                            }
                        }
                    });
            }

            @Override
            public void onStopped(RecorderSource<AudioChunk> recorderSource) {
                LogUtil.d("NuanceWakeupService: listening stopped");
                isListening = false;
                if (onStoppedListener != null) {
                    onStoppedListener.onStopped();
                    onStoppedListener = null;
                }
            }

            @Override
            public void onError(RecorderSource<AudioChunk> recorderSource) {
                if (!isActive) {
                    return;
                }
                SystemUtil.postToMainThread(new Runnable() {
                    @Override
                    public void run() {
                        startListeningInternal();
                    }
                }, retryTimeout);
            }
        });
    }

    @Override
    public void stopListening(OnStoppedListener listener) {
        LogUtil.d("NuanceWakeupService.stopListening");
        if (isListening) {
            onStoppedListener = listener;
        } else if (listener != null) {
            listener.onStopped();
        }
        if (isActive) {
            isActive = false;
            if (onStateChangeListener != null) {
                onStateChangeListener.onWakeupStopped();
            }
        }
        if (recorder != null) {
            recorder.stopRecording();
            recorder = null;
        }
    }

    @Override
    public boolean isActive() {
        return isActive;
    }

    @Override
    public void destroy() {
        LogUtil.d("NuanceWakeupService.destroy");

        if (recorder != null) {
            recorder.stopRecording();
            recorder = null;
        }

        if (elvisRecognizer != null) {
            elvisRecognizer.release();
            elvisRecognizer = null;
        }
        wakeupParam = null;
        isActive = false;
        isListening = false;
        onStoppedListener = null;
    }
}