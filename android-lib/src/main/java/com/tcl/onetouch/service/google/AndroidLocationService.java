package com.tcl.onetouch.service.google;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.tcl.onetouch.base.OneTouchApplication;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.util.SystemUtil;

public class AndroidLocationService extends LocationService implements LocationListener {
    private LocationManager locationManager;
    private Location lastLocation;

    @Override
    public void init(Context context, Listener listener) {
        super.init(context, listener);
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        resume();
    }

    @Override
    protected Location getRealLocation() {
        return lastLocation;
    }

    @Override
    public Location getLastVehicleLocation() {
        return null;
    }

    @Override
    public void pause() {
        if (ActivityCompat.checkSelfPermission(OneTouchApplication.getInstance(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(OneTouchApplication.getInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.removeUpdates(this);
    }

    @Override
    public void resume() {
        if (ActivityCompat.checkSelfPermission(OneTouchApplication.getInstance(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(OneTouchApplication.getInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (SystemUtil.hasGpsModule(OneTouchApplication.getInstance())) {
            locationManager.requestLocationUpdates("gps", 0L, 0.0F, this);
        }
        if (SystemUtil.hasNetworkModule(OneTouchApplication.getInstance())) {
            locationManager.requestLocationUpdates("network", 0L, 0.0F, this);
        }
        locationManager.requestLocationUpdates("passive", 0L, 0.0F, this);
    }

    @Override
    public void destroy() {
        listener = null;
        locationManager = null;
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        if (listener != null) {
            listener.onLocationChanged(location);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
