package com.tcl.onetouch.service.nuance;

import android.annotation.TargetApi;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.audiofx.AcousticEchoCanceler;
import android.media.audiofx.NoiseSuppressor;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;

import com.nuance.dragon.toolkit.audio.AbstractAudioChunk;
import com.nuance.dragon.toolkit.audio.AudioChunk;
import com.nuance.dragon.toolkit.audio.AudioType;
import com.nuance.dragon.toolkit.audio.sources.RecorderSource;

public class AudioRecordSource<AudioChunkType extends AbstractAudioChunk> extends RecorderSource<AudioChunkType> {
    private AudioType audioType;
    private int audioSource;
    private AudioRecord audioRecord;
    private AcousticEchoCanceler acousticEchoCanceler;
    private NoiseSuppressor noiseSuppressor;
    private boolean isRecording;
    private int chunckLength;
    private int totalReadLength;
    private long startTimestamp;
    private final int bufferLengthInMs;
    private final int chunkLengthInMs;
    private Handler handler;
    private Runnable runnable;
    protected int channelConfig;
    protected int channelCount;

    protected AudioRecordSource(AudioType audioType) {
        this(audioType, 400, null);
    }

    protected AudioRecordSource(int sourceType, AudioType audioType) {
        this(sourceType, audioType, 400, null);
    }

    protected AudioRecordSource(AudioType audioType, Handler workerThreadHandler) {
        this(audioType, 400, workerThreadHandler);
    }

    protected AudioRecordSource(AudioType audioType, int bufferLengthInMs, Handler workerThreadHandler) {
        this(MediaRecorder.AudioSource.VOICE_RECOGNITION, audioType, bufferLengthInMs, workerThreadHandler);
    }

    protected AudioRecordSource(int sourceType, AudioType audioType, int bufferLengthInMs, Handler workerThreadHandler) {
        this(sourceType, audioType, bufferLengthInMs, 20, workerThreadHandler);
    }

    protected AudioRecordSource(int sourceType, AudioType audioType, int bufferLengthInMs, int chunkLengthInMs, Handler workerThreadHandler) {
        super(audioType, workerThreadHandler);
        channelConfig = AudioFormat.CHANNEL_IN_MONO;
        channelCount = 1;
        audioSource = sourceType;
        this.bufferLengthInMs = bufferLengthInMs;
        this.chunkLengthInMs = chunkLengthInMs;
        if (workerThreadHandler != null) {
            handler = workerThreadHandler;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    protected boolean startRecordingInternal(AudioType audioType) {
        this.audioType = audioType;
        this.totalReadLength = 0;
        int sampleRateInHz;
        if (audioType.frequency == AudioType.Frequency.FREQ_8KHZ) {
            sampleRateInHz = AudioType.Frequency.FREQ_8KHZ;
        } else if (audioType.frequency == AudioType.Frequency.FREQ_11KHZ) {
            sampleRateInHz = AudioType.Frequency.FREQ_11KHZ;
        } else if (audioType.frequency == AudioType.Frequency.FREQ_22KHZ) {
            sampleRateInHz = AudioType.Frequency.FREQ_22KHZ;
        } else if (audioType.frequency == AudioType.Frequency.FREQ_44KHZ) {
            sampleRateInHz = AudioType.Frequency.FREQ_44KHZ;
        } else if (audioType.frequency == AudioType.Frequency.FREQ_48KHZ) {
            sampleRateInHz = AudioType.Frequency.FREQ_48KHZ;
        } else {
            sampleRateInHz = AudioType.Frequency.FREQ_16KHZ;
        }

        int minBufferSize = AudioRecord.getMinBufferSize(sampleRateInHz, channelConfig, AudioFormat.ENCODING_PCM_16BIT);
        int bufferSize = 2 * sampleRateInHz * bufferLengthInMs * channelCount / 1000;
        chunckLength = sampleRateInHz * chunkLengthInMs * channelCount / 1000;
        audioRecord = new AudioRecord(audioSource, sampleRateInHz, channelConfig, AudioFormat.ENCODING_PCM_16BIT, minBufferSize > bufferSize ? minBufferSize : bufferSize);
        if (AcousticEchoCanceler.isAvailable()) {
            acousticEchoCanceler = AcousticEchoCanceler.create(audioRecord.getAudioSessionId());
            if (acousticEchoCanceler != null && !acousticEchoCanceler.getEnabled()) {
                acousticEchoCanceler.setEnabled(true);
            }
        }
        if (NoiseSuppressor.isAvailable()) {
            noiseSuppressor = NoiseSuppressor.create(audioRecord.getAudioSessionId());
            if (noiseSuppressor != null && !noiseSuppressor.getEnabled()) {
                noiseSuppressor.setEnabled(true);
            }
        }
        if (audioRecord.getState() != AudioRecord.STATE_INITIALIZED) {
            release();
            return false;
        } else {
            audioRecord.startRecording();
            if (audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
                release();
                return false;
            }
            isRecording = true;
            if (handler == null) {
                handler = new Handler();
            }
            runnable = new Runnable() {
                public final void run() {
                    if (isRecording) {
                        audioRecord.setNotificationMarkerPosition(chunckLength / 2);
                        AudioChunkType newAudio = getNewAudio();
                        if (newAudio != null) {
                            handleNewAudio(newAudio);
                        }
                        handler.postDelayed(this, (long) chunkLengthInMs / 2);
                    }
                }
            };
            AudioRecord.OnRecordPositionUpdateListener updateListener = new AudioRecord.OnRecordPositionUpdateListener() {
                private boolean isStarted = false;

                public final void onMarkerReached(AudioRecord ar) {
                    if (isRecording) {
                        handler.removeCallbacks(runnable);
                        ar.setNotificationMarkerPosition(chunckLength / 2);
                        AudioChunkType newAudio = getNewAudio();
                        if (newAudio != null) {
                            if (!isStarted) {
                                isStarted = true;
                                handleStarted();
                            }
                            handleNewAudio(newAudio);
                        }
                        handler.postDelayed(runnable, (long) chunkLengthInMs / 2);
                    }
                }

                public final void onPeriodicNotification(AudioRecord ar) {
                }
            };
            audioRecord.setRecordPositionUpdateListener(updateListener);
            updateListener.onMarkerReached(audioRecord);
            return true;
        }
    }

    protected void stopRecordingInternal() {
        if (isRecording) {
            handler.removeCallbacks(runnable);
            AudioChunkType newAudio = getNewAudio();
            if (newAudio != null) {
                handleNewAudio(newAudio);
            }

            release();
            handleSourceClosed();
        }
    }

    private void release() {
        isRecording = false;
        if (acousticEchoCanceler != null) {
            acousticEchoCanceler.release();
            acousticEchoCanceler = null;
        }
        if (noiseSuppressor != null) {
            noiseSuppressor.release();
            noiseSuppressor = null;
        }
        if (audioRecord != null) {
            if (audioRecord.getState() == AudioRecord.STATE_INITIALIZED) {
                audioRecord.stop();
            }
            audioRecord.release();
            audioRecord = null;
        }
    }

    private AudioChunkType getNewAudio() {
        if (!isRecording) {
            return null;
        } else {
            int readSize = 0;
            int requestSize = chunckLength;
            short[] audioData = new short[chunckLength];

            int size;
            while ((size = audioRecord.read(audioData, readSize, requestSize)) >= 0) {
                readSize += size;
                if ((requestSize -= size) <= 0) {
                    break;
                }
            }

            if (readSize <= 0) {
                release();
                handleSourceClosed(false);
                return null;
            } else {
                if (totalReadLength == 0) {
                    startTimestamp = SystemClock.uptimeMillis() - (long) audioType.getDuration(readSize);
                }

                long timestamp = startTimestamp + (long) audioType.getDuration(totalReadLength);
                totalReadLength += readSize;
                return createNewAudioChunk(audioType, audioData, timestamp);
            }
        }
    }

    protected boolean isCodecSupported(AudioType type) {
        return type.encoding == AudioType.Encoding.PCM_16;
    }

    protected AudioChunkType createNewAudioChunk(AudioType type, short[] data, long timestamp) {
        return (AudioChunkType) (new AudioChunk(type, data, timestamp));
    }
}
