package com.tcl.onetouch.base;

import android.Manifest;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tcl.onetouch.base.OneTouchApplication;
//import com.tcl.onetouch.onroad.BuildConfig;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.util.LogUtil;

import java.util.List;

public class ActivityDetectionService extends IntentService {
    public final static String LAST_IN_VEHICLE_TIME_KEY = "LAST_IN_VEHICLE_TIME_LOCATION";
    public final static String LAST_OUT_VEHICLE_LOCATION_KEY = "LAST_OUT_VEHICLE_LOCATION";
    public final static String LAST_IN_VEHICLE_ACTIVITY_KEY = "LAST_IN_VEHICLE_ACTIVITY";
    public final static String IN_VEHICLE_KEY = "IN_VEHICLE";

    private final static String RECORD_TIME_KEY_SUFFIX = "_TIME";
    private final static long ACTIVITY_UPDATE_INTERVAL = 0;
    private final static long SAVE_VEHICLE_LOCATION_INTERVAL = 300000L; // 5 minutes

    //    private final static String PROCESS_ACTIVITY_ACTION = BuildConfig.APPLICATION_ID + ".PROCESS_ACTIVITY";
//    private final static String REGISTER_ACTIVITY_LISTENER_ACTION = BuildConfig.APPLICATION_ID + ".REGISTER_ACTIVITY_LISTENER";
    private final static String PROCESS_ACTIVITY_ACTION = ".PROCESS_ACTIVITY";
    private final static String REGISTER_ACTIVITY_LISTENER_ACTION = ".REGISTER_ACTIVITY_LISTENER";

    public static void registerActivityListener(Context context) {
//        Intent serviceIntent = new Intent(context, ActivityDetectionService.class);
//        serviceIntent.setAction(REGISTER_ACTIVITY_LISTENER_ACTION);
//        context.startService(serviceIntent);
    }

    public ActivityDetectionService() {
        super("ActivityDetectionService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (REGISTER_ACTIVITY_LISTENER_ACTION.equals(intent.getAction())) {
            GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(ActivityRecognition.API)
                .build();
            ConnectionResult result = googleApiClient.blockingConnect();
            if (result.isSuccess()) {
                Intent serviceIntent = new Intent(this, ActivityDetectionService.class);
                serviceIntent.setAction(PROCESS_ACTIVITY_ACTION);
                PendingIntent callbackIntent = PendingIntent.getService(OneTouchApplication.getInstance(), 0, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(googleApiClient, ACTIVITY_UPDATE_INTERVAL, callbackIntent);
                LogUtil.f("registered activity listener");
            } else {
                LogUtil.f("GoogleApiClient(ActivityRecognition) connection failed: " + result.toString());
            }
        } else {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            List<DetectedActivity> detectedActivities = result.getProbableActivities();
            boolean inVehicle = OneTouchApplication.getInstance().getBooleanPreference(IN_VEHICLE_KEY, false);
            for (DetectedActivity detectedActivity : detectedActivities) {
                int type = detectedActivity.getType();
                int confidence = detectedActivity.getConfidence();
                if (confidence >= 70) {
                    if (type == DetectedActivity.IN_VEHICLE) {
                        OneTouchApplication.getInstance().savePreference(IN_VEHICLE_KEY, true);
                        if (!inVehicle) {
                            saveLastLocation(LocationService.VEHICLE_LOCATION_KEY);
                        } else {
                            int lastInVehicleActivity = OneTouchApplication.getInstance().getIntPreference(LAST_IN_VEHICLE_ACTIVITY_KEY, -1);
                            long lastRecordTime = OneTouchApplication.getInstance().getLongPreference(LocationService.VEHICLE_LOCATION_KEY + RECORD_TIME_KEY_SUFFIX, 0);
                            if (lastInVehicleActivity != type ||
                                System.currentTimeMillis() - lastRecordTime > SAVE_VEHICLE_LOCATION_INTERVAL) {
                                saveLastLocation(LocationService.VEHICLE_LOCATION_KEY);
                            }
                        }
                        OneTouchApplication.getInstance().savePreference(LAST_IN_VEHICLE_ACTIVITY_KEY, type);
                        OneTouchApplication.getInstance().savePreference(LAST_IN_VEHICLE_TIME_KEY, System.currentTimeMillis());
                        break;
                    } else if (inVehicle) {
                        if (type == DetectedActivity.TILTING) {
                            saveLastLocation(LocationService.VEHICLE_LOCATION_KEY);
                            OneTouchApplication.getInstance().savePreference(LAST_IN_VEHICLE_ACTIVITY_KEY, type);
                            OneTouchApplication.getInstance().savePreference(LAST_IN_VEHICLE_TIME_KEY, System.currentTimeMillis());
                            break;
                        } else if (type == DetectedActivity.STILL) {
                            int lastInVehicleActivity = OneTouchApplication.getInstance().getIntPreference(LAST_IN_VEHICLE_ACTIVITY_KEY, -1);
                            if (lastInVehicleActivity != type) {
                                saveLastLocation(LocationService.VEHICLE_LOCATION_KEY);
                            }
                            OneTouchApplication.getInstance().savePreference(LAST_IN_VEHICLE_ACTIVITY_KEY, type);
                            OneTouchApplication.getInstance().savePreference(LAST_IN_VEHICLE_TIME_KEY, System.currentTimeMillis());
                            break;
                        } else {
                            saveLastLocation(LAST_OUT_VEHICLE_LOCATION_KEY);
                            OneTouchApplication.getInstance().savePreference(IN_VEHICLE_KEY, false);
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
            StringBuilder sb = new StringBuilder();
            for (DetectedActivity detectedActivity : detectedActivities) {
                if (sb.length() > 0) {
                    sb.append(";");
                }
                sb.append(detectedActivity.toString());
            }
            if (sb.length() > 0) {
                LogUtil.f(sb.toString());
            }
        }
    }

    private void saveLastLocation(String key) {
        Location location = getLastLocation();
        if (location != null) {
            LogUtil.f("Saving last location for " + key + ": " + location);
            Gson gson = new GsonBuilder().create();
            OneTouchApplication.getInstance().savePreference(key, gson.toJson(location));
            OneTouchApplication.getInstance().savePreference(key + RECORD_TIME_KEY_SUFFIX, System.currentTimeMillis());
        }
    }

    private Location getLastLocation() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .build();
        ConnectionResult result = googleApiClient.blockingConnect();
        if (result.isSuccess()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            return LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        } else {
            LogUtil.f("GoogleApiClient(LocationServices) connection failed: " + result.toString());
            return null;
        }
    }
}
