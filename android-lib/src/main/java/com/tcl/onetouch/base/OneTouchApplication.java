package com.tcl.onetouch.base;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.tcl.onetouch.BuildConfig;
import com.tcl.onetouch.R;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.SpeechRecognizerService;
import com.tcl.onetouch.service.TtsService;
import com.tcl.onetouch.service.WakeupService;
import com.tcl.onetouch.util.LogUtil;

import java.util.HashSet;
import java.util.Set;

public class OneTouchApplication extends Application {
    public static final String KEYBOARD_TAG = "KEYBOARD";

    private static final String PREFS_NAME = "GlobalPref";

    protected static OneTouchApplication sInstance;

    private SharedPreferences prefs;
    private Bundle metaBundle;

    private AudioManager.OnAudioFocusChangeListener afChangeListener;
    private AudioManager audioManager;
    private Set<String> ttsAudioFocusRequests;
    private Set<String> disableWakeupServiceRequests;

    private SoundPool soundPool;
    private int speechHintSound;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        LogUtil.setPackageName(getPackageName());

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            public void onAudioFocusChange(int focusChange) {
                switch (focusChange) {
                    case AudioManager.AUDIOFOCUS_GAIN:
//                        LogUtil.d("OnRoadApplication audiofocus change: AUDIOFOCUS_GAIN");
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
//                        LogUtil.d("OnRoadApplication audiofocus change: AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
//                        LogUtil.d("OnRoadApplication audiofocus change: AUDIOFOCUS_LOSS_TRANSIENT");
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS:
//                        LogUtil.d("OnRoadApplication audiofocus change: AUDIOFOCUS_LOSS");
                        break;
                    default:
//                        LogUtil.d("OnRoadApplication audiofocus change: unknown " + focusChange);
                        break;
                }
            }
        };
        ttsAudioFocusRequests = new HashSet<>();
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        speechHintSound = soundPool.load(this, R.raw.speech_hint, 1);

        disableWakeupServiceRequests = new HashSet<>();

        Thread.currentThread().setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                LogUtil.f("Uncaught Exception @Thread:" + thread, ex);
            }
        });

        try {
            ApplicationInfo ai = getPackageManager().getApplicationInfo(
                getPackageName(),
                PackageManager.GET_META_DATA);
            metaBundle = ai.metaData;
        } catch (Exception ignore) {
        }
    }

    public static OneTouchApplication getInstance() {
        return sInstance;
    }

    public void tts(String text, String source, int priority, TtsService.OnSpeechListener listener) {
        if (SpeechRecognizerService.getDefaultService().isRecognizing()) {
            return;
        }
        TtsService.getDefaultService().speak(text, source, priority, listener);
    }

    public void tts(String text, String source, int priority) {
        tts(text, source, priority, null);
    }

    public void tts(String text, String source) {
        tts(text, source, TtsService.SPEECH_PRIORITY_NORMAL, null);
    }

    public void tts(@StringRes int resourceId, String source, int priority, TtsService.OnSpeechListener listener) {
        tts(getString(resourceId), source, priority, listener);
    }

    public void tts(@StringRes int resourceId, String source, int priority) {
        tts(getString(resourceId), source, priority, null);
    }

    public void tts(@StringRes int resourceId, String source) {
        tts(getString(resourceId), source, TtsService.SPEECH_PRIORITY_NORMAL, null);
    }

    public void tts(@StringRes int resourceId) {
        tts(getString(resourceId), "", TtsService.SPEECH_PRIORITY_NORMAL, null);
    }

    public void savePreference(String key, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBooleanPreference(int resId, boolean defValue) {
        try {
            return getBooleanPreference(getResources().getString(resId), defValue);
        } catch (Resources.NotFoundException e) {
            return defValue;
        }
    }

    public boolean getBooleanPreference(String key, boolean defValue) {
        return prefs.getBoolean(key, defValue);
    }

    public void savePreference(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int getIntPreference(int resId, int defValue) {
        try {
            return getIntPreference(getResources().getString(resId), defValue);
        } catch (Resources.NotFoundException e) {
            return defValue;
        }
    }

    public int getIntPreference(String key, int defValue) {
        return prefs.getInt(key, defValue);
    }

    public void savePreference(String key, long value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public Long getLongPreference(int resId, long defValue) {
        try {
            return getLongPreference(getResources().getString(resId), defValue);
        } catch (Resources.NotFoundException e) {
            return defValue;
        }
    }

    public Long getLongPreference(String key, long defValue) {
        return prefs.getLong(key, defValue);
    }

    public void savePreference(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getStringPreference(int resId, String defValue) {
        try {
            return getStringPreference(getResources().getString(resId), defValue);
        } catch (Resources.NotFoundException e) {
            return defValue;
        }
    }

    public String getStringPreference(String key, String defValue) {
        return prefs.getString(key, defValue);
    }

    public void handleError(ErrorCode error) {
        LogUtil.d("OneTouchApplication.handleError: " + error);
        if (BuildConfig.DEBUG) {
            StringBuilder sb = new StringBuilder("Error: ");
            sb.append(this.getString(error.getDisplayResId()));
            if (error.getMessage() != null && !error.getMessage().isEmpty()) {
                sb.append("\n").append(error.getMessage());
            }
            if (error.getService() != null) {
                sb.append("\n").append(error.getService().getClass().getSimpleName());
            }
            Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void requestTtsAudioFocus(String source) {
//        LogUtil.d("OneTouchApplication.requestTtsAudioFocus(): " + source);
        if (ttsAudioFocusRequests.isEmpty()) {
            int result = audioManager.requestAudioFocus(afChangeListener, TtsService.STREAM_TYPE, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
//                LogUtil.d("OneTouchApplication.requestTtsAudioFocus() request granted");
                ttsAudioFocusRequests.add(source);
            }
        } else {
            ttsAudioFocusRequests.add(source);
        }
    }

    public void releaseTtsAudioFocus(String source) {
//        LogUtil.d("OneTouchApplication.releaseTtsAudioFocus(): " + source);
        if (ttsAudioFocusRequests.remove(source) && ttsAudioFocusRequests.isEmpty()) {
            audioManager.abandonAudioFocus(afChangeListener);
//            LogUtil.d("OneTouchApplication.requestTtsAudioFocus() request abandoned");
        }
    }

    public void enableWakeupService(String source) {
//        LogUtil.d("OneTouchApplication.enableWakeupService(): " + source);
        if (disableWakeupServiceRequests.remove(source) && disableWakeupServiceRequests.isEmpty()) {
            WakeupService.getDefaultService().startListening();
        }
    }

    public void disableWakeupService(String source, WakeupService.OnStoppedListener listener) {
        LogUtil.d("OneTouchApplication.disableWakeupService(): " + source);
        disableWakeupServiceRequests.add(source);
        WakeupService.getDefaultService().stopListening(listener);
    }

    public void playSpeechHintSound() {
        soundPool.play(speechHintSound, 1, 1, 1, 0, 1);
    }

    public String getMetaStringValue(String key) {
        if (metaBundle != null) {
            return metaBundle.getString(key);
        } else {
            return null;
        }
    }
}
