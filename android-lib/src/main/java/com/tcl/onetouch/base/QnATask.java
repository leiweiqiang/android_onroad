package com.tcl.onetouch.base;

import android.support.annotation.StringRes;
import android.widget.Toast;

import com.tcl.onetouch.R;
import com.tcl.onetouch.model.ErrorCode;
import com.tcl.onetouch.service.LocationService;
import com.tcl.onetouch.service.NluService;
import com.tcl.onetouch.service.SpeechRecognizerService;
import com.tcl.onetouch.service.TtsService;
import com.tcl.onetouch.service.WakeupService;
import com.tcl.onetouch.util.SystemUtil;

import java.util.List;
import java.util.Map;

public class QnATask {
    public static final String TAG = QnATask.class.getName();
    private static final int DEFAULT_RECOGNITION_TIMEOUT = 30000;

    public static interface AnswerHandler {
        public void onAnswer(String answer);
    }

    public static interface ResultHandler {
        public void onResult(Map<String, Object> result);
    }

    public static interface ErrorHandler {
        public boolean onError(ErrorCode error);
    }

    private static final int PARSE_API_NONE = 0;
    private static final int PARSE_API_INTENT = 1;
    private static final int PARSE_API_YES_NO = 2;
    private static final int PARSE_API_TIME = 3;
    private static final int PARSE_API_NUMBER = 4;
    private static final int PARSE_API_SELECT = 5;

    private String source;
    private String question;
    private String hint;
    private int recognitionTimeout;
    private int parseApi;

    private AnswerHandler answerHandler;
    private ResultHandler resultHandler;
    private ErrorHandler errorHandler;

    private String yesNoMode;
    private List<String> selectItems;

    private boolean isActive;
    private boolean isSpeaking;
    private boolean isListening;
    private boolean isParsing;

    private static QnATask currentTask;

    public static QnATask getCurrentTask() {
        return currentTask;
    }

    public QnATask() {
        source = TAG;
        question = null;
        hint = null;
        recognitionTimeout = DEFAULT_RECOGNITION_TIMEOUT;
        isSpeaking = false;
        isListening = false;
        isParsing = false;
        isActive = false;
        parseApi = PARSE_API_NONE;
    }

    public boolean isSpeaking() {
        return isSpeaking;
    }

    public boolean isListening() {
        return isListening;
    }

    public boolean isParsing() {
        return isParsing;
    }

    public boolean isActive() {
        return isActive;
    }

    public QnATask setSource(String source) {
        this.source = source;
        return this;
    }

    public QnATask setQuestion(String question) {
        this.question = question;
        return this;
    }

    public QnATask setQuestion(@StringRes int resId) {
        return setQuestion(OneTouchApplication.getInstance().getString(resId));
    }

    public QnATask setHint(String hint) {
        this.hint = hint;
        return this;
    }

    public QnATask setRecognitionTimeout(int recognitionTimeout) {
        this.recognitionTimeout = recognitionTimeout;
        return this;
    }

    public int getRecognitionTimeout() {
        return recognitionTimeout;
    }

    public QnATask setHint(@StringRes int resId) {
        return setHint(OneTouchApplication.getInstance().getString(resId));
    }

    public QnATask setAnswerHandler(AnswerHandler answerHandler) {
        this.answerHandler = answerHandler;
        return this;
    }

    protected void setResultHandler(ResultHandler resultHandler) {
        this.resultHandler = resultHandler;
        yesNoMode = null;
        selectItems = null;
        parseApi = PARSE_API_NONE;
    }

    public QnATask setIntentResultHandler(ResultHandler resultHandler) {
        setResultHandler(resultHandler);
        parseApi = PARSE_API_INTENT;
        return this;
    }

    public QnATask setYesNoResultHandler(String mode, ResultHandler resultHandler) {
        setResultHandler(resultHandler);
        yesNoMode = mode;
        parseApi = PARSE_API_YES_NO;
        return this;
    }

    public QnATask setTimeResultHandler(ResultHandler resultHandler) {
        setResultHandler(resultHandler);
        parseApi = PARSE_API_TIME;
        return this;
    }

    public QnATask setNumberResultHandler(ResultHandler resultHandler) {
        setResultHandler(resultHandler);
        parseApi = PARSE_API_TIME;
        return this;
    }

    public QnATask setSelectionItems(List<String> items) {
        selectItems = items;
        return this;
    }

    public QnATask setSelectionResultHandler(ResultHandler resultHandler) {
        setResultHandler(resultHandler);
        parseApi = PARSE_API_SELECT;
        return this;
    }

    public QnATask setErrorHandler(ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
        return this;
    }

    public QnATask start() {
        end();
        isActive = true;
        isSpeaking = false;
        isListening = false;
        isParsing = false;
        OneTouchApplication.getInstance().requestTtsAudioFocus(TAG);
        start(question);
        currentTask = this;
        return this;
    }

    protected void start(String q) {
        OneTouchApplication.getInstance().disableWakeupService(TAG, null);
        isSpeaking = true;
        TtsService.getDefaultService().speak(q, source, TtsService.SPEECH_PRIORITY_HIGH, new TtsService.OnSpeechListener() {
            @Override
            public void onFinished() {
                if (!isActive) {
                    return;
                }
                isSpeaking = false;
                OneTouchApplication.getInstance().disableWakeupService(TAG, new WakeupService.OnStoppedListener() {
                    @Override
                    public void onStopped() {
                        SpeechRecognizerService.getDefaultService().startRecognize(recognitionTimeout, new SpeechRecognizerService.OnResultListener() {
                            @Override
                            public void onResult(String result) {
                                if (!isActive) {
                                    return;
                                }
                                if (SystemUtil.isDev()) {
                                    Toast.makeText(OneTouchApplication.getInstance(), result, Toast.LENGTH_SHORT).show();
                                }
                                OneTouchApplication.getInstance().enableWakeupService(TAG);
                                isListening = false;
                                if (answerHandler != null) {
                                    answerHandler.onAnswer(result);
                                }
                                if (parseApi != PARSE_API_NONE) {
                                    NluService.ResultListener resultListener = new NluService.ResultListener() {
                                        @Override
                                        public void onResult(Map<String, Object> result) {
                                            if (!isActive) {
                                                return;
                                            }
                                            isParsing = false;
                                            if (resultHandler != null) {
                                                resultHandler.onResult(result);
                                            }
                                            end();
                                        }

                                        @Override
                                        public void onError(ErrorCode error) {
                                            if (!isActive) {
                                                return;
                                            }
                                            isParsing = false;
                                            if (errorHandler == null || !errorHandler.onError(error)) {
                                                if (error.getCode() == ErrorCode.NLU_ERROR_NO_RESULT ||
                                                    error.getCode() == ErrorCode.NLU_ERROR_UNSUPPORTED_DOMAIN ||
                                                    error.getCode() == ErrorCode.NLU_ERROR_UNSUPPORTED_INTENT) {
                                                    retry();
                                                } else {
                                                    OneTouchApplication.getInstance().handleError(error);
                                                    end();
                                                }
                                            }
                                        }
                                    };
                                    switch (parseApi) {
                                        case PARSE_API_INTENT:
                                            NluService.getDefaultService().parseIntent(
                                                result,
                                                LocationService.getDefaultService().getLastLocation(),
                                                resultListener);
                                            break;
                                        case PARSE_API_YES_NO:
                                            NluService.getDefaultService().parseYesNo(
                                                result,
                                                yesNoMode,
                                                resultListener);
                                            break;
                                        case PARSE_API_TIME:
                                            NluService.getDefaultService().parseTime(
                                                result,
                                                resultListener);
                                            break;
                                        case PARSE_API_NUMBER:
                                            NluService.getDefaultService().parseNumber(
                                                result,
                                                resultListener);
                                            break;
                                        case PARSE_API_SELECT:
                                            NluService.getDefaultService().parseSelection(
                                                result,
                                                selectItems,
                                                resultListener);
                                            break;
                                    }
                                    isParsing = true;
                                } else {
                                    end();
                                }
                            }

                            @Override
                            public void onPartialResult(String partialResult) {
                            }

                            @Override
                            public void onError(ErrorCode error) {
                                if (!isActive) {
                                    return;
                                }
                                OneTouchApplication.getInstance().enableWakeupService(TAG);
                                isListening = false;
                                end();
                                if (errorHandler == null || !errorHandler.onError(error)) {
                                    if (error.getCode() == ErrorCode.SPEECH_ERROR_NO_RESULT) {
                                        retry();
                                    } else {
                                        OneTouchApplication.getInstance().handleError(error);
                                        end();
                                    }
                                }
                            }
                        });
                    }
                });
                isListening = true;
            }

            @Override
            public void onStopped() {
                if (!isActive) {
                    return;
                }
                isSpeaking = false;
                end();
            }

            @Override
            public void onError(ErrorCode error) {
                if (!isActive) {
                    return;
                }
                isSpeaking = false;
                if (errorHandler == null || !errorHandler.onError(error)) {
                    OneTouchApplication.getInstance().handleError(error);
                }
                end();
            }
        });
        isSpeaking = true;
    }

    protected void retry() {
        String retryQuestion;
        if (hint != null) {
            retryQuestion = hint;
        } else {
            retryQuestion = OneTouchApplication.getInstance().getString(R.string.tts_unsupport_intent) + " ";
        }
        retryQuestion += question;
        start(retryQuestion);
    }

    public void end() {
        if (!isActive) {
            return;
        }
        if (isSpeaking) {
            TtsService.getDefaultService().stop(source);
        } else if (isListening) {
            SpeechRecognizerService.getDefaultService().cancelRecognize();
            OneTouchApplication.getInstance().enableWakeupService(TAG);
        }
        isSpeaking = false;
        isListening = false;
        isParsing = false;
        OneTouchApplication.getInstance().releaseTtsAudioFocus(TAG);
        currentTask = null;
        isActive = false;
    }
}
