package com.tcl.onetouch.model;

public class MapCoordinate {
    private double latitude;
    private double longitude;
    private double bearing;

    public MapCoordinate() {
        this(0.0d, 0.0d, 0.0d);
    }

    public MapCoordinate(double latitude, double longitude) {
        this(latitude, longitude, 0.0d);
    }

    public MapCoordinate(double latitude, double longitude, double bearing) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.bearing = bearing;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }

    public int hashCode() {
        int code = 1;
        long cov = Double.doubleToLongBits(latitude);
        code = 31 * code + (int) (cov ^ cov >>> 32);
        cov = Double.doubleToLongBits(longitude);
        code = 31 * code + (int) (cov ^ cov >>> 32);
        cov = Double.doubleToLongBits(bearing);
        code = 31 * code + (int) (cov ^ cov >>> 32);
        return code;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof MapCoordinate)) {
            return false;
        } else {
            MapCoordinate mc = (MapCoordinate) o;
            return Double.doubleToLongBits(latitude) == Double.doubleToLongBits(mc.latitude) &&
                Double.doubleToLongBits(longitude) == Double.doubleToLongBits(mc.longitude) &&
                Double.doubleToLongBits(bearing) == Double.doubleToLongBits(mc.bearing);
        }
    }

}
