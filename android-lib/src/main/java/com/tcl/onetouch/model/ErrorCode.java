package com.tcl.onetouch.model;

import com.tcl.onetouch.R;
import com.tcl.onetouch.service.BaseService;

public class ErrorCode {

    // Network errors: 1000 - 1999
    public static final int NETWORK_ERROR = 1000;
    public static final int NETWORK_ERROR_NO_NETWORK = 1001;
    public static final int NETWORK_ERROR_TIMEOUT = 1002;
    public static final int NETWORK_ERROR_TIMEOUT_CLIENT = 1003;
    public static final int NETWORK_ERROR_TIMEOUT_SERVER = 1004;
    public static final int NETWORK_ERROR_HTTP_REQUEST = 1101;
    public static final int NETWORK_ERROR_HTTP_SERVER = 1102;
    public static final int NETWORK_ERROR_HTTP_TIMEOUT = 1103;
    public static final int NETWORK_ERROR_HTTP_TIMEOUT_CLIENT = 1104;
    public static final int NETWORK_ERROR_HTTP_TIMEOUT_GATEWAY = 1105;

    // TTS errors: 2000 - 2999
    public static final int TTS_ERROR = 2000;
    public static final int TTS_ERROR_INVALID_REQUEST = 2001;
    public static final int TTS_ERROR_NO_DATA = 2002;
    public static final int TTS_ERROR_OUTPUT = 2003;
    public static final int TTS_ERROR_SERVICE = 2004;
    public static final int TTS_ERROR_SYNTHESIS = 2005;
    public static final int TTS_ERROR_MISSING_LANG = 2006;
    public static final int TTS_ERROR_UNSUPPORTED_LANG = 2007;

    // Speech Recognize errors: 3000 - 3999
    public static final int SPEECH_ERROR = 3000;
    public static final int SPEECH_ERROR_NO_SERVICE = 3001;
    public static final int SPEECH_ERROR_TIMEOUT = 3002;
    public static final int SPEECH_ERROR_CONNECTION = 3003;
    public static final int SPEECH_ERROR_AUDIO = 3101;
    public static final int SPEECH_ERROR_CLIENT = 3102;
    public static final int SPEECH_ERROR_SERVER = 3103;
    public static final int SPEECH_ERROR_NO_RESULT = 3201;

    // Wakeup errors: 4000 - 4999
    public static final int WAKEUP_ERROR = 4000;
    public static final int WAKEUP_ERROR_CANT_START = 4001;
    public static final int WAKEUP_ERROR_AUDIO = 4002;
    public static final int WAKEUP_ERROR_NO_RESULT = 4003;

    // NLU errors: 5000 - 5999
    public static final int NLU_ERROR = 5000;
    public static final int NLU_ERROR_SERVER = 5001;
    public static final int NLU_ERROR_UNSUPPORTED_DOMAIN = 5002;
    public static final int NLU_ERROR_UNSUPPORTED_INTENT = 5003;
    public static final int NLU_ERROR_NO_RESULT = 5004;

    // Geo service errors: 6000 - 6999
    public static final int GEO_ERROR = 6000;
    public static final int GEO_ERROR_NO_RESULT = 6001;
    public static final int GEO_ERROR_OVER_QUERY_LIMIT = 6002;
    public static final int GEO_ERROR_REQUEST_DENIED = 6003;
    public static final int GEO_ERROR_INVALID_REQUEST = 6004;
    public static final int GEO_ERROR_NO_SERVICE = 6005;
    public static final int GEO_ERROR_SERVICE_ERROR = 6006;

    protected int error;
    protected BaseService service;
    protected String message;
    protected String logString;
    protected int displayResId;
    protected int ttsResId;

    public ErrorCode(int error) {
        this(error, null, null);
    }

    public ErrorCode(int error, String message) {
        this(error, message, null);
    }

    public ErrorCode(int error, String message, BaseService service) {
        this.error = error;
        this.service = service;
        this.message = message;
        switch (error) {
            case NETWORK_ERROR:
                logString = "NETWORK_ERROR";
                displayResId = R.string.NETWORK_ERROR_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_TTS;
                break;
            case NETWORK_ERROR_NO_NETWORK:
                logString = "NETWORK_ERROR_NO_NETWORK";
                displayResId = R.string.NETWORK_ERROR_NO_NETWORK_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_NO_NETWORK_TTS;
                break;
            case NETWORK_ERROR_TIMEOUT:
                logString = "NETWORK_ERROR_TIMEOUT";
                displayResId = R.string.NETWORK_ERROR_TIMEOUT_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_TIMEOUT_TTS;
                break;
            case NETWORK_ERROR_TIMEOUT_CLIENT:
                logString = "NETWORK_ERROR_TIMEOUT_CLIENT";
                displayResId = R.string.NETWORK_ERROR_TIMEOUT_CLIENT_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_TIMEOUT_CLIENT_TTS;
                break;
            case NETWORK_ERROR_TIMEOUT_SERVER:
                logString = "NETWORK_ERROR_TIMEOUT_SERVER";
                displayResId = R.string.NETWORK_ERROR_TIMEOUT_SERVER_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_TIMEOUT_SERVER_TTS;
                break;
            case NETWORK_ERROR_HTTP_REQUEST:
                logString = "NETWORK_ERROR_HTTP_REQUEST";
                displayResId = R.string.NETWORK_ERROR_HTTP_REQUEST_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_HTTP_REQUEST_TTS;
                break;
            case NETWORK_ERROR_HTTP_SERVER:
                logString = "NETWORK_ERROR_HTTP_SERVER";
                displayResId = R.string.NETWORK_ERROR_HTTP_SERVER_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_HTTP_SERVER_TTS;
                break;
            case NETWORK_ERROR_HTTP_TIMEOUT:
                logString = "NETWORK_ERROR_HTTP_TIMEOUT";
                displayResId = R.string.NETWORK_ERROR_HTTP_TIMEOUT_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_HTTP_TIMEOUT_TTS;
                break;
            case NETWORK_ERROR_HTTP_TIMEOUT_CLIENT:
                logString = "NETWORK_ERROR_HTTP_TIMEOUT_CLIENT";
                displayResId = R.string.NETWORK_ERROR_HTTP_TIMEOUT_CLIENT_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_HTTP_TIMEOUT_CLIENT_TTS;
                break;
            case NETWORK_ERROR_HTTP_TIMEOUT_GATEWAY:
                logString = "NETWORK_ERROR_HTTP_TIMEOUT_GATEWAY";
                displayResId = R.string.NETWORK_ERROR_HTTP_TIMEOUT_GATEWAY_DISPLAY;
                ttsResId = R.string.NETWORK_ERROR_HTTP_TIMEOUT_GATEWAY_TTS;
                break;
            case SPEECH_ERROR:
                logString = "SPEECH_ERROR";
                displayResId = R.string.SPEECH_ERROR_DISPLAY;
                ttsResId = R.string.SPEECH_ERROR_TTS;
                break;
            case SPEECH_ERROR_NO_SERVICE:
                logString = "SPEECH_ERROR_NO_SERVICE";
                displayResId = R.string.SPEECH_ERROR_NO_SERVICE_DISPLAY;
                ttsResId = R.string.SPEECH_ERROR_NO_SERVICE_TTS;
                break;
            case SPEECH_ERROR_TIMEOUT:
                logString = "SPEECH_ERROR_TIMEOUT";
                displayResId = R.string.SPEECH_ERROR_TIMEOUT_DISPLAY;
                ttsResId = R.string.SPEECH_ERROR_TIMEOUT_TTS;
                break;
            case SPEECH_ERROR_CONNECTION:
                logString = "SPEECH_ERROR_CONNECTION";
                displayResId = R.string.SPEECH_ERROR_CONNECTION_DISPLAY;
                ttsResId = R.string.SPEECH_ERROR_CONNECTION_TTS;
                break;
            case SPEECH_ERROR_AUDIO:
                logString = "SPEECH_ERROR_AUDIO";
                displayResId = R.string.SPEECH_ERROR_AUDIO_DISPLAY;
                ttsResId = R.string.SPEECH_ERROR_AUDIO_TTS;
                break;
            case SPEECH_ERROR_CLIENT:
                logString = "SPEECH_ERROR_CLIENT";
                displayResId = R.string.SPEECH_ERROR_CLIENT_DISPLAY;
                ttsResId = R.string.SPEECH_ERROR_CLIENT_TTS;
                break;
            case SPEECH_ERROR_SERVER:
                logString = "SPEECH_ERROR_SERVER";
                displayResId = R.string.SPEECH_ERROR_SERVER_DISPLAY;
                ttsResId = R.string.SPEECH_ERROR_SERVER_TTS;
                break;
            case SPEECH_ERROR_NO_RESULT:
                logString = "SPEECH_ERROR_NO_RESULT";
                displayResId = R.string.SPEECH_ERROR_NO_RESULT_DISPLAY;
                ttsResId = R.string.SPEECH_ERROR_NO_RESULT_TTS;
                break;
            case WAKEUP_ERROR:
                logString = "WAKEUP_ERROR";
                displayResId = R.string.WAKEUP_ERROR_DISPLAY;
                ttsResId = R.string.WAKEUP_ERROR_TTS;
                break;
            case WAKEUP_ERROR_CANT_START:
                logString = "WAKEUP_ERROR_CANT_START";
                displayResId = R.string.WAKEUP_ERROR_CANT_START_DISPLAY;
                ttsResId = R.string.WAKEUP_ERROR_CANT_START_TTS;
                break;
            case WAKEUP_ERROR_AUDIO:
                logString = "WAKEUP_ERROR_AUDIO";
                displayResId = R.string.WAKEUP_ERROR_AUDIO_DISPLAY;
                ttsResId = R.string.WAKEUP_ERROR_AUDIO_TTS;
                break;
            case WAKEUP_ERROR_NO_RESULT:
                logString = "WAKEUP_ERROR_NO_RESULT";
                displayResId = R.string.WAKEUP_ERROR_NO_RESULT_DISPLAY;
                ttsResId = R.string.WAKEUP_ERROR_NO_RESULT_TTS;
                break;
            case NLU_ERROR:
                logString = "NLU_ERROR";
                displayResId = R.string.NLU_ERROR_DISPLAY;
                ttsResId = R.string.NLU_ERROR_TTS;
                break;
            case NLU_ERROR_SERVER:
                logString = "NLU_ERROR_SERVER";
                displayResId = R.string.NLU_ERROR_SERVER_DISPLAY;
                ttsResId = R.string.NLU_ERROR_SERVER_TTS;
                break;
            case NLU_ERROR_UNSUPPORTED_DOMAIN:
                logString = "NLU_ERROR_UNSUPPORTED_DOMAIN";
                displayResId = R.string.NLU_ERROR_UNSUPPORTED_DOMAIN_DISPLAY;
                ttsResId = R.string.NLU_ERROR_UNSUPPORTED_DOMAIN_TTS;
                break;
            case NLU_ERROR_UNSUPPORTED_INTENT:
                logString = "NLU_ERROR_UNSUPPORTED_INTENT";
                displayResId = R.string.NLU_ERROR_UNSUPPORTED_INTENT_DISPLAY;
                ttsResId = R.string.NLU_ERROR_UNSUPPORTED_INTENT_TTS;
                break;
            case NLU_ERROR_NO_RESULT:
                logString = "NLU_ERROR_NO_RESULT";
                displayResId = R.string.NLU_ERROR_NO_RESULT_DISPLAY;
                ttsResId = R.string.NLU_ERROR_NO_RESULT_TTS;
                break;
            case TTS_ERROR:
                logString = "TTS_ERROR";
                displayResId = R.string.TTS_ERROR_DISPLAY;
                ttsResId = R.string.TTS_ERROR_TTS;
                break;
            case TTS_ERROR_INVALID_REQUEST:
                logString = "TTS_ERROR_INVALID_REQUEST";
                displayResId = R.string.TTS_ERROR_INVALID_REQUEST_DISPLAY;
                ttsResId = R.string.TTS_ERROR_INVALID_REQUEST_TTS;
                break;
            case TTS_ERROR_NO_DATA:
                logString = "TTS_ERROR_NO_DATA";
                displayResId = R.string.TTS_ERROR_NO_DATA_DISPLAY;
                ttsResId = R.string.TTS_ERROR_NO_DATA_TTS;
                break;
            case TTS_ERROR_OUTPUT:
                logString = "TTS_ERROR_OUTPUT";
                displayResId = R.string.TTS_ERROR_OUTPUT_DISPLAY;
                ttsResId = R.string.TTS_ERROR_OUTPUT_TTS;
                break;
            case TTS_ERROR_SERVICE:
                logString = "TTS_ERROR_SERVICE";
                displayResId = R.string.TTS_ERROR_SERVICE_DISPLAY;
                ttsResId = R.string.TTS_ERROR_SERVICE_TTS;
                break;
            case TTS_ERROR_SYNTHESIS:
                logString = "TTS_ERROR_SYNTHESIS";
                displayResId = R.string.TTS_ERROR_SYNTHESIS_DISPLAY;
                ttsResId = R.string.TTS_ERROR_SYNTHESIS_TTS;
                break;
            case TTS_ERROR_MISSING_LANG:
                logString = "TTS_ERROR_MISSING_LANG";
                displayResId = R.string.TTS_ERROR_MISSING_LANG_DISPLAY;
                ttsResId = R.string.TTS_ERROR_MISSING_LANG_TTS;
                break;
            case TTS_ERROR_UNSUPPORTED_LANG:
                logString = "TTS_ERROR_UNSUPPORTED_LANG";
                displayResId = R.string.TTS_ERROR_UNSUPPORTED_LANG_DISPLAY;
                ttsResId = R.string.TTS_ERROR_UNSUPPORTED_LANG_TTS;
                break;
            case GEO_ERROR:
                logString = "GEO_ERROR";
                displayResId = R.string.GEO_ERROR_DISPLAY;
                ttsResId = R.string.GEO_ERROR_TTS;
                break;
            case GEO_ERROR_NO_RESULT:
                logString = "GEO_ERROR_NO_RESULT";
                displayResId = R.string.GEO_ERROR_NO_RESULT_DISPLAY;
                ttsResId = R.string.GEO_ERROR_NO_RESULT_TTS;
                break;
            case GEO_ERROR_OVER_QUERY_LIMIT:
                logString = "GEO_ERROR_OVER_QUERY_LIMIT";
                displayResId = R.string.GEO_ERROR_OVER_QUERY_LIMIT_DISPLAY;
                ttsResId = R.string.GEO_ERROR_OVER_QUERY_LIMIT_TTS;
                break;
            case GEO_ERROR_REQUEST_DENIED:
                logString = "GEO_ERROR_REQUEST_DENIED";
                displayResId = R.string.GEO_ERROR_REQUEST_DENIED_DISPLAY;
                ttsResId = R.string.GEO_ERROR_REQUEST_DENIED_TTS;
                break;
            case GEO_ERROR_INVALID_REQUEST:
                logString = "GEO_ERROR_INVALID_REQUEST";
                displayResId = R.string.GEO_ERROR_INVALID_REQUEST_DISPLAY;
                ttsResId = R.string.GEO_ERROR_INVALID_REQUEST_TTS;
                break;
            case GEO_ERROR_NO_SERVICE:
                logString = "GEO_ERROR_NO_SERVICE";
                displayResId = R.string.GEO_ERROR_NO_SERVICE_DISPLAY;
                ttsResId = R.string.GEO_ERROR_NO_SERVICE_TTS;
                break;
            case GEO_ERROR_SERVICE_ERROR:
                logString = "GEO_ERROR_SERVICE_ERROR";
                displayResId = R.string.GEO_ERROR_SERVICE_ERROR_DISPLAY;
                ttsResId = R.string.GEO_ERROR_SERVICE_ERROR_TTS;
                break;
            default:
                logString = "UNKNOWN_ERROR";
                displayResId = R.string.UNKNOWN_ERROR_DISPLAY;
                ttsResId = R.string.UNKNOWN_ERROR_TTS;
                break;
        }
    }

    public int getCode() {
        return error;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setService(BaseService service) {
        this.service = service;
    }

    public BaseService getService() {
        return service;
    }

    public String getLogString() {
        return logString;
    }

    public int getDisplayResId() {
        return displayResId;
    }

    public int getTtsResId() {
        return ttsResId;
    }

    public String toString() {
        return "ErrorCode [code=" + logString +
            ",msg=" + message +
            ",svc=" + (service != null ? service.getClass().getSimpleName() : "") + "]";
    }
}
