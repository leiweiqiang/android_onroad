package com.tcl.onetouch.model;

import android.location.Location;
import android.util.Pair;

import com.tcl.onetouch.util.DataUtil;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class AddressInfo {
    public static class Components {
        private String pointOfInterest;
        private String room;
        private String building;
        private String streetNumber;
        private String route;
        private String neighborhood;
        private String city;
        private String county;
        private String state;
        private String stateCode;
        private String country;
        private String countryCode;
        private String postalCode;

        public Components() {
            pointOfInterest = null;
            room = null;
            building = null;
            streetNumber = null;
            route = null;
            neighborhood = null;
            city = null;
            county = null;
            state = null;
            stateCode = null;
            country = null;
            countryCode = null;
            postalCode = null;
        }

        public String getPointOfInterest() {
            return pointOfInterest;
        }

        public void setPointOfInterest(String pointOfInterest) {
            this.pointOfInterest = pointOfInterest;
        }

        public String getRoom() {
            return room;
        }

        public void setRoom(String room) {
            this.room = room;
        }

        public String getBuilding() {
            return building;
        }

        public void setBuilding(String building) {
            this.building = building;
        }

        public String getStreetNumber() {
            return streetNumber;
        }

        public void setStreetNumber(String streetNumber) {
            this.streetNumber = streetNumber;
        }

        public String getRoute() {
            return route;
        }

        public void setRoute(String route) {
            this.route = route;
        }

        public String getNeighborhood() {
            return neighborhood;
        }

        public void setNeighborhood(String neighborhood) {
            this.neighborhood = neighborhood;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCounty() {
            return county;
        }

        public void setCounty(String county) {
            this.county = county;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getStateCode() {
            return stateCode;
        }

        public void setStateCode(String stateCode) {
            this.stateCode = stateCode;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        @Override
        public String toString() {
            return "Components{" +
                "pointOfInterest='" + pointOfInterest + '\'' +
                ", room='" + room + '\'' +
                ", building='" + building + '\'' +
                ", streetNumber='" + streetNumber + '\'' +
                ", route='" + route + '\'' +
                ", neighborhood='" + neighborhood + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", state='" + state + '\'' +
                ", stateCode='" + stateCode + '\'' +
                ", country='" + country + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
        }

        public static Components createFromGoogleGeocode(Collection<Object> addressComponents) {
            Components components = new Components();
            Map<String, Pair<String, String>> entries = new HashMap<>();
            Iterator<Object> cit = addressComponents.iterator();
            while (cit.hasNext()) {
                Map<String, Object> component = (Map<String, Object>) cit.next();
                Collection<Object> types = DataUtil.getCollection(component, "types");
                if (types != null && types.size() > 0) {
                    String type = types.iterator().next().toString();
                    entries.put(type, new Pair<String, String>(
                        DataUtil.getString(component, "long_name"),
                        DataUtil.getString(component, "short_name")));
                }
            }
            Pair<String, String> entry = entries.get("point_of_interest");
            if (entry != null) {
                components.setPointOfInterest(entry.first);
            }
            entry = entries.get("subpremise");
            if (entry != null) {
                components.setRoom(entry.first);
            }
            entry = entries.get("premise");
            if (entry != null) {
                components.setBuilding(entry.first);
            }
            entry = entries.get("street_number");
            if (entry != null) {
                components.setStreetNumber(entry.first);
            }
            entry = entries.get("route");
            if (entry != null) {
                components.setRoute(entry.first);
            }
            entry = entries.get("neighborhood");
            if (entry != null) {
                components.setNeighborhood(entry.first);
            }
            entry = entries.get("locality");
            if (entry != null) {
                components.setCity(entry.first);
            }
            entry = entries.get("administrative_area_level_2");
            if (entry != null) {
                components.setCounty(entry.first);
            }
            entry = entries.get("administrative_area_level_1");
            if (entry != null) {
                components.setState(entry.first);
                components.setStateCode(entry.second);
            }
            entry = entries.get("postal_code");
            if (entry != null) {
                components.setPostalCode(entry.first);
            }
            return components;
        }
    }

    private String formattedAddress;
    private Components components;

    public AddressInfo() {
        formattedAddress = "";
        components = null;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        if (formattedAddress == null) {
            return;
        }
        this.formattedAddress = formattedAddress;
    }

    public String getShortAddress() {
        StringBuilder sb = new StringBuilder();
        if (components != null) {
            if (components.getStreetNumber() != null) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(components.getStreetNumber());
                if (components.getRoute() != null) {
                    if (sb.length() > 0) {
                        sb.append(" ");
                    }
                    sb.append(components.getRoute());
                }
            } else if (components.getRoute() != null) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(components.getRoute());
            }
            if (components.getCity() != null) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(components.getCity());
            } else if (components.getState() != null) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(components.getState());
            } else if (components.getCountry() != null) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(components.getCountry());
            }
        } else if (!formattedAddress.isEmpty()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(formattedAddress);
        }
        return sb.toString();
    }

    public Components getComponents() {
        return components;
    }

    public void setComponents(Components components) {
        this.components = components;
    }

    @Override
    public String toString() {
        return "AddressInfo{" +
            ", formattedAddress='" + formattedAddress + '\'' +
            ", components=" + components +
            '}';
    }

    public static AddressInfo createFromGoogleGeocode(Map<String, Object> geocode) {
        AddressInfo addressInfo = new AddressInfo();
        Collection<Object> components = DataUtil.getCollection(geocode, "address_components");
        if (components != null) {
            addressInfo.setComponents(Components.createFromGoogleGeocode(components));
        }
        addressInfo.setFormattedAddress(DataUtil.getString(geocode, "formatted_address"));
        return addressInfo;
    }
}
