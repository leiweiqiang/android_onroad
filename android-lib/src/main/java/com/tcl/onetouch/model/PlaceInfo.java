package com.tcl.onetouch.model;

import android.location.Location;

import com.tcl.onetouch.util.DataUtil;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class PlaceInfo {
    protected String id;
    protected String name;
    protected String bookmark;
    protected Location location;
    protected AddressInfo addressInfo;
    protected float rating;
    protected int priceLevel;
    protected boolean isOpenNow;
    protected Set<String> typeSet;
    protected boolean hasDetailedInfo;

    public PlaceInfo() {
        id = "";
        name = "";
        bookmark = null;
        location = new Location("");
        addressInfo = null;
        rating = -1;
        priceLevel = -1;
        isOpenNow = false;
        typeSet = null;
        hasDetailedInfo = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id == null) {
            return;
        }
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            return;
        }
        this.name = name;
    }

    public String getDisplayName() {
        if (bookmark != null && !bookmark.isEmpty()) {
            return bookmark;
        }
        return name;
    }


    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        if (location == null) {
            return;
        }
        this.location = location;
    }

    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(AddressInfo addressInfo) {
        this.addressInfo = addressInfo;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getPriceLevel() {
        return priceLevel;
    }

    public void setPriceLevel(int priceLevel) {
        this.priceLevel = priceLevel;
    }

    public boolean isOpenNow() {
        return isOpenNow;
    }

    public void setIsOpenNow(boolean isOpenNow) {
        this.isOpenNow = isOpenNow;
    }

    // check if this is a type of a place, for list of types check
    // https://developers.google.com/places/supported_types
    public boolean isType(String type) {
        return typeSet != null && typeSet.contains(type);
    }

    public Set<String> getTypes() {
        return typeSet;
    }

    public void addType(String type) {
        if (typeSet == null) {
            typeSet = new HashSet<>();
        }
        typeSet.add(type);
    }

    public void addTypes(Collection<String> types) {
        if (types == null || types.isEmpty()) {
            return;
        }
        if (typeSet == null) {
            typeSet = new HashSet<>();
        }
        typeSet.addAll(types);
    }

    public boolean hasDetailedInfo() {
        return hasDetailedInfo;
    }

    public void setHasDetailedInfo(boolean hasDetailedInfo) {
        this.hasDetailedInfo = hasDetailedInfo;
    }

    public String getDisplayString() {
        String address = addressInfo != null ? addressInfo.getShortAddress() : "";
        if (bookmark != null && !bookmark.isEmpty()) {
            return bookmark + ", " + address;
        } else if (!name.isEmpty()) {
            return name + ", " + address;
        } else {
            return address;
        }
    }

    public void updateFromGoogleGeocode(Map<String, Object> geocode) {
        setId(DataUtil.getString(geocode, "place_id"));
        Collection<Object> types = DataUtil.getCollection(geocode, "types");
        if (types != null) {
            Iterator<Object> tit = types.iterator();
            while (tit.hasNext()) {
                addType(tit.next().toString());
            }
        }
        Map<String, Object> geometry = DataUtil.getMap(geocode, "geometry");
        if (geometry != null) {
            Map<String, Object> location = DataUtil.getMap(geometry, "location");
            if (location != null) {
                Location loc = new Location("");
                loc.setLatitude(DataUtil.getDouble(location, "lat"));
                loc.setLongitude(DataUtil.getDouble(location, "lng"));
                setLocation(loc);
            }
        }
        setAddressInfo(AddressInfo.createFromGoogleGeocode(geocode));
        setRating(DataUtil.getFloat(geocode, "rating"));
        setPriceLevel(DataUtil.getInt(geocode, "price_level"));
        Map<String, Object> openingHours = DataUtil.getMap(geocode, "opening_hours");
        if (openingHours != null) {
            setIsOpenNow(DataUtil.getBoolean(openingHours, "open_now"));
        }
        String name = DataUtil.getString(geocode, "name");
        if (!isType("street_address") && !isType("political") &&
            !addressInfo.getShortAddress().startsWith(name.toLowerCase())) {
            setName(name);
        }
    }

    public static PlaceInfo createFromGoogleGeocode(Map<String, Object> geocode) {
        PlaceInfo placeInfo = new PlaceInfo();
        placeInfo.updateFromGoogleGeocode(geocode);
        return placeInfo;
    }
}
